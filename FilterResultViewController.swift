//
//  FilterResultViewController.swift
//  LMRA
//
//  Created by Mac on 30/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire

class FilterResultViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UIGestureRecognizerDelegate{
    @IBOutlet weak var MainView: UIView!
    
    @IBOutlet weak var ScrollView: UIScrollView!
    @IBOutlet weak var BtnConfirm: UIButton!
    @IBOutlet weak var LblsliderMax: UILabel!
    @IBOutlet weak var LblsliderMin: UILabel!
    @IBOutlet weak var LblResultValue: UILabel!
    @IBOutlet weak var SliderBar: UISlider!
    @IBOutlet weak var FilterLangCollectionView: UICollectionView!
    @IBOutlet weak var FilterNationalityCollectionView: UICollectionView!
    @IBOutlet weak var BtnHourlyRate: UIButton!
    @IBOutlet weak var BtnFixedRate: UIButton!
    
    
    var LangIDString = ""
    var LangPriority = ""
    var Language_Name = ""
    var BtnFixeRateVal = ""
    var BtnHourlyRateVal = ""
    var NationalityData = ""
    var GetSelectedbtnValue = ""
    var SliderMinVal = ""
    var SliderMinValInt:Int!
    var SliderMaxVal = ""
    var SliderMaxVallInt:Int!
    var CurrentValStrig = ""
    var currentValue:Int!
    var authentication_key_val = ""
    
    
    var BtnSelctedTag:Int!
    
    @IBOutlet var BackgroundView: UIView!
    var selectedIndexPath: Int!
    var Language_Id:Int!
    var LanguageSelectedID:Int!
    var arrLangNameSelectedIndex = [IndexPath]() // This is selected cell Index array
    var arrLangIDSelectedIndex = [String]() // This is selected cell Index array
    var arrLangNameSelectedData = [String]() // This is selected cell data array
    var Language_NameString_Array = [String] ()
    var LangIDDict = [NSDictionary]()
    var LangIDString_Array = [String] ()
    var GetLangName_Array = [String] ()
    var Nationality_Array = [String] ()
    var NewGetLangName_Array = [String] ()
    var GetLangID_Array = [String] ()
    var LangPriority_Array = [String] ()
    var LangPriorityOne_Array = [String] ()
    var LangPriorityZero_Array = [String] ()
    var FilterDataDict = [String:Any]()
    var IsFromFilterResult:Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.layer.cornerRadius = 5;
        view.layer.masksToBounds = true;
        
        FilterLangCollectionView.delegate = self
        FilterLangCollectionView.dataSource = self
        
        FilterNationalityCollectionView.delegate = self
        FilterNationalityCollectionView.dataSource = self
        
        //MainView.roundCorners(corners: [.topLeft, .topRight], radius: 30)
        
        self.BtnFixedRate.setTitleColor(COLOR.WhiteColor, for: .normal)
        self.BtnFixedRate.backgroundColor = COLOR.PurpleColor
        
        self.BtnHourlyRate.setTitleColor(COLOR.GrayColor, for: .normal)
        self.BtnHourlyRate.backgroundColor = COLOR.LightGrayColor
        
        BtnHourlyRate.layer.borderWidth = 0.4
        BtnHourlyRate.layer.borderColor = (UIColor( red: 0.142, green: 0.154, blue:0.160, alpha: 1.0 )).cgColor
        
        self.GetSelectedbtnValue = self.BtnFixedRate.titleLabel?.text ?? "Fixed Rate"
        self.SliderMinVal = "0"
        self.SliderMaxVal = "1000"
        self.SliderMinValInt = 0
        self.SliderMaxVallInt = 1000
        self.LblsliderMin.text = "B" + "\(self.SliderMinVal)"
        self.LblsliderMax.text = "B" + "\(self.SliderMaxVal)"
        
        
         self.SliderBar.thumbTintColor = COLOR.PurpleColor
        
        self.Nationality_Array = ["India","Japan","Egypt","India","Pakistan","Egypt"]
   
        self.authentication_key_val = UserDefaults.standard.string(forKey: "authentication_key") ?? ""//get
        print("authentication_key_valFilter :\(String(describing: self.authentication_key_val))") //get
        
        self.get_languages()
        self.registerNibsLang()
        self.registerNibsNationality()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    
        let panGesture:UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handleDismiss(_:)))
        panGesture.delegate = self
        self.MainView.addGestureRecognizer(panGesture)
    }
    
    func registerNibsLang(){
        
        FilterLangCollectionView.register(UINib(nibName: "LanguagesCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "LanguagesCell")
        
    }
    func registerNibsNationality(){
        
        FilterNationalityCollectionView.register(UINib(nibName: "LanguagesCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "LanguagesCell")
        
    }

    var viewTranslation = CGPoint(x: 0, y: 0)
          @objc func handleDismiss(_ sender: UIPanGestureRecognizer) {
              switch sender.state {
              case .changed:
                  let velocity = sender.velocity(in: MainView)
                  if(velocity.y > 0) {
                      viewTranslation = sender.translation(in: MainView)
                      UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                          self.MainView.transform = CGAffineTransform(translationX: 0, y: self.viewTranslation.y)
                      })
                  }

              case .ended:
                  let velocity = sender.velocity(in: MainView)
                  if(velocity.y > 0) {
                      if viewTranslation.y < 200 {
                          UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                              self.MainView.transform = .identity
                          })
                      } else {
                          self.MainView.transform = .identity
                          viewTranslation = CGPoint(x: 0, y: 0)
                         self.dismiss(animated: true, completion: nil)

                      }
                  }
              default:
                  break
              }
          }
    @IBAction func SliderClick(_ sender: UISlider) {
        
        
        self.currentValue = Int(sender.value)
        self.CurrentValStrig = String(currentValue)
        self.LblResultValue.text = "B" + "\(self.CurrentValStrig)"
        print("LblResultValue:",self.LblResultValue.text ?? "0B")
        //        sender.thumbTintColor = (UIColor( red: 243/255, green: 243/255, blue:241/255, alpha: 1.0 ))
        
        sender.thumbTintColor = COLOR.PurpleColor
        
    }
    
    @IBAction func BtnFixedRateClick(_ sender: UIButton) {
        BtnFixedRate.isSelected = true
        BtnHourlyRate.isSelected = false
        self.BtnFixedRate.setTitleColor(COLOR.WhiteColor, for: .normal)
        self.BtnFixedRate.backgroundColor = COLOR.PurpleColor
        
        self.BtnHourlyRate.setTitleColor(COLOR.GrayColor, for: .normal)
        self.BtnHourlyRate.backgroundColor = COLOR.LightGrayColor
        
        BtnFixedRate.layer.borderWidth = 0.4
        BtnFixedRate.layer.borderColor = (UIColor( red: 0.142, green: 0.154, blue:0.160, alpha: 1.0 )).cgColor
        
        self.BtnFixeRateVal = BtnFixedRate.titleLabel?.text ?? ""
        self.GetSelectedbtnValue = self.BtnFixeRateVal
        print("BtnFixeRateVal:",self.BtnFixeRateVal)
        
    }
    
    @IBAction func BtnHourlyRateClick(_ sender: UIButton) {
        BtnHourlyRate.isSelected = true
        BtnFixedRate.isSelected = false
        
        self.BtnHourlyRate.setTitleColor(COLOR.WhiteColor, for: .normal)
        self.BtnHourlyRate.backgroundColor = COLOR.PurpleColor
        
        self.BtnFixedRate.setTitleColor(COLOR.GrayColor, for: .normal)
        self.BtnFixedRate.backgroundColor = COLOR.LightGrayColor
        
        BtnHourlyRate.layer.borderWidth = 0.4
        BtnHourlyRate.layer.borderColor = (UIColor( red: 0.142, green: 0.154, blue:0.160, alpha: 1.0 )).cgColor
        
        self.BtnHourlyRateVal = BtnHourlyRate.titleLabel?.text ?? ""
        self.GetSelectedbtnValue =  self.BtnHourlyRateVal
        print("BtnHourlyRateVal:",self.BtnHourlyRateVal)
        
    }
    
    @IBAction func BtnFilterViewCancelClick(_ sender: Any) {
        
         self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func BtnFilterViewConfirmClick(_ sender: Any) {
        
        self.IsFromFilterResult = true
        print("GetSelectedbtnValue:",self.GetSelectedbtnValue)
        print("SliderMinVal:",self.SliderMinValInt)
        print("LblResultValue:",self.LblResultValue.text ?? "0B")
        print("arrSelectedIndex:",arrLangNameSelectedIndex)
        print("arrSelectedData:",arrLangNameSelectedData)
        print("arrLangIDSelectedIndex:",arrLangIDSelectedIndex)
        print("NationalityData:",self.NationalityData)
        
      
         self.FilterDataDict["SelectedRateValue"] = self.GetSelectedbtnValue
         self.FilterDataDict["SelectedSliderMinValue"] = self.SliderMinValInt
         self.FilterDataDict["SliderResultValue"] = self.currentValue
         self.FilterDataDict["SelectedNationalityValue"] = self.NationalityData
         self.FilterDataDict["SelectedLangIndexArrayValue"] = self.arrLangNameSelectedIndex
         self.FilterDataDict["SelectedLangNameArrayValue"] = self.arrLangNameSelectedData
         self.FilterDataDict["SelectedLangIDArrayValue"] = self.arrLangIDSelectedIndex
         self.FilterDataDict["IsfromFliterResultValue"] = self.IsFromFilterResult
        
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SelectedFilterData"), object: nil, userInfo: FilterDataDict)
        
        print("FilterDataDict:",self.FilterDataDict)

               
           self.dismiss(animated: true, completion: nil)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.FilterLangCollectionView {
            return self.LangPriorityOne_Array.count
        }
        else {
            
            return self.Nationality_Array.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //        let cell =  LanguageCollectionView.dequeueReusableCell(withReuseIdentifier: "LanguagesCell", for: indexPath as IndexPath) as! LanguagesCollectionViewCell
        
        
        if collectionView == self.FilterLangCollectionView {
            let LangCell1 =  FilterLangCollectionView.dequeueReusableCell(withReuseIdentifier: "LanguagesCell", for: indexPath as IndexPath) as! LanguagesCollectionViewCell
            
            LangCell1.InnerView.layer.cornerRadius = 2;
            if arrLangNameSelectedIndex.contains(indexPath) { // You need to check wether selected index array
                LangCell1.LblSelectedValue.textColor = COLOR.WhiteColor
                LangCell1.InnerView.backgroundColor = COLOR.PurpleColor
            }
            else {
                LangCell1.LblSelectedValue.textColor = COLOR.GrayColor
                LangCell1.InnerView.backgroundColor = COLOR.LightGrayColor
            }
            
            let LangData = self.Language_NameString_Array[indexPath.item]
            LangCell1.LblSelectedValue.text = LangData
            
            return LangCell1
            
        }
        else {
            //            let NationalityCell = FilterNationalityCollectionView.dequeueReusableCell(withReuseIdentifier: "FilterNationalityCell", for: indexPath as IndexPath)as! FilterNationalityCollectionViewCell
            
            let NationalityCell =  FilterNationalityCollectionView.dequeueReusableCell(withReuseIdentifier: "LanguagesCell", for: indexPath as IndexPath) as! LanguagesCollectionViewCell
            
            
            NationalityCell.LblSelectedValue.textColor = COLOR.GrayColor
            NationalityCell.InnerView.backgroundColor = COLOR.LightGrayColor
            
            NationalityCell.InnerView.layer.cornerRadius = 2;
            self.NationalityData = self.Nationality_Array[indexPath.item]
            NationalityCell.LblSelectedValue.text = NationalityData
            
            
            if let indexPaths = FilterNationalityCollectionView.indexPathsForSelectedItems,
                let indexP = indexPaths.first {
                NationalityCell.InnerView.backgroundColor = indexP == indexPath ? COLOR.PurpleColor : COLOR.LightGrayColor
                NationalityCell.LblSelectedValue.textColor = indexP == indexPath ? COLOR.WhiteColor: COLOR.GrayColor
                
            }
            
            return NationalityCell
        }
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("You selected cell #\(indexPath.item)")
        let IndexPathInt = Int(indexPath.item)
        self.selectedIndexPath = IndexPathInt
        if collectionView == self.FilterLangCollectionView {
            
            //            let LangCell = FilterLangCollectionView.dequeueReusableCell(withReuseIdentifier: "LangCell", for: indexPath as IndexPath)as! LanguageCollectionViewCell
            let LangCell1 =  FilterLangCollectionView.dequeueReusableCell(withReuseIdentifier: "LanguagesCell", for: indexPath as IndexPath) as! LanguagesCollectionViewCell
            
            
            let LangNameData = self.Language_NameString_Array[indexPath.item]
            let LangIDData = self.LangIDString_Array[indexPath.item]
            
            if arrLangNameSelectedIndex.contains(indexPath) {
                arrLangNameSelectedIndex = arrLangNameSelectedIndex.filter { $0 != indexPath}
                arrLangNameSelectedData = arrLangNameSelectedData.filter { $0 != LangNameData}
                arrLangIDSelectedIndex = arrLangIDSelectedIndex.filter { $0 != LangIDData}
            }
            else {
                arrLangNameSelectedIndex.append(indexPath)
                arrLangNameSelectedData.append(LangNameData)
                arrLangIDSelectedIndex.append(LangIDData)
            }
            print("arrSelectedIndex:",arrLangNameSelectedIndex)
            print("arrSelectedData:",arrLangNameSelectedData)
            print("arrLangIDSelectedIndex:",arrLangIDSelectedIndex)
            FilterLangCollectionView.reloadData()
        }
        
        if collectionView == self.FilterNationalityCollectionView {
            // for nationality
            //            let NationalityCell = FilterNationalityCollectionView.dequeueReusableCell(withReuseIdentifier: "FilterNationalityCell", for: indexPath as IndexPath)as! FilterNationalityCollectionViewCell
            
            let NationalityCell =  FilterNationalityCollectionView.dequeueReusableCell(withReuseIdentifier: "LanguagesCell", for: indexPath as IndexPath) as! LanguagesCollectionViewCell
            
            //            if let NationalityCell = FilterNationalityCollectionView.cellForItem(at: indexPath) as? FilterNationalityCollectionViewCell
            if let NationalityCell = FilterNationalityCollectionView.cellForItem(at: indexPath) as? LanguagesCollectionViewCell
            {
                NationalityCell.InnerView.backgroundColor = COLOR.PurpleColor
                NationalityCell.LblSelectedValue.textColor = COLOR.WhiteColor
                
            }
            
            self.NationalityData = self.Nationality_Array[indexPath.item]
            print("NationalityData: \(self.NationalityData)")
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        //        if let NationalityCell = FilterNationalityCollectionView.cellForItem(at: indexPath) as? FilterNationalityCollectionViewCell
        if let NationalityCell = FilterNationalityCollectionView.cellForItem(at: indexPath) as? LanguagesCollectionViewCell
        {
            NationalityCell.InnerView.backgroundColor = COLOR.LightGrayColor
            NationalityCell.LblSelectedValue.textColor = UIColor.darkGray
        }
    }
    
    
    
    func get_languages()
    {
        let urlString = ConstantsClass.baseUrl + ConstantsClass.get_languages
        print("urlString: \(urlString)")
        
        let headers = [
            "Authorization": "\(self.authentication_key_val)"
        ]
        print("headers: \(headers)")
        Alamofire.request(urlString, method: .get, parameters: nil, encoding: URLEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if (response["status"] as? Bool) != nil {
                        if let respArray = response["response"] as? [[String:Any]] {
                            print("respArray: \(respArray)")
                            
                            self.LangIDDict = response["response"] as! [NSDictionary]
                            print("LangIDDict:",self.LangIDDict)
                            for Data in self.LangIDDict
                            {
                                self.Language_Name = Data["language_name"]as! String
                                self.Language_Id = Data["language_id"]as? Int
                                self.LangIDString = String( self.Language_Id)
                                self.LangPriority = Data["language_priority"] as! String
                                
                                if self.LangPriority == "1"
                                {
                                    self.LangPriorityOne_Array.append(self.LangPriority)
                                    self.Language_NameString_Array.append(self.Language_Name)
                                    self.LangIDString_Array.append(self.LangIDString)
                                }
                                if self.LangPriority == "0"
                                {
                                    self.LangPriorityZero_Array.append(self.LangPriority)
                                }
                            }
                            self.GetLangName_Array = self.Language_NameString_Array
                            
                        }
                    }
                    // Make sure to update UI in main thread
                    DispatchQueue.main.async {
                        self.FilterLangCollectionView.reloadData()
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
        
        
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
