//
//  ReviewNdConfirmViewController.swift
//  LMRA
//
//  Created by Mac on 08/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire

class ReviewNdConfirmViewController: UIViewController ,UIGestureRecognizerDelegate{
    
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var ImgUser: UIImageView!
    @IBOutlet weak var AddressView: UIView!
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var BtnConfirm: UIButton!
    @IBOutlet weak var LblFamilyname: UILabel!
    @IBOutlet weak var ViewDetails: UIView!
    @IBOutlet weak var ViewAddress: UIView!
    @IBOutlet weak var LblSetAddressType: UILabel!
    @IBOutlet weak var LblFullAddressType: UILabel!
    @IBOutlet weak var LblName: UILabel!
    @IBOutlet weak var LblNationality: UILabel!
    @IBOutlet weak var LblCelNo: UILabel!
    @IBOutlet weak var LblEmailID: UILabel!
    @IBOutlet weak var LbEmail: UILabel!
    var IsFromReviewNdConfirm:Bool = false
    var AuthKeyFromSetAddress = ""
    var FullAddress = ""
    var address_nickname = ""
    var  WhiteColor = UIColor(red:243, green:243, blue:241, alpha:1.0)
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        BtnConfirm.layer.cornerRadius = 2;
        self.BtnConfirm.setTitleColor(WhiteColor, for: .normal)
        BtnConfirm.backgroundColor = UIColor(red: 255/255, green: 55/255, blue: 94/255, alpha: 1.0)
        
        AddressView.layer.shadowColor = UIColor.gray.cgColor
        AddressView.layer.shadowOpacity = 0.5
        AddressView.layer.shadowOffset = CGSize.zero
        AddressView.layer.shadowRadius = 6
        AddressView.layer.cornerRadius = 5
        AddressView.clipsToBounds = false
        
        ImgUser.layer.cornerRadius = ImgUser.frame.height/2
        ImgUser.clipsToBounds = true
        //Implement swipe gesture
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        scrollview.addGestureRecognizer(tap)
        
        
    }
    
    @IBAction func ConfirmbtnClick(_ sender: Any) {
        
        let UserLoginWithNumber: Bool = true
        UserDefaults.standard.set(UserLoginWithNumber,forKey: "RegisterNumberkey")
        /////////////////////////////////////////////////
          let rootViewController = Storyboard().dashFlowStoryboard.instantiateViewController(withIdentifier: "CustomerDashBoardViewController")
          let navigationController = UINavigationController(rootViewController: rootViewController)
          let rightViewController = Storyboard().dashFlowStoryboard.instantiateViewController(withIdentifier: "CutomerMenuVC")
          let sideMenuController = LGSideMenuController(rootViewController: navigationController,
                                                        leftViewController: nil,
                                                        rightViewController: rightViewController)
          sideMenuController.leftViewWidth = 0
          sideMenuController.leftViewPresentationStyle = .slideBelow;
          
          sideMenuController.rightViewWidth = (UIApplication.shared.keyWindow?.bounds.size.width ?? 320) - 20
          sideMenuController.leftViewPresentationStyle = .slideBelow
          navigationController.isNavigationBarHidden = true
          navigationController.interactivePopGestureRecognizer?.delegate = self
          self.view.window?.rootViewController = sideMenuController
          self.view.window?.makeKeyAndVisible()
          
          print("confirm_otp_success")
          self.view.makeToast("Phone No Verified", duration: 6.0, position: .bottom)
          //                                        let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
          //                                        storyBoard.instantiateViewController(withIdentifier: "DashBoardTabVC") as! DashBoardTabViewController
          let nextViewController =  Storyboard().dashFlowStoryboard.instantiateViewController(withIdentifier: "CustomerDashBoardViewController")
          self.navigationController?.pushViewController(nextViewController, animated: true)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.confirm_review_address_customer()
    }
    
 
    @objc func dismissKeyboard() {
        scrollview.contentOffset.y = 0
        view.endEditing(true)
    }
    
    //horizontal scrollview lock
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollview.contentOffset.x != 0 {
            scrollview.contentOffset.x = 0
        }
    }
    
    @IBAction func BtnEditAddressClick(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "EditAddressDetailsVC") as! EditAddressDetailsViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
        self.IsFromReviewNdConfirm = true
        UserDefaults.standard.set(self.IsFromReviewNdConfirm,forKey: "IsFromReviewNdConfirm")
        
    }
    
    
    func confirm_review_address_customer()
    {
        
        self.AuthKeyFromSetAddress = UserDefaults.standard.string(forKey: "authentication_key") ?? ""
        print("AuthKeyFromSetAddress: \(AuthKeyFromSetAddress)")
        let urlString = ConstantsClass.baseUrl + ConstantsClass.confirm_review_address_customer
        print("urlString: \(urlString)")
        
        let headers = [
            "Authorization": "\(AuthKeyFromSetAddress)" // authkey from setaddress response to get address data
        ]
        print("headers: \(headers)")
        Helper.sharedInstance.showLoader()
        Alamofire.request(urlString, method: .get, parameters: nil, encoding: URLEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                       if (response["status"] as? Bool) != nil {
                        print("Get status true here")
                       
                            if let respArray = response["response"] as? [String:Any]{
                                print("respArray: \(respArray)")
                                
                                let Get_CurrentCellNo = respArray["cell_phone_number"] as? String ?? ""
                                let Get_Name =  respArray ["name"]as? String ?? ""
                                let Get_FamilyName =  respArray ["family_name"]as? String  ?? ""
                                let Get_Nationality = respArray ["nationality"]as? String ?? ""
                                let Get_EmailKey =  respArray ["email_id"]as? String ?? ""
                                let Get_ProfileImage =  respArray ["customer_profile"]as? String ?? ""
                                print("Get_ProfileImage: \(Get_ProfileImage)")
                                
                                if let AddressArray = respArray["Address"] as? [[String:Any]]{
                                    
                                    print("AddressArray: \(AddressArray)")
                                    for AddressArrayData in AddressArray
                                    {
                                        self.address_nickname = AddressArrayData["address_nickname"] as? String ?? ""
                                        let area =  AddressArrayData ["area"]as? String ?? ""
                                        let appartment =  AddressArrayData ["appartment"]as? String  ?? ""
                                        let street = AddressArrayData ["street"]as? String ?? ""
                                        let building_name =  AddressArrayData ["building_name"]as? String ?? ""
                                        let floor =  AddressArrayData ["floor"]as? String ?? ""
                                        let appartment_no =  AddressArrayData ["appartment_no"]as? String ?? ""
                                        let additional_directions =  AddressArrayData ["additional_directions"]as? String ?? ""
                                        let GetAddress_Id = AddressArrayData ["address_id"]as? String ?? ""
                                        print("GetAddress_Id: \(GetAddress_Id)")
                                        UserDefaults.standard.set(GetAddress_Id, forKey: "AddressIDKey")
                                        
                                        //                                self.FullAddress =  area + "," + "" +  appartment + "," + "" +
                                        //                                              street + "," + "" +  building_name + "," + "" + floor + "," + "" + appartment_no + "," + "" + additional_directions // for now
                                        self.FullAddress = street + ", " + building_name + ", " + floor + ", "  + appartment_no + ", " + area
                                        
                                        print("FullAddress: \(self.FullAddress)")
                                        
                                        
                                        self.LblSetAddressType.text = self.address_nickname
                                        self.LblFullAddressType.text = self.FullAddress
                                        
                                        
                                    }
                                }
                                //Set values
                                self.LblName.text = Get_Name
                                self.LblFamilyname.text = Get_FamilyName
                                self.LblNationality.text = Get_Nationality
                                self.LblCelNo.text = Get_CurrentCellNo
                                self.LblEmailID.text = Get_EmailKey
                                self.ImgUser.downloaded(from:Get_ProfileImage )
                                self.ImgUser.contentMode = .scaleAspectFill //3
                                
                                
                                
                            }
                            // Make sure to update UI in main thread
                            DispatchQueue.main.async {
                                print("set_address_success")
                                Helper.sharedInstance.hideLoader()
                            }
                        
                    }
                    
                }
                else
                {
                    Helper.sharedInstance.hideLoader()
                    print("Error occured") // serialized json response
                }
        }
        
        
    }
    
    
    
}
