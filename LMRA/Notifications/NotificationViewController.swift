//
//  NotificationViewController.swift
//  LMRA
//
//  Created by Mac on 13/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire
import FittedSheets

class NotificationViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var NotificationTableview: UITableView!
    
    var Loginauthentication_key_val = ""
    var Get_User_Type = ""
    var IsDualRole:Bool!
    //    var StringGet_User_Type = ""
    //       var AllDataDict = [[String:Any]]()
    var AllNotificationDataDict = [NSDictionary]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        self.Get_User_Type = UserDefaults.standard.string(forKey: UserDataManager_KEY.userType) ?? ""//for now// LoginUser_Type
        //        self.StringGet_User_Type = String(self.Get_User_Type)
        self.Loginauthentication_key_val = UserDefaults.standard.string(forKey: "authentication_key") ?? ""//get
        print("Loginauthentication_key_val:",self.Loginauthentication_key_val)
        print("Get_User_Type:",self.Get_User_Type)
        
       
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
         self.get_all_notifications()
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.AllNotificationDataDict.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = NotificationTableview.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath as IndexPath) as! NotificationTableViewCell
        
        
        let AllData =  self.AllNotificationDataDict[indexPath.row]
        print("AllDataCellForRow:",AllData)
        
        
        let notification_id = AllData["notification_id"] as? String ?? ""
        let task_title = AllData["task_title"] as? String ?? ""
        let task_date = AllData["task_date"] as? String ?? ""
        let task_description = AllData["task_description"] as? String ?? ""
        let task_invitation_status = AllData["task_invitation_status"] as? String ?? ""
        let task_timeslot = AllData["task_timeslot"] as? String ?? ""
        let task_day_time = AllData["task_day_time"] as? String ?? ""
        let customer_name = AllData["customer_name"] as? String ?? ""
        let tasker_name = AllData["tasker_name"] as? String ?? ""
        let customer_profile = AllData["customer_profile"] as? String ?? ""
        let tasker_profile = AllData["tasker_profile"] as? String ?? ""
        let task_user_type = AllData["task_user_type"] as? String ?? ""
        let task_subcategory = AllData["task_subcategory"] as? String ?? ""
        let Notification_Time = AllData["notification_time"] as? String ?? ""
        let Notification_DayTime = AllData["task_day_time"] as? String ?? ""
        if (IsDualRole) {
            if (Get_User_Type == "2") {
                if (task_invitation_status == "Waiting")
                {
                    cell.ColorView.backgroundColor = COLOR.GreenColor
                    cell.LblTaskTitleName.text = task_title
                    cell.LblServiceName.text = task_subcategory
                    cell.LblCustomerName.text = customer_name
                    cell.ImgCustomer.downloaded(from: customer_profile)
                    cell.LblStatus.text = task_invitation_status
                    cell.IconStatus.image = UIImage(named: "WaitingStatusIcon")
                    cell.LblTime.text = Notification_DayTime + " " + Notification_Time
                    
                    //redirect to task details
                    
                }
                else if (task_invitation_status == "Cancelled")
                {
                    cell.ColorView.backgroundColor = COLOR.GreenColor
                    cell.LblTaskTitleName.text = task_title
                    cell.LblServiceName.text = task_subcategory
                    cell.LblCustomerName.text = customer_name
                    cell.ImgCustomer.downloaded(from: customer_profile)
                    cell.LblStatus.text = "Your Task Is Cancelled"
                    cell.IconStatus.image = UIImage(named: "InfoIcon")
                    cell.LblTime.text = Notification_DayTime + " " + Notification_Time
                    //nothing
                }
                else if (task_invitation_status == "Declined")
                {
                    cell.ColorView.backgroundColor = COLOR.GreenColor
                    cell.LblTaskTitleName.text = task_title
                    cell.LblServiceName.text = task_subcategory
                    cell.LblCustomerName.text = customer_name
                    cell.ImgCustomer.downloaded(from: customer_profile)
                    cell.LblStatus.text = "Your Task Is Declined"
                    cell.IconStatus.image = UIImage(named: "InfoIcon")
                    cell.LblTime.text = Notification_DayTime + " " + Notification_Time
                    //nothing
                }
                else if (task_invitation_status == "Accepted")
                {
                    cell.ColorView.backgroundColor = COLOR.GreenColor
                    cell.LblTaskTitleName.text = task_title
                    cell.LblServiceName.text = task_subcategory
                    cell.LblCustomerName.text = customer_name
                    cell.ImgCustomer.downloaded(from: customer_profile)
                    cell.LblStatus.text = "Your Task Is Accepted"
                    cell.IconStatus.image = UIImage(named: "InfoIcon")
                    cell.LblTime.text = Notification_DayTime + " " + Notification_Time
                    //bottomsheet
                    //                    self.SwitchToCustomer()
                }
                
            }
            else if (Get_User_Type == "1") {
                if (task_invitation_status == "Waiting")
                {
                    cell.ColorView.backgroundColor = COLOR.PinkColor
                    cell.LblTaskTitleName.text = task_title
                    cell.LblServiceName.text = task_subcategory
                    cell.LblCustomerName.text = customer_name
                    cell.ImgCustomer.downloaded(from: customer_profile)
                    cell.LblStatus.text = "New Task Invite"
                    cell.IconStatus.image = UIImage(named: "WaitingStatusIcon")
                    cell.LblTime.text = Notification_DayTime + " " + Notification_Time
                    //bottomsheet
                    //                    self.SwitchToTasker()
                }
                else if (task_invitation_status == "Cancelled")
                {
                    cell.ColorView.backgroundColor = COLOR.PinkColor
                    cell.LblTaskTitleName.text = task_title
                    cell.LblServiceName.text = task_subcategory
                    cell.LblCustomerName.text = customer_name
                    cell.ImgCustomer.downloaded(from: customer_profile)
                    cell.LblStatus.text = "Your Task Is Cancelled"
                    cell.IconStatus.image = UIImage(named: "InfoIcon")
                    cell.LblTime.text = Notification_DayTime + " " + Notification_Time
                    //bootom sheet
                    //                    self.SwitchToTasker()
                    
                }
                else if (task_invitation_status == "Declined")
                {
                    cell.ColorView.backgroundColor = COLOR.PinkColor
                    cell.LblTaskTitleName.text = task_title
                    cell.LblServiceName.text = task_subcategory
                    cell.LblCustomerName.text = customer_name
                    cell.ImgCustomer.downloaded(from: customer_profile)
                    cell.LblStatus.text = "Your Task Is Declined"
                    cell.IconStatus.image = UIImage(named: "InfoIcon")
                    cell.LblTime.text = Notification_DayTime + " " + Notification_Time
                    //nothing
                }
                else if (task_invitation_status == "Accepted")
                {
                    cell.ColorView.backgroundColor = COLOR.PinkColor
                    cell.LblTaskTitleName.text = task_title
                    cell.LblServiceName.text = task_subcategory
                    cell.LblCustomerName.text = customer_name
                    cell.ImgCustomer.downloaded(from: customer_profile)
                    cell.LblStatus.text = "Your Task Is Accepted"
                    cell.IconStatus.image = UIImage(named: "InfoIcon")
                    cell.LblTime.text = Notification_DayTime + " " + Notification_Time
                    //nothing
                }
                
            }
            
        }
        else
        {
            if (Get_User_Type == "2")
            {
                if (task_invitation_status == "Waiting")
                {
                    
                    cell.LblTaskTitleName.text = task_title
                    cell.LblServiceName.text = task_subcategory
                    cell.LblCustomerName.text = customer_name
                    cell.ImgCustomer.downloaded(from: customer_profile)
                    cell.LblStatus.text = "New Task Invite"
                    cell.IconStatus.image = UIImage(named: "WaitingStatusIcon")
                    cell.LblTime.text = Notification_DayTime + " " + Notification_Time
                    //details page
                    
                }
                else if (task_invitation_status == "Cancelled")
                {
                    
                    cell.LblTaskTitleName.text = task_title
                    cell.LblServiceName.text = task_subcategory
                    cell.LblCustomerName.text = customer_name
                    cell.ImgCustomer.downloaded(from: customer_profile)
                    cell.LblStatus.text = "Your Task Is Cancelled"
                    cell.IconStatus.image = UIImage(named: "InfoIcon")
                    cell.LblTime.text = Notification_DayTime + " " + Notification_Time
                    //nothing
                }
                
            }
                
            else if (Get_User_Type == "1")
            {
                
                if (task_invitation_status == "Cancelled")
                {
                    
                    cell.LblTaskTitleName.text = task_title
                    cell.LblServiceName.text = task_subcategory
                    cell.LblCustomerName.text = customer_name
                    cell.ImgCustomer.downloaded(from: customer_profile)
                    cell.LblStatus.text = "Your Task Is Cancelled"
                    cell.IconStatus.image = UIImage(named: "InfoIcon")
                    cell.LblTime.text = Notification_DayTime + " " + Notification_Time
                    //nothing
                }
                else if (task_invitation_status == "Declined")
                {
                    
                    cell.LblTaskTitleName.text = task_title
                    cell.LblServiceName.text = task_subcategory
                    cell.LblCustomerName.text = customer_name
                    cell.ImgCustomer.downloaded(from: customer_profile)
                    cell.LblStatus.text = "Your Task Is Declined"
                    cell.IconStatus.image = UIImage(named: "InfoIcon")
                    cell.LblTime.text = Notification_DayTime + " " + Notification_Time
                    //nothing
                }
                else if (task_invitation_status == "Accepted")
                {
                    
                    cell.LblTaskTitleName.text = task_title
                    cell.LblServiceName.text = task_subcategory
                    cell.LblCustomerName.text = customer_name
                    cell.ImgCustomer.downloaded(from: customer_profile)
                    cell.LblStatus.text = "Your Task Is Accepted"
                    cell.IconStatus.image = UIImage(named: "InfoIcon")
                    cell.LblTime.text = Notification_DayTime + " " + Notification_Time
                    //nothing
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let cell = NotificationTableview.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath as IndexPath) as! NotificationTableViewCell
        
        
        let AllData =  self.AllNotificationDataDict[indexPath.row]
        print("AllDataCellForRow:",AllData)
        
        let task_invitation_status = AllData["task_invitation_status"] as? String ?? ""
        if (IsDualRole) {
            if (Get_User_Type == "2") {
                if (task_invitation_status == "Waiting")
                {
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Tasker", bundle:nil)
                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "Task_DetailsViewController") as! Task_DetailsViewController
                    
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                }
                else if (task_invitation_status == "Accepted")
                {
                    //bottom sheet for customer
                    self.SwitchToCustomer()
                }
                
            }
            else if (Get_User_Type == "1") {
                if (task_invitation_status == "Waiting")
                {
                    //bottom sheet for tasker
                    self.SwitchToTasker()
                    
                }
                else if (task_invitation_status == "Cancelled")
                {
                    //bottom sheet for tasker
                    self.SwitchToTasker()
                    
                }
              
            }
        }
        else
        {
            if (Get_User_Type == "2")
            {
                if (task_invitation_status == "Waiting")
                {
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Tasker", bundle:nil)
                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "Task_DetailsViewController") as! Task_DetailsViewController
                    
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                }
            }
            else if (Get_User_Type == "1")
            {
                if (task_invitation_status == "Accepted")
                {
                    let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TaskerTaskVC") as! TaskerTaskDetailsViewController
                    
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                }
            }
            
        }
    }
    
    func SwitchToCustomer()
    {
        //bottom sheet implementation for calendar
        
        let controller = SwitchToCustomerVC()
        let sheetController = SheetViewController(controller: controller)
        controller.preferredContentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height / 2)
        self.present(sheetController, animated: true, completion: nil)
    }
    
    func SwitchToTasker()
    {
        //bottom sheet implementation for calendar
        
        let controller = SwitchToTaskerVC()
        let sheetController = SheetViewController(controller: controller)
        controller.preferredContentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height / 2)
        self.present(sheetController, animated: true, completion: nil)
        
        
    }
    
    
    func get_all_notifications()
    {
        
        let urlString = ConstantsClass.baseUrl + ConstantsClass.get_all_notifications
        print("urlString: \(urlString)")
        
        let headers = [
            "Authorization": "\(self.Loginauthentication_key_val)"
        ]
        print("headers: \(headers)")
        
        let parameters = [
            "user_type" : "\(self.Get_User_Type)"
            
            ] as [String : Any]
        print("parametersNotification: \(parameters)")
        Helper.sharedInstance.showLoader()
        Alamofire.request(urlString
            , method: .post, parameters: parameters, encoding:JSONEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if let status = response["status"] as? Bool {
                        print("Get status true here")
                        
                        self.IsDualRole = response["is_dual_role"]as? Bool
                        print("IsDualRole: \(self.IsDualRole)")
                        if status
                        {
                            
                            if let respArray = response["response"] as? [[String:Any]] {
                                print("respArray: \(respArray)")
                                
                                self.AllNotificationDataDict = response["response"] as! [NSDictionary]
                                print("AllNotificationDataDict:",self.AllNotificationDataDict)
                                Helper.sharedInstance.hideLoader()
                            }
                            
                        }
                        else
                        {
                            Helper.sharedInstance.hideLoader()
                            self.NotificationTableview.isHidden = true
                            // self.view.makeToast("Notifications Not Found", duration: 6.0, position: .bottom)
                        }
                    }
                    
                    // Make sure to update UI in main thread
                    DispatchQueue.main.async {
                        self.NotificationTableview.reloadData()
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
        
    }
    
    @IBAction func BackBtnClick(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
