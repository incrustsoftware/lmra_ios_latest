//
//  SwitchToCustomerVC.swift
//  LMRA
//
//  Created by Mac on 03/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class SwitchToCustomerVC: UIViewController {
    @IBOutlet weak var BtnConfirm: UIButton!
    @IBOutlet weak var BtnCancel: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
        self.BtnConfirm.setTitleColor(COLOR.WhiteColor, for: .normal)
        
        self.BtnConfirm.backgroundColor = COLOR.PinkColor
        self.BtnConfirm.layer.borderColor = COLOR.pinkCgColor
        self.BtnConfirm.layer.cornerRadius = 2
        self.BtnConfirm.layer.borderWidth = 1
        
      
        self.BtnCancel.layer.cornerRadius = 2
        self.BtnCancel.layer.borderWidth = 1
        self.BtnCancel.layer.borderColor = COLOR.pinkCgColor
        self.BtnCancel.setTitleColor(COLOR.PinkColor, for: .normal)
        self.BtnCancel.backgroundColor = COLOR.WhiteColor
        
    }
    
    @IBAction func BtnConfirmClick(_ sender: Any) {
        
        let stroyboard:UIStoryboard = UIStoryboard(name: "DashFlow", bundle: nil)
        let objVc:DashBoardTabViewController =
            storyboard?.instantiateViewController(withIdentifier: "DashBoardTabVC") as! DashBoardTabViewController
        
        self.navigationController?.pushViewController(objVc, animated: true)
    }
    
    @IBAction func BtnCancelClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
