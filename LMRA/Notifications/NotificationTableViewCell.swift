//
//  NotificationTableViewCell.swift
//  LMRA
//
//  Created by Mac on 13/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var CustomerView: UIView!
   
    @IBOutlet weak var ColorView: UIView!
    @IBOutlet weak var BtnForwardArrow: UIButton!
    @IBOutlet weak var LblTime: UILabel!
    @IBOutlet weak var LblStatus: UILabel!
    @IBOutlet weak var IconStatus: UIImageView!
    @IBOutlet weak var LblUpdate: UILabel!
    @IBOutlet weak var LblCustomerName: UILabel!
    @IBOutlet weak var ImgCustomer: UIImageView!
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var LblTaskTitleName: UILabel!
    
    @IBOutlet weak var LblServiceName: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        MainView.layer.shadowColor = UIColor.gray.cgColor
        MainView.layer.shadowOpacity = 0.5
        MainView.layer.shadowOffset = CGSize.zero
        MainView.layer.shadowRadius = 6
        MainView.layer.cornerRadius = 2
        MainView.clipsToBounds = false
        
        ImgCustomer.layer.cornerRadius = ImgCustomer.frame.size.width/2
        ImgCustomer.clipsToBounds = true
        self.ImgCustomer.contentMode = .scaleAspectFill //3
        
        
        // Configure the view for the selected state
    }

}
