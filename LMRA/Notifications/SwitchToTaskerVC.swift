//
//  SwitchToTaskerVC.swift
//  LMRA
//
//  Created by Mac on 03/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class SwitchToTaskerVC: UIViewController, UIGestureRecognizerDelegate {
    @IBOutlet weak var BtnConfirm: UIButton!
    @IBOutlet weak var BtnCancel: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.BtnConfirm.setTitleColor(COLOR.WhiteColor, for: .normal)
          
          self.BtnConfirm.backgroundColor = COLOR.PinkColor
          self.BtnConfirm.layer.borderColor = COLOR.pinkCgColor
          self.BtnConfirm.layer.cornerRadius = 2
          self.BtnConfirm.layer.borderWidth = 1
          
        
          self.BtnCancel.layer.cornerRadius = 2
          self.BtnCancel.layer.borderWidth = 1
          self.BtnCancel.layer.borderColor = COLOR.pinkCgColor
          self.BtnCancel.setTitleColor(COLOR.PinkColor, for: .normal)
          self.BtnCancel.backgroundColor = COLOR.WhiteColor
        
        self.preferredContentSize = CGSize(width: 414, height: 300)
        // Do any additional setup after loading the view.
    }

    @IBAction func BtnConfirmClick(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let rootViewController = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "TaskerDasboardVC")
        
        let navigationController = UINavigationController(rootViewController: rootViewController)
        let rightViewController = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "SideMenuViewController")
        let sideMenuController = LGSideMenuController(rootViewController: navigationController,
                                                      leftViewController: nil,
                                                      rightViewController: rightViewController)
        sideMenuController.leftViewWidth = 0
        sideMenuController.leftViewPresentationStyle = .slideBelow;
        
        sideMenuController.rightViewWidth = (appDelegate?.window?.bounds.size.width ?? 320) - 20
        sideMenuController.leftViewPresentationStyle = .slideBelow
        navigationController.isNavigationBarHidden = true
        navigationController.interactivePopGestureRecognizer?.delegate = self
        //self.interactivePopGestureRecognizer.enabled = NO;
        appDelegate?.window?.rootViewController = sideMenuController
        appDelegate?.window?.makeKeyAndVisible()
       }
       
       @IBAction func BtnCancelClick(_ sender: Any) {
           self.dismiss(animated: true, completion: nil)
           
       }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
