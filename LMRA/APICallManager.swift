//
//  APICallManager.swift
//  LMRA
//
//  Created by Mac on 17/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation

class APICallManager:NSObject {
    
    static let shared = APICallManager()
    
    /*****************************************************************************
     * Function :   sendOTPApi
     * Input    :   [:]
     * Output   :   Return  [String : Any] data
     * Comment  :   -----
     *
     ****************************************************************************/
    public func sendOTPApi(subUrl:String,parameter:[String:Any],isLoadingIndicatorShow:Bool = true,completionHandler: @escaping ([String:Any]) -> Void){
        CloudManager().makeCloudRequest(subUrl: subUrl, parameter: parameter, requestType: .post) { response in
            switch response?.result {
            case .success?:
                if let data = response?.data {
                    DispatchQueue.global(qos: .background).async {
                        do {
                            completionHandler(response?.result.value as! [String : Any])
                        }
                        catch {
                            print(error)
                            print(error.localizedDescription)
                        }
                    }
                }
                break
            case .failure?:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            case .none:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            }
        }
    }
    
    /*****************************************************************************
     * Function :   verifyOtpTaskerMobileAccountAPI
     * Input    :   [:]
     * Output   :   Return  Verify_OTP_DataModel data
     * Comment  :   -----
     *
     ****************************************************************************/
    public func verifyOtpTaskerMobileAccountAPI(subUrl:String,parameter:[String:Any],isLoadingIndicatorShow:Bool = true,isfromLogin:Bool = false,completionHandler: @escaping (Verify_OTP_DataModel) -> Void){
        CloudManager().makeCloudRequest(subUrl: subUrl, parameter: parameter, requestType: .post) { response in
            switch response?.result {
            case .success?:
                if let data = response?.data {
                    DispatchQueue.global(qos: .background).async {
                        do {
                            if isfromLogin {
                                UserDataManager.shared.setUserDetails(userDetails: data)
                            }
                            let responseData = try JSONDecoder().decode(Verify_OTP_DataModel.self, from: data)
                            completionHandler(responseData)
                        }
                        catch {
                            print(error)
                            print(error.localizedDescription)
                        }
                    }
                }
                break
            case .failure?:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            case .none:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            }
        }
    }
    
    
    /*****************************************************************************
     * Function :   confirmPersonalDetailsTaskerApi
     * Input    :   [:]
     * Output   :   Return  [String : Any] data
     * Comment  :   -----
     *
     ****************************************************************************/
    public func confirmPersonalDetailsTaskerApi(subUrl:String,parameter:[String:Any],isLoadingIndicatorShow:Bool = true,completionHandler: @escaping ([String:Any]) -> Void){
        CloudManager().makeCloudRequest(subUrl: subUrl, parameter: parameter, requestType: .post) { response in
            switch response?.result {
            case .success?:
                if let data = response?.data {
                    DispatchQueue.global(qos: .background).async {
                        do {
                            completionHandler(response?.result.value as! [String : Any])
                        }
                        catch {
                            print(error)
                            print(error.localizedDescription)
                        }
                    }
                }
                break
            case .failure?:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            case .none:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            }
        }
    }
    
    /*****************************************************************************
     * Function :   sendOtpOnemailTaskerApi
     * Input    :   [:]
     * Output   :   Return  [String : Any] data
     * Comment  :   -----
     *
     ****************************************************************************/
    public func sendOtpOnemailTaskerApi(subUrl:String,parameter:[String:Any],isLoadingIndicatorShow:Bool = true,completionHandler: @escaping ([String:Any]) -> Void){
        CloudManager().makeCloudRequest(subUrl: subUrl, parameter: parameter, requestType: .post) { response in
            switch response?.result {
            case .success?:
                if let data = response?.data {
                    DispatchQueue.global(qos: .background).async {
                        do {
                            completionHandler(response?.result.value as! [String : Any])
                        }
                        catch {
                            print(error)
                            print(error.localizedDescription)
                        }
                    }
                }
                break
            case .failure?:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            case .none:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            }
        }
    }
    
    /*****************************************************************************
     * Function :   verifyOtpTaskerAccountApi
     * Input    :   [:]
     * Output   :   Return  Verify_OTP_DataModel  data
     * Comment  :   -----
     *
     ****************************************************************************/
    public func verifyOtpTaskerAccountApi(subUrl:String,parameter:[String:Any],isLoadingIndicatorShow:Bool = true,completionHandler: @escaping (Verify_OTP_DataModel) -> Void){
        CloudManager().makeCloudRequest(subUrl: subUrl, parameter: parameter, requestType: .post) { response in
            switch response?.result {
            case .success?:
                if let data = response?.data {
                    DispatchQueue.global(qos: .background).async {
                        do {
                            let responseData = try JSONDecoder().decode(Verify_OTP_DataModel.self, from: data)
                            if responseData.status {
                                UserDataManager.shared.setUserDetails(userDetails: data)
                            }
                            completionHandler(responseData)
                        }
                        catch {
                            print(error)
                            print(error.localizedDescription)
                        }
                    }
                }
                break
            case .failure?:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            case .none:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            }
        }
    }
    
    /*****************************************************************************
     * Function :   changeCellPhoneNumberTaskerAPi
     * Input    :   [:]
     * Output   :   Return  [String : Any] data
     * Comment  :   -----
     *
     ****************************************************************************/
    public func changeCellPhoneNumberTaskerAPi(subUrl:String,parameter:[String:Any],isLoadingIndicatorShow:Bool = true,completionHandler: @escaping ([String:Any]) -> Void){
        CloudManager().makeCloudRequest(subUrl: subUrl, parameter: parameter, requestType: .post) { response in
            switch response?.result {
            case .success?:
                if let data = response?.data {
                    DispatchQueue.global(qos: .background).async {
                        do {
                            completionHandler(response?.result.value as! [String : Any])
                        }
                        catch {
                            print(error)
                            print(error.localizedDescription)
                        }
                    }
                }
                break
            case .failure?:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            case .none:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            }
        }
    }
    
    /*****************************************************************************
     * Function :   getServicesSubcategoriesAPi
     * Input    :   [:]
     * Output   :   Return  [String : Any] data
     * Comment  :   -----
     *
     ****************************************************************************/
    public func getServicesSubcategoriesAPi(subUrl:String,parameter:[String:Any],isLoadingIndicatorShow:Bool = true,completionHandler: @escaping (Any) -> Void){
        CloudManager().makeCloudRequest(subUrl: subUrl, parameter: parameter, requestType: .post) { response in
            switch response?.result {
            case .success(let JSON)?:
                if let data = response?.data {
                    DispatchQueue.global(qos: .background).async {
                        do {
                            completionHandler(JSON)
                        }
                        catch {
                            print(error)
                            print(error.localizedDescription)
                        }
                    }
                }
                break
            case .failure?:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            case .none:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            }
        }
    }
    
    /*****************************************************************************
     * Function :   getTaskerSubcategoriesAPi
     * Input    :   [:]
     * Output   :   Return  [String : Any] data
     * Comment  :   -----
     *
     ****************************************************************************/
    public func getTaskerSubcategoriesAPi(subUrl:String,parameter:[String:Any],isLoadingIndicatorShow:Bool = true,completionHandler: @escaping ([String:Any]) -> Void){
        CloudManager().makeCloudRequest(subUrl: subUrl, parameter: parameter, requestType: .post) { response in
            switch response?.result {
            case .success?:
                if let data = response?.data {
                    DispatchQueue.global(qos: .background).async {
                        do {
                            completionHandler(response?.result.value as! [String : Any])
                        }
                        catch {
                            print(error)
                            print(error.localizedDescription)
                        }
                    }
                }
                break
            case .failure?:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            case .none:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            }
        }
    }
    
    
    /*****************************************************************************
     * Function :   get_categoryViUserTypeAPi
     * Input    :   [:]
     * Output   :   Return  [String : Any] data
     * Comment  :   -----
     *
     ****************************************************************************/
    public func get_categoryViUserTypeAPi(subUrl:String,parameter:[String:Any],isLoadingIndicatorShow:Bool = true,completionHandler: @escaping ([String:Any]) -> Void){
        CloudManager().makeCloudRequest(subUrl: subUrl, parameter: parameter, requestType: .post) { response in
            switch response?.result {
            case .success?:
                if let data = response?.data {
                    DispatchQueue.global(qos: .background).async {
                        do {
                            completionHandler(response?.result.value as! [String : Any])
                        }
                        catch {
                            print(error)
                            print(error.localizedDescription)
                        }
                    }
                }
                break
            case .failure?:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            case .none:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            }
        }
    }
    
    /*****************************************************************************
     * Function :   getOfflineReasonsApi
     * Input    :   [:]
     * Output   :   Return  OfflineModeReasonDataModel data
     * Comment  :   -----
     *
     ****************************************************************************/
    public func getOfflineReasonsApi(subUrl:String,parameter:[String:Any],isLoadingIndicatorShow:Bool = true,completionHandler: @escaping (OfflineModeReasonDataModel) -> Void){
        CloudManager().makeCloudRequest(subUrl: subUrl, parameter: parameter, requestType: .post) { response in
            switch response?.result {
            case .success?:
                if let data = response?.data {
                    DispatchQueue.global(qos: .background).async {
                        do {
                            let responseData = try JSONDecoder().decode(OfflineModeReasonDataModel.self, from: data)
                            completionHandler(responseData)
                        }
                        catch {
                            print(error)
                            print(error.localizedDescription)
                        }
                    }
                }
                break
            case .failure?:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            case .none:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            }
        }
    }
    
    /*****************************************************************************
     * Function :   getTaskDetailsApi
     * Input    :   [:]
     * Output   :   Return  TaskerDetailDataModel data
     * Comment  :   -----
     *
     ****************************************************************************/
    public func getTaskDetailsApi(subUrl:String,parameter:[String:Any],isLoadingIndicatorShow:Bool = true,completionHandler: @escaping (TaskerDetailDataModel) -> Void){
        CloudManager().makeCloudRequest(subUrl: subUrl, parameter: parameter, requestType: .post) { response in
            switch response?.result {
            case .success?:
                if let data = response?.data {
                    DispatchQueue.global(qos: .background).async {
                        do {
                            let responseData = try JSONDecoder().decode(TaskerDetailDataModel.self, from: data)
                            completionHandler(responseData)
                        }
                        catch {
                            print(error)
                            print(error.localizedDescription)
                        }
                    }
                }
                break
            case .failure?:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            case .none:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            }
        }
    }
    
    /*****************************************************************************
     * Function :   getOpenTaskTaskerApi
     * Input    :   [:]
     * Output   :   Return  [String:Any] data
     * Comment  :   -----
     *
     ****************************************************************************/
    public func getOpenTaskTaskerApi(subUrl:String,parameter:[String:Any],isLoadingIndicatorShow:Bool = true,completionHandler: @escaping ([String:Any]) -> Void){
        CloudManager().makeCloudRequest(subUrl: subUrl, parameter: parameter, requestType: .post) { response in
            switch response?.result {
            case .success?:
                if let data = response?.data {
                    DispatchQueue.global(qos: .background).async {
                        do {
                            completionHandler(response?.result.value as! [String : Any])
                        }
                        catch {
                            print(error)
                            print(error.localizedDescription)
                        }
                    }
                }
                break
            case .failure?:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            case .none:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            }
        }
    }
    
    /*****************************************************************************
     * Function :   getUpcomingTaskTaskerApi
     * Input    :   [:]
     * Output   :   Return  [String:Any] data
     * Comment  :   -----
     *
     ****************************************************************************/
    public func getUpcomingTaskTaskerApi(subUrl:String,parameter:[String:Any],isLoadingIndicatorShow:Bool = true,completionHandler: @escaping ([String:Any]) -> Void){
        CloudManager().makeCloudRequest(subUrl: subUrl, parameter: parameter, requestType: .post) { response in
            switch response?.result {
            case .success?:
                if let data = response?.data {
                    DispatchQueue.global(qos: .background).async {
                        do {
                            completionHandler(response?.result.value as! [String : Any])
                        }
                        catch {
                            print(error)
                            print(error.localizedDescription)
                        }
                    }
                }
                break
            case .failure?:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            case .none:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            }
        }
    }
    /*****************************************************************************
     * Function :   offlineModeApi
     * Input    :   [:]
     * Output   :   Return  OfflineModeDataModel data
     * Comment  :   -----
     *
     ****************************************************************************/
    public func offlineModeApi(subUrl:String,parameter:[String:Any],isLoadingIndicatorShow:Bool = true,completionHandler: @escaping (OfflineModeDataModel) -> Void){
        CloudManager().makeCloudRequest(subUrl: subUrl, parameter: parameter, requestType: .post) { response in
            switch response?.result {
            case .success?:
                if let data = response?.data {
                    DispatchQueue.global(qos: .background).async {
                        do {
                            let responseData = try JSONDecoder().decode(OfflineModeDataModel.self, from: data)
                            completionHandler(responseData)
                        }
                        catch {
                            print(error)
                            print(error.localizedDescription)
                        }
                    }
                }
                break
            case .failure?:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            case .none:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            }
        }
    }
    
    /*****************************************************************************
     * Function :   cancelTaskApi
     * Input    :   [:]
     * Output   :   Return  DeclineTaskResponseModel data
     * Comment  :   -----
     *
     ****************************************************************************/
    public func cancelTaskApi(subUrl:String,parameter:[String:Any],isLoadingIndicatorShow:Bool = true,completionHandler: @escaping (DeclineTaskResponseModel) -> Void){
        CloudManager().makeCloudRequest(subUrl: subUrl, parameter: parameter, requestType: .post) { response in
            switch response?.result {
            case .success?:
                if let data = response?.data {
                    DispatchQueue.global(qos: .background).async {
                        do {
                            let responseData = try JSONDecoder().decode(DeclineTaskResponseModel.self, from: data)
                            completionHandler(responseData)
                        }
                        catch {
                            print(error)
                            print(error.localizedDescription)
                        }
                    }
                }
                break
            case .failure?:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            case .none:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            }
        }
    }
    
    /*****************************************************************************
     * Function :   decline_task_reasonApi
     * Input    :   [:]
     * Output   :   Return  DeclineReasoneDataModel data
     * Comment  :   -----
     *
     ****************************************************************************/
    public func decline_task_reasonApi(subUrl:String,parameter:[String:Any],isLoadingIndicatorShow:Bool = true,completionHandler: @escaping (DeclineReasoneDataModel) -> Void){
        CloudManager().makeCloudRequest(subUrl: subUrl, parameter: parameter, requestType: .post) { response in
            switch response?.result {
            case .success?:
                if let data = response?.data {
                    DispatchQueue.global(qos: .background).async {
                        do {
                            let responseData = try JSONDecoder().decode(DeclineReasoneDataModel.self, from: data)
                            completionHandler(responseData)
                        }
                        catch {
                            print(error)
                            print(error.localizedDescription)
                        }
                    }
                }
                break
            case .failure?:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            case .none:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            }
        }
    }
    
    /*****************************************************************************
     * Function :   decline_task_invitationAPI
     * Input    :   [:]
     * Output   :   Return  DeclineTaskResponseModel data
     * Comment  :   -----
     *
     ****************************************************************************/
    public func decline_task_invitationAPI(subUrl:String,parameter:[String:Any],isLoadingIndicatorShow:Bool = true,completionHandler: @escaping (DeclineTaskResponseModel) -> Void){
        CloudManager().makeCloudRequest(subUrl: subUrl, parameter: parameter, requestType: .post) { response in
            switch response?.result {
            case .success?:
                if let data = response?.data {
                    DispatchQueue.global(qos: .background).async {
                        do {
                            let responseData = try JSONDecoder().decode(DeclineTaskResponseModel.self, from: data)
                            completionHandler(responseData)
                        }
                        catch {
                            print(error)
                            print(error.localizedDescription)
                        }
                    }
                }
                break
            case .failure?:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            case .none:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            }
        }
    }
    
    /*****************************************************************************
     * Function :   getAccountProfileAPI
     * Input    :   [:]
     * Output   :   Return  TaskerProfileDetailDataModel data
     * Comment  :   -----
     *
     ****************************************************************************/
    public func getAccountProfileAPI(subUrl:String,parameter:[String:Any],isLoadingIndicatorShow:Bool = true,completionHandler: @escaping (TaskerProfileDetailDataModel) -> Void){
        CloudManager().makeCloudRequest(subUrl: subUrl, parameter: parameter, requestType: .post) { response in
            switch response?.result {
            case .success?:
                if let data = response?.data {
                    DispatchQueue.global(qos: .background).async {
                        do {
                            let responseData = try JSONDecoder().decode(TaskerProfileDetailDataModel.self, from: data)
                            completionHandler(responseData)
                        }
                        catch {
                            print(error)
                            print(error.localizedDescription)
                        }
                    }
                }
                break
            case .failure?:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            case .none:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            }
        }
    }
    
    /*****************************************************************************
     * Function :   getActiveTaskAPI
     * Input    :   [:]
     * Output   :   Return  TaskerTaskResponseModel data
     * Comment  :   -----
     *
     ****************************************************************************/
    public func getActiveTaskAPI(subUrl:String,parameter:[String:Any],isLoadingIndicatorShow:Bool = true,completionHandler: @escaping (TaskerTaskDataModel) -> Void){
        CloudManager().makeCloudRequest(subUrl: subUrl, parameter: parameter, requestType: .post) { response in
            switch response?.result {
            case .success?:
                if let data = response?.data {
                    DispatchQueue.global(qos: .background).async {
                        do {
                            let responseData = try JSONDecoder().decode(TaskerTaskDataModel.self, from: data)
                            completionHandler(responseData)
                        }
                        catch {
                            print(error)
                            print(error.localizedDescription)
                        }
                    }
                }
                break
            case .failure?:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            case .none:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            }
        }
    }
    
    /*****************************************************************************
     * Function :   getTimeSlotAPI
     * Input    :   [:]
     * Output   :   Return  TimeSlotesDataModel data
     * Comment  :   -----
     *
     ****************************************************************************/
    public func getTimeSlotAPI(subUrl:String,parameter:[String:Any],isLoadingIndicatorShow:Bool = true,completionHandler: @escaping (TimeSlotDataModel) -> Void){
        CloudManager().makeCloudRequest(subUrl: subUrl, parameter: parameter, requestType: .post) { response in
            switch response?.result {
            case .success?:
                if let data = response?.data {
                    DispatchQueue.global(qos: .background).async {
                        do {
                            let responseData = try JSONDecoder().decode(TimeSlotDataModel.self, from: data)
                            completionHandler(responseData)
                        }
                        catch {
                            print(error)
                            print(error.localizedDescription)
                        }
                    }
                }
                break
            case .failure?:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            case .none:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            }
        }
    }
    
    /*****************************************************************************
     * Function :   updateTaskerWorkingScheduleAPI
     * Input    :   [:]
     * Output   :   Return  UpdateTaskerWorkPrefernceDataModel data
     * Comment  :   -----
     *
     ****************************************************************************/
    public func updateTaskerWorkingScheduleAPI(subUrl:String,parameter:[String:Any],isLoadingIndicatorShow:Bool = true,completionHandler: @escaping (UpdateTaskerWorkPrefernceDataModel) -> Void){
        CloudManager().makeCloudRequest(subUrl: subUrl, parameter: parameter, requestType: .post) { response in
            switch response?.result {
            case .success?:
                if let data = response?.data {
                    DispatchQueue.global(qos: .background).async {
                        do {
                            let responseData = try JSONDecoder().decode(UpdateTaskerWorkPrefernceDataModel.self, from: data)
                            completionHandler(responseData)
                        }
                        catch {
                            print(error)
                            print(error.localizedDescription)
                        }
                    }
                }
                break
            case .failure?:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            case .none:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            }
        }
    }
    
    /*****************************************************************************
     * Function :   updateTaskerWorkingScheduleAPI
     * Input    :   [:]
     * Output   :   Return  UpdateTaskerWorkPrefernceDataModel data
     * Comment  :   -----
     *
     ****************************************************************************/
    public func getLanguageAPI(subUrl:String,parameter:[String:Any],isLoadingIndicatorShow:Bool = true,completionHandler: @escaping (LanguageDataModel) -> Void){
        CloudManager().makeCloudRequest(subUrl: subUrl, parameter: parameter, requestType: .post) { response in
            switch response?.result {
            case .success?:
                if let data = response?.data {
                    DispatchQueue.global(qos: .background).async {
                        do {
                            let responseData = try JSONDecoder().decode(LanguageDataModel.self, from: data)
                            completionHandler(responseData)
                        }
                        catch {
                            print(error)
                            print(error.localizedDescription)
                        }
                    }
                }
                break
            case .failure?:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            case .none:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            }
        }
    }
    
    /*****************************************************************************
     * Function :   updateTaskerLanguageAPI
     * Input    :   [:]
     * Output   :   Return  UpdateLanguageDataModel data
     * Comment  :   -----
     *
     ****************************************************************************/
    public func updateTaskerLanguageAPI(subUrl:String,parameter:[String:Any],isLoadingIndicatorShow:Bool = true,completionHandler: @escaping (UpdateLanguageDataModel) -> Void){
        CloudManager().makeCloudRequest(subUrl: subUrl, parameter: parameter, requestType: .post) { response in
            switch response?.result {
            case .success?:
                if let data = response?.data {
                    DispatchQueue.global(qos: .background).async {
                        do {
                            let responseData = try JSONDecoder().decode(UpdateLanguageDataModel.self, from: data)
                            completionHandler(responseData)
                        }
                        catch {
                            print(error)
                            print(error.localizedDescription)
                        }
                    }
                }
                break
            case .failure?:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            case .none:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            }
        }
    }
    
    /*****************************************************************************
     * Function :   logoutApi
     * Input    :   [:]
     * Output   :   Return  [String:Any] data
     * Comment  :   -----
     *
     ****************************************************************************/
    public func logoutApi(subUrl:String,parameter:[String:Any],isLoadingIndicatorShow:Bool = true,completionHandler: @escaping ([String:Any]) -> Void){
        CloudManager().makeCloudRequest(subUrl: subUrl, parameter: parameter, requestType: .post) { response in
            switch response?.result {
            case .success?:
                if let data = response?.data {
                    DispatchQueue.global(qos: .background).async {
                        do {
                            completionHandler(response?.result.value as! [String : Any])
                        }
                        catch {
                            print(error)
                            print(error.localizedDescription)
                        }
                    }
                }
                break
            case .failure?:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            case .none:
                self.getcurrentControllerview().makeToast("Something went's wrong")
                break
            }
        }
    }
}


