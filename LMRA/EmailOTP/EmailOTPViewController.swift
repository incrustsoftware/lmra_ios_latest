//
//  EmailOTPViewController.swift
//  LMRA
//
//  Created by Mac on 07/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire
class EmailOTPViewController: UIViewController {
    
    @IBOutlet weak var Mainview: UIView!
    @IBOutlet weak var LblTimer: UILabel!
    @IBOutlet weak var BtnVerify: UIButton!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var TFFirst: UITextField!
    @IBOutlet weak var TFSecond: UITextField!
    @IBOutlet weak var TFThird: UITextField!
    @IBOutlet weak var TFFourth: UITextField!
    @IBOutlet weak var BtnResend: UIButton!
    
    var Get_Email_ID = ""
    var Get_CPR_No = ""
    var EmailOTP = ""
    var Firstval = ""
    var Secondval = ""
    var Thirdval = ""
    var FourthVal = ""
    var Emailauthentication_key = ""
    var getSMSAuthenticationKey = ""
    var is_guest_user = "0"
    var UpdatedCellNo = ""
    var Get_CPR_Number = ""
    var seconds = 120 //This variable will hold a starting value of seconds. It could be any amount above 0.
    var timer = Timer()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        BtnVerify.layer.cornerRadius = 2;
        runTimer()//otp timer function
      
        addDoneButtonOnKeyboard()
        self.getSMSAuthenticationKey = UserDefaults.standard.string(forKey: "SMSAuthKey") ?? ""
        
        TFFirst.addTarget(self, action: #selector(EmailOTPViewController.txtFieldChanged(_:)),
                          for: .editingChanged)
        TFSecond.addTarget(self, action: #selector(EmailOTPViewController.txtFieldChanged),
                           for: .editingChanged)
        TFThird.addTarget(self, action: #selector(EmailOTPViewController.txtFieldChanged),
                          for: .editingChanged)
        TFFourth.addTarget(self, action: #selector(EmailOTPViewController.txtFieldChanged),
                           for: .editingChanged)
        
        self.TFFirst.delegate = self
        self.TFSecond.delegate = self
        self.TFThird.delegate = self
        self.TFFourth.delegate = self
        self.TFFirst.addTarget(self, action: #selector (self.changeCharacter), for: .editingChanged)
        self.TFSecond.addTarget(self, action: #selector (self.changeCharacter), for: .editingChanged)
        self.TFThird.addTarget(self, action: #selector (self.changeCharacter), for: .editingChanged)
        self.TFFourth.addTarget(self, action: #selector (self.changeCharacter), for: .editingChanged)
        
        self.BtnVerify.setTitleColor(COLOR.WhiteColor, for: .normal)
        BtnVerify.backgroundColor = COLOR.PinkColor
    }
    
    @objc func changeCharacter(textField: UITextField)
    {
        if textField.text!.utf8.count == 1
        {
            switch textField {
            case TFFirst:
                TFSecond.becomeFirstResponder()
            case TFSecond:
                TFThird.becomeFirstResponder()
            case TFThird:
                TFFourth.becomeFirstResponder()
            case TFFourth:
                print("OTP send to server")// here send otp to server
            default:
                break
            }
        }
        else if textField.text!.isEmpty
        {
            switch textField {
            case TFFourth:
                TFThird.becomeFirstResponder()
            case TFThird:
                TFSecond.becomeFirstResponder()
            case TFSecond:
                TFFirst.becomeFirstResponder()
            default:
                break
            }
        }
    }
    
    func runTimer() {
        self.BtnResend.isEnabled = false
        //        self.BtnResend.setTitleColor(BtnResendOffColor, for: .normal)
        BtnResend.setTitleColor(.gray, for: .normal)
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: #selector(EmailOTPViewController.updateTimer), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        seconds -= 1  //This will decrement(count down)the seconds.
        
        LblTimer.text = timeString(time: TimeInterval(seconds))
        if seconds ==  0
        {
            BtnResend.isEnabled = true
            self.BtnResend.setTitleColor(COLOR.PinkColor, for: .normal)
            
            timer.invalidate()
        }
    }
    
    func timeString(time:TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i", minutes, seconds)
    }
    
    
    
    @IBAction func VerifyBtnClick(_ sender: Any) {
        self.Firstval = self.TFFirst.text!
        self.Secondval = self.TFSecond.text!
        self.Thirdval = self.TFThird.text!
        self.FourthVal = self.TFFourth.text!
        
        print("firstval: \(Firstval)")
        print("Secondval: \(Secondval)")
        print("Thirdval: \(Thirdval)")
        print("FourthVal: \(FourthVal)")
        
        self.EmailOTP = (Firstval) + (Secondval) + (Thirdval) + (FourthVal)
        print("EmailOTP: \(self.EmailOTP)")
        
        self.Get_Email_ID = UserDefaults.standard.string(forKey: "EmailKey")!//get
        self.Get_CPR_No = UserDefaults.standard.string(forKey: "CPRNoKey")!//get
        
        print("Get_Email_ID: \(Get_Email_ID)")
        print("Get_CPR_No: \(Get_CPR_No)")
        
        self.VerifyOTPEmail()
    }
    
    
    
    func VerifyOTPEmail()
    {
        let url = ConstantsClass.baseUrl + ConstantsClass.verify_otp_customer_account
        print("url: \(url)")
        
        let parameters = [
            "email_id" : "\(self.Get_Email_ID)",
            "cpr_no" : "\(self.Get_CPR_No)",
            "otp" : "\(self.EmailOTP)",
            "is_guest_user" : "\(self.is_guest_user)"
            ] as [String : Any]
        
        print("parametersVerifySMS: \(parameters)")
        Helper.sharedInstance.showLoader()
        Alamofire.request(url
            , method: .post, parameters: parameters, encoding:JSONEncoding.default).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any]
                {
                    print("response: \(response)")
                    
                    if let status = response["status"] as? Bool
                    {
                        if status
                        {
                            if let responseDict = response["response"]as? [String:Any]
                            {
                        self.Emailauthentication_key = responseDict["authentication_key"]as? String ?? ""
                        print("Emailauthentication_key: \(self.Emailauthentication_key)")
                        
                        UserDefaults.standard.set(self.Emailauthentication_key, forKey: "authentication_key") //set email auhkey
                        let Cust_User_Type = 1 // set customerUserType
                        UserDefaults.standard.set(Cust_User_Type,forKey: UserDataManager_KEY.userType)//set userType here also and customer button tapper also
                       
                                DispatchQueue.main.async
                                    {
                                        Helper.sharedInstance.hideLoader()
                                        self.view.makeToast("Email_ID Verified", duration: 6.0, position: .bottom)
                                        
                                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "EmailVerifyVC") as! EmailverifiedViewController
                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                }
                            }
                            else
                            {
//                                self.view.makeToast("Something went wrong", duration: 3.0, position: .bottom)
                                print("Something went wrong")
                                
                            }
                        }
                        else
                        {
                            Helper.sharedInstance.hideLoader()
                            self.view.makeToast("Confirm OTP failed", duration: 3.0, position: .bottom)
                            
                        }
                    }
                }
        }
        
    }
    
    @objc func txtFieldChanged(_ textField: UITextField) {
        if (TFFirst.text!.count == 0) || (TFSecond.text!.count == 0) || (TFThird.text!.count == 0) || (TFFourth.text!.count == 0)
        {
           
            self.BtnVerify.setTitleColor(COLOR.GrayColor, for: .normal)
            BtnVerify.backgroundColor = COLOR.LightGrayColor
        }
        else
        {
          
            self.BtnVerify.setTitleColor(COLOR.WhiteColor, for: .normal)
            BtnVerify.backgroundColor = COLOR.PinkColor
        }
    }
    
    
    @IBAction func CancleBtnClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ResendBtnClick(_ sender: Any) {
        timer.invalidate()
        seconds = 120
        LblTimer.text = timeString(time: TimeInterval(seconds))
        runTimer()
        
        self.TFFirst.text = ""
        self.TFSecond.text = ""
        self.TFThird.text = ""
        self.TFFourth.text = ""
        let CheckOTPFromChnageCellNo: Bool = false
        UserDefaults.standard.set(CheckOTPFromChnageCellNo,forKey: "isCellNoChange")
        self.ResendOTPEMAIL()// same api for email but different parameter
        
    }
    
    func ResendOTPEMAIL()
       {
          self.UpdatedCellNo = UserDefaults.standard.string(forKey: "CellNoKey")!//get
         self.Get_CPR_Number = UserDefaults.standard.string(forKey: "CPRNoKey")!//get
          let GetEmail = UserDefaults.standard.string(forKey: "EmailKey")!//get
        
           let url = ConstantsClass.baseUrl + ConstantsClass.send_otp_on_email_customer
           print("url: \(url)")
           let parameters = [
               "cell_no" : "\(self.UpdatedCellNo)",
               "cpr_no" : "\(self.Get_CPR_Number)",
               "email_id" : "\(GetEmail)",
               ] as [String : Any]

           print("parametersRESENDOTPforEmail: \(parameters)")
        
        let headers = [
                  "Authorization": "\(self.getSMSAuthenticationKey)"
              ]
         print("headers: \(headers)")
        
           Alamofire.request(url
                      , method: .post, parameters: parameters, encoding:JSONEncoding.default,headers: headers).responseJSON
               {
                   response in

                   if let response = response.result.value as? [String:Any] {
                       print("response: \(response)")

                       if let status = response["status"] as? Bool {
                           print("Get status true here")

                           if status
                           {
//                            DispatchQueue.main.async
//                                {
//                                    Helper.sharedInstance.hideLoader()
//                                    self.view.makeToast("Email_ID Verified", duration: 6.0, position: .bottom)
//
//                                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//                                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "EmailVerifyVC") as! EmailverifiedViewController
//                                    self.navigationController?.pushViewController(nextViewController, animated: true)
//                            }
                           }
                           else
                           {
                            Helper.sharedInstance.hideLoader()
                               self.view.makeToast("Confirm OTP failed", duration: 6.0, position: .bottom)
                           }
                       }
                       else
                       {
                           self.view.makeToast("Something went wrong", duration: 3.0, position: .bottom)
                           print("Something went wrong")
                       }
                   }
                   else
                   {
                       print("Error occured") // serialized json response
                   }
           }
       }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 20))
        doneToolbar.barStyle = .default
        let Cancel: UIBarButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(self.doneButtonAction))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [Cancel,flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.TFFirst.inputAccessoryView = doneToolbar
        self.TFSecond.inputAccessoryView = doneToolbar
        self.TFThird.inputAccessoryView = doneToolbar
        self.TFFourth.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.TFFirst.resignFirstResponder()
        self.TFSecond.resignFirstResponder()
        self.TFThird.resignFirstResponder()
        self.TFFourth.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.TFFirst.resignFirstResponder()
        self.TFSecond.resignFirstResponder()
        self.TFThird.resignFirstResponder()
        self.TFFourth.resignFirstResponder()
        return true
    }
    
 
}

extension EmailOTPViewController: UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.text?.utf8.count == 1 && !string.isEmpty
        {
            return false
        }
        else
        {
            return true
        }
    }
}
