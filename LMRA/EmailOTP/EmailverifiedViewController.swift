//
//  EmailverifiedViewController.swift
//  LMRA
//
//  Created by Mac on 07/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class EmailverifiedViewController: UIViewController {
    
    @IBOutlet weak var BtnContinue: UIButton!
    override func viewDidLoad()
    {
        super.viewDidLoad()
          BtnContinue.layer.cornerRadius = 2;
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
    }
    
    @IBAction func ContinueBtnClick(_ sender: Any) {
        
        UserDefaults.standard.set(false,forKey: "isAddAdress")
        let stroyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let objVc:SetAddressViewController =
            storyboard?.instantiateViewController(withIdentifier: "SetAddressVC") as! SetAddressViewController
        
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(objVc, animated: true)
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case .right:
                print("Swiped right")
            case .down:
                print("Swiped down")
            case .left:
                let stroyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let objVc:SetAddressViewController =
                    storyboard?.instantiateViewController(withIdentifier: "SetAddressVC") as!
                SetAddressViewController
                // objVc.venderId = VendorId
                self.navigationController?.isNavigationBarHidden = true
                self.navigationController?.pushViewController(objVc, animated: true)
                print("Swiped left")
            case .up:
                print("Swiped up")
            default:
                break
            }
        }
    }
}
