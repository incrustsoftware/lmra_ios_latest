//
//  BottomsheetViewController.swift
//  LMRA
//
//  Created by Mac on 05/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Toast_Swift
import FittedSheets
class BottomsheetViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    
    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var authentication_key_val = ""
    var Language_Name = ""
    var Language_Id:Int!
    var languagePriority:String = ""
    var LanguageSelectedID:Int!
    var Language_NameString_Array = [String] ()
    var GetTaskTitle = ""
    var GetTaskDescription = ""
    var GetTaskID = ""
    var LangIDString = ""
    var LangIDString_Array:[String] = []
    var GetLangID_Array:[String] = []
    var GetLangID = ""
    var GetLang = ""
    var BtnSelctedTag:Int!
    let defaults = UserDefaults.standard
    var FilterDataDict = [String:Any]()
    var languageArray:[Language] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "LanguagesTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "langCell")
        self.tableView.reloadData()
        self.authentication_key_val = UserDefaults.standard.string(forKey: "authentication_key")!//get
        get_languages()
    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    func get_languages()
    {
        let urlString = ConstantsClass.baseUrl + ConstantsClass.get_languages
        print("urlString: \(urlString)")
        
        let headers = [
            "Authorization": "\(self.authentication_key_val)"
        ]
        Alamofire.request(urlString, method: .get, parameters: nil, encoding: URLEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if (response["status"] as? Bool) != nil {
                        if let respArray = response["response"] as? [[String:Any]] {
                            print("respArray: \(respArray)")
                            
                            for Data in respArray
                            {
                                //                            self.languagePriority = Data["language_priority"]as? Int
                                //                                if self.languagePriority == 1
                                //                                {
                                self.Language_Name = Data["language_name"]as! String
                                self.Language_Id = Data["language_id"]as? Int
                                self.LangIDString = String( self.Language_Id)
                                self.Language_NameString_Array.append(self.Language_Name)
                                self.LangIDString_Array.append(self.LangIDString)
                                self.languagePriority = Data["language_priority"]as! String
                                if self.languagePriority == "0"
                                {
                                    self.languageArray.append(Language(language_name: self.Language_Name, language_priority: self.languagePriority, language_id: self.Language_Id))
                                }
                                
                                //}
                                
                            }
                            print("Language_NameString_Array: \(self.Language_NameString_Array)")
                            
                        }
                    }
                    // Make sure to update UI in main thread
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
        
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return languageArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:LanguagesTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "langCell", for: indexPath) as! LanguagesTableViewCell
        let temp:Language = languageArray[indexPath.row]
        if temp.language_priority == "0"
        {
            cell.languageNameLanel.text = temp.language_name
            //  let temp:String = LangIDString_Array[indexPath.row]
            cell.checkBoxButton.tag = indexPath.row
            
            cell.checkBoxButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
            
            if self.BtnSelctedTag == indexPath.row
            {
                self.GetLangID = String(temp.language_id) //self.LangIDString_Array[indexPath.row]
                self.GetLang = temp.language_name //self.Language_NameString_Array[indexPath.row]
                print("GetLangID :\(self.GetLangID)")
                cell.checkBoxButton.setImage(UIImage(named: "tasker_checked"), for: .normal)
                if self.GetLangID_Array.contains(self.GetLangID) {
                    if let itemToRemoveIndex = GetLangID_Array.firstIndex(of: self.GetLangID) {
                        GetLangID_Array.remove(at: itemToRemoveIndex)
                        stringsLang.remove(at:itemToRemoveIndex)
                        cell.checkBoxButton.setImage(UIImage(named: "tasker_uncheck"), for: .normal)
                    }
                } else {
                    self.GetLangID_Array.append(self.GetLangID)
                    strings.append(self.GetLangID)
                    UserDefaults.standard.set(strings, forKey: "UD_LANGARRAY")
                    stringsLang.append(self.GetLang)
                    UserDefaults.standard.set(stringsLang, forKey: "UD_LANG")
                    
                }
                
                print("GetLangID_Array :\(self.GetLangID_Array)")
                print("LangStr\(stringsLang)")
            }
            
        }
        return cell
    }
    @objc func buttonPressed(sender:UIButton){
        self.BtnSelctedTag = sender.tag
        print("Selected item in row \(sender.tag)")
        self.tableView.reloadData()
        if sender.isSelected{
            sender.isSelected = false
        }
        else
        {
            sender.isSelected = true
        }
    }
    @available(iOS 12.0, *)
    func updateLanguagesApi()
    {
        
        let api:APIEngine = APIEngine()
        api.addLanguagesAPI(param:["tasker_languages" : (self.GetLangID_Array)])
        { responseObject, error in
            print(Parameters.self)
            switch responseObject?.result  {
            case .success(let JSON)?:
                print("Success with JSON: \(JSON)")
                let jsonResponse = JSON
                
                if(jsonResponse["status"].bool == true) {
                    //   self.navToTimeSlot()
                }
                else
                {
                    Helper.sharedInstance.showAlert(title: "Message", message:jsonResponse["message_code"].stringValue )
                }
            case.failure(let error)?:
                print("error: \(error)")
                
            case .none:
                print("error: ")
            }
        }
    }
    
    @available(iOS 12.0, *)
    @IBAction func continueBtnAction(_ sender: Any) {
        //        let defaults = UserDefaults.standard
        //        defaults.set(self.GetLangID, forKey: UD_LANGARRAY)
        // self.updateLanguagesApi()
        
        self.FilterDataDict["SelectedLangNameArrayValue"] = stringsLang
        self.FilterDataDict["SelectedLangIDArrayValue"] = self.GetLangID_Array
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SelectedFilterData"), object: nil, userInfo: FilterDataDict)
        
        print("FilterDataDict:",self.FilterDataDict)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
}
