//
//  AddRateTableViewCell.swift
//  LMRA
//
//  Created by Mac on 02/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
protocol AddRateTableViewCellDelegate: class {
    func didTapButton(cell: AddRateTableViewCell)
}
class AddRateTableViewCell: UITableViewCell {

    @IBOutlet weak var categoryNameLabel: UILabel!
    
    @IBOutlet weak var editButton: UIButton!
    
    @IBOutlet weak var rateLabel: UILabel!
    
    @IBOutlet weak var rateTypeLabel: UILabel!
    
    
    @IBAction func editButton(_ sender: Any) {
        cellDelegate?.didTapButton(cell: self)
    }
     weak var cellDelegate: AddRateTableViewCellDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
