//
//  ProfileServicesTableViewCell.swift
//  LMRA
//
//  Created by Mac on 06/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class ProfileServicesTableViewCell: UITableViewCell {

    @IBOutlet weak var serviceNameLabel: UILabel!
    
    @IBOutlet weak var rateLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
