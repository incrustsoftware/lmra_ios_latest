//
//  LeftViewController.swift
//  NGU
//
//  Created by Mac on 24/03/17.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class LeftViewController: UITableViewController {
    var titlesArray: NSMutableArray = []
    override func viewDidLoad() {
        super.viewDidLoad()
        titlesArray.add(["title": "Home", "imgName": #imageLiteral(resourceName: "Home"),"viewcontroller":"DashboardVC","color":UIColor.orange])
        titlesArray.add(["title": "Create new stickie", "imgName": #imageLiteral(resourceName: "Create New"),"viewcontroller":"SelectStickieType","color":UIColor.white])
        titlesArray.add(["title": "My Stickies", "imgName": #imageLiteral(resourceName: "stickies"),"viewcontroller":"MyStickiesVC","color":UIColor.darkGray])
        titlesArray.add(["title": "Favourite Stickies", "imgName": #imageLiteral(resourceName: "Favorite"),"viewcontroller":"FavouriteStickiesVC","color":UIColor.red])
        //titlesArray.add(["title": "Shared Stickies", "imgName": #imageLiteral(resourceName: "Shared"),"viewcontroller":"SharedStickiesVC","color":UIColor.brown])
        titlesArray.add(["title": "Restore", "imgName": #imageLiteral(resourceName: "Restore"),"viewcontroller":"Restore","color":UIColor.green])
        titlesArray.add(["title": "No Advertise", "imgName": #imageLiteral(resourceName: "Remove ADS"),"viewcontroller":"No Advertise","color":UIColor.cyan])
        titlesArray.add(["title": "Feedback", "imgName": #imageLiteral(resourceName: "Feedback"),"viewcontroller":"FeedbackVC","color":UIColor.magenta])
        //titlesArray.add(["title": "Print Stickie", "imgName": #imageLiteral(resourceName: "stickies"),"viewcontroller":"MyStickiesVC","color":UIColor.purple])
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .fade
    }
    
    // MARK: - UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titlesArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeftViewCell", for: indexPath) as! LeftViewCell
        
        let cellInfo: NSDictionary = self.titlesArray[indexPath.row] as! NSDictionary
        cell.lblTitle?.text = cellInfo.value(forKey: "title") as! String? ?? ""
        cell.imgCatgory?.image = cellInfo.value(forKey: "imgName") as! UIImage?
        //cell.imgBackView?.backgroundColor = cellInfo.value(forKey: "color") as! UIColor?
        //let bgColorView = UIView()
        
        //let projectUtil = ProjectUtil()
        //let color =  projectUtil.hexStringToUIColor(hex: projectUtil.defaultColor() as String)
        
        //bgColorView.backgroundColor = color//UIColor(red: 255.0, green: 173.5, blue: 0.0, alpha: 1.0)
        //cell.selectedBackgroundView = bgColorView
        
        return cell
    }
    override public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cellIdentifier = "HeaderTVCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! HeaderTVCell
        return cell
        //        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 44))
        //        footerView.backgroundColor = UIColor.gray
        //        self.tableView.footerView(forSection: 1)
        //        return footerView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let cellIdentifier = "HeaderTVCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! HeaderTVCell
        return cell.frame.size.height
    }
    
    // MARK: - UITableViewDelegate
    
    //    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //        return (indexPath.row == 1 || indexPath.row == 3) ? 22.0 : 44.0
    //    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cellInfo: NSDictionary = self.titlesArray[indexPath.row] as! NSDictionary
        let mainViewController = sideMenuController!
        
        let navigationController = sideMenuController?.rootViewController as! UINavigationController
        
        print(navigationController.topViewController ?? "")
        
        let previousclassNAme = NSStringFromClass((navigationController.topViewController?.classForCoder)!)
        
        print(previousclassNAme)
        
        if cellInfo.value(forKey: "viewcontroller") as! String? ?? "" == "Restore" {
        }
        else if cellInfo.value(forKey: "viewcontroller") as! String? ?? "" == "No Advertise" {
        }
        else {
            let viewController: UIViewController! = self.storyboard!.instantiateViewController(withIdentifier: cellInfo.value(forKey: "viewcontroller") as! String? ?? "")
            
            let currentclassName = NSStringFromClass(viewController.classForCoder)
            if currentclassName == previousclassNAme {
                
            }
            else {
                navigationController.pushViewController(viewController, animated: true)
            }
            //        }
        }
        //mainViewController.hideLeftView(animated: true, completionHandler: nil)
        mainViewController.hideRightView(animated: true) { 
            
        }
    }
 }
