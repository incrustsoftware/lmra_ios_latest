//
//  LeftViewCell.swift
//  NGU
//
//  Created by Mac on 24/03/17.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class LeftViewCell: UITableViewCell {

    @IBOutlet weak var imgCatgory: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgBackView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
