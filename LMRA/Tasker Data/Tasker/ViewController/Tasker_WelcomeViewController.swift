//
//  Tasker_WelcomeViewController.swift
//  LMRA
//
//  Created by Mac on 25/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class Tasker_WelcomeViewController: UIViewController {

   
       @IBOutlet weak var MainView: UIView!
       @IBOutlet var ContentView: UIView!
       
       override func viewDidLoad() {
           super.viewDidLoad()
           
           let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
           swipeLeft.direction = .left
           self.MainView.addGestureRecognizer(swipeLeft)
       }
       
       @IBAction func btnContinueClicked(_ sender: Any) {
           let objVc:Tasker_ConfirmDetailsViewController =
               Storyboard().taskerStoryboard?.instantiateViewController(withIdentifier: "Tasker_ConfirmDetailsViewController") as!
           Tasker_ConfirmDetailsViewController
           // objVc.venderId = VendorId
           self.navigationController?.isNavigationBarHidden = true
           self.navigationController?.pushViewController(objVc, animated: true)
       }
       
       @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
           
           if let swipeGesture = gesture as? UISwipeGestureRecognizer {
               
               switch swipeGesture.direction {
               case .right:
                    print("Swiped right")
               case .down:
                   print("Swiped down")
               case .left:
                   let objVc:Tasker_ConfirmDetailsViewController =
                       Storyboard().taskerStoryboard?.instantiateViewController(withIdentifier: "Tasker_ConfirmDetailsViewController") as!
                   Tasker_ConfirmDetailsViewController
                   // objVc.venderId = VendorId
                   self.navigationController?.isNavigationBarHidden = true
                   self.navigationController?.pushViewController(objVc, animated: true)
                   print("Swiped left")
               case .up:
                   print("Swiped up")
               default:
                   break
               }
           }
       }
}
