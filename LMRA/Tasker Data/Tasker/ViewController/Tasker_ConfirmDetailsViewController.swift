//
//  ConfirmYourDetailsVC.swift
//  LMRA
//
//  Created by Mac on 03/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire

class Tasker_ConfirmDetailsViewController: UIViewController ,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate{
    
    @IBOutlet weak var outerImageView: UIImageView!
    @IBOutlet weak var ImgCamera: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var txtFieldName: UITextField!
    @IBOutlet weak var lblIfamilyName: UILabel!
    @IBOutlet weak var txtFieldFamilyname: UITextField!
    @IBOutlet weak var lblNationality: UILabel!
    @IBOutlet weak var txtFieldNationality: UITextField!
    @IBOutlet weak var lblCellNumber: UILabel!
    @IBOutlet weak var txtfieldCellNumber: UITextField!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var txtFieldEmail: UITextField!
    @IBOutlet weak var LblCurrentCellNumber: UILabel!
    @IBOutlet weak var BtnConfirm: UIButton!
    @IBOutlet weak var txtFieldNameHeight: NSLayoutConstraint!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var txtfieldFamilyNameHeight: NSLayoutConstraint!
    @IBOutlet weak var txtFieldNtionalityHeight: NSLayoutConstraint!
    @IBOutlet weak var txtFiledCellNumberHeight: NSLayoutConstraint!
    @IBOutlet weak var txtFieldEmailHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    
    //Variables
    var imageString = ""
    var Name = ""
    var FamilyName = ""
    var Nationality = ""
    var EmailID = ""
    var Get_FirstCell_Number = ""
    var Get_UpdatedCellNo = ""
    var CurrentCellNumber = ""
    var Get_CPR_Number = ""
    let defaults = UserDefaults.standard
    var BtnCofirmColor = UIColor(red:243, green:243, blue:241, alpha:1.0)
    let picker = UIImagePickerController()
    var imagePicker = UIImagePickerController()
    var maxLenName:Int = 20
    var maxLenFamilyName:Int = 20
    var MaxLenNationality:Int = 20
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        imagePicker.delegate = self as! UIImagePickerControllerDelegate & UINavigationControllerDelegate
        outerImageView.layer.cornerRadius = outerImageView.frame.height / 2
        ImgCamera.layer.cornerRadius = ImgCamera.frame.height / 2
        
        self.txtFieldName.delegate = self
        self.txtFieldFamilyname.delegate = self
        self.txtFieldNationality.delegate = self
        self.txtFieldEmail.delegate = self
        let nameTap = UITapGestureRecognizer(target:self,action:#selector(self.NameTapped))
        
        // add it to the label
        lblName.addGestureRecognizer(nameTap)
        lblName.isUserInteractionEnabled = true
        let familyNameTap = UITapGestureRecognizer(target:self,action:#selector(self.familyNameTapped))
        
        // add it to the label
        lblIfamilyName.addGestureRecognizer(familyNameTap)
        lblIfamilyName.isUserInteractionEnabled = true
        let nationalityTap = UITapGestureRecognizer(target:self,action:#selector(self.NationalityTapped))
        
        // add it to the label
        lblNationality.addGestureRecognizer(nationalityTap)
        lblNationality.isUserInteractionEnabled = true
        
        let emailTap = UITapGestureRecognizer(target:self,action:#selector(self.emailTapped))
        
        // add it to the label
        lblEmail.addGestureRecognizer(emailTap)
        lblEmail.isUserInteractionEnabled = true
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        scrollView.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        addDoneButtonOnKeyboard()
        
        self.Get_CPR_Number = UserDefaults.standard.string(forKey: "CPRNoKey")!//get
        let isCalledFromChangedNumber = UserDefaults.standard.bool(forKey: "isCellNoChange")// this
        
        if(isCalledFromChangedNumber == true)
        {
            self.Get_UpdatedCellNo = UserDefaults.standard.string(forKey: "ChnageCellNo")!//get
            print("UpdatedCellNo: \(self.Get_UpdatedCellNo)")
            self.CurrentCellNumber = self.Get_UpdatedCellNo
        }
        else
        {
            self.Get_FirstCell_Number = UserDefaults.standard.string(forKey: "cellNoKey")!//get
            print("UpdatedCellNo: \(self.Get_FirstCell_Number)")
            self.CurrentCellNumber = self.Get_FirstCell_Number
        }
        
        print("CurrentCellNumber: \(self.CurrentCellNumber)")
        UserDefaults.standard.set( self.CurrentCellNumber, forKey: "CurrentCellNoKey")
        
        self.LblCurrentCellNumber.text = CurrentCellNumber
    }
    
    //method to hide keyboard
    @objc func dismissKeyboard() {
        scrollView.contentOffset.y = 0
        // checkForValidation()
        view.endEditing(true)
    }
    
    @objc func NameTapped()
    {
        txtFieldNameHeight.constant = 30
        txtFieldName.becomeFirstResponder()
    }
    
    @objc func familyNameTapped()
    {
        txtfieldFamilyNameHeight.constant = 30
        txtFieldFamilyname.becomeFirstResponder()
    }
    
    @objc func NationalityTapped()
    {
        txtFieldNtionalityHeight.constant = 30
        txtFieldNationality.becomeFirstResponder()
    }
    
    @objc func emailTapped()
    {
        txtFieldEmailHeight.constant = 30
        txtFieldEmail.becomeFirstResponder()
    }
    
    @IBAction func btnContinueClicked(_ sender: Any) {
        self.validation()
        let nextViewController = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "Tasker_EmailOTPViewController") as! Tasker_EmailOTPViewController
         self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    func validation()
    {
        if (txtFieldName.text == "")
        {
            BtnConfirm.backgroundColor = colorWithHexString(hexString: "#8E9AA0")
            self.view.makeToast("Please Enter Name")
        }
        else if (txtFieldFamilyname.text == "")
        {
            BtnConfirm.backgroundColor = colorWithHexString(hexString: "#8E9AA0")
            self.view.makeToast("Please Enter Family Name")
        }
        else if (txtFieldNationality.text == "")
        {
            BtnConfirm.backgroundColor = colorWithHexString(hexString: "#8E9AA0")
            self.view.makeToast("Please Enter Nationality")
        }
        else if (isValidEmail(testStr: txtFieldEmail.text ?? "") == false)
        {
            BtnConfirm.backgroundColor = colorWithHexString(hexString: "#8E9AA0")
            self.view.makeToast("Please enter valid email address")
        }
        else
        {
            self.Name =  txtFieldName.text!
            self.FamilyName = txtFieldFamilyname.text!
            self.Nationality =  txtFieldNationality.text!
            self.EmailID = txtFieldEmail.text!
            
            defaults.set(self.Name, forKey: "NameKey")//set
            defaults.set(self.FamilyName, forKey: "FamilyKey")//set
            defaults.set(self.Nationality, forKey: "NtionalityKey")//set
            defaults.set(self.EmailID, forKey: "EmailKey")//set
            
            print("Name: \(self.Name)")
            print("FamilyName: \(self.FamilyName)")
            print("Nationality: \(self.Nationality)")
            print("EmailID: \(self.EmailID)")
            
            self.confirm_personal_details()
            
            if (txtFieldName.text != "" ) && (txtFieldFamilyname.text != "" ) || (txtFieldNationality.text != "" ) || (txtFieldEmail.text != "")
            {
                BtnConfirm.isEnabled = true
                self.BtnConfirm.setTitleColor(BtnCofirmColor, for: .normal)
                BtnConfirm.backgroundColor = colorWithHexString(hexString: "#ff375e")
            }
        }
    }
    
    func confirm_personal_details()
    {
        let url = ConstantsClass.baseUrl + ConstantsClass.confirm_personal_details_tasker
        print("url: \(url)")
        let parameters = [
            "name" : "\(self.Name)",
            "family_name" : "\(self.FamilyName)",
            "nationality" : "\(self.Nationality)",
            "email_id" : "\(self.EmailID)",
            "cell_no" : "\(self.CurrentCellNumber)",
            "cpr_no" : "\(self.Get_CPR_Number)",
            "bitmap_image":""
            ] as [String : Any]
        
        print("parametersPersonaldetails: \(parameters)")
        
        Alamofire.request(url
            , method: .post, parameters: parameters, encoding:JSONEncoding.default).responseJSON
            {
                response in
                
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if let status = response["status"] as? Bool {
                        
                        if status{
                            print("send_otp_success")
                            DispatchQueue.main.async {
                                let storyBoard : UIStoryboard = UIStoryboard(name: "Tasker", bundle:nil)
                                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "Tasker_EmailOTPViewController") as! Tasker_EmailOTPViewController
                                
                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                self.sendOtpEmail() 
                            }
                        }
                    }
                    else
                    {
                        self.view.makeToast("Something went wrong", duration: 3.0, position: .bottom)
                        print("Something went wrong")
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
    }
    func sendOtpEmail()  {
        let url = ConstantsClass.baseUrl + ConstantsClass.send_otp_onemail_tasker
        print("url: \(url)")
        let parameters = [
            
            "email_id" : "\(self.EmailID)",
            "cell_no" : "\(self.CurrentCellNumber)",
            "cpr_no" : "\(self.Get_CPR_Number)",
            
            ] as [String : Any]
        
        print("parametersPersonaldetails: \(parameters)")
        
        Alamofire.request(url
            , method: .post, parameters: parameters, encoding:JSONEncoding.default).responseJSON
            {
                response in
                
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if let status = response["status"] as? Bool {
                        
                        if status{
                            print("send_otp_success")
//                            DispatchQueue.main.async {
//                                let storyBoard : UIStoryboard = UIStoryboard(name: "Tasker", bundle:nil)
//                                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "Tasker_EmailOTPViewController") as! Tasker_EmailOTPViewController
//
//                                self.navigationController?.pushViewController(nextViewController, animated: true)
//                            }
                        }
                    }
                    else
                    {
                        self.view.makeToast("Something went wrong", duration: 3.0, position: .bottom)
                        print("Something went wrong")
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
    }
    
    @IBAction func btnBackClicked(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func SelectImageBtnTapped(_ sender: Any) {
        print("camera button click")
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as! UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    //What to do when the picker returns with a photo
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        self.imgUser.isHidden = true
        let chosenImage = info[UIImagePickerController.InfoKey.originalImage]
            as? UIImage //2
        outerImageView.contentMode = .scaleAspectFit //3
        outerImageView.image = chosenImage //4
        dismiss(animated: true, completion: nil) //5
        
        let scaledImage = UIImage.scaleImageWithDivisor(img: chosenImage!, divisor: 3)
        print("scaledImage:",scaledImage)
        
        let imageData:NSData = scaledImage.pngData()! as NSData
        self.imageString = imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
    }
    
    //What to do if the image picker cancels.
    private func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func colorWithHexString(hexString: String, alpha:CGFloat = 1.0) -> UIColor
    {
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    
    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 20))
        doneToolbar.barStyle = .default
        let Cancel: UIBarButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(self.doneButtonAction))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [Cancel,flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.txtFieldName.inputAccessoryView = doneToolbar
        self.txtFieldFamilyname.inputAccessoryView = doneToolbar
        self.txtFieldNationality.inputAccessoryView = doneToolbar
        self.txtFieldEmail.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.txtFieldName.resignFirstResponder()
        self.txtFieldFamilyname.resignFirstResponder()
        self.txtFieldNationality.resignFirstResponder()
        self.txtFieldEmail.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == txtFieldName){
            let currentText = textField.text! + string
            return currentText.count <= maxLenName
        }
        
        if(textField == txtFieldFamilyname){
            let currentText = textField.text! + string
            return currentText.count <= maxLenFamilyName
        }
        
        if(textField == txtFieldNationality){
            let currentText = textField.text! + string
            return currentText.count <= MaxLenNationality
        }
        
        return true;
    }
    
    @objc func keyboardWillShow(notification: NSNotification)
    {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                //                        self.view.frame.origin.y -= keyboardSize.height
                self.view.frame.origin.y -= 150
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification)
    {
        if self.view.frame.origin.y != 0 {
            //                    self.view.frame.origin.y = 0
            self.view.frame.origin.y += 150
        }
    }
    
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func validate(value: String) -> Bool
    {
        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
}


