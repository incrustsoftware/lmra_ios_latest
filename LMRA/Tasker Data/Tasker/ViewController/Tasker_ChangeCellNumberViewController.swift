//
//  Tasker_ChangeCellNumberViewController.swift
//  LMRA
//
//  Created by Mac on 25/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire
class Tasker_ChangeCellNumberViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var scrollview: UIScrollView!
       @IBOutlet weak var btnContinue: UIButton!
       @IBOutlet weak var TFCellNumber: UITextField!
       @IBOutlet weak var LblCellNo: UILabel!
       @IBOutlet weak var CellNumHt: NSLayoutConstraint!
       
       var Get_CPR_Number = ""
       var cell_number = ""
       var maxLenCellNo:Int = 12
       var cellNochangeOTP:Int = 0
       var BtnContinueColor = UIColor(red:243, green:243, blue:241, alpha:1.0)
       let defaults = UserDefaults.standard
       
       override func viewDidLoad() {
           super.viewDidLoad()
           
           self.btnContinue.isEnabled = true
           self.TFCellNumber.delegate = self
           let CellNoTapped = UITapGestureRecognizer(target:self,action:#selector(self.CellNoTapped))
           
           // add it to the label
           LblCellNo.addGestureRecognizer(CellNoTapped)
           LblCellNo.isUserInteractionEnabled = true
           
        if #available(iOS 12.0, *) {
            TFCellNumber.addTarget(self, action: #selector(ChnagePhnNoViewController.textFieldDidChange(_:)),
                                   for: .editingChanged)
        } else {
            // Fallback on earlier versions
        }
           
           NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
           NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
           
           addDoneButtonOnKeyboard()
       }
       
       func checkValidation()  {
           if (TFCellNumber.text! == "" ) || (TFCellNumber.text!.count != 12 ){
               btnContinue.backgroundColor = colorWithHexString(hexString: "#8E9AA0")
               self.view.makeToast("Please Enter valid 12 Digit Mobile no")
           }
           else
           {
               self.cell_number =  TFCellNumber.text!
               self.Get_CPR_Number = UserDefaults.standard.string(forKey: "CPRNoKey")!//get
               
               if (TFCellNumber.text!.count == 12 )
               {
                if #available(iOS 12.0, *) {
                    self.Cell_chnage_SentOTPSMS()
                } else {
                    // Fallback on earlier versions
                }
               }
           }
       }
       
       func Cell_chnage_SentOTPSMS()
       {
           let url = ConstantsClass.baseUrl + ConstantsClass.change_cell_phone_number_tasker
           print("url: \(url)")
           let parameters = [
               "cell_no" : "\(self.cell_number)",
               "cpr_no" : "\(self.Get_CPR_Number)"
               ] as [String : Any]
           
           print("parameters: \(parameters)")
           
           Alamofire.request(url
               , method: .post, parameters: parameters, encoding:JSONEncoding.default).responseJSON
               {
                   response in
                   
                   if let response = response.result.value as? [String:Any] {
                       print("response: \(response)")
                       
                       if let status = response["status"] as? Bool {
                           print("Get OTP by SMS")
                           
                           self.cellNochangeOTP = response["otp"]as! Int
                           print("cellNochangeOTP: \(self.cellNochangeOTP)")
                                                   
                           if status{
                               let CheckOTPFromChnageCellNo: Bool = true
                               UserDefaults.standard.set(CheckOTPFromChnageCellNo,forKey: "isCellNoChange")
                               UserDefaults.standard.set(self.cell_number, forKey: "ChnageCellNo")
                               UserDefaults.standard.set(self.cellNochangeOTP, forKey: "cellNochangeOTPKey")// set
                               DispatchQueue.main.async {
                                   let nextViewController = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "Tasker_VerifyOTPViewController") as! Tasker_VerifyOTPViewController
                                   
                                   self.navigationController?.pushViewController(nextViewController, animated: true)
                               }
                           }
                       }
                       else
                       {
                           self.view.makeToast("Something went wrong", duration: 3.0, position: .bottom)
                           print("Something went wrong")
                       }
                   }
                   else
                   {
                       print("Error occured") // serialized json response
                   }
           }
       }
       
       @objc func textFieldDidChange(_ textField: UITextField) {
           if (TFCellNumber.text!.count == 12 )
           {
               self.btnContinue.setTitleColor(BtnContinueColor, for: .normal)
               btnContinue.backgroundColor = colorWithHexString(hexString: "#00C48C")
           }
           else
           {
               self.btnContinue.setTitleColor(BtnContinueColor, for: .normal)
               btnContinue.backgroundColor = colorWithHexString(hexString: "#8E9AA0")
           }
       }
       
       func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
           
           if(textField == TFCellNumber){
               let currentText = textField.text! + string
               return currentText.count <= maxLenCellNo
           }
           return true;
       }
       
       @IBAction func Backbtnclick(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
       }
       
       @objc func CellNoTapped()
       {
           CellNumHt.constant = 40
           TFCellNumber.becomeFirstResponder()
       }
       
       @IBAction func ContinueBtnClick(_ sender: Any) {
           checkValidation()
       }
       
       func addDoneButtonOnKeyboard()
       {
           let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 20))
           doneToolbar.barStyle = .default
           let Cancel: UIBarButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(self.doneButtonAction))
           let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
           let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
           
           let items = [Cancel,flexSpace, done]
           doneToolbar.items = items
           doneToolbar.sizeToFit()
           
           self.TFCellNumber.inputAccessoryView = doneToolbar
       }
       
       @objc func doneButtonAction()
       {
           self.TFCellNumber.resignFirstResponder()
       }
       
       func colorWithHexString(hexString: String, alpha:CGFloat = 1.0) -> UIColor {
           // Convert hex string to an integer
           let hexint = Int(self.intFromHexString(hexStr: hexString))
           let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
           let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
           let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
           
           // Create color object, specifying alpha as well
           let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
           return color
       }
       
       func intFromHexString(hexStr: String) -> UInt32 {
           var hexInt: UInt32 = 0
           // Create scanner
           let scanner: Scanner = Scanner(string: hexStr)
           // Tell scanner to skip the # character
           scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
           // Scan hex value
           scanner.scanHexInt32(&hexInt)
           return hexInt
       }
       
       @objc func keyboardWillShow(notification: NSNotification) {
           if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
               if self.view.frame.origin.y == 0 {
                   //                        self.view.frame.origin.y -= keyboardSize.height
                   self.view.frame.origin.y -= 150
               }
           }
       }
       
       @objc func keyboardWillHide(notification: NSNotification) {
           if self.view.frame.origin.y != 0 {
               //                    self.view.frame.origin.y = 0
               self.view.frame.origin.y += 150
           }
       }}
