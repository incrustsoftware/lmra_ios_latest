//
//  Category.swift
//  LMRA
//
//  Created by Mac on 21/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
class Category:NSObject
{
    var id:Int = 0
    var category:String =  ""
    var category_image:String = ""
    
    init(id:Int,category:String,category_image:String) {
        self.id = id
        self.category = category
        self.category_image = category_image
    }

}
