//
//  TimeSlot.swift
//  LMRA
//
//  Created by Mac on 04/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
class TimeSlot: NSObject {
    var timeslot_name:String = ""
    var timeslot_id:Int = 0
    var timeslot_daytime:String = ""
    
    init(timeslot_name:String,timeslot_id:Int,timeslot_daytime:String) {
        self.timeslot_name = timeslot_name
        self.timeslot_id = timeslot_id
        self.timeslot_daytime = timeslot_daytime
    }
}
