//
//  TaskList.swift
//  LMRA
//
//  Created by Mac on 08/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
class TaskList: NSObject {
    var task_id:Int =  0
     var task_title:String = ""
     var task_date:String = ""
    var  task_description:String = ""
     var customer_id:Int = 0
     var customer_name:String = ""
     var customer_family_name:String = ""
     var customer_nationality:String = ""
     var customer_cell_phone_number:String = ""
    var customer_profile:String = ""
     var customer_identity_card_number:String = ""
     var customer_hire_count: Int = 0
    var task_invitation_status_id:String =  ""
     var task_invitation_status: String = ""
      var tasker_offline_status:String = ""
      var task_update_time: String = ""
      var tasker_name:String = ""
    var tasker_profile:String = ""
    
    init(task_id:Int,
        task_title:String,
      task_date:String,
     task_description:String,
     customer_id:Int,
     customer_name:String,
    customer_family_name:String ,
     customer_nationality:String,
     customer_cell_phone_number:String,
     customer_profile:String,
      customer_identity_card_number:String ,
      customer_hire_count: Int,
     task_invitation_status_id:String,
      task_invitation_status: String,
       tasker_offline_status:String,
     task_update_time: String,
       tasker_name:String ,
     tasker_profile:String) {
        self.customer_cell_phone_number = customer_cell_phone_number
        self.customer_family_name = customer_family_name
        self.customer_hire_count = customer_hire_count
        self.customer_id = customer_id
        self.customer_identity_card_number = customer_identity_card_number
        self.customer_name = customer_name
        self.customer_nationality = customer_nationality
        self.customer_profile = customer_profile
        self.task_id = task_id
        self.task_date = task_date
        self.task_title = task_title
        self.tasker_name = tasker_name
        self.tasker_profile = tasker_profile
        self.task_description = task_description
        self.task_update_time = task_update_time
        self.tasker_offline_status = tasker_offline_status
        self.task_invitation_status_id = task_invitation_status_id
        self.task_invitation_status = task_invitation_status
        
        
        
        
        
        
        
        
    }
}
