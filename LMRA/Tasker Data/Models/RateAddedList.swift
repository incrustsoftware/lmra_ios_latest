//
//  RateAddedList.swift
//  LMRA
//
//  Created by Mac on 02/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
class RateAddedList: NSObject {
    var rate_id:String = ""
    var is_fixed:String = ""
    var rate:String = ""
    var subcategory_name:String = ""
          
    init(rate_id:String,is_fixed:String,rate:String,
            subcategory_name:String
            ) {
               
               self.rate_id = rate_id
               self.is_fixed = is_fixed
               self.rate = rate
               self.subcategory_name = subcategory_name
          }
}
