//
//  AddRate.swift
//  LMRA
//
//  Created by Mac on 24/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
class AddRate: NSObject {
     var rate_id:String = ""
       var is_fixed:String = ""
       var rate:String = ""
       var subcategory_name:String = ""
        var subcategory_id:String = ""
        var dic:[String:Any] = [:]
    
       init(rate_id:String,is_fixed:String,rate:String,
            subcategory_name:String,subcategory_id:String,dic:[String:Any]
               ) {
                  
                  self.rate_id = rate_id
                  self.is_fixed = is_fixed
                  self.rate = rate
                  self.subcategory_name = subcategory_name
                  self.subcategory_id = subcategory_id
                  self.dic = dic
             }
}
