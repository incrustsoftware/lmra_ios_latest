//
//  ProfileDetails.swift
//  LMRA
//
//  Created by Mac on 07/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
class ProfileDetails: NSObject {
   var tasker_id:String = ""
   var tasker_name:String = ""
   var tasker_cell_phone_number:String = ""
    var tasker_identity_card_number:String = ""
    var tasker_email_id:String = ""
    var tasker_address:[String] = []
    var tasker_languages:[String] = []
    var tasker_rates:[String] = []
    var tasker_working_days:[String] = []
    var tasker_working_time:[String] = []
             
    init(tasker_id:String,tasker_name:String,tasker_cell_phone_number:String,tasker_identity_card_number:String,tasker_email_id:String,tasker_address:[String],tasker_languages:[String],tasker_rates:[String],tasker_working_days:[String],tasker_working_time:[String])
       {
                  
        self.tasker_id = tasker_id
        self.tasker_name = tasker_name
        self.tasker_cell_phone_number = tasker_cell_phone_number
        self.tasker_identity_card_number = tasker_identity_card_number
        self.tasker_email_id = tasker_email_id
        self.tasker_address = tasker_address
        self.tasker_languages = tasker_languages
        self.tasker_rates = tasker_rates
        self.tasker_working_time = tasker_working_time
        self.tasker_working_days = tasker_working_days
       }
}
