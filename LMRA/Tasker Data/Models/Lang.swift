//
//  Lang.swift
//  LMRA
//
//  Created by Mac on 07/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
class Languages: NSObject {
    var language_id:String = ""
    var language_name:String = ""
     init(language_id:String,language_name:String) {
        self.language_id = language_id
        self.language_name = language_name
    }
}
