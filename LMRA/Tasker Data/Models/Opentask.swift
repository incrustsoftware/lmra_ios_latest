//
//  Opentask.swift
//  LMRA
//
//  Created by Mac on 13/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
class OpenTask: NSObject {
    var customer_cell_phone_number:String = ""
    var customer_email_id:String = ""
    var customer_id:String = ""
    var customer_identity_card_number:String = ""
    var customer_name:String = ""
    var customer_user_image:String = ""
    var task_date:String = ""
    var task_description:String = ""
    var task_id:String = ""
    var task_title:String = ""
     init(customer_cell_phone_number:String,customer_email_id:String,customer_id:String,customer_identity_card_number:String,customer_name:String,customer_user_image:String,task_date:String,task_description:String,task_id:String,task_title:String ) {
        self.customer_cell_phone_number = customer_cell_phone_number
        self.customer_email_id = customer_email_id
        self.customer_id = customer_id
        self.customer_identity_card_number = customer_identity_card_number
        self.customer_name = customer_name
        self.customer_user_image = customer_user_image
        self.task_date = task_date
        self.task_description = task_description
        self.task_title = task_title
        self.task_id = task_id
    
    
    
    }
}
