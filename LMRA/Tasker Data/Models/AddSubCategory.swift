//
//  AddSubCategory.swift
//  LMRA
//
//  Created by Mac on 29/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
class AddSubCategory: NSObject {
   
     var subcategory_id:String = ""
     var subcategory_name:String = ""
    
      init(subcategory_id:String,
      subcategory_name:String
      ) {
         
         
         self.subcategory_id = subcategory_id
         self.subcategory_name = subcategory_name
    }
}
