//
//  Rates.swift
//  LMRA
//
//  Created by Mac on 07/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
class Rates: NSObject {
    
    var rate_id:String = ""
    var subcategory_name:String = ""
    var rate:String = ""
    var is_fixed:String = ""
    init(rate_id:String,subcategory_name:String,rate:String ,is_fixed:String) {
        self.is_fixed = is_fixed
        self.rate = rate
        self.rate_id = rate_id
        self.subcategory_name = subcategory_name
    }
}
