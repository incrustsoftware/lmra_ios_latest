//
//  MainCateogory.swift
//  LMRA
//
//  Created by Mac on 28/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
class MainCategory: NSObject {
     var id:Int = 0
     var user_type:String = ""
     var category:String = ""
    
      init(id:Int,user_type:String,
      category:String
      ) {
         
         self.id = id
         self.user_type = user_type
         self.category = category
    }
}
