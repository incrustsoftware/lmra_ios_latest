//
//  Language.swift
//  LMRA
//
//  Created by Mac on 01/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
struct Language {
    var language_name:String = ""
    var language_priority:String = ""
    var language_id:Int = 0
    var isSelected:Bool = false
    
    init(language_name:String,language_priority:String,language_id:Int) {
        self.language_id = language_id
        self.language_priority = language_priority
        self.language_name = language_name
    }
}
