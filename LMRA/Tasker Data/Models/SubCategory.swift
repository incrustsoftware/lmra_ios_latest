//
//  SubCategory.swift
//  LMRA
//
//  Created by Mac on 29/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
class SubCategory: NSObject {
        var category_id:Int = 0
        var subcategory_id:Int = 0
        var subcategory_name:String = ""
        var subcategory_image:String = ""
    
         init(category_id:Int,subcategory_id:Int,
         subcategory_name:String,subcategory_image:String
         ) {
            
            self.category_id = category_id
            self.subcategory_id = subcategory_id
            self.subcategory_name = subcategory_name
            self.subcategory_image = subcategory_image
       }
}
