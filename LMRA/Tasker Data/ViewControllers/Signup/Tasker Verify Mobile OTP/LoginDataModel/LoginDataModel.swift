//
//  LoginDataModel.swift
//  LMRA
//
//  Created by Mac on 14/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
//["status": 1, "response": {
//    "cell_phone_number" = 97396969696;
//    "email_id" = "vaibhavbhosale.4391@gmail.com";
//    "family_name" = bhosale;
//    "identity_card_number" = 969696969;
//    name = vaibhav;
//    nationality = "Indian ";
//    otp = 3157;
//    token = "Bearer e5571ff01a95a96171f1e16b5048eebe62d4fbe0";
//    "user_type" = 2;
//}, "code": 200, "message_code": sign_in_success]

import Foundation

struct Login_DataModel:Codable {
    var status:Bool!
    var response:Login_Response!
    var code:Int!
    var message_code:String!
}

struct Login_Response:Codable {
    var cell_phone_number:String!
    var email_id:String!
    var family_name:String!
    var identity_card_number:String!
    var name:String!
    var nationality:String!
    var otp:Int!
    var token:String!
    var user_type:Int!
}
