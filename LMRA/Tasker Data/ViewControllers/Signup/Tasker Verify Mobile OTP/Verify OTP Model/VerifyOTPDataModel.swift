//
//  VerifyOTPDataModel.swift
//  LMRA
//
//  Created by Mac on 14/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
//(lldb) po print(response.result.value)
//Optional({
//    code = 200;
//    "message_code" = "confirm_otp_success";
//    response =     {
//        "authentication_key" = "Bearer bbdd7541be333cc1192d3b3717adeb22edfb4ffb";
//        "cell_phone_number" = 97396969696;
//        "email_id" = "vaibhavbhosale.4391@gmail.com";
//        "identity_card_number" = 969696969;
//        name = vaibhav;
//        otp = 3707;
//    };
//    status = 1;
//})
struct Verify_OTP_DataModel:Codable {
    var status:Bool!
    var response:Verify_OTP_ResponseModel!
    var code:Int!
    var message_code:String!
}

struct Verify_OTP_ResponseModel:Codable {
    var authentication_key:String!
    var cell_phone_number:String!
    var email_id:String!
    var identity_card_number:String!
    var name:String!
    var nationality:String!
    var otp:String!
}
