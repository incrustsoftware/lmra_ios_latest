//
//  OTPViewController.swift
//  LMRA
//
//  Created by Mac on 02/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire

class Tasker_VerifyOTPViewController: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var LblTimer: UILabel!
    @IBOutlet weak var BtnVerify: UIButton!
    @IBOutlet weak var BtnResend: UIButton!
    @IBOutlet weak var TFFirst: UITextField!
    @IBOutlet weak var TFSecond: UITextField!
    @IBOutlet weak var TFThird: UITextField!
    @IBOutlet weak var TFFourth: UITextField!
    
    var otp:String!
    
    var BtnVerifyColor = UIColor(red:243, green:243, blue:241, alpha:1.0)
    var Get_Cell_Number = ""
    var Get_CPR_Number = ""
    var Get_SMS_OTPString = ""
    var Get_ChnageCellNoSMS_OTP:Int = 0
    var Get_SMS_ResendOTPString = ""
    var RESEND_SMS_OTP:Int = 0
    var UpdatedCellNo = ""
    var authentication_key = ""
    var seconds = 120 //This variable will hold a starting value of seconds. It could be any amount above 0.
    var timer = Timer()
    var isTimerRunning = false //This will be used to make sure only one timer is created at a time.
    var firstval:Int = 0
    var secondval:Int = 0
    var thirdval:Int = 0
    var Fourthdval:Int = 0
    var Get_LoginSMS_OTP:Int!
    var StringGet_LoginSMS_OTP = ""
    var StringGet_SignuSMS_OTP = ""
    var isfromLogin:Bool = false
    var isfromChangeNo:Bool = false
    var isfromResendNo:Bool = false
    var BtnResendOnColor = UIColor(red:255, green:255, blue:94, alpha:1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        runTimer()//otp timer function
        self.navigationController?.navigationBar.isHidden = true
        self.Get_Cell_Number = UserDefaults.standard.string(forKey: "cellNoKey")!//get
        self.Get_CPR_Number = UserDefaults.standard.string(forKey: "CPRNoKey")!//get
        
        self.Get_LoginSMS_OTP =  UserDefaults.standard.integer(forKey: "LoginOTPKey")
        self.StringGet_LoginSMS_OTP = String(self.Get_LoginSMS_OTP)
        
        if otp != nil {
            self.StringGet_SignuSMS_OTP = otp
        }
        
        self.Get_ChnageCellNoSMS_OTP = UserDefaults.standard.integer(forKey: "cellNochangeOTPKey")
        print("Get_Cell_Number: \(self.Get_Cell_Number)")
        
        let isCalledFromChangedNumber = UserDefaults.standard.bool(forKey: "isCellNoChange")// this is how you retrieve the bool value
        
        if (isfromChangeNo == true) {
            self.Get_SMS_OTPString = String(Get_ChnageCellNoSMS_OTP)
            self.TFFirst.text = ""
            self.TFSecond.text = ""
            self.TFThird.text = ""
            self.TFFourth.text = ""
            self.Get_SMS_OTPString = String(Get_ChnageCellNoSMS_OTP)
        }
        else {
            if isfromLogin == true {
                self.Get_SMS_OTPString = String(Get_LoginSMS_OTP)
            }
            else {
                self.Get_SMS_OTPString = String(otp)
            }
        }
        print("Get_SMS_OTPString: \(self.Get_SMS_OTPString)")
        
        let digits = Get_SMS_OTPString.compactMap{ $0.wholeNumberValue }
        self.firstval = digits[0]
        self.secondval = digits[1]
        self.thirdval = digits[2]
        self.Fourthdval = digits[3]
        
        TFFirst.text = String(self.firstval)
        TFSecond.text = String(self.secondval)
        TFThird.text = String(self.thirdval)
        TFFourth.text = String(self.Fourthdval)
        //TFFirst.textContentType = .oneTimeCode
        //TFSecond.textContentType = .oneTimeCode
        //TFThird.textContentType = .oneTimeCode
        //TFFourth.textContentType = .oneTimeCode
        
        //addDoneButtonOnKeyboard()
        
        self.TFFirst.delegate = self
        self.TFSecond.delegate = self
        self.TFThird.delegate = self
        self.TFFourth.delegate = self
        
        self.TFFirst.addTarget(self, action: #selector (self.changeCharacter), for: .editingChanged)
        self.TFSecond.addTarget(self, action: #selector (self.changeCharacter), for: .editingChanged)
        self.TFThird.addTarget(self, action: #selector (self.changeCharacter), for: .editingChanged)
        self.TFFourth.addTarget(self, action: #selector (self.changeCharacter), for: .editingChanged)
        self.BtnVerify.setTitleColor(BtnVerifyColor, for: .normal)
        BtnVerify.backgroundColor = colorWithHexString(hexString: "#00C48C")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setStatusBarColor(color: .white, styleForStatus: .default)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    func setUI(text:String) {
        let font = UIFont(name: "STCForward-Bold", size: 12.0)!
        let font2 = UIFont(name: "STCForward-Bold", size: 12.0)!
        
        let attributes: [NSAttributedString.Key: Any] = [
            .font: font,
            .foregroundColor: UIColor.init(hexStringColor: "#C0C6C8")
        ]
        let attributes1: [NSAttributedString.Key: Any] = [
            .font: font2,
            .foregroundColor: UIColor.init(hexStringColor: "#1D252D")
        ]
        
        
        let attributedQuote = NSAttributedString(string: "YOU CAN RESEND CODE AFTER", attributes: attributes)
        let attributedQuote1 = NSAttributedString(string:" \(text)", attributes: attributes1)
        let label = NSMutableAttributedString()
        label.append(attributedQuote)
        label.append(attributedQuote1)
        self.LblTimer.attributedText = label
    }
    
    @objc func changeCharacter(textField: UITextField) {
        if textField.text!.utf8.count == 1 {
            switch textField {
            case TFFirst:
                TFSecond.becomeFirstResponder()
            case TFSecond:
                TFThird.becomeFirstResponder()
            case TFThird:
                TFFourth.becomeFirstResponder()
            case TFFourth:
                print("OTP send to server")// here send otp to server
            default:
                break
            }
        }
        else if textField.text!.isEmpty {
            switch textField {
            case TFFourth:
                TFThird.becomeFirstResponder()
            case TFThird:
                TFSecond.becomeFirstResponder()
            case TFSecond:
                TFFirst.becomeFirstResponder()
            default:
                break
            }
        }
    }
    
    func runTimer() {
        self.BtnResend.isEnabled = false
        BtnResend.setTitleColor(.gray, for: .normal)
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: #selector(OTPViewController.updateTimer), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        seconds -= 1  //This will decrement(count down)the seconds.
        //   LblTimer.text = timeString(time: TimeInterval(seconds))
        setUI(text: timeString(time: TimeInterval(seconds)))
        if seconds ==  0
        {
            BtnResend.isEnabled = true
            self.BtnResend.setTitleColor(BtnResendOnColor, for: .normal)
            BtnResend.setTitleColor(colorWithHexString(hexString: "#00C48C"), for: UIControl.State.normal)
            timer.invalidate()
        }
    }
    
    func timeString(time:TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i", minutes, seconds)
    }
    
    @IBAction func btncloseClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnVerifyOtpClicked(_ sender: Any) {
        if isfromResendNo == true {
            self.verifyOTPSMS(otp: self.Get_SMS_OTPString)
        }
        else if isfromChangeNo == true {
            self.verifyOTPSMS(otp: String(self.Get_ChnageCellNoSMS_OTP))
        }
        else if isfromLogin == true {
            self.verifyOTPSMS(otp: self.StringGet_LoginSMS_OTP)
        }
        else {
            self.verifyOTPSMS(otp: self.StringGet_SignuSMS_OTP)
        }
    }
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 20))
        doneToolbar.barStyle = .default
        let Cancel: UIBarButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(self.doneButtonAction))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        let items = [Cancel,flexSpace, done]
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.TFFirst.inputAccessoryView = doneToolbar
        self.TFSecond.inputAccessoryView = doneToolbar
        self.TFThird.inputAccessoryView = doneToolbar
        self.TFFourth.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction() {
        self.TFFirst.resignFirstResponder()
        self.TFSecond.resignFirstResponder()
        self.TFThird.resignFirstResponder()
        self.TFFourth.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.TFFirst.resignFirstResponder()
        self.TFSecond.resignFirstResponder()
        self.TFThird.resignFirstResponder()
        self.TFFourth.resignFirstResponder()
        
        return true
    }
    
    @IBAction func ResendBtnClick(_ sender: Any) {
        timer.invalidate()
        seconds = 120
        LblTimer.text = timeString(time: TimeInterval(seconds))
        runTimer()
        
        self.TFFirst.text = ""
        self.TFSecond.text = ""
        self.TFThird.text = ""
        self.TFFourth.text = ""
        let CheckOTPFromChnageCellNo: Bool = false
        UserDefaults.standard.set(CheckOTPFromChnageCellNo,forKey: "isCellNoChange")
        
        self.resendOTPSMS()
    }
    
    @IBAction func ChangeNumClick(_ sender: Any) {
        let nextViewController = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "Tasker_ChangeCellNumberViewController") as! Tasker_ChangeCellNumberViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    func colorWithHexString(hexString: String, alpha:CGFloat = 1.0) -> UIColor {
        
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    
    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        
        return hexInt
    }
}

extension Tasker_VerifyOTPViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.text?.utf8.count == 1 && !string.isEmpty {
            return false
        }
        else {
            return true
        }
    }
}

//MARK:- ViewController API Call's
extension Tasker_VerifyOTPViewController {
    func verifyOTPSMS(otp:String) {
        //let url = ConstantsClass.baseUrl + ConstantsClass.verify_otp_tasker_mobile_account
        //print("url: \(url)")
        
        let isCalledFromChangedNumber = UserDefaults.standard.bool(forKey: "isCellNoChange")// this
        
        if(isCalledFromChangedNumber == true) {
            self.UpdatedCellNo = UserDefaults.standard.string(forKey: "ChnageCellNo")!//get
            self.Get_Cell_Number = self.UpdatedCellNo
            print("Get_Cell_Number: \(self.Get_Cell_Number)")
        }
        
        let parameters = [
            "cell_no" : "\(self.Get_Cell_Number)",
            "cpr_no" : "\(self.Get_CPR_Number)",
            "otp" : otp
            ] as [String : Any]
        
        print("parametersVerifySMS: \(parameters)")
        
        APICallManager.shared.verifyOtpTaskerMobileAccountAPI(subUrl: ConstantsClass.verify_otp_tasker_mobile_account, parameter: parameters, isLoadingIndicatorShow: true,isfromLogin: self.isfromLogin) { (response) in
            DispatchQueue.main.async {
                if response.status {
                    self.view.makeToast("Phone No Verified", duration: 5.0, position: .bottom)
                    if self.isfromLogin == true {
                        let rootViewController = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "TaskerDasboardVC")
                        let navigationController = UINavigationController(rootViewController: rootViewController)
                        let rightViewController = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "SideMenuViewController")
                        let sideMenuController = LGSideMenuController(rootViewController: navigationController,
                                                                      leftViewController: nil,
                                                                      rightViewController: rightViewController)
                        sideMenuController.leftViewWidth = 0
                        sideMenuController.leftViewPresentationStyle = .slideBelow;
                        
                        sideMenuController.rightViewWidth = (UIApplication.shared.keyWindow?.bounds.size.width ?? 320) - 20
                        sideMenuController.leftViewPresentationStyle = .slideBelow
                        navigationController.isNavigationBarHidden = true
                        navigationController.interactivePopGestureRecognizer?.delegate = self
                        self.view.window?.rootViewController = sideMenuController
                        self.view.window?.makeKeyAndVisible()
                    }
                    else {
                        let nextViewController = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "Tasker_WelcomeViewController") as! Tasker_WelcomeViewController
                        self.navigationController?.pushViewController(nextViewController, animated: true)
                    }
                    self.view.makeToast(response.message_code, duration: 5.0, position: .bottom)
                }
                else {
                    self.view.makeToast(response.message_code, duration: 5.0, position: .bottom)
                }
            }
        }
        
        
        //        Alamofire.request(url,method: .post, parameters: parameters, encoding:JSONEncoding.default).responseJSON { response in
        //
        //            if let data = response.data {
        //                do {
        //                    let responseData = try JSONDecoder().decode(Verify_OTP_DataModel.self, from: data)
        //
        //                    if responseData.status {
        //                        UserDataManager.shared.setUserDetails(userDetails: data)
        //                        self.view.makeToast("Phone No Verified", duration: 5.0, position: .bottom)
        //
        //                        if self.isfromLogin == true {
        //                            let rootViewController = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "TaskerDasboardVC")
        //                            let navigationController = UINavigationController(rootViewController: rootViewController)
        //                            let rightViewController = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "SideMenuViewController")
        //                            let sideMenuController = LGSideMenuController(rootViewController: navigationController,
        //                                                                          leftViewController: nil,
        //                                                                          rightViewController: rightViewController)
        //                            sideMenuController.leftViewWidth = 0
        //                            sideMenuController.leftViewPresentationStyle = .slideBelow;
        //
        //                            sideMenuController.rightViewWidth = (UIApplication.shared.keyWindow?.bounds.size.width ?? 320) - 20
        //                            sideMenuController.leftViewPresentationStyle = .slideBelow
        //                            navigationController.isNavigationBarHidden = true
        //                            navigationController.interactivePopGestureRecognizer?.delegate = self
        //                            self.view.window?.rootViewController = sideMenuController
        //                            self.view.window?.makeKeyAndVisible()
        //                        }
        //                        else {
        //                            let storyBoard : UIStoryboard = UIStoryboard(name: "Tasker", bundle:nil)
        //                            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "Tasker_WelcomeViewController") as! Tasker_WelcomeViewController
        //                            self.navigationController?.pushViewController(nextViewController, animated: true)
        //                        }
        //                    }
        //                    else {
        //                        self.view.makeToast(responseData.message_code, duration: 5.0, position: .bottom)
        //                    }
        //                }
        //                catch {
        //                    print(error)
        //                    //self.view.makeToast("something went's wrong please try again", duration: 5.0, position: .bottom)
        //                }
        //            }
        //        }
    }
    
    func resendOTPSMS() {
        //let url = ConstantsClass.baseUrl + ConstantsClass.resend_otp_to_create_tasker_account
        //print("url: \(url)")
        let parameters = [
            "cell_no" : "\(self.Get_Cell_Number)",
            "cpr_no" : "\(self.Get_CPR_Number)"
            ] as [String : Any]
        
        print("parametersRESENDOTP: \(parameters)")
        
        APICallManager.shared.sendOTPApi(subUrl: ConstantsClass.send_otp_to_create_tasker_account, parameter: parameters, isLoadingIndicatorShow: true) { (response) in
            DispatchQueue.main.async {
                if let responseDict = response["response"]as? [String:Any]
                {
                    let otp = responseDict["otp"]as! Int
                    print("SMS_OTP: \(otp)")
                    
                    if let status = response["status"] as? Bool {
                        if status{
                            self.RESEND_SMS_OTP = response["response"]as! Int
                            print("RESEND_SMS_OTP: \(self.RESEND_SMS_OTP)")
                            
                            self.Get_SMS_OTPString = String(self.RESEND_SMS_OTP)
                            print("Get_SMS_OTPString: \(self.Get_SMS_OTPString)")
                            self.isfromResendNo = true
                            let digits = self.Get_SMS_OTPString.compactMap{ $0.wholeNumberValue }
                            self.firstval = digits[0]
                            self.secondval = digits[1]
                            self.thirdval = digits[2]
                            self.Fourthdval = digits[3]
                            self.TFFirst.text = String(self.firstval)
                            self.TFSecond.text = String(self.secondval)
                            self.TFThird.text = String(self.thirdval)
                            self.TFFourth.text = String(self.Fourthdval)
                        }
                        else {
                            self.view.makeToast("Something went wrong", duration: 3.0, position: .bottom)
                            print("Something went wrong")
                        }
                    }
                    else {
                        print("Error occured") // serialized json response
                    }
                }
            }
        }
        
        //        Alamofire.request(url
        //            , method: .post, parameters: parameters, encoding:JSONEncoding.default).responseJSON {
        //                response in
        //
        //                if let response = response.result.value as? [String:Any] {
        //                    print("response: \(response)")
        //
        //                    if let status = response["status"] as? Bool {
        //                        print("Get status true here")
        //
        //                        if status {
        //                            self.RESEND_SMS_OTP = response["response"]as! Int
        //                            print("RESEND_SMS_OTP: \(self.RESEND_SMS_OTP)")
        //
        //                            self.Get_SMS_OTPString = String(self.RESEND_SMS_OTP)
        //                            print("Get_SMS_OTPString: \(self.Get_SMS_OTPString)")
        //                            self.isfromResendNo = true
        //                            let digits = self.Get_SMS_OTPString.compactMap{ $0.wholeNumberValue }
        //                            self.firstval = digits[0]
        //                            self.secondval = digits[1]
        //                            self.thirdval = digits[2]
        //                            self.Fourthdval = digits[3]
        //                            self.TFFirst.text = String(self.firstval)
        //                            self.TFSecond.text = String(self.secondval)
        //                            self.TFThird.text = String(self.thirdval)
        //                            self.TFFourth.text = String(self.Fourthdval)
        //                        }
        //                        else {
        //                            self.view.makeToast("Invalid OTP", duration: 6.0, position: .bottom)
        //                        }
        //                    }
        //                    else {
        //                        self.view.makeToast("Something went wrong", duration: 3.0, position: .bottom)
        //                        print("Something went wrong")
        //                    }
        //                }
        //                else {
        //                    print("Error occured") // serialized json response
        //                }
        //        }
    }
}
