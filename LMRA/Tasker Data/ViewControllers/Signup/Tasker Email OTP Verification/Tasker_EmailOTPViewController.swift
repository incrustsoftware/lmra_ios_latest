//
//  Tasker_EmailOTPViewController.swift
//  LMRA
//
//  Created by Mac on 25/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire

class Tasker_EmailOTPViewController: UIViewController {
    
    @IBOutlet weak var Mainview: UIView!
    @IBOutlet weak var LblTimer: UILabel!
    @IBOutlet weak var BtnVerify: UIButton!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var TFFirst: UITextField!
    @IBOutlet weak var TFSecond: UITextField!
    @IBOutlet weak var TFThird: UITextField!
    @IBOutlet weak var TFFourth: UITextField!
    @IBOutlet weak var BtnResend: UIButton!
    var authentication_key = ""
    var Get_Email_ID = ""
    var Get_CPR_No = ""
    var EmailOTP = ""
    var Firstval = ""
    var Secondval = ""
    var Thirdval = ""
    var FourthVal = ""
    var BtnResendOnColor = UIColor(red:255, green:255, blue:94, alpha:1.0)
    var BtnVerifyColor = UIColor(red:243, green:243, blue:241, alpha:1.0)
    var seconds = 120 //This variable will hold a starting value of seconds. It could be any amount above 0.
    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        runTimer()//otp timer function
        //addDoneButtonOnKeyboard()
        self.TFFirst.delegate = self
        self.TFSecond.delegate = self
        self.TFThird.delegate = self
        self.TFFourth.delegate = self
        self.TFFirst.addTarget(self, action: #selector (self.changeCharacter), for: .editingChanged)
        self.TFSecond.addTarget(self, action: #selector (self.changeCharacter), for: .editingChanged)
        self.TFThird.addTarget(self, action: #selector (self.changeCharacter), for: .editingChanged)
        self.TFFourth.addTarget(self, action: #selector (self.changeCharacter), for: .editingChanged)
        
        self.BtnVerify.setTitleColor(BtnVerifyColor, for: .normal)
        BtnVerify.backgroundColor = colorWithHexString(hexString: "#00C48C")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setStatusBarColor(color: .white, styleForStatus: .default)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    //MARK:- ViewController IBAction's
    @IBAction func VerifyBtnClick(_ sender: Any) {
        self.Firstval = self.TFFirst.text!
        self.Secondval = self.TFSecond.text!
        self.Thirdval = self.TFThird.text!
        self.FourthVal = self.TFFourth.text!
        
        print("firstval: \(Firstval)")
        print("Secondval: \(Secondval)")
        print("Thirdval: \(Thirdval)")
        print("FourthVal: \(FourthVal)")
        
        self.EmailOTP = (Firstval) + (Secondval) + (Thirdval) + (FourthVal)
        print("EmailOTP: \(self.EmailOTP)")
        
        self.Get_Email_ID = UserDefaults.standard.string(forKey: "EmailKey")!//get
        self.Get_CPR_No = UserDefaults.standard.string(forKey: "CPRNoKey")!//get
        
        print("Get_Email_ID: \(Get_Email_ID)")
        print("Get_CPR_No: \(Get_CPR_No)")
        
        self.VerifyOTPEmail()
    }
    
    @IBAction func CancleBtnClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ResendBtnClick(_ sender: Any) {
        timer.invalidate()
        seconds = 120
        LblTimer.text = timeString(time: TimeInterval(seconds))
        runTimer()
        
        self.TFFirst.text = ""
        self.TFSecond.text = ""
        self.TFThird.text = ""
        self.TFFourth.text = ""
        let CheckOTPFromChnageCellNo: Bool = false
        UserDefaults.standard.set(CheckOTPFromChnageCellNo,forKey: "isCellNoChange")
        sendOtpEmail()
    }
    
    //MARK:- ViewController Own Defined methods
    @objc func changeCharacter(textField: UITextField) {
        if textField.text!.utf8.count == 1 {
            switch textField {
            case TFFirst:
                TFSecond.becomeFirstResponder()
            case TFSecond:
                TFThird.becomeFirstResponder()
            case TFThird:
                TFFourth.becomeFirstResponder()
            case TFFourth:
                print("OTP send to server")// here send otp to server
            default:
                break
            }
        }
        else if textField.text!.isEmpty{
            switch textField {
            case TFFourth:
                TFThird.becomeFirstResponder()
            case TFThird:
                TFSecond.becomeFirstResponder()
            case TFSecond:
                TFFirst.becomeFirstResponder()
            default:
                break
            }
        }
    }
    
    func setUI(text:String) {
        let font = UIFont(name: "STCForward-Bold", size: 12.0)!
        let font2 = UIFont(name: "STCForward-Bold", size: 12.0)!
        //        let paragraphStyle = NSMutableParagraphStyle()
        //        paragraphStyle.alignment = .left
        //        paragraphStyle.firstLineHeadIndent = 5.0
        
        let attributes: [NSAttributedString.Key: Any] = [
            .font: font,
            .foregroundColor: UIColor.init(hexStringColor: "#C0C6C8")
        ]
        let attributes1: [NSAttributedString.Key: Any] = [
            .font: font2,
            .foregroundColor: UIColor.init(hexStringColor: "#1D252D")
        ]
        
        
        let attributedQuote = NSAttributedString(string: "YOU CAN RESEND CODE AFTER", attributes: attributes)
        let attributedQuote1 = NSAttributedString(string:" \(text)", attributes: attributes1)
        let label = NSMutableAttributedString()
        label.append(attributedQuote)
        label.append(attributedQuote1)
        self.LblTimer.attributedText = label
    }
    
    func runTimer() {
        self.BtnResend.isEnabled = false
        BtnResend.setTitleColor(.gray, for: .normal)
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: #selector(Tasker_EmailOTPViewController.updateTimer), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        seconds -= 1  //This will decrement(count down)the seconds.
        setUI(text: timeString(time: TimeInterval(seconds)))
        // LblTimer.text = timeString(time: TimeInterval(seconds))
        if seconds ==  0 {
            BtnResend.isEnabled = true
            self.BtnResend.setTitleColor(BtnResendOnColor, for: .normal)
            BtnResend.setTitleColor(UIColor.init(named: "colorTaskerGreen"), for: UIControl.State.normal)
            timer.invalidate()
        }
    }
    
    func timeString(time:TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i", minutes, seconds)
    }
    
    func colorWithHexString(hexString: String, alpha:CGFloat = 1.0) -> UIColor {
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    
    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 20))
        doneToolbar.barStyle = .default
        let Cancel: UIBarButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(self.doneButtonAction))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [Cancel,flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.TFFirst.inputAccessoryView = doneToolbar
        self.TFSecond.inputAccessoryView = doneToolbar
        self.TFThird.inputAccessoryView = doneToolbar
        self.TFFourth.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction() {
        self.TFFirst.resignFirstResponder()
        self.TFSecond.resignFirstResponder()
        self.TFThird.resignFirstResponder()
        self.TFFourth.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.TFFirst.resignFirstResponder()
        self.TFSecond.resignFirstResponder()
        self.TFThird.resignFirstResponder()
        self.TFFourth.resignFirstResponder()
        return true
    }
}

extension Tasker_EmailOTPViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text?.utf8.count == 1 && !string.isEmpty {
            return false
        }
        else {
            return true
        }
    }
}

//MARK:- ViewController API Call's
extension Tasker_EmailOTPViewController {
    func sendOtpEmail()  {
        guard let cellNo = UserDefaults.standard.value(forKey: "CurrentCellNoKey") else {
            return
        }
        guard let email = UserDefaults.standard.value(forKey: "EmailKey") else {
            return
        }
        guard let cprNo = UserDefaults.standard.value(forKey: "CPRNoKey")else {
            return
        }
        //let url = ConstantsClass.baseUrl + ConstantsClass.send_otp_onemail_tasker
        //print("url: \(url)")
        let parameters = [
            "email_id" : email,
            "cell_no" : cellNo,
            "cpr_no" : cprNo,
            ] as [String : Any]
        
        print("parametersPersonaldetails: \(parameters)")
        
        APICallManager.shared.sendOtpOnemailTaskerApi(subUrl: ConstantsClass.send_otp_onemail_tasker, parameter: parameters, isLoadingIndicatorShow: true) { (response) in
            DispatchQueue.main.async {
                if let status = response["status"] as? Bool {
                    if status {
                        print("send_otp_success")
                    }
                }
                else {
                    self.view.makeToast("Something went wrong", duration: 3.0, position: .bottom)
                    print("Something went wrong")
                }
            }
        }
        
        //        Alamofire.request(url
        //            , method: .post, parameters: parameters, encoding:JSONEncoding.default).responseJSON { response in
        //
        //                if let response = response.result.value as? [String:Any] {
        //                    print("response: \(response)")
        //
        //                    if let status = response["status"] as? Bool {
        //                        if status {
        //                            print("send_otp_success")
        //                        }
        //                    }
        //                    else {
        //                        self.view.makeToast("Something went wrong", duration: 3.0, position: .bottom)
        //                        print("Something went wrong")
        //                    }
        //                }
        //                else {
        //                    print("Error occured") // serialized json response
        //                }
        //        }
    }
    
    func VerifyOTPEmail() {
        let url = ConstantsClass.baseUrl + ConstantsClass.verify_otp_tasker_account
        print("url: \(url)")
        
        let parameters = [
            "email_id" : "\(self.Get_Email_ID)",
            "cpr_no" : "\(self.Get_CPR_No)",
            "otp" : "\(self.EmailOTP)"
            ] as [String : Any]
        
        print("parametersVerifySMS: \(parameters)")
        APICallManager.shared.verifyOtpTaskerAccountApi(subUrl: ConstantsClass.verify_otp_tasker_account, parameter: parameters, isLoadingIndicatorShow: true) { (response) in
            DispatchQueue.main.async {
                if response.status {
                    UserDataManager.shared.setTaskerOrCustomer(userType: UserDataManager.UserType.TASKER)
                    self.view.makeToast("Email_ID Verified", duration: 6.0, position: .bottom)
                    let nextViewController = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "Tasker_EmailVerifyViewController") as! Tasker_EmailVerifyViewController
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                }
                else {
                    self.view.makeToast("Invalid OTP", duration: 6.0, position: .bottom)
                }
            }
        }
    }
    
    //        Alamofire.request(url
    //            , method: .post, parameters: parameters, encoding:JSONEncoding.default).responseJSON {
    //                response in
    //                if let data = response.data {
    //                    do {
    //                        let responseData = try JSONDecoder().decode(Verify_OTP_DataModel.self, from: data)
    //
    //                        if responseData.status {
    //                            UserDataManager.shared.setUserDetails(userDetails: data)
    //                            if let response = response.result.value as? [String:Any] {
    //                                print("response: \(response)")
    //
    //                                if let status = response["status"] as? Bool {
    //                                    print("Email  Verified  here")
    //
    //                                    //     var dic = [String:Any]()
    //                                    if let dic = (response["response"]as?[String:Any]) {
    //                                        print("Dictionary\(dic)")
    //                                        if let key = dic["authentication_key"] {
    //                                            self.authentication_key = key as! String
    //                                            print("authentication_key: \(self.authentication_key)")
    //                                            UserDefaults.standard.set(self.authentication_key, forKey: "authentication_key")
    //                                        }
    //                                    }
    //                                    else {
    //                                        print("empty dictionary.")
    //                                    }
    //
    //                                    if status {
    //                                        DispatchQueue.main.async {
    //                                            self.view.makeToast("Email_ID Verified", duration: 6.0, position: .bottom)
    //
    //                                            let storyBoard : UIStoryboard = UIStoryboard(name: "Tasker", bundle:nil)
    //                                            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "Tasker_EmailVerifyViewController") as! Tasker_EmailVerifyViewController
    //                                            self.navigationController?.pushViewController(nextViewController, animated: true)
    //                                        }
    //                                    }
    //                                    else {
    //                                        self.view.makeToast("Invalid OTP", duration: 6.0, position: .bottom)
    //                                    }
    //                                }
    //                                else {
    //                                    self.view.makeToast("Something went wrong", duration: 3.0, position: .bottom)
    //                                    print("Something went wrong")
    //                                }
    //                            }
    //                            else {
    //                                print("Error occured") // serialized json response
    //                            }
    //
    //                        }
    //                    }
    //                    catch {
    //                        print(error)
    //                        //self.view.makeToast("something went's wrong please try again", duration: 5.0, position: .bottom)
    //                    }
    //                }
    //        }
}
