//
//  Tasker_EmailVerifyViewController.swift
//  LMRA
//
//  Created by Mac on 26/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class Tasker_EmailVerifyViewController: UIViewController {

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //           let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        //           swipeLeft.direction = .left
        //           self.view.addGestureRecognizer(swipeLeft)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setStatusBarColor(color: UIColor.init(named: "colorTaskerGreen")!, styleForStatus: .lightContent)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    @IBAction func ContinueBtnClick(_ sender: Any) {
        let objVc:Tasker_SelectCategoryViewController =
            Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "Tasker_SelectCategoryViewController") as! Tasker_SelectCategoryViewController
        self.navigationController?.pushViewController(objVc, animated: true)
    }

    //    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
    //
    //           if let swipeGesture = gesture as? UISwipeGestureRecognizer {
    //
    //               switch swipeGesture.direction {
    //               case .right:
    //                   print("Swiped right")
    //               case .down:
    //                   print("Swiped down")
    //               case .left:
    //                   let stroyboard:UIStoryboard = UIStoryboard(name: "Tasker", bundle: nil)
    //                   let objVc =
    //                       stroyboard.instantiateViewController(withIdentifier: "Tasker_SelectCategoryViewController") as!
    //                   Tasker_SelectCategoryViewController
    //                   // objVc.venderId = VendorId
    //                   self.navigationController?.isNavigationBarHidden = true
    //                   self.navigationController?.pushViewController(objVc, animated: true)
    //                   print("Swiped left")
    //               case .up:
    //                   print("Swiped up")
    //               default:
    //                   break
    //               }
    //           }
    //       }
}
