//
//  Tasker_ChangeCellNumberViewController.swift
//  LMRA
//
//  Created by Mac on 25/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire

class Tasker_ChangeCellNumberViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var TFCellNumber: UITextField!
    @IBOutlet weak var LblCellNo: UILabel!
    @IBOutlet weak var CellNumHt: NSLayoutConstraint!
    
    var Get_CPR_Number = ""
    var cell_number = ""
    var maxLenCellNo:Int = 11
    var cellNochangeOTP:Int = 0
    var BtnContinueColor = UIColor(red:243, green:243, blue:241, alpha:1.0)
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnContinue.isEnabled = true
        self.TFCellNumber.delegate = self
        let CellNoTapped = UITapGestureRecognizer(target:self,action:#selector(self.CellNoTapped))
        //
        //           // add it to the label
        //           LblCellNo.addGestureRecognizer(CellNoTapped)
        //           LblCellNo.isUserInteractionEnabled = true
        //
        //        if #available(iOS 12.0, *) {
        //            TFCellNumber.addTarget(self, action: #selector(ChnagePhnNoViewController.textFieldDidChange(_:)),
        //                                   for: .editingChanged)
        //        } else {
        //            // Fallback on earlier versions
        //        }
        //
        //           NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        //           NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        //
        //           addDoneButtonOnKeyboard()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setStatusBarColor(color: .white, styleForStatus: .default)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    @IBAction func cellNoDidBegin(_ sender: Any) {
        //        self.btnContinue.setTitleColor(BtnContinueColor, for: .normal)
        //        btnContinue.backgroundColor = colorWithHexString(hexString: "#00C48C")
        
    }
    @IBAction func cellNoDidEnd(_ sender: Any) {
        //checkValidation()
        if (TFCellNumber.text!.count > 0 )
        {
            btnContinue.isEnabled = true
            self.btnContinue.setTitleColor(BtnContinueColor, for: .normal)
            btnContinue.backgroundColor = colorWithHexString(hexString: "#00C48C")
        }
        else
        {
            btnContinue.backgroundColor = colorWithHexString(hexString: "#F1F2F3")
            btnContinue.setTitleColor(colorWithHexString(hexString: "#8E9AA0"), for: .normal)
        }
    }
    
    func checkValidation()  {
        if (TFCellNumber.text!.isBlank) || (TFCellNumber.text!.count != 11 ){
            //   btnContinue.backgroundColor = colorWithHexString(hexString: "#F1F2F3")
            self.view.makeToast("Please Enter valid 11 Digit Mobile no")
        }
        else if (!TFCellNumber.text!.hasPrefix("973")) {
            self.view.makeToast("Phone number should starts with 973")
        }
            
        else {
            self.cell_number =  TFCellNumber.text!
            self.Get_CPR_Number = UserDefaults.standard.string(forKey: "CPRNoKey")!//get
            
            if (TFCellNumber.text!.count == 11 ) {
                btnContinue.isEnabled = true
                self.btnContinue.setTitleColor(BtnContinueColor, for: .normal)
                btnContinue.backgroundColor = colorWithHexString(hexString: "#00C48C")
                self.Cell_chnage_SentOTPSMS()
            }
        }
    }
    
    //       @objc func textFieldDidChange(_ textField: UITextField) {
    //           if (TFCellNumber.text!.count == 11 )
    //           {
    //               self.btnContinue.setTitleColor(BtnContinueColor, for: .normal)
    //               btnContinue.backgroundColor = colorWithHexString(hexString: "#00C48C")
    //           }
    //           else
    //           {
    //               self.btnContinue.setTitleColor(BtnContinueColor, for: .normal)
    //               btnContinue.backgroundColor = colorWithHexString(hexString: "#8E9AA0")
    //           }
    //       }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == TFCellNumber){
            let currentText = textField.text! + string
            return currentText.count <= maxLenCellNo
        }
        return true;
    }
    
    @IBAction func Backbtnclick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func CellNoTapped()
    {
        CellNumHt.constant = 40
        TFCellNumber.becomeFirstResponder()
    }
    
    @IBAction func ContinueBtnClick(_ sender: Any) {
        checkValidation()
    }
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 20))
        doneToolbar.barStyle = .default
        let Cancel: UIBarButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(self.doneButtonAction))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [Cancel,flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.TFCellNumber.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction() {
        self.TFCellNumber.resignFirstResponder()
    }
    
    func colorWithHexString(hexString: String, alpha:CGFloat = 1.0) -> UIColor {
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    
    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                //                        self.view.frame.origin.y -= keyboardSize.height
                self.view.frame.origin.y -= 150
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            //                    self.view.frame.origin.y = 0
            self.view.frame.origin.y += 150
        }
    }}

//MARK:- ViewController API'calls
extension Tasker_ChangeCellNumberViewController {
    func Cell_chnage_SentOTPSMS() {
        guard let currentNo = UserDefaults.standard.string(forKey: "cellNoKey")else
        {
            return
        }
        let url = ConstantsClass.baseUrl + ConstantsClass.change_cell_phone_number_tasker
        print("url: \(url)")
        let parameters = [
            "cell_no" : "\(self.cell_number)",
            "cpr_no" : "\(self.Get_CPR_Number)",       "current_cell_no":currentNo
            ] as [String : Any]
        
        print("parameters: \(parameters)")
        
        APICallManager.shared.changeCellPhoneNumberTaskerAPi(subUrl: ConstantsClass.send_otp_onemail_tasker, parameter: parameters, isLoadingIndicatorShow: true) { (response) in
            DispatchQueue.main.async {
                print("response: \(response)")
                
                if let status = response["status"] as? Bool {
                    print("Get OTP by SMS")
                    
                    self.cellNochangeOTP = response["otp"]as! Int
                    print("cellNochangeOTP: \(self.cellNochangeOTP)")
                    
                    if status{
                        let CheckOTPFromChnageCellNo: Bool = true
                        UserDefaults.standard.set(CheckOTPFromChnageCellNo,forKey: "isCellNoChange")
                        UserDefaults.standard.set(self.cell_number, forKey: "ChnageCellNo")
                        UserDefaults.standard.set(self.cellNochangeOTP, forKey: "cellNochangeOTPKey")// set
                        DispatchQueue.main.async {
                            let nextViewController = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "Tasker_VerifyOTPViewController")
                                
                                as! Tasker_VerifyOTPViewController
                            nextViewController.isfromChangeNo = true
                            self.navigationController?.pushViewController(nextViewController, animated: true)
                        }
                    }
                }
                else
                {
                    self.view.makeToast("Something went wrong", duration: 3.0, position: .bottom)
                    print("Something went wrong")
                }
                
            }
        }
        
        //        Alamofire.request(url
        //            , method: .post, parameters: parameters, encoding:JSONEncoding.default).responseJSON
        //            {
        //                response in
        //
        //                if let response = response.result.value as? [String:Any] {
        //                    print("response: \(response)")
        //
        //                    if let status = response["status"] as? Bool {
        //                        print("Get OTP by SMS")
        //
        //                        self.cellNochangeOTP = response["otp"]as! Int
        //                        print("cellNochangeOTP: \(self.cellNochangeOTP)")
        //
        //                        if status{
        //                            let CheckOTPFromChnageCellNo: Bool = true
        //                            UserDefaults.standard.set(CheckOTPFromChnageCellNo,forKey: "isCellNoChange")
        //                            UserDefaults.standard.set(self.cell_number, forKey: "ChnageCellNo")
        //                            UserDefaults.standard.set(self.cellNochangeOTP, forKey: "cellNochangeOTPKey")// set
        //                            DispatchQueue.main.async {
        //
        //                                let storyBoard : UIStoryboard = UIStoryboard(name: "Tasker", bundle:nil)
        //                                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "Tasker_VerifyOTPViewController")
        //
        //                                    as! Tasker_VerifyOTPViewController
        //                                nextViewController.isfromChangeNo = true
        //                                self.navigationController?.pushViewController(nextViewController, animated: true)
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        self.view.makeToast("Something went wrong", duration: 3.0, position: .bottom)
        //                        print("Something went wrong")
        //                    }
        //                }
        //                else
        //                {
        //                    print("Error occured") // serialized json response
        //                }
        //        }
    }
}
