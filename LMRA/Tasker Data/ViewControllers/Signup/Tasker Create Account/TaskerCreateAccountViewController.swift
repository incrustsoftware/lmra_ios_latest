//
//  TaskerCreateAccountViewController.swift
//  LMRA
//
//  Created by Mac on 25/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import UIKit
import Foundation
import Toast_Swift
import Alamofire
import TextFieldEffects

class TaskerCreateAccountViewController: UIViewController ,UIGestureRecognizerDelegate,UITextFieldDelegate{
    
    @IBOutlet weak var txtfieldCellNumber: UITextField!
    @IBOutlet weak var txtFieldidCard: UITextField!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var BtnCheckbox: UIButton!
    
    var iconClick = false
    var maxLenCellNo:Int = 11
    var maxLenCPR:Int = 9
    
    var Cell_Number = ""
    var CPR_Number = ""
    var GetRelayID:Int!
    var StringGetRelayID = ""
    //var SMS_OTP:Int = 0
    var User_type:Int!
    var User_TypeString = ""
    var GetUserDeviceType = ""
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtfieldCellNumber.delegate = self
        self.txtFieldidCard.delegate = self
        
        txtfieldCellNumber.addTarget(self, action: #selector(TaskerCreateAccountViewController.txtFieldChangedForCellNo(_:)),
                                     for: .editingChanged)
        txtFieldidCard.addTarget(self, action: #selector(TaskerCreateAccountViewController.txtFieldChangedForCPRNo(_:)),
                                 for: .editingChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setStatusBarColor(color: .white, styleForStatus: .default)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    //MARK:- ViewController IBAction's
    @IBAction func OnCheckboxClick(_ sender: AnyObject) {
        if(iconClick == true) {
            BtnCheckbox.setImage(UIImage(named: "Uncheck"), for: .normal)
            self.continueBtn(isUnable: false)
            iconClick = !iconClick
//            self.btnContinue.setTitleColor(COLOR.GrayColor, for: .normal)
//            btnContinue.backgroundColor = COLOR.LightGrayColor
//            btnContinue.isUserInteractionEnabled = false
        }
        else {
            BtnCheckbox.setImage(UIImage(named: "tasker_checked"), for: .normal)
            if (txtfieldCellNumber.text!.count > 0) && (txtFieldidCard.text!.count > 0) {
                self.continueBtn(isUnable: true)
            }
            else {
                self.continueBtn(isUnable: false)
            }
            iconClick = !iconClick
//            btnContinue.isUserInteractionEnabled = true
//            BtnCheckbox.setImage(UIImage(named: "tasker_checked"), for: .normal)
//            if (txtfieldCellNumber.text!.count > 0) && (txtFieldidCard.text!.count > 0) {
//                self.btnContinue.setTitleColor(COLOR.WhiteColor, for: .normal)
//                btnContinue.backgroundColor = Helper.sharedInstance.hexStringToUIColor(hex: "#00C48C")
//            }
//            else {
//                self.btnContinue.setTitleColor(COLOR.WhiteColor, for: .normal)
//                btnContinue.backgroundColor = Helper.sharedInstance.hexStringToUIColor(hex: "#00C48C")
//            }
        }
        
    }
    
    @IBAction func btnBackClicked(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnContinueClicked(_ sender: Any) {
        let CheckOTPFromChnageCellNo: Bool = false
        UserDefaults.standard.set(CheckOTPFromChnageCellNo,forKey: "isCellNoChange")
        checkValidation()
    }
    
    func checkValidation()  {
        if (!txtfieldCellNumber.text!.hasPrefix("973")) {
            self.view.makeToast("Cell phone number should start with 973")
        }
        else if (txtfieldCellNumber.text!.isBlank) || (txtfieldCellNumber.text!.count < 11 ){
            self.view.makeToast("Please enter valid 11 digit cell phone number")
        }
        else if (txtFieldidCard.text!.isBlank) || (txtFieldidCard.text!.count < 9) {
            self.view.makeToast("Please enter valid identity card number(CPR)")
        }
        else {
            self.Cell_Number =  txtfieldCellNumber.text!
            self.CPR_Number = txtFieldidCard.text!
            
            defaults.set(self.Cell_Number, forKey: "cellNoKey")//set
            defaults.set(self.CPR_Number, forKey: "CPRNoKey")//set
            
            print("Cell_Number: \(self.Cell_Number)")
            print("CPR_Number: \(self.self.CPR_Number)")
            
            if (txtfieldCellNumber.text!.count == 11 ) && (txtFieldidCard.text!.count == 9 ) && (iconClick == true){
                self.SentOTPSMS()
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == txtfieldCellNumber){
            let currentText = textField.text! + string
            return currentText.count <= maxLenCellNo
        }
        if(textField == txtFieldidCard){
            let currentText = textField.text! + string
            return currentText.count <= maxLenCPR
        }
        return true;
    }
    
    @objc func txtFieldChangedForCellNo(_ textField: UITextField) {
        if (txtfieldCellNumber.text!.count > 0) && (txtFieldidCard.text!.count > 0) && (iconClick == true) {
            self.continueBtn(isUnable: true)
        }
        else {
            self.continueBtn(isUnable: false)
        }
    }
    
    @objc func txtFieldChangedForCPRNo(_ textField: UITextField) {
        if (txtfieldCellNumber.text!.count > 0) && (txtFieldidCard.text!.count > 0) && (iconClick == true) {
            self.continueBtn(isUnable: true)
        }
        else{
            self.continueBtn(isUnable: false)
        }
    }
    
    func continueBtn(isUnable:Bool){
        if isUnable {
            self.btnContinue.setTitleColor(COLOR.WhiteColor, for: .normal)
            btnContinue.backgroundColor = UIColor.init(named: "colorTaskerGreen")
            btnContinue.isUserInteractionEnabled = true
        }
        else{
            self.btnContinue.setTitleColor(COLOR.GrayColor, for: .normal)
            btnContinue.backgroundColor = UIColor.init(named: "colorTaskerLightGray")
            btnContinue.isUserInteractionEnabled = false
        }
    }
}

//MARK:- ViewController API Call's)
extension TaskerCreateAccountViewController {
    func SentOTPSMS(){
        //let url = ConstantsClass.baseUrl + ConstantsClass.send_otp_to_create_tasker_account
        //print("url: \(url)")
        self.User_type =  UserDefaults.standard.integer(forKey: UserDataManager_KEY.userType)
        self.User_TypeString = String(self.User_type)
        
        let parameters = [
            "cell_no" : "\(self.Cell_Number)",
            "cpr_no" : "\(self.CPR_Number)",
            "user_type" : "\(self.User_TypeString)",
            "relay_id":"gthtdrgtfrgtrdgtrfgyrfthgfhrtfdgte",
            "user_device_type":"2"
            ] as [String : Any]
        
        APICallManager.shared.sendOTPApi(subUrl: ConstantsClass.send_otp_to_create_tasker_account, parameter: parameters, isLoadingIndicatorShow: true) { (response) in
            DispatchQueue.main.async {
                if let responseDict = response["response"]as? [String:Any]
                {
                    let otp = responseDict["otp"]as! Int
                    print("SMS_OTP: \(otp)")
                    
                    if let status = response["status"] as? Bool {
                        if status{
                            DispatchQueue.main.async {
                                let nextViewController = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "Tasker_VerifyOTPViewController") as! Tasker_VerifyOTPViewController
                                nextViewController.otp = "\(otp)"
                                self.navigationController?.pushViewController(nextViewController, animated: true)
                            }
                        }
                        else {
                            self.view.makeToast("Something went wrong", duration: 3.0, position: .bottom)
                            print("Something went wrong")
                        }
                    }
                    else {
                        self.view.makeToast("\(response["message_code"])")
                        print("Error occured") // serialized json response
                    }
                }
            }
        }
        
        
        
        
        
//        Alamofire.request(url
//            , method: .post, parameters: parameters, encoding:JSONEncoding.default).responseJSON
//            {
//                response in
//
//                if let response = response.result.value as? [String:Any] {
//                    print("response: \(response)")
//
//                    if let responseDict = response["response"]as? [String:Any]
//                    {
//                        self.SMS_OTP = responseDict["otp"]as! Int
//                        print("SMS_OTP: \(self.SMS_OTP)")
//                        UserDefaults.standard.set(self.SMS_OTP, forKey: "SMS_OTPKey")
//
//                        if let status = response["status"] as? Bool {
//                            if status{
//                                DispatchQueue.main.async {
//                                    let storyBoard : UIStoryboard = UIStoryboard(name: "Tasker", bundle:nil)
//                                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "Tasker_VerifyOTPViewController") as! Tasker_VerifyOTPViewController
//
//                                    self.navigationController?.pushViewController(nextViewController, animated: true)
//                                }
//                            }
//                            else {
//                                self.view.makeToast("Something went wrong", duration: 3.0, position: .bottom)
//                                print("Something went wrong")
//                            }
//                        }
//                        else {
//                            print("Error occured") // serialized json response
//                        }
//                    }
//                }
//        }
    }
}
