//
//  CreateYourAccountVC.swift
//  LMRA
//
//  Created by Mac on 02/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Foundation
import Toast_Swift
import Alamofire
import TextFieldEffects

class Tasker_CreateYourAccount: UIViewController,UIGestureRecognizerDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var lblCellPhoneNumber: UILabel!
    @IBOutlet weak var lblIdentityCard: UILabel!
    @IBOutlet weak var txtFieldCellPhoneNumberHeight: NSLayoutConstraint!
    @IBOutlet weak var txtFieldIdentityCardHeight: NSLayoutConstraint!
    @IBOutlet weak var txtfieldCellNumber: HoshiTextField!
    @IBOutlet weak var txtFieldidCard: HoshiTextField!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var BtnCheckbox: UIButton!
    @IBOutlet weak var acceptLabel: UILabel!
    var iconClick = true //Visible
    var maxLenCellNo:Int = 11
    var maxLenCPR:Int = 9
    var BtnContinueColor = UIColor(red:243, green:243, blue:241, alpha:1.0)
    var Cell_Number = ""
    var CPR_Number = ""
    var SMS_OTP:Int = 0
    var User_type:Int!
    var User_TypeString = ""
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        txtFieldidCard.keyboardAppearance = .dark
        //        txtfieldCellNumber.keyboardAppearance = .dark
        //
        //        self.txtfieldCellNumber.delegate = self
        //        self.txtFieldidCard.delegate = self
        //        let cellnumberTap = UITapGestureRecognizer(target:self,action:#selector(self.cellnumberTapTapped))
        
        // add it to the label
        //        lblCellPhoneNumber.addGestureRecognizer(cellnumberTap)
        //        lblCellPhoneNumber.isUserInteractionEnabled = true
        //
        //        let idCardTap = UITapGestureRecognizer(target:self,action:#selector(self.idCardTapTapped))
        //
        //        // add it to the label
        //        lblIdentityCard.addGestureRecognizer(idCardTap)
        //        lblIdentityCard.isUserInteractionEnabled = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        addDoneButtonOnKeyboard()
        self.navigationController?.navigationBar.isHidden = false
        //setUI()
    }
    //     func setUI()
    //        {
    //
    //            let font = UIFont(name: "STCForward-Regular", size: 12.0)!
    //            let font2 = UIFont(name: "STCForward-Bold", size: 12.0)!
    //    //        let paragraphStyle = NSMutableParagraphStyle()
    //    //        paragraphStyle.alignment = .left
    //    //        paragraphStyle.firstLineHeadIndent = 5.0
    //
    //            let attributes: [NSAttributedString.Key: Any] = [
    //                .font: font,
    //                .foregroundColor: UIColor.init(hexStringColor: "#000")
    //            ]
    //            let attributes1: [NSAttributedString.Key: Any] = [
    //                .font: font2,
    //                .foregroundColor: UIColor.init(hexStringColor: "#00C48C")
    //            ]
    //
    //
    //            let attributedQuote = NSAttributedString(string: "Agree to", attributes: attributes)
    //            let attributedQuote1 = NSAttributedString(string:"  TERMS & CONDITIONS", attributes: attributes1)
    //            let label = NSMutableAttributedString()
    //            label.append(attributedQuote)
    //            label.append(attributedQuote1)
    //            self.acceptLabel.attributedText = label
    //        }
    
    
    
    @IBAction func cprNoEditingDidBegin(_ sender: Any) {
        self.btnContinue.setTitleColor(BtnContinueColor, for: .normal)
        btnContinue.backgroundColor = colorWithHexString(hexString: "#00C48C")
    }
    @IBAction func cellNoEditingDidBegin(_ sender: Any) {
        self.btnContinue.setTitleColor(BtnContinueColor, for: .normal)
        btnContinue.backgroundColor = colorWithHexString(hexString: "#00C48C")
    }
    
    
    
    @IBAction func cprNoEditingDidEnd(_ sender: Any) {
        if self.txtFieldidCard.text!.count > 0 || self.txtfieldCellNumber.text!.count > 0
        {
            self.btnContinue.setTitleColor(BtnContinueColor, for: .normal)
            btnContinue.backgroundColor = colorWithHexString(hexString: "#00C48C")
        }
        
        checkValidation()
        
    }
    @IBAction func cellNoEditingDidEnd(_ sender: Any) {
        //        if self.txtfieldCellNumber.text!.count > 0
        //        {
        //        self.btnContinue.setTitleColor(BtnContinueColor, for: .normal)
        //        btnContinue.backgroundColor = colorWithHexString(hexString: "#00C48C")
        //        }
        
        checkValidation()
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == txtfieldCellNumber){
            let currentText = textField.text! + string
            return currentText.count <= maxLenCellNo
        }
        if(textField == txtFieldidCard){
            let currentText = textField.text! + string
            return currentText.count <= maxLenCPR
        }
        return true;
    }
    
    @objc func cellnumberTapTapped()
    {
        txtFieldCellPhoneNumberHeight.constant = 30
        txtfieldCellNumber.becomeFirstResponder()
    }
    
    @objc func idCardTapTapped()
    {
        txtFieldIdentityCardHeight.constant = 30
        txtFieldidCard.becomeFirstResponder()
    }
    
    @IBAction func OnCheckboxClick(_ sender: Any) {
        
        if(iconClick == false)
        {
            BtnCheckbox.setImage(UIImage(named: "Uncheck"), for: .normal)
            self.btnContinue.setTitleColor(BtnContinueColor, for: .normal)
            btnContinue.backgroundColor = colorWithHexString(hexString: "#F3F3F1")
            btnContinue.setTitleColor(colorWithHexString(hexString: "#8E9AA0"), for: .normal)
        }
        else {
            // btnContinue.isEnabled = true
            
            BtnCheckbox.setImage(UIImage(named: "tasker_checked"), for: .normal)
            if (txtfieldCellNumber.text!.count == 11) && (txtFieldidCard.text!.count == 9) && (iconClick == true)
            {
                btnContinue.isEnabled = true
                self.btnContinue.setTitleColor(BtnContinueColor, for: .normal)
                btnContinue.backgroundColor = colorWithHexString(hexString: "#00C48C")
            }
            else
            {
                btnContinue.isEnabled = false
                self.btnContinue.setTitleColor(BtnContinueColor, for: .normal)
                btnContinue.backgroundColor = colorWithHexString(hexString: "#F3F3F1")
                btnContinue.setTitleColor(colorWithHexString(hexString: "#8E9AA0"), for: .normal)
            }
        }
        iconClick = !iconClick
    }
    
    @IBAction func btnBackClicked(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnContinueClicked(_ sender: Any) {
        let CheckOTPFromChnageCellNo: Bool = false
        UserDefaults.standard.set(CheckOTPFromChnageCellNo,forKey: "isCellNoChange")
        
        checkValidation()
    }
    
    func checkValidation()  {
        if (txtfieldCellNumber.text! == "" ) || (txtfieldCellNumber.text!.count != 11 ){
            // btnContinue.backgroundColor = colorWithHexString(hexString: "#F3F3F1")
            self.view.makeToast("Please Enter valid 11 Digit Mobile no")
        }
        else if self.txtFieldidCard.text!.count > 0 && self.txtfieldCellNumber.text!.count > 0
        {
            self.btnContinue.setTitleColor(BtnContinueColor, for: .normal)
            btnContinue.backgroundColor = colorWithHexString(hexString: "#00C48C")
        }
            
        else if (txtFieldidCard.text?.isEmpty ?? true) || (txtFieldidCard.text!.count != 9)
        {
            //btnContinue.backgroundColor = colorWithHexString(hexString: "#F3F3F1")
            self.view.makeToast("Please Enter valid 9 Digit CPR no")
        }
        else if(iconClick == true)
        {
            self.view.makeToast("Please Read & accept Terms & conditions")
        }
        else if (!txtfieldCellNumber.text!.hasPrefix("973"))
        {
            self.view.makeToast("Phone number should starts with 973")
        }
        else
        {
            self.Cell_Number =  txtfieldCellNumber.text!
            self.CPR_Number = txtFieldidCard.text!
            
            defaults.set(self.Cell_Number, forKey: "cellNoKey")//set
            defaults.set(self.CPR_Number, forKey: "CPRNoKey")//set
            
            print("Cell_Number: \(self.Cell_Number)")
            print("CPR_Number: \(self.self.CPR_Number)")
            
            if (txtfieldCellNumber.text!.count == 11 ) && (txtFieldidCard.text!.count == 9 )
            {
                btnContinue.isEnabled = true
                self.btnContinue.setTitleColor(BtnContinueColor, for: .normal)
                btnContinue.backgroundColor = colorWithHexString(hexString: "#00C48C")
                
                self.SentOTPSMS()
            }
        }
    }
    
    func SentOTPSMS()
    {
        
        let url = ConstantsClass.baseUrl + ConstantsClass.send_otp_to_create_tasker_account
        print("url: \(url)")
        self.User_type =  UserDefaults.standard.integer(forKey: UserDataManager_KEY.userType)
        self.User_TypeString = String(self.User_type)
        let parameters = [
            "cell_no" : "\(self.Cell_Number)",
            "cpr_no" : "\(self.CPR_Number)",
            "user_type" : "\(self.User_TypeString)",
            "relay_id":"gthtdrgtfrgtrdgtrfgyrfthgfhrtfdgte",
            "user_device_type":"2"
            ] as [String : Any]
        
        print("parameters: \(parameters)")
        
        Alamofire.request(url
            , method: .post, parameters: parameters, encoding:JSONEncoding.default).responseJSON
            {
                response in
                
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if let responseDict = response["response"]as? [String:Any]
                    {
                        self.SMS_OTP = responseDict["otp"]as! Int
                        print("SMS_OTP: \(self.SMS_OTP)")
                        UserDefaults.standard.set(self.SMS_OTP, forKey: "SMS_OTPKey")
                        
                        if let status = response["status"] as? Bool {
                            if status{
                                DispatchQueue.main.async {
                                    let nextViewController = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "Tasker_VerifyOTPViewController") as! Tasker_VerifyOTPViewController
                                    
                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                }
                            }
                            else
                            {
                                self.view.makeToast("Something went wrong", duration: 3.0, position: .bottom)
                                print("Something went wrong")
                            }
                        }
                        else
                        {
                            print("Error occured") // serialized json response
                            
                        }
                    }
                }
        }
    }
    
    func colorWithHexString(hexString: String, alpha:CGFloat = 1.0) -> UIColor {
        
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    
    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 20))
        doneToolbar.barStyle = .default
        let Cancel: UIBarButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(self.doneButtonAction))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [Cancel,flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.txtfieldCellNumber.inputAccessoryView = doneToolbar
        self.txtFieldidCard.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.txtfieldCellNumber.resignFirstResponder()
        self.txtFieldidCard.resignFirstResponder()
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        self.txtfieldCellNumber.resignFirstResponder()
        self.txtFieldidCard.resignFirstResponder()
        
        return Int(string) != nil
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                //                        self.view.frame.origin.y -= keyboardSize.height
                self.view.frame.origin.y -= 150
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            //                    self.view.frame.origin.y = 0
            self.view.frame.origin.y += 150
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
}
