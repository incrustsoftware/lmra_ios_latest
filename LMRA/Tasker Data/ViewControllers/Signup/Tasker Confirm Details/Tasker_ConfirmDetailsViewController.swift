//
//  ConfirmYourDetailsVC.swift
//  LMRA
//
//  Created by Mac on 03/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire
import TextFieldEffects

class Tasker_ConfirmDetailsViewController: UIViewController, UITextFieldDelegate{
    
    @IBOutlet weak var outerImageView: UIImageView!
    @IBOutlet weak var ImgCamera: UIImageView!
    @IBOutlet weak var txtFieldName: UITextField!
    @IBOutlet weak var txtFieldFamilyname: UITextField!
    @IBOutlet weak var txtFieldNationality: UITextField!
    @IBOutlet weak var txtfieldCellNumber: UITextField!
    @IBOutlet weak var txtFieldEmail: UITextField!
    @IBOutlet weak var BtnConfirm: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var placeholderImg: UIImageView!
    
    //Variables
    var imageString = ""
    var Name = ""
    var FamilyName = ""
    var Nationality = ""
    var EmailID = ""
    var Get_FirstCell_Number = ""
    var Get_UpdatedCellNo = ""
    var CurrentCellNumber = ""
    var Get_CPR_Number = ""
    let defaults = UserDefaults.standard
    var BtnCofirmColor = UIColor(red:243, green:243, blue:241, alpha:1.0)
    let picker = UIImagePickerController()
    var imagePicker = UIImagePickerController()
    var maxLenName:Int = 50
    var maxLenFamilyName:Int = 50
    var MaxLenNationality:Int = 20
    var isEdit:Bool = false
    var trimedString:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.placeholderImg.isHidden = false
        imagePicker.delegate = self
        outerImageView.layer.cornerRadius = outerImageView.frame.size.height / 2
        ImgCamera.layer.cornerRadius = ImgCamera.frame.size.height / 2
        
        self.txtFieldName.delegate = self
        self.txtFieldFamilyname.delegate = self
        self.txtFieldNationality.delegate = self
        self.txtFieldEmail.delegate = self
        
        self.Get_CPR_Number = UserDefaults.standard.string(forKey: "CPRNoKey")!//get
        let isCalledFromChangedNumber = UserDefaults.standard.bool(forKey: "isCellNoChange")// this
        
        if(isCalledFromChangedNumber == true) {
            self.Get_UpdatedCellNo = UserDefaults.standard.string(forKey: "ChnageCellNo")!//get
            print("UpdatedCellNo: \(self.Get_UpdatedCellNo)")
            self.CurrentCellNumber = self.Get_UpdatedCellNo
        }
        else{
            self.Get_FirstCell_Number = UserDefaults.standard.string(forKey: "cellNoKey")!//get
            print("UpdatedCellNo: \(self.Get_FirstCell_Number)")
            self.CurrentCellNumber = self.Get_FirstCell_Number
        }
        
        print("CurrentCellNumber: \(self.CurrentCellNumber)")
        UserDefaults.standard.set( self.CurrentCellNumber, forKey: "CurrentCellNoKey")
        
        self.txtfieldCellNumber.text = CurrentCellNumber
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setStatusBarColor(color: .white, styleForStatus: .default)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    //MARK:- ViewController IBAction's
    @IBAction func btnContinueClicked(_ sender: Any) {
        self.validation()
    }
    
    @IBAction func SelectImageBtnTapped(_ sender: Any) {
        print("camera button click")
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func textFieldEditingChangedCheck(_ sender: UITextField) {
        if txtFieldName.text != "" && txtFieldFamilyname.text != "" && txtFieldNationality.text != "" && txtfieldCellNumber.text != "" && txtFieldEmail.text != "" {
            self.BtnConfirm.setTitleColor(COLOR.WhiteColor, for: .normal)
            self.BtnConfirm.backgroundColor = UIColor.init(named: "colorTaskerGreen")
            self.BtnConfirm.isUserInteractionEnabled = true
        }
        else {
            self.BtnConfirm.setTitleColor(COLOR.GrayColor, for: .normal)
            self.BtnConfirm.backgroundColor = UIColor.init(named: "colorTaskerLightGray")
            self.BtnConfirm.isUserInteractionEnabled = false
        }
    }
}

//MARK:- ViewController TextFieldDelgate Methods
extension Tasker_ConfirmDetailsViewController {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == txtFieldName){
            let currentText = textField.text! + string
            return currentText.count <= maxLenName
        }
        
        if(textField == txtFieldFamilyname){
            let currentText = textField.text! + string
            return currentText.count <= maxLenFamilyName
        }
        
        if(textField == txtFieldNationality){
            let currentText = textField.text! + string
            return currentText.count <= MaxLenNationality
        }
        
        return true;
    }
}

//MARK:- ViewController Own Defined Methods
extension Tasker_ConfirmDetailsViewController {
    func openCamera() {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary() {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func validation() {
        self.trimedString = self.txtFieldEmail.text!
        self.trimedString =  self.trimedString.replacingOccurrences(of: " ", with: "")
        
        if (txtFieldName.text!.isBlank) {
            self.view.makeToast("Please Enter Name")
        }
        else if (txtFieldFamilyname.text!.isBlank) {
            self.view.makeToast("Please Enter Family Name")
        }
        else if (txtFieldNationality.text!.isBlank) {
            self.view.makeToast("Please Enter Nationality")
        }
        else if (isValidEmail(testStr: trimedString) == false) {
            self.view.makeToast("Please enter valid email address")
        }
        else {
            self.Name =  txtFieldName.text!
            self.FamilyName = txtFieldFamilyname.text!
            self.Nationality =  txtFieldNationality.text!
            self.EmailID = trimedString
            
            defaults.set(self.Name, forKey: "NameKey")//set
            defaults.set(self.FamilyName, forKey: "FamilyKey")//set
            defaults.set(self.Nationality, forKey: "NtionalityKey")//set
            defaults.set(self.EmailID, forKey: "EmailKey")//set
            defaults.set(self.imageString,forKey:"profileUrl")//set
            print("Name: \(self.Name)")
            print("FamilyName: \(self.FamilyName)")
            print("Nationality: \(self.Nationality)")
            print("EmailID: \(self.EmailID)")
            self.confirm_personal_details()
        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func validate(value: String) -> Bool {
        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    func colorWithHexString(hexString: String, alpha:CGFloat = 1.0) -> UIColor {
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    
    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
}

extension UIImage {
    var highestQualityJPEGNSData: Data { return self.jpegData(compressionQuality: 1.0)! }
    var highQualityJPEGNSData: Data    { return self.jpegData(compressionQuality: 0.75)!}
    var mediumQualityJPEGNSData: Data  { return self.jpegData(compressionQuality: 0.5)! }
    var lowQualityJPEGNSData: Data     { return self.jpegData(compressionQuality: 0.25)!}
    var lowestQualityJPEGNSData: Data  { return self.jpegData(compressionQuality: 0.0)! }
}

//MARK:- ViewController ImagePicker Delegate Methods
extension Tasker_ConfirmDetailsViewController:UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // self.imgUser.isHidden = true
        let chosenImage = info[UIImagePickerController.InfoKey.originalImage]
            as? UIImage //2
        outerImageView.contentMode = .scaleAspectFill //3
        outerImageView.image = chosenImage //4
        dismiss(animated: true, completion: nil) //5
        
        let scaledImage = UIImage.scaleImageWithDivisor(img: chosenImage!, divisor: 3)
        print("scaledImage:",scaledImage)
        
        //let imageData:NSData = scaledImage.pngData()! as NSData
        let imageData = scaledImage.lowestQualityJPEGNSData
        self.imageString = imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        print("imageString....\(imageString)")
        
        placeholderImg.isHidden = true
    }
    
    //What to do if the image picker cancels.
    private func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

//MARK:- ViewController API Call's
extension Tasker_ConfirmDetailsViewController {
    func confirm_personal_details() {
        //let url = ConstantsClass.baseUrl + ConstantsClass.confirm_personal_details_tasker
        //print("url: \(url)")
        let parameters = [
            "name" : "\(self.Name)",
            "family_name" : "\(self.FamilyName)",
            "nationality" : "\(self.Nationality)",
            "email_id" : "\(self.EmailID)",
            "cell_no" : "\(self.CurrentCellNumber)",
            "cpr_no" : "\(self.Get_CPR_Number)",
            "bitmap_image":"\(self.imageString)"
            ] as [String : Any]
        
        print("parametersPersonaldetails: \(parameters)")
        
        APICallManager.shared.confirmPersonalDetailsTaskerApi(subUrl: ConstantsClass.confirm_personal_details_tasker, parameter: parameters, isLoadingIndicatorShow: true) { (response) in
            DispatchQueue.main.async {
                if let status = response["status"] as? Bool {
                    if status {
                        print("send_otp_success")
                        let nextViewController = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "Tasker_EmailOTPViewController") as! Tasker_EmailOTPViewController
                        self.navigationController?.pushViewController(nextViewController, animated: true)
                    }
                }
                else {
                    self.view.makeToast("Something went wrong", duration: 3.0, position: .bottom)
                    print("Something went wrong")
                }
            }
        }
        
        //        Alamofire.request(url
        //            , method: .post, parameters: parameters, encoding:JSONEncoding.default).responseJSON { response in
        //
        //                if let response = response.result.value as? [String:Any] {
        //                    print("response: \(response)")
        //
        //                    if let status = response["status"] as? Bool {
        //
        //                        if status {
        //                            Helper.sharedInstance.hideLoader()
        //                            print("send_otp_success")
        //                            //self.sendOtpEmail()
        //                            DispatchQueue.main.async {
        //                                let storyBoard : UIStoryboard = UIStoryboard(name: "Tasker", bundle:nil)
        //                                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "Tasker_EmailOTPViewController") as! Tasker_EmailOTPViewController
        //                                self.navigationController?.pushViewController(nextViewController, animated: true)
        //                            }
        //                        }
        //                    }
        //                    else {
        //                        self.view.makeToast("Something went wrong", duration: 3.0, position: .bottom)
        //                        print("Something went wrong")
        //                    }
        //                }
        //                else {
        //                    print("Error occured") // serialized json response
        //                }
        //        }
    }
}
