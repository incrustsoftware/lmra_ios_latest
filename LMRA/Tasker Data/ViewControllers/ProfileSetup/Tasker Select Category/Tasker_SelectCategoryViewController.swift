//
//  Tasker_SelectCategoryViewController.swift
//  LMRA
//
//  Created by Mac on 27/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Foundation
import Toast_Swift
import Alamofire
import SwiftyJSON
import SDWebImage

class Tasker_SelectCategoryViewController: UIViewController,SelectCatgoryCollectionViewCellDelegate {
    
    @IBOutlet weak var lblViewTitle: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var mainCategoryView: UIView!
    @IBOutlet weak var upperDividerView: UIView!
    @IBOutlet weak var selectServicesLabel: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var servicesView: UIView!
    @IBOutlet weak var selectedServicesCollectionView: UICollectionView!
    @IBOutlet weak var labelView: UIView!
    @IBOutlet weak var selectSpecialityLabel: UILabel!
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet weak var continueButton: UIButton!
    
    var isForEdit: Bool!
    
    var Get_User_Type:Int!
    var category = ""
    var SubCategory_Array:[SubCategory] = []
    var CategoryArray = [String] ()
    var categoryIdArry = [Int] ()
    var selectedSubCategoryIdArray:[SubCategory] = []
    var AddSubCatArray:[AddSubCategory] = []
    var selectedCategory:[String] = []
    var finalArray:[String] = []
    var isFromEditProfile:Bool = false
    var isFromEdit:Bool = false
    var AllDataDict = [NSDictionary]()
    var categoryParseArray:[Category] = []
    var searchActive : Bool = false
    var filteredString : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setStatusBarColor(color: .white, styleForStatus: .default)
        //categoryCollectionView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        if isForEdit != nil && isForEdit{
            self.lblViewTitle.isHidden = false
        }
        else {
            self.lblViewTitle.isHidden = true
        }
        getCategoryApi()
        self.searchBar.delegate = self
        self.searchBar.barTintColor = UIColor.gray;
        self.searchBar.tintColor = UIColor.gray
        UIBarButtonItem.appearance().tintColor = UIColor.white
        let nib1 = UINib.init(nibName: "SelectCatgoryCollectionViewCell", bundle: nil)
        self.selectedServicesCollectionView?.register(nib1, forCellWithReuseIdentifier: "selectCatCell")
        self.categoryCollectionView.reloadData()
        self.selectedServicesCollectionView.reloadData()
        self.labelView.isHidden = true
        searchBar.setImage(UIImage(named: "tasker_searchImage"), for: .search, state: .normal)
        if isFromEdit == true {
            self.title = "Change Services"
        }
        
        self.servicesView.isHidden = true
        self.labelView.isHidden = false
        self.mainCategoryView.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setStatusBarColor(color: .white, styleForStatus: .default)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidLayoutSubviews() {
        DispatchQueue.main.async {
            var contentRect = CGRect.zero
            
            for view in self.scrollView.subviews {
                contentRect = contentRect.union(view.frame)
            }
            
            self.scrollView.contentSize.height = contentRect.size.height
        }
    }
    
    func configureSearchBar() {
        for textField in searchBar.subviews.first!.subviews where textField is UITextField {
            textField.backgroundColor = Helper.sharedInstance.hexStringToUIColor(hex: "#F3F3F1")
        }
    }
    
    //MARK:- ViewController IBAction's
    @IBAction func btnBackClickAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func continueButton(_ sender: Any) {
        finalArray.removeAll()
        for (_,element) in selectedSubCategoryIdArray.enumerated() {
            finalArray.append(String(element.subcategory_id))
        }
        if continueButton.isEnabled == true {
            let api:APIEngine = APIEngine()
            api.getAddSubcategoryListAPI(param:["service_subcategories":finalArray])
            { responseObject, error in
                print(Parameters.self)
                switch responseObject?.result  {
                case .success(let JSON)?:
                    print("Success with JSON: \(JSON)")
                    let jsonResponse = JSON
                    
                    if(jsonResponse["status"].bool == true) {
                        let parse:Parser = Parser()
                        self.AddSubCatArray =  parse.parseAddSubCategoryList(data: JSON)
                        print("AddsubCat:\(self.AddSubCatArray)")
                        self.selectedServicesCollectionView.reloadData()
                        self.Navigation()
                        
                    }
                    else {
                        Helper.sharedInstance.showAlert(title: "Message", message:jsonResponse["message_code"].stringValue )
                    }
                case.failure(let error)?:
                    print("error: \(error)")
                    
                case .none:
                    print("error: ")
                }
            }
        }
    }
    
    @IBAction func servicesBtnAction(_ sender: Any) {
        self.categoryLabel.text = ""
        self.categoryParseArray.removeAll()
        self.SubCategory_Array.removeAll()
        self.labelView.isHidden = false
        self.getCategoryApi()
        
        categoryCollectionView.reloadData()
    }
    
    //MARK:- ViewController Own Defined Methods
    func Navigation() {
        let nextViewController = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "Tasker_AddRateViewController") as! Tasker_AddRateViewController
        if self.isFromEdit == true {
            nextViewController.isForEdit = true
        }
        if  self.isFromEditProfile == true {
            nextViewController.isForEdit = true
        }
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    func setUI() {
        if selectedSubCategoryIdArray.count > 0 {
            self.continueButton.isEnabled = true
            self.continueButton.backgroundColor = UIColor.init(hexStringColor: "#00C48C")
            self.continueButton.setTitleColor(UIColor.white, for: .normal)
            //colorWithHexString(hexString: "#00C48C")
        }
        else {
            self.continueButton.isEnabled = false
            //Helper.sharedInstance.showAlert(title: "Alert", message: "Please select at least 3 speciality")
            self.continueButton.backgroundColor = UIColor.init(hexStringColor: "#F3F3F1")
            self.continueButton.setTitleColor(Helper.sharedInstance.hexStringToUIColor(hex: "#8E9AA0"), for: .normal)
        }
    }
    
    func didTapButton(cell: SelectCatgoryCollectionViewCell) {
        if let indexPath = selectedServicesCollectionView.indexPath(for: cell) {
            print(indexPath.row)
            print("Close")
            self.selectedSubCategoryIdArray.remove(at: indexPath.row)
            
            self.selectedServicesCollectionView.reloadData()
            self.setUI()
            if selectedSubCategoryIdArray.count == 0 {
                self.labelView.isHidden = false
                self.servicesView.isHidden = true
                self.getCategoryApi()
                self.categoryCollectionView.reloadData()
                self.mainCategoryView.isHidden = false
                // self.categoryLabel.text = ""
            }
            else {
                // Fallback on earlier versions
            }
        }
    }
}

//MARK:- UISearchBar Delegate Methods
extension Tasker_SelectCategoryViewController:UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isBlank {
            self.categoryParseArray.removeAll()
            self.getCategoryApi()
            categoryCollectionView.reloadData()
        }
        else {
            categoryParseArray = searchText.isEmpty ? categoryParseArray : categoryParseArray.filter({$0.category.range(of: searchBar.text!) != nil})
            categoryCollectionView.reloadData()
        }
    }
}

//MARK:- CollectionView Delegate Methods
extension Tasker_SelectCategoryViewController:UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.categoryCollectionView {
            if SubCategory_Array.count > 0 {
                return SubCategory_Array.count
            }
            else {
                return categoryParseArray.count
            }
        }
        else if collectionView == self.selectedServicesCollectionView {
            return selectedSubCategoryIdArray.count
            
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.categoryCollectionView {
            let cell:CatCollectionViewCell = self.categoryCollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)as!CatCollectionViewCell
            cell.contentView.layer.borderWidth = 1.0
            cell.contentView.layer.borderColor = UIColor.clear.cgColor
            cell.contentView.layer.masksToBounds = true
            
            cell.layer.shadowColor = UIColor.lightGray.cgColor
            cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
            cell.layer.shadowRadius = 6.0
            cell.layer.shadowOpacity = 1.0
            cell.layer.masksToBounds = false
            cell.layer.backgroundColor = UIColor.gray.cgColor
            if SubCategory_Array.count > 0 {
                let temp:SubCategory = SubCategory_Array[indexPath.row]
                cell.categoryNameLabel.text = temp.subcategory_name
                cell.imageView.sd_setImage(with: URL(string:(temp.subcategory_image) ), placeholderImage: UIImage(named: "Image"))
            }
            else {
                let temp:Category = categoryParseArray[indexPath.row]
                cell.categoryNameLabel.text = temp.category
                cell.imageView.sd_setImage(with: URL(string:(temp.category_image) ), placeholderImage: UIImage(named: "Image"))
            }
            return cell
        }
        else {
            let cell:SelectCatgoryCollectionViewCell = self.selectedServicesCollectionView.dequeueReusableCell(withReuseIdentifier: "selectCatCell", for: indexPath)as!SelectCatgoryCollectionViewCell
            //let temp:String = selectedCategory[indexPath.row]
            cell.categoryNameLabel.text = selectedSubCategoryIdArray[indexPath.row].subcategory_name//temp.description
            cell.cellDelegate = self
            Helper.sharedInstance.shadowView(view: cell.contentView)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.categoryCollectionView {
            if categoryParseArray.count > 0 {
                let temp:Category = categoryParseArray[indexPath.row]
                
                self.mainCategoryView.isHidden = false
                getSubCategory(category_id: temp.id)
                self.categoryLabel.text = temp.category
                self.categoryParseArray.removeAll()
            }
            
            if SubCategory_Array.count > 0 {
                if selectedSubCategoryIdArray.count < 3 {
                    let containData = selectedSubCategoryIdArray.filter { catalogName in
                        return catalogName.subcategory_id == SubCategory_Array[indexPath.row].subcategory_id
                    }
                    
                    if containData.count == 0 {
                        selectedSubCategoryIdArray.append(SubCategory_Array[indexPath.row])
                    }
                    else {
                        self.view.makeToast("Please select another category")
                    }
                }
                else {
                    self.view.makeToast("You can add upto 3 services only")
                }
                servicesView.isHidden = false
                self.selectedServicesCollectionView.reloadData()
                self.setUI()
                
                let index = IndexPath(item: (self.SubCategory_Array.count) - 1, section: 0)
                self.selectedServicesCollectionView.scrollToItem(at: index, at: UICollectionView.ScrollPosition.right, animated: false)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == categoryCollectionView {
            return CGSize (width: (categoryCollectionView.frame.size.width/2)-15, height: (categoryCollectionView.frame.size.width/2)-15)
        }
        else if collectionView == selectedServicesCollectionView {
            return CGSize (width: (self.view.frame.size.width/2), height:collectionView.frame.size.height)
        }
        else {
            return collectionView.frame.size
        }
    }
}

//MARK:- ViewController API Call's
extension Tasker_SelectCategoryViewController {
    func getSubCategory(category_id:Int) {
        //self.Get_User_Type =  UserDefaults.standard.integer(forKey: UserDataManager_KEY.userType)
        //let param:[String:String] = ["user_id":"\(Get_User_Type!)"]
        
//        APICallManager.shared.getServicesSubcategoriesAPi(subUrl: ConstantsClass.send_otp_onemail_tasker, parameter: ["category_id":"\(category_id)"], isLoadingIndicatorShow: true) { (response) in
//            DispatchQueue.main.async {
//                if(response["status"] as? Bool == true) {
//                    // Helper.sharedInstance.hideLoader()
//                    let parse:Parser = Parser()
//                    self.SubCategory_Array =  parse.parseSubCategoryList(data: response)
//                    print("subCat:\(self.SubCategory_Array)")
//                    self.categoryCollectionView.reloadData()
//                }
//                else {
//                    Helper.sharedInstance.showAlert(title: "Message", message:jsonResponse["message_code"].stringValue )
//                }
//            }
//        }
        
        let api:APIEngine = APIEngine()
        api.getSubcategoryListAPI(param:["category_id":"\(category_id)"]){ responseObject, error in
            
            switch responseObject?.result  {
            case .success(let JSON)?:
                print("Success with JSON: \(JSON)")
                let jsonResponse = JSON
                
                if(jsonResponse["status"].bool == true) {
                    // Helper.sharedInstance.hideLoader()
                    let parse:Parser = Parser()
                    self.SubCategory_Array =  parse.parseSubCategoryList(data: JSON)
                    print("subCat:\(self.SubCategory_Array)")
                    self.categoryCollectionView.reloadData()
                }
                else {
                    Helper.sharedInstance.showAlert(title: "Message", message:jsonResponse["message_code"].stringValue )
                }
            case.failure(let error)?:
                print("error: \(error)")
                
            case .none:
                print("error: ")
            }
        }
    }
    
    func getCategoryApi() {
        self.Get_User_Type =  UserDefaults.standard.integer(forKey: UserDataManager_KEY.userType)
        //let urlString = ConstantsClass.baseUrl + ConstantsClass.get_category + ("?user_type=\(self.Get_User_Type!)")
        //print("urlString: \(urlString)")
        
        APICallManager.shared.get_categoryViUserTypeAPi(subUrl: ConstantsClass.get_category + ("?user_type=\(self.Get_User_Type!)"), parameter: [:], isLoadingIndicatorShow: true) { (response) in
            DispatchQueue.main.async {
                if (response["status"] as? Bool) != nil {
                    if let respArray = response["response"] as? [[String:Any]] {
                        print("respArray: \(respArray)")
                        
                        for Data in respArray {
                            guard let categoryString =  Data["category"]as? String else{
                                return
                            }
                            guard let categoryID =  Data["id"]as? Int else{
                                return
                            }
                            guard let categoryImg =  Data["category_image"]as? String else {
                                return
                            }
                            self.categoryParseArray.append(Category(id: categoryID, category: categoryString, category_image: categoryImg))
                            self.filteredString.append(categoryString)
                        }
                        print("Category_Array: \(self.CategoryArray)")
                        
                        print("CategoryString_Array: \(self.categoryParseArray)")
                        print("id:\(self.categoryIdArry)")
                        self.AllDataDict = response["response"] as! [NSDictionary]
                    }
                }
                // Make sure to update UI in main thread
                DispatchQueue.main.async {
                    self.categoryCollectionView.reloadData()
                }
            }
        }
        
        
//        Alamofire.request(urlString, method: .get, parameters: nil, encoding: URLEncoding.default).responseJSON { response in
//            if let response = response.result.value as? [String:Any] {
//                print("response: \(response)")
//
//                if (response["status"] as? Bool) != nil {
//                    if let respArray = response["response"] as? [[String:Any]] {
//                        print("respArray: \(respArray)")
//
//                        for Data in respArray {
//                            guard let categoryString =  Data["category"]as? String else{
//                                return
//                            }
//                            guard let categoryID =  Data["id"]as? Int else{
//                                return
//                            }
//                            guard let categoryImg =  Data["category_image"]as? String else {
//                                return
//                            }
//                            self.categoryParseArray.append(Category(id: categoryID, category: categoryString, category_image: categoryImg))
//                            self.filteredString.append(categoryString)
//                        }
//                        print("Category_Array: \(self.CategoryArray)")
//
//                        print("CategoryString_Array: \(self.categoryParseArray)")
//                        print("id:\(self.categoryIdArry)")
//                        self.AllDataDict = response["response"] as! [NSDictionary]
//                    }
//                }
//                // Make sure to update UI in main thread
//                DispatchQueue.main.async {
//                    self.categoryCollectionView.reloadData()
//                }
//            }
//            else {
//                print("Error occured") // serialized json response
//            }
//        }
    }
    
    //    func getSubCategoryApi(category_id:Int) {
    //        //self.Get_User_Type =  UserDefaults.standard.integer(forKey: UserDataManager_KEY.userType)
    //        let urlString = ConstantsClass.baseUrl + ConstantsClass.get_services_subcategories
    //        print("urlString: \(urlString)")
    //
    //        Alamofire.request(urlString, method: .post, parameters: ["category_id":category_id], encoding: URLEncoding.default).responseJSON {
    //            response in
    //            if let response = response.result.value as? [String:Any] {
    //                print("response: \(response)")
    //
    //                if (response["status"] as? Bool) != nil {
    //                    if let respArray = response["response"] as? [[String:Any]] {
    //                        print("respArray: \(respArray)")
    //
    //                        for Data in respArray {
    //                            self.category = Data["category"]as! String
    //                            self.CategoryArray.append(self.category)
    //                            self.categoryIdArry.append(Data["id"] as! Int)
    //                        }
    //                        print("CategoryString_Array: \(self.CategoryArray)")
    //                        print("id:\(self.categoryIdArry)")
    //                    }
    //                }
    //                // Make sure to update UI in main thread
    //                DispatchQueue.main.async {
    //                    self.categoryCollectionView.reloadData()
    //                }
    //            }
    //            else {
    //                print("Error occured") // serialized json response
    //            }
    //        }
    //    }
}
