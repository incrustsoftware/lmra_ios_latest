//
//  SelectCatgoryCollectionViewCell.swift
//  LMRA
//
//  Created by Mac on 27/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
protocol SelectCatgoryCollectionViewCellDelegate: class {
    func didTapButton(cell: SelectCatgoryCollectionViewCell)
}

class SelectCatgoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    
    weak var cellDelegate: SelectCatgoryCollectionViewCellDelegate?
    
    @IBAction func closeButton(_ sender: Any) {
        cellDelegate?.didTapButton(cell: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
