//
//  TaskerProfileViewController.swift
//  LMRA
//
//  Created by Mac on 06/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import Toast_Swift
import SwiftyJSON
class TaskerProfileViewController: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var lblViewTitle: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var serviceTitleLabel: UILabel!
    @IBOutlet weak var firstCategoryView: UIView!
    @IBOutlet weak var firstCatNameLabel: UILabel!
    @IBOutlet weak var fristCatRateLabel: UILabel!
    @IBOutlet weak var secondCatView: UIView!
    @IBOutlet weak var secondCatNameLabel: UILabel!
    @IBOutlet weak var secondCateRateLabel: UILabel!
    @IBOutlet weak var thirdCateView: UIView!
    @IBOutlet weak var thirdCatRateLabel: UILabel!
    @IBOutlet weak var thirdCatNameLabel: UILabel!
    @IBOutlet weak var serviceView: UIView!
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var serviceTopView: UIView!
    @IBOutlet weak var serviceimageView: UIImageView!
    @IBOutlet weak var editServiceBtn: UIButton!
    @IBOutlet weak var timingTopView: UIView!
    @IBOutlet weak var timingImageView: UIImageView!
    @IBOutlet weak var timingLabel: UILabel!
    @IBOutlet weak var editTimingBtn: UIButton!
    @IBOutlet weak var editTimingLabel: UILabel!
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var languageView: UIView!
    @IBOutlet weak var editLanguageView: UIView!
    @IBOutlet weak var langimageview: UIImageView!
    @IBOutlet weak var langLabel: UILabel!
    @IBOutlet weak var editLangBtn: UIButton!
    @IBOutlet weak var languagesLabel: UILabel!
    
    var profileData:TaskerProfileDetailResponseModel!
    var selectedWorkingDay:TaskerWorkingDayDataModel!
    var selectedWorkingTime:TaskerWorkingTimeDataModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setStatusBarColor(color: .white, styleForStatus: .default)
        //Helper.sharedInstance.timeslotshadowView(view: self.serviceView)
        //Helper.sharedInstance.timeslotshadowView(view: self.timeView)
        //Helper.sharedInstance.timeslotshadowView(view: self.editLanguageView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setStatusBarColor(color: .white, styleForStatus: .default)
        setNeedsStatusBarAppearanceUpdate()
        getProileApi()
    }
    
    override func viewDidLayoutSubviews() {
        DispatchQueue.main.async {
            var contentRect = CGRect.zero
            
            for view in self.scrollView.subviews {
                contentRect = contentRect.union(view.frame)
            }
            
            self.scrollView.contentSize.height = contentRect.size.height
        }
    }
    
    //MARK:- ViewController IBAction's
    @IBAction func btnBackClickAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnCloseClickAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func editLangBtnAction(_ sender: Any) {
        let vc:Tasker_LanguagesViewController = self.storyboard?.instantiateViewController(withIdentifier: "Tasker_LanguagesViewController")as!Tasker_LanguagesViewController
        vc.isForEditFirstTime = true
        vc.selectedTasker_languages = self.profileData.tasker_languages
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func editServicesBtnAction(_ sender: Any) {
        let vc:Tasker_AddRateViewController = self.storyboard?.instantiateViewController(withIdentifier: "Tasker_AddRateViewController")as!Tasker_AddRateViewController
        vc.isForEditFirstTime = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func editTmingbtnAction(_ sender: Any) {
        let vc:WorkPreferViewController = self.storyboard?.instantiateViewController(withIdentifier: "WorkPreferViewController")as!WorkPreferViewController
        vc.isForEditFirstTime = true
        vc.selectedWorkingDay = self.selectedWorkingDay
        vc.selectedWorkingTime = self.selectedWorkingTime
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func confrimBtnAction(_ sender: Any) {
        var isPopView = false
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: TaskerDasboardVC.self) {
                isPopView = true
                self.navigationController?.popToViewController(controller, animated: true)
                break
            }
        }
        
        if isPopView == false {
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            let rootViewController = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "TaskerDasboardVC")
            
            let navigationController = UINavigationController(rootViewController: rootViewController)
            let rightViewController = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "SideMenuViewController")
            let sideMenuController = LGSideMenuController(rootViewController: navigationController,
                                                          leftViewController: nil,
                                                          rightViewController: rightViewController)
            sideMenuController.leftViewWidth = 0
            sideMenuController.leftViewPresentationStyle = .slideBelow;
            
            sideMenuController.rightViewWidth = (appDelegate?.window?.bounds.size.width ?? 320) - 20
            sideMenuController.leftViewPresentationStyle = .slideBelow
            navigationController.isNavigationBarHidden = true
            navigationController.interactivePopGestureRecognizer?.delegate = self
            //self.interactivePopGestureRecognizer.enabled = NO;
            appDelegate?.window?.rootViewController = sideMenuController
            appDelegate?.window?.makeKeyAndVisible()
        }
        
    }
}

//MARK:- ViewController IBAction's
extension TaskerProfileViewController {
    func getProileApi() {
        APICallManager.shared.getAccountProfileAPI(subUrl: ConstantsClass.get_Tasker_profileDetails, parameter: [:], isLoadingIndicatorShow: true) { (data) in
            if data.status {
                if data.response.count > 0 {
                    self.profileData = data.response[0]
                    DispatchQueue.main.async {
                        var strLanguage = ""
                        for (_,element) in self.profileData.tasker_languages.enumerated() {
                            if strLanguage == "" {
                                strLanguage = "\(element.language_name)"
                            }
                            else {
                                strLanguage = "\(strLanguage), \(element.language_name)"
                            }
                        }
                        
                        self.languagesLabel.text = strLanguage
                        
                        var strWorkingHr = ""
                        for(_,element) in self.profileData.tasker_working_days.enumerated() {
                            strWorkingHr = "\(element.day_name)"
                            self.selectedWorkingDay = element
                        }
                        
                        for(_,element) in self.profileData.tasker_working_time.enumerated() {
                            strWorkingHr = "\(strWorkingHr), \(element.timeslot_name)"
                            self.selectedWorkingTime = element
                        }
                        
                        self.timingLabel.text = strWorkingHr
                        
                        if self.profileData.tasker_rates.count > 0 {
                            self.fristCatRateLabel.text = "\(self.profileData.tasker_rates[0].rate) BHD/\(self.profileData.tasker_rates[0].is_fixed)"
                            self.firstCatNameLabel.text = "\(self.profileData.tasker_rates[0].subcategory_name)"
                            if self.profileData.tasker_rates.count > 1 {
                                self.secondCatNameLabel.text = "\(self.profileData.tasker_rates[1].subcategory_name)"
                                self.secondCateRateLabel.text = "\(self.profileData.tasker_rates[1].rate) BHD/\(self.profileData.tasker_rates[1].is_fixed)"
                            }
                            if self.profileData.tasker_rates.count > 2 {
                                self.thirdCatNameLabel.text = "\(self.profileData.tasker_rates[2].subcategory_name)"
                                self.thirdCatRateLabel.text = "\(self.profileData.tasker_rates[2].rate) BHD/\(self.profileData.tasker_rates[2].is_fixed)"
                            }
                        }
                    }
                }
            }
        }
    }
}
