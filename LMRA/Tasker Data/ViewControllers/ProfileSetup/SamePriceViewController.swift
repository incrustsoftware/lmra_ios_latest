//
//  SamePriceViewController.swift
//  LMRA
//
//  Created by Mac on 28/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Toast_Swift
import Alamofire
import SwiftyJSON
import RadioButton

class SamePriceViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var topLabe: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var checkBoxButton: UIButton!
    @IBOutlet weak var samepriceLabel: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var samePriceView: UIView!
    @IBOutlet weak var samePriceLabel: UILabel!
    @IBOutlet weak var fixedRateRadioView: UIView!
    @IBOutlet weak var fixedRateLabel: UILabel!
    @IBOutlet weak var fixedRateRadioButton: RadioButton!
    @IBOutlet weak var fixedRateTextView: UIView!
    @IBOutlet weak var minimumRateTextField: UITextField!
    @IBOutlet weak var fixedRateDividerView: UIView!
    @IBOutlet weak var hourlyRateFirstView: UIView!
    @IBOutlet weak var hourlyRateFirstRadioButton: RadioButton!
    @IBOutlet weak var hourlyRateLabel: UILabel!
    @IBOutlet weak var hourlyRateTextView: UIView!
    @IBOutlet weak var hourlyRateTextField: UITextField!
    @IBOutlet weak var hourlyRateDividerView: UIView!
    @IBOutlet weak var hourlyRateSecondRadioView: UIView!
    @IBOutlet weak var hourlyRateRadioButton: RadioButton!
    @IBOutlet weak var hourlyRateSecondLabel: UILabel!
    
    var fixedRateClicked = true
    var hourlyFirstButtonClicked = true
    var hourlySecondButtonClicked = true
    var categoryName = [String]()
    var categoryID = [String]()
    var isHour:Bool = false
    var isFix:Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        getCategorys()
        // SentOTPSMS()
        self.tableView.tableFooterView = UIView()
        self.tableView.reloadData()
        checkBoxButton.setImage(UIImage(named: "tasker_checked"), for: .normal)
        checkBoxButton.isEnabled = false
        self.hourlyRateTextView.isHidden = true
        self.hourlyRateSecondRadioView.isHidden = true
        self.fixedRateTextView.isHidden = true
        fixedRateRadioButton.setImage(UIImage(named: "radioUnselected"), for: .normal)
        hourlyRateRadioButton.setImage(UIImage(named: "radioUnselected"), for: .normal)
        hourlyRateFirstRadioButton.setImage(UIImage(named: "radioUnselected"), for: .normal)
        let nib = UINib(nibName: "SelectCatTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "selectCell")
        self.navigationController?.navigationBar.isHidden = false
        
    }
    func getCategorys()
    {
        let url = ConstantsClass.baseUrl + ConstantsClass.get_tasker_subcategories
        print("url: \(url)")
        guard let auth_key = UserDefaults.standard.string(forKey: "authentication_key") else {
            return
        }
        let headers = [
            "Authorization": "\(auth_key)"
        ]
        Alamofire.request(url
            , method: .get, encoding:JSONEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if (response["status"] as? Bool) != nil {
                        if let respArray = response["response"] as? [[String:Any]] {
                            print("respArray: \(respArray)")
                            
                            for Data in respArray
                            {
                                self.categoryName.append(Data["subcategory_name"] as! String)
                                self.categoryID.append(Data["subcategory_id"]as!String)
                                print(self.categoryName)
                            }
                        }
                    }
                    // Make sure to update UI in main thread
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
    }
    
    @IBAction func fixedRateRadioButtonAction(_ sender: Any) {
        if(fixedRateClicked == false)
        {
            fixedRateRadioButton.setImage(UIImage(named: "radioUnselected"), for: .normal)
            self.fixedRateClicked = true
            self.fixedRateTextView.isHidden = true
            self.hourlyRateSecondRadioView.isHidden = true
            self.hourlyRateFirstView.isHidden = false
            hourlyRateRadioButton.setImage(UIImage(named: "radioUnselected"), for: .normal)
            self.isFix = false
            self.isHour = false
        }
        else {
            fixedRateRadioButton.setImage(UIImage(named: "radioSelected"), for: .normal)
            self.fixedRateClicked = false
            self.fixedRateTextView.isHidden = false
            self.hourlyRateSecondRadioView.isHidden = false
            self.hourlyRateFirstView.isHidden = true
            self.isFix = true
            self.isHour = false
            
        }
    }
    
    @IBAction func hourlyFirstRadioButtonAction(_ sender: Any) {
        
        if(hourlyFirstButtonClicked == false)
        {
            hourlyRateFirstRadioButton.setImage(UIImage(named: "radioUnselected"), for: .normal)
            
            self.hourlyFirstButtonClicked = true
            self.fixedRateTextView.isHidden = true
            self.hourlyRateSecondRadioView.isHidden = true
            self.hourlyRateFirstView.isHidden = false
            self.hourlyRateSecondRadioView.isHidden = true
            self.hourlyRateTextView.isHidden = true
            self.isHour = false
            self.isFix = false
        }
        else {
            hourlyRateFirstRadioButton.setImage(UIImage(named: "radioSelected"), for: .normal)
            self.hourlyFirstButtonClicked = false
            self.fixedRateTextView.isHidden = true
            self.hourlyRateSecondRadioView.isHidden = true
            self.hourlyRateFirstView.isHidden = false
            self.hourlyRateTextView.isHidden = false
            self.isHour = true
            self.isFix = false
        }
        
        
        
    }
    
    @IBAction func hourlySecondRadioButtonAction(_ sender: Any) {
        if(hourlySecondButtonClicked == false)
        {
            hourlyRateRadioButton.setImage(UIImage(named: "radioUnselected"), for: .normal)
            fixedRateRadioButton.setImage(UIImage(named: "radioUnselected"), for: .normal)
            self.hourlySecondButtonClicked = true
            self.fixedRateTextView.isHidden = true
            self.hourlyRateSecondRadioView.isHidden = true
            self.hourlyRateFirstView.isHidden = false
            self.isHour = false
            self.isFix = false
        }
        else {
            hourlyRateRadioButton.setImage(UIImage(named: "radioSelected"), for: .normal)
            self.hourlySecondButtonClicked = false
            self.fixedRateTextView.isHidden = true
            self.hourlyRateFirstView.isHidden = false
            self.hourlyRateFirstView.isHidden = false
            self.hourlyRateSecondRadioView.isHidden = true
            hourlyRateFirstRadioButton.setImage(UIImage(named: "radioSelected"), for: .normal)
            fixedRateRadioButton.setImage(UIImage(named: "radioUnselected"), for: .normal)
            hourlyRateTextView.isHidden = false
            self.isHour = true
            self.isFix = false
            
        }
        
    }

    func getSamePriceForallFixedApi(rate_type:String,minimum_rate:String)
    {
        
        let api:APIEngine = APIEngine()
        api.getSamePriceForallFiexdListAPI(param:["subcategory_id" : (self.categoryID),
                                                  "rate_type":rate_type,
                                                  "minimum_rate":minimum_rate])
        { responseObject, error in
            print(Parameters.self)
            switch responseObject?.result  {
            case .success(let JSON)?:
                print("Success with JSON: \(JSON)")
                let jsonResponse = JSON
                
                if(jsonResponse["status"].bool == true) {
//                    if self.isFromEdit == true {
//                        let storyBoard : UIStoryboard = UIStoryboard(name: "Tasker", bundle:nil)
//                        let vc = storyBoard.instantiateViewController(withIdentifier: "TaskerProfileViewController")as!TaskerProfileViewController
//                        self.navigationController?.pushViewController(vc, animated: true)
//                    }
//                    else if self.isFromEditProfile == true {
//                        let vc:TaskerProfileViewController = self.storyboard?.instantiateViewController(withIdentifier: "TaskerProfileViewController")as!TaskerProfileViewController
//                        vc.isFromEdit = true
//                        self.navigationController?.pushViewController(vc, animated: true)
//                    }
//                    else {
//                        self.navToTimeSlot()
//                    }
                }
                else {
                    Helper.sharedInstance.showAlert(title: "Message", message:jsonResponse["message_code"].stringValue )
                }
            case.failure(let error)?:
                print("error: \(error)")
                
            case .none:
                print("error: ")
            }
        }
    }

    func getSamePriceForallHourlyApi(rate_type:String,minimum_rate:String)
    {
        
        let api:APIEngine = APIEngine()
        api.getSamePriceForallHourlyListAPI(param:["subcategory_id" : (self.categoryID),
                                                   "rate_type":rate_type,
                                                   "minimum_rate":minimum_rate])
        { responseObject, error in
            print(Parameters.self)
            switch responseObject?.result  {
            case .success(let JSON)?:
                print("Success with JSON: \(JSON)")
                let jsonResponse = JSON
                
                if(jsonResponse["status"].bool == true) {
                    self.navToTimeSlot()
                }
                else
                {
                    Helper.sharedInstance.showAlert(title: "Message", message:jsonResponse["message_code"].stringValue )
                }
            case.failure(let error)?:
                print("error: \(error)")
                
            case .none:
                print("error: ")
            }
        }
    }
    @IBAction func checkBoxBtnAction(_ sender: Any) {
        //        if(iconClick == false)
        //               {
        //                   checkBoxButton.setImage(UIImage(named: "tasker_uncheck"), for: .normal)
        //                  self.iconClick = true
        //
        //               }
        //               else {
        //                   checkBoxButton.setImage(UIImage(named: "tasker_checked"), for: .normal)
        //                   self.iconClick = false
        //
        //
        //        }
    }
    
    @IBAction func continueButton(_ sender: Any) {
        if self.isFix == true
        {
            getSamePriceForallFixedApi(rate_type: "Fixed Rate", minimum_rate: self.minimumRateTextField.text!)
        }
        else if self.isHour == true
        {
            getSamePriceForallHourlyApi(rate_type: "Hourly Rate", minimum_rate: self.hourlyRateTextField.text!)
            
        }
        
    }
    func navToTimeSlot()  {
        let nextViewController = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "WorkPreferViewController") as! WorkPreferViewController
        
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    @IBAction func closeBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "selectCell", for: indexPath)as!SelectCatTableViewCell
        
        cell.backgroundColor = UIColor.lightGray
        cell.addrateButton.isHidden = true
        cell.categoryNameLabel.text = categoryName[indexPath.row]
        // cell.categoryNameLabel.text = categoryName[indexPath.row]
        //cell.cellDelegate = self
        return cell
    }
}
