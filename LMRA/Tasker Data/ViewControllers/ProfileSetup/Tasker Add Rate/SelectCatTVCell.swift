//
//  SelectCatTVCell.swift
//  LMRA
//
//  Created by Mac on 09/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class SelectCatTVCell: UITableViewCell {

    @IBOutlet weak var lblCatTitle: UILabel!
    @IBOutlet weak var btnAddRate: UIButton!
    @IBOutlet weak var backView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
