//
//  Tasker_AddRatesViewController.swift
//  LMRA
//
//  Created by Mac on 31/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import RadioButton
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toast_Swift
import TextFieldEffects
// protocol used for sending data back
protocol DataEnteredDelegate: class {
    func userDidEnterInformation(subcategory_id:String, rate_type:String, minimum_rate:String,isPopView:Bool)
    func userEdit(isEdit:Bool,rate_id:String,rate_type:String,minimum_rate:String)
}
class Tasker_AddRatesViewController: UIViewController {
    
    @IBOutlet weak var lblNavTitle: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var minimumRateTextField: HoshiTextField!
    @IBOutlet weak var hourlyRateTextField: HoshiTextField!
    @IBOutlet weak var lblRateFor: UILabel!
    @IBOutlet weak var fixedRateMimBaseView: UIView!
    @IBOutlet weak var hourlyRateMinBaseView: UIView!
    @IBOutlet weak var btnFixedRate: RadioButton!
    @IBOutlet weak var btnHourlyRate: RadioButton!
    
    var categoryName:String = ""
    var categoryID:String = ""
    var rate_id:String = ""
    var titles:String = ""
    var rateType:String = ""
    var rate:String = ""
    var isFix:Bool = false
    var isForEdit:Bool = false
    var isForNewAdd:Bool = false

    weak var delegate: DataEnteredDelegate? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setStatusBarColor(color: .white, styleForStatus: .default)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    func setUI() {
        lblNavTitle.text = isForNewAdd ? "Add Rate" : "Edit Rate"
        continueButton.setTitle(isForNewAdd ? "Confirm" : "Continue", for: .normal)
        if isForEdit {
            if rateType == "Fixed Rate" {
                self.minimumRateTextField.text = rate
                self.fixedRateMimBaseView.isHidden = false
                self.isFix = true
                self.btnHourlyRate.setImage(UIImage(named: "radioUnselected"), for: .normal)
                self.btnFixedRate.setImage(UIImage(named: "radioSelected"), for: .normal)
            }
            else {
                self.hourlyRateTextField.text = rate
                self.hourlyRateMinBaseView.isHidden = false
                self.isFix = false
                self.btnHourlyRate.setImage(UIImage(named: "radioSelected"), for: .normal)
                self.btnFixedRate.setImage(UIImage(named: "radioUnselected"), for: .normal)
            }
        }
        else {
            self.btnHourlyRate.setImage(UIImage(named: "radioUnselected"), for: .normal)
            self.btnFixedRate.setImage(UIImage(named: "radioUnselected"), for: .normal)
        }
        
        
        let font = UIFont(name: "STCForward-Regular", size: 16.0)!
        let font2 = UIFont(name: "STCForward-Bold", size: 16.0)!
        
        let attributes: [NSAttributedString.Key: Any] = [
            .font: font,
            .foregroundColor: UIColor.init(hexStringColor: "#000")
        ]
        let attributes1: [NSAttributedString.Key: Any] = [
            .font: font2,
            .foregroundColor: UIColor.init(hexStringColor: "#000")
        ]
        
        let attributedQuote = NSAttributedString(string: "Rate for ", attributes: attributes)
        let attributedQuote1 = NSAttributedString(string:categoryName, attributes: attributes1)
        let label = NSMutableAttributedString()
        label.append(attributedQuote)
        label.append(attributedQuote1)
        self.lblRateFor.attributedText = label
    }
    
    @IBAction func closeBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func FIXEDRateTxtfiled(_ sender: Any) {
        if self.minimumRateTextField.text!.count > 5 {
            minimumRateTextField.deleteBackward()
        }
        self.buttonValidation()
    }
    
    @IBAction func hourlyTexfieldEditingchanged(_ sender: Any) {
        if self.hourlyRateTextField.text!.count > 5 {
            hourlyRateTextField.deleteBackward()
        }
        self.buttonValidation()
    }
    
    @IBAction func btnFixedRateClickAction(_ sender: Any) {
        self.changeRateFor(fixed: true)
    }
    
    @IBAction func btnHourlyRateClickAction(_ sender: Any) {
        self.changeRateFor(fixed: false)
    }
    
    @IBAction func continueButton(_ sender: Any) {
        if self.isFix == true {
            if isForEdit == true {
                getEditRateApi(rate_id: self.rate_id, rate_type: "Fixed Rate", minimum_rate: self.minimumRateTextField.text!)
            }
            else {
                getAddRateApi(subcategory_id: self.categoryID, rate_type: "Fixed Rate", minimum_rate: minimumRateTextField.text!)
            }
            self.navigationController?.popViewController(animated: true)
        }
        else {
            if isForEdit == true {
                getEditRateApi(rate_id: self.rate_id, rate_type: "Hourly Rate", minimum_rate: self.hourlyRateTextField.text!)
            }
            else {
                getAddRateApi(subcategory_id: self.categoryID, rate_type: "Hourly Rate", minimum_rate: hourlyRateTextField.text!)
            }
            self.navigationController?.popViewController(animated: true)
        }
    }

    //MARK:- ViewController Own Defined Method
    func changeRateFor(fixed:Bool) {
        self.isFix = fixed
        if fixed {
            self.fixedRateMimBaseView.isHidden = false
            self.hourlyRateMinBaseView.isHidden = true
            self.hourlyRateTextField.text = ""
            self.btnFixedRate.setImage(UIImage(named: "radioSelected"), for: .normal)
            self.btnHourlyRate.setImage(UIImage(named: "radioUnselected"), for: .normal)
        }
        else {
            self.fixedRateMimBaseView.isHidden = true
            self.hourlyRateMinBaseView.isHidden = false
            self.minimumRateTextField.text = ""
            self.btnFixedRate.setImage(UIImage(named: "radioUnselected"), for: .normal)
            self.btnHourlyRate.setImage(UIImage(named: "radioSelected"), for: .normal)
        }
        self.buttonValidation()
    }
    func buttonValidation() {
        if self.minimumRateTextField.text!.count > 0 || self.hourlyRateTextField.text!.count > 0 {
            self.continueButton.backgroundColor = Helper.sharedInstance.colorWithHexString(hexString: "00C48C")
            self.continueButton.setTitleColor(UIColor.white, for: .normal)
            continueButton.isEnabled = true
        }
        else {
            self.continueButton.backgroundColor = Helper.sharedInstance.colorWithHexString(hexString: "#F3F3F1")
            self.continueButton.setTitleColor(Helper.sharedInstance.hexStringToUIColor(hex: "#8E9AA0"), for: .normal)
            self.continueButton.isEnabled = false
        }
    }
}

//MARK:- ViewController API Call's
extension Tasker_AddRatesViewController {
    func getAddRateApi(subcategory_id:String,rate_type:String,minimum_rate:String) {
        //self.Get_User_Type =  UserDefaults.standard.integer(forKey: UserDataManager_KEY.userType)
        //let param:[String:String] = ["user_id":"\(Get_User_Type!)"]
        
        let api:APIEngine = APIEngine()
        api.getAddTaskerRateAPI(param:["subcategory_id":subcategory_id,"rate_type":rate_type,"minimum_rate":minimum_rate]){ responseObject, error in
            
            switch responseObject?.result  {
            case .success(let JSON)?:
                print("Success with JSON: \(JSON)")
                let jsonResponse = JSON
                
                if(jsonResponse["status"].bool == true) {
                    if self.isFix == true{
                        self.delegate?.userDidEnterInformation(subcategory_id: self.categoryID, rate_type: "Fixed Rate", minimum_rate: self.minimumRateTextField.text!,isPopView: true)
                        
                    }
                    else{
                        self.delegate?.userDidEnterInformation(subcategory_id: self.categoryID, rate_type: "Hourly Rate", minimum_rate: self.hourlyRateTextField.text!,isPopView: true)
                    }
                }
                else
                {
                    Helper.sharedInstance.showAlert(title: "Message", message:jsonResponse["message_code"].stringValue )
                }
            case.failure(let error)?:
                print("error: \(error)")
                
            case .none:
                print("error: ")
            }
        }
    }
    
    func getEditRateApi(rate_id:String,rate_type:String,minimum_rate:String) {
        let api:APIEngine = APIEngine()
        api.getUpdateTaskerRateAPI(param:["rate_id":rate_id,"rate_type":rate_type,"minimum_rate":minimum_rate]){ responseObject, error in
            switch responseObject?.result  {
            case .success(let JSON)?:
                print("Success with JSON: \(JSON)")
                let jsonResponse = JSON
                
                if(jsonResponse["status"].bool == true) {
                    // let parse:Parser = Parser()
                    if self.isFix == true {
                        self.delegate?.userEdit(isEdit: true, rate_id: self.rate_id,rate_type:"Fixed Rate",minimum_rate:self.minimumRateTextField.text!  )
                    }
                    else {
                        self.delegate?.userEdit(isEdit: true, rate_id: self.rate_id,rate_type:"Hourly Rate",minimum_rate:self.hourlyRateTextField.text!)
                    }
                }
                else {
                    Helper.sharedInstance.showAlert(title: "Message", message:jsonResponse["message_code"].stringValue )
                }
            case.failure(let error)?:
                print("error: \(error)")
                
            case .none:
                print("error: ")
            }
        }
    }
}
