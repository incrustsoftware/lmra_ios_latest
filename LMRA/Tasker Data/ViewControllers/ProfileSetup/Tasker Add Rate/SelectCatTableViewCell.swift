//
//  SelectCatTableViewCell.swift
//  LMRA
//
//  Created by Mac on 02/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
protocol SelectCatTableViewCellDelegate: class {
    func didTapButton(cell: SelectCatTableViewCell)
}
class SelectCatTableViewCell: UITableViewCell {

    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var superView: UIView!
    @IBOutlet weak var categoryNameLabel: UILabel!
    
    @IBOutlet weak var addrateButton: UIButton!
    weak var cellDelegate: SelectCatTableViewCellDelegate?
    
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var rateTypeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnTapped(_ sender: Any) {
        cellDelegate?.didTapButton(cell: self)
    }
    
    
}
