//
//  SelectCatEditTVCell.swift
//  LMRA
//
//  Created by Mac on 09/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class SelectCatEditTVCell: UITableViewCell {

    @IBOutlet weak var lblCatTitle: UILabel!
    @IBOutlet weak var lblRateType: UILabel!
    @IBOutlet weak var lblRate: UILabel!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var btnEdit: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
