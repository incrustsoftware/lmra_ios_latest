//
//  Tasker_AddRateViewController.swift
//  LMRA
//
//  Created by Mac on 28/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Toast_Swift
import Alamofire
import SwiftyJSON
import RadioButton
import TextFieldEffects

class Tasker_AddRateViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,SelectCatTableViewCellDelegate,DataEnteredDelegate,AddRateTableViewCellDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate{
    
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var lblViewTitle: UILabel!
    @IBOutlet weak var tblHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var topLabe: UILabel!
    @IBOutlet weak var topLabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var checkBoxButton: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var fixedRateRadioView: UIView!
    @IBOutlet weak var fixedRateRadioButton: RadioButton!
    @IBOutlet weak var fixedRateTextView: UIView!
    @IBOutlet weak var minimumRateTextField: HoshiTextField!
    @IBOutlet weak var fixedRateDividerView: UIView!
    @IBOutlet weak var hourlyRateFirstView: UIView!
    @IBOutlet weak var hourlyRateFirstRadioButton: RadioButton!
    @IBOutlet weak var hourlyRateTextView: UIView!
    @IBOutlet weak var hourlyRateTextField: HoshiTextField!
    @IBOutlet weak var samePriceStackView: UIStackView!
    
    var isForEdit:Bool!//Already added and going to edit
    var isForEditFirstTime:Bool!//First time confirmation and edit
    
    var ispoped:Bool = false
    var ratedAddedListArray:[RateAddedList] = []
    var Editing: Bool = false
    var index:Int = 0
    //var isFromEdit:Bool = false
    //var isFromEditProfile:Bool = false
    var indexArray:[Int] = []
    var AddrateArray:[AddRate] = []
    var rateId:String = ""
    var rate:String = ""
    var isfixed:String = ""
    var dic:[String:Any] = [:]
    var fixedRateClicked = true
    
    var rateDigit:Int = 11
    var isHour:Bool = false
    var isFix:Bool = false
    let delayTime = DispatchTime.now() + 3.0
    
    var iconClick = false
    
    var getList:[GetSubCategoryList] = []
    var categoryName = [String]()
    var categoryID = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isFix = false
        if isForEdit != nil && isForEdit{
            self.progressView.isHidden = true
            self.topLabelHeightConstraint.constant = 0
            self.lblViewTitle.isHidden = false
            self.continueButton.setTitle("update", for: .normal)
        }
        else {
            self.progressView.isHidden = false
            self.topLabelHeightConstraint.constant = 81
            self.lblViewTitle.isHidden = true
        }
        //  getTaskerSubCategoryList()
        getCategorys()
        self.tableView.tableFooterView = UIView()
        
        let nib = UINib(nibName: "SelectCatTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "selectCell")
        
        let nib1 = UINib(nibName: "SelectCatTVCell", bundle: nil)
        self.tableView.register(nib1, forCellReuseIdentifier: "SelectCatTVCell")
        
        let nib2 = UINib(nibName: "SelectCatEditTVCell", bundle: nil)
        self.tableView.register(nib2, forCellReuseIdentifier: "SelectCatEditTVCell")
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableView.automaticDimension
        
        self.samePriceStackView.isHidden = true
        self.tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setStatusBarColor(color: .white, styleForStatus: .default)
        setNeedsStatusBarAppearanceUpdate()
        
        if isForEdit != nil && isForEdit {
            continueBtnUnaibleDisable(isEnable:true)
            self.continueButton.setTitle("Update", for: .normal)
        }
        else if self.minimumRateTextField.text!.count > 0 || self.hourlyRateTextField.text!.count > 0 {
            continueBtnUnaibleDisable(isEnable:true)
        }
        else {
            continueBtnUnaibleDisable(isEnable:false)
        }
    }
    
    override func viewWillLayoutSubviews() {
        self.updateContaintSize()
    }
    
    //MARK:- ViewController IBAction's
    @IBAction func fixedRateRadioButtonAction(_ sender: Any) {
        hourlyRateFirstRadioButton.tag = 0
        self.isHour = false
        self.hourlyRateTextField.text = ""
        
        if((sender as AnyObject).tag == 0) {
            fixedRateRadioButton.setImage(UIImage(named: "radioSelected"), for: .normal)
            hourlyRateFirstRadioButton.setImage(UIImage(named: "radioUnselected"), for: .normal)
            fixedRateRadioButton.tag = 1
            hourlyRateTextView.isHidden = true
            fixedRateTextView.isHidden = false
            
            self.isFix = true
        }
        else {
            fixedRateRadioButton.setImage(UIImage(named: "radioUnselected"), for: .normal)
            hourlyRateFirstRadioButton.setImage(UIImage(named: "radioUnselected"), for: .normal)
            fixedRateRadioButton.tag = 0
            hourlyRateTextView.isHidden = true
            fixedRateTextView.isHidden = true
            
            self.isFix = false
        }
    }
    
    @IBAction func hourlyFirstRadioButtonAction(_ sender: Any) {
        fixedRateRadioButton.tag = 0
        self.isFix = false
        self.minimumRateTextField.text = ""
        
        if((sender as AnyObject).tag == 0) {
            //Hourly RateView
            hourlyRateFirstRadioButton.setImage(UIImage(named: "radioSelected"), for: .normal)
            
            hourlyRateFirstRadioButton.tag = 1
            hourlyRateTextView.isHidden = false
            
            //Fixed RateView
            fixedRateTextView.isHidden = true
            fixedRateRadioButton.setImage(UIImage(named: "radioUnselected"), for: .normal)
            
            self.isHour = true
        }
        else {
            //Hourly RateView
            hourlyRateFirstRadioButton.setImage(UIImage(named: "radioUnselected"), for: .normal)
            hourlyRateFirstRadioButton.tag = 0
            hourlyRateTextView.isHidden = true
            
            //Fixed RateView
            fixedRateTextView.isHidden = true
            fixedRateRadioButton.setImage(UIImage(named: "radioUnselected"), for: .normal)
            
            self.isHour = false
        }
    }
    
    @IBAction func checkBoxBtnAction(_ sender: Any) {
        self.hourlyRateTextField.text = ""
        self.minimumRateTextField.text = ""
        
        self.isFix = false
        self.isHour = false
        self.hourlyRateFirstRadioButton.setImage(UIImage(named: "radioUnselected"), for: .normal)
        self.fixedRateRadioButton.setImage(UIImage(named: "radioUnselected"), for: .normal)
        self.fixedRateTextView.isHidden = true
        self.hourlyRateTextView.isHidden = true
        self.hourlyRateFirstRadioButton.tag = 0
        self.fixedRateRadioButton.tag = 0
        continueBtnUnaibleDisable(isEnable:false)
        
        if (sender as AnyObject).tag == 0 {
            (sender as! UIButton).tag = 1
            
            self.iconClick = true
            
            checkBoxButton.setImage(UIImage(named: "tasker_checked"), for: .normal)
            self.samePriceStackView.isHidden = false
            self.tableView.reloadData()
        }
        else {
            (sender as! UIButton).tag = 0
            
            self.iconClick = false
            self.checkAllCategoryAmountEnter()
            
            checkBoxButton.setImage(UIImage(named: "tasker_uncheck"), for: .normal)
            self.samePriceStackView.isHidden = true
            self.tableView.reloadData()
        }
    }
    
    @IBAction func hourlyDidChanged(_ sender: Any) {
        if iconClick == true && !(self.hourlyRateTextField.text!.isBlank) {
            continueBtnUnaibleDisable(isEnable:true)
        }
        else {
            continueBtnUnaibleDisable(isEnable:false)
        }
        
        if self.hourlyRateTextField.text!.count > 5 {
            hourlyRateTextField.deleteBackward()
        }
    }
    
    @IBAction func minimumRateLabelTextField(_ sender: Any) {
        if iconClick == true && !(self.minimumRateTextField.text!.isBlank) {
            continueBtnUnaibleDisable(isEnable:true)
        }
        else {
            continueBtnUnaibleDisable(isEnable:false)
        }
        
        if self.minimumRateTextField.text!.count > 5 {
            minimumRateTextField.deleteBackward()
        }
    }
    
    @IBAction func continueButton(_ sender: Any) {
        if self.isFix == true{
            getSamePriceForallFixedApi(rate_type: "Fixed Rate", minimum_rate: self.minimumRateTextField.text!)
        }
        else if self.isHour == true {
            self.getSamePriceForallHourlyApi(rate_type: "Hourly Rate", minimum_rate: self.hourlyRateTextField.text!)
        }
        else {
            if isForEdit != nil && isForEdit {
                self.navigationController?.popViewController(animated: true)
            }
            else {
                self.navToTimeSlot()
            }
        }
    }
    
    @IBAction func btnBackClickAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCloseClickAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- ViewController Own Defined methods
    func userDidEnterInformation(subcategory_id: String, rate_type: String, minimum_rate: String,isPopView:Bool) {
        self.AddrateArray.removeAll()
        self.getCategorys()
    }
    
    func userEdit(isEdit: Bool, rate_id: String, rate_type: String, minimum_rate: String) {
        self.AddrateArray.removeAll()
        self.getCategorys()
        self.Editing = isEdit
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let str = (minimumRateTextField.text! + text)
        if str.count <= 5 {
            return true
        }
        textView.text = str.substring(to: str.index(str.startIndex, offsetBy: 10))
        return false
    }
    
    func navToTimeSlot() {
        let nextViewController = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "WorkPreferViewController") as! WorkPreferViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    func didTapButton(cell: SelectCatTableViewCell) {
        if let indexPath = tableView.indexPath(for: cell) {
            print(indexPath.row)
            let temp:AddRate = AddrateArray[indexPath.row]
            if temp.dic.count == 0 {
                let vc = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "Tasker_AddRatesViewController")as!Tasker_AddRatesViewController
                //let temp:String = categoryName[indexPath.row]
                vc.categoryName = temp.subcategory_name
                vc.categoryID = temp.subcategory_id
                vc.isForEdit = false
                vc.delegate = self
                vc.titles = "Add Rates"
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else {
                let vc = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "Tasker_AddRatesViewController")as!Tasker_AddRatesViewController
                // let temp:AddRate = a[indexPath.row]
                vc.categoryName = temp.subcategory_name
                vc.rate_id = temp.rate_id
                vc.isForEdit = true
                
                vc.delegate = self
                vc.titles = "Edit Rates"
                vc.rate = temp.rate
                vc.rateType = temp.is_fixed
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func didTapButton(cell: AddRateTableViewCell) {
        if let indexPath = tableView.indexPath(for: cell) {
            
        }
    }
    
    @objc func addRate(_ sender: Any) {
        if let temp:AddRate = Optional.some(AddrateArray[(sender as AnyObject).tag]) {
            let vc = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "Tasker_AddRatesViewController")as!Tasker_AddRatesViewController
            //let temp:String = categoryName[indexPath.row]
            vc.categoryName = temp.subcategory_name
            vc.categoryID = temp.subcategory_id
            vc.isForEdit = false
            
            if (isForEditFirstTime != nil && isForEditFirstTime) || (isForEdit != nil && isForEdit) {
                vc.isForNewAdd = false
            }
            else {
                vc.isForNewAdd = true
            }
            
            vc.delegate = self
            vc.titles = "Add Rates"
            print(index)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func editRate(_ sender: Any) {
        if let temp:AddRate = Optional.some(AddrateArray[(sender as AnyObject).tag]) {
            let vc = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "Tasker_AddRatesViewController")as!Tasker_AddRatesViewController
            // let temp:AddRate = a[indexPath.row]
            vc.rate_id = temp.rate_id
            vc.isForEdit = true
            
            if (isForEditFirstTime != nil && isForEditFirstTime) || (isForEdit != nil && isForEdit) {
                vc.isForNewAdd = false
            }
            else {
                vc.isForNewAdd = true
            }
            
            vc.categoryName = temp.subcategory_name
            vc.delegate = self
            vc.titles = "Edit Rates"
            vc.rate = temp.rate
            vc.rateType = temp.is_fixed
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func continueBtnUnaibleDisable(isEnable:Bool) {
        if isEnable {
            self.continueButton.isEnabled = true
            self.continueButton.backgroundColor = Helper.sharedInstance.colorWithHexString(hexString: "00C48C")
            self.continueButton.setTitleColor(UIColor.white, for: .normal)
        }
        else {
            self.continueButton.isEnabled = false
            self.continueButton.backgroundColor = Helper.sharedInstance.colorWithHexString(hexString: "#F3F3F1")
            self.continueButton.setTitleColor(Helper.sharedInstance.hexStringToUIColor(hex: "#8E9AA0"), for: .normal)
        }
    }
    
    func checkAllCategoryAmountEnter() {
        var allAmountFill = false
        for (_,element) in self.AddrateArray.enumerated() {
            if element.dic.count == 0  {
                allAmountFill = true
            }
        }
        
        if allAmountFill == false && self.iconClick == false{
            self.continueBtnUnaibleDisable(isEnable:true)
        }
    }
    
    func updateContaintSize(){
        DispatchQueue.main.async {
            var contentRect = CGRect.zero
            
            for view in self.scrollView.subviews {
                contentRect = contentRect.union(view.frame)
            }
            
            self.scrollView.contentSize.height = contentRect.size.height
        }
    }
}

//MARK:- Tableview Delegate methods
extension Tasker_AddRateViewController {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AddrateArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let temp:AddRate = AddrateArray[indexPath.row]
        if temp.dic.count > 0 {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "SelectCatEditTVCell", for: indexPath)as!SelectCatEditTVCell
            cell.lblRate.text = temp.rate+" "+"BHD"
            cell.lblCatTitle.text = temp.subcategory_name
            cell.lblRateType.text = temp.is_fixed
            
            cell.btnEdit.tag = indexPath.row
            cell.btnEdit.addTarget(self, action: #selector(editRate(_:)), for: .touchUpInside)
            if iconClick == true {
                cell.backView.backgroundColor = UIColor(named: "colorTaskerSilver")
                cell.btnEdit.isHidden = true
            }
            else {
                cell.backView.backgroundColor = UIColor(named: "colorTaskerIndigo")
                cell.btnEdit.isHidden = false
            }
            return cell
        }
        else {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "SelectCatTVCell", for: indexPath)as!SelectCatTVCell
            cell.btnAddRate.tag = indexPath.row
            cell.lblCatTitle.text = temp.subcategory_name
            cell.btnAddRate.addTarget(self, action: #selector(addRate(_:)), for: .touchUpInside)
            
            if iconClick == true {
                cell.backView.backgroundColor = UIColor(named: "colorTaskerSilver")
                cell.btnAddRate.isHidden = true
            }
            else {
                cell.backView.backgroundColor = UIColor(named: "colorTaskerIndigo")
                cell.btnAddRate.isHidden = false
            }
            return cell
        }
    }
}

//MARK:- ViewController API Call's
extension Tasker_AddRateViewController {
    func getCategorys() {
        //        let url = ConstantsClass.baseUrl + ConstantsClass.get_tasker_subcategories
        //        print("url: \(url)")
        //        guard let auth_key = UserDefaults.standard.string(forKey: "authentication_key") else {
        //            return
        //        }
        //        let headers = [
        //            "Authorization": "\(auth_key)"
        //        ]
        
        APICallManager.shared.getTaskerSubcategoriesAPi(subUrl: ConstantsClass.get_tasker_subcategories, parameter: [:], isLoadingIndicatorShow: true) { (response) in
            DispatchQueue.main.async {
                if (response["status"] as? Bool) != nil {
                    if let respArray = response["response"] as? [[String:Any]] {
                        print("respArray: \(respArray)")
                        //self.setUI()
                        self.categoryID.removeAll()
                        self.categoryName.removeAll()
                        
                        respArray.forEach({ (item) in
                            self.categoryName.append(item["subcategory_name"] as! String)
                            self.categoryID.append(item["subcategory_id"]as!String)
                            print(self.categoryName)
                            guard let categoryName = item["subcategory_name"] as? String else {
                                return
                            }
                            guard let categoryID = item["subcategory_id"] as? String else {
                                return
                            }
                            if let dic = (item["rate_data"]as?[String:Any]) {
                                self.dic = dic
                                print("Dictionary\(dic)")
                                if let rate_id = dic["rate_id"]as? String {
                                    self.rateId = rate_id
                                    print("authentication_key: \(rate_id)")
                                }
                                if let rate = dic["rate"]as? String {
                                    self.rate = rate
                                    print("rate: \(rate)")
                                }
                                if let isfixed = dic["is_fixed"]as? String {
                                    self.isfixed = isfixed
                                    print("isfixed: \(isfixed)")
                                }
                            } else {
                                print("empty dictionary.")
                            }
                            self.AddrateArray.append(AddRate(rate_id: self.rateId, is_fixed: self.isfixed, rate: self.rate, subcategory_name: categoryName, subcategory_id: categoryID,dic:self.dic))
                        })
                        print("AddrateArray: \(self.AddrateArray)")
                        
                    }
                }
                
                self.checkAllCategoryAmountEnter()
                
                // Make sure to update UI in main thread
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3, execute: {self.tblHeightConstraint.constant = self.tableView.contentSize.height})
                }
                
            }
        }
        
        //        Alamofire.request(url
        //            , method: .get, encoding:JSONEncoding.default,headers: headers).responseJSON { response in
        //                if let response = response.result.value as? [String:Any] {
        //                    Helper.sharedInstance.showLoader()
        //                    print("response: \(response)")
        //
        //                    if (response["status"] as? Bool) != nil {
        //                        if let respArray = response["response"] as? [[String:Any]] {
        //                            print("respArray: \(respArray)")
        //                            //self.setUI()
        //                            self.categoryID.removeAll()
        //                            self.categoryName.removeAll()
        //
        //                            respArray.forEach({ (item) in
        //                                self.categoryName.append(item["subcategory_name"] as! String)
        //                                self.categoryID.append(item["subcategory_id"]as!String)
        //                                print(self.categoryName)
        //                                guard let categoryName = item["subcategory_name"] as? String else {
        //                                    return
        //                                }
        //                                guard let categoryID = item["subcategory_id"] as? String else {
        //                                    return
        //                                }
        //                                if let dic = (item["rate_data"]as?[String:Any]) {
        //                                    self.dic = dic
        //                                    print("Dictionary\(dic)")
        //                                    if let rate_id = dic["rate_id"]as? String {
        //                                        self.rateId = rate_id
        //                                        print("authentication_key: \(rate_id)")
        //                                    }
        //                                    if let rate = dic["rate"]as? String {
        //                                        self.rate = rate
        //                                        print("rate: \(rate)")
        //                                    }
        //                                    if let isfixed = dic["is_fixed"]as? String {
        //                                        self.isfixed = isfixed
        //                                        print("isfixed: \(isfixed)")
        //                                    }
        //                                } else {
        //                                    print("empty dictionary.")
        //                                }
        //                                self.AddrateArray.append(AddRate(rate_id: self.rateId, is_fixed: self.isfixed, rate: self.rate, subcategory_name: categoryName, subcategory_id: categoryID,dic:self.dic))
        //                            })
        //                            print("AddrateArray: \(self.AddrateArray)")
        //
        //                        }
        //                    }
        //
        //                    self.checkAllCategoryAmountEnter()
        //
        //                    // Make sure to update UI in main thread
        //                    DispatchQueue.main.async {
        //                        self.tableView.reloadData()
        //                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3, execute: {self.tblHeightConstraint.constant = self.tableView.contentSize.height})
        //                        Helper.sharedInstance.hideLoader()
        //                    }
        //                }
        //                else {
        //                    print("Error occured") // serialized json response
        //                }
        //        }
    }
    
    func getEditRateApi(rate_id:String,rate_type:String,minimum_rate:String) {
        //self.Get_User_Type =  UserDefaults.standard.integer(forKey: UserDataManager_KEY.userType)
        //let param:[String:String] = ["user_id":"\(Get_User_Type!)"]
        
        let api:APIEngine = APIEngine()
        api.getUpdateTaskerRateAPI(param:["rate_id":rate_id,"rate_type":rate_type,"minimum_rate":minimum_rate]){ responseObject, error in
            
            switch responseObject?.result  {
            case .success(let JSON)?:
                print("Success with JSON: \(JSON)")
                let jsonResponse = JSON
                
                if(jsonResponse["status"].bool == true) {
                    let parse:Parser = Parser()
                    self.ratedAddedListArray =  parse.parseRateAddedList(data: JSON).reversed()
                    print("subCat:\(self.ratedAddedListArray)")
                    self.tableView.reloadData()
                }
                else {
                    Helper.sharedInstance.showAlert(title: "Message", message:jsonResponse["message_code"].stringValue )
                }
            case.failure(let error)?:
                print("error: \(error)")
                
            case .none:
                print("error: ")
            }
        }
    }
    
    func getAddRateApi(subcategory_id:String,rate_type:String,minimum_rate:String) {
        //self.Get_User_Type =  UserDefaults.standard.integer(forKey: UserDataManager_KEY.userType)
        //let param:[String:String] = ["user_id":"\(Get_User_Type!)"]
        
        let api:APIEngine = APIEngine()
        api.getAddTaskerRateAPI(param:["subcategory_id":subcategory_id,"rate_type":rate_type,"minimum_rate":minimum_rate]){ responseObject, error in
            
            switch responseObject?.result  {
            case .success(let JSON)?:
                print("Success with JSON: \(JSON)")
                let jsonResponse = JSON
                
                if(jsonResponse["status"].bool == true) {
                    let parse:Parser = Parser()
                    self.ratedAddedListArray =  parse.parseRateAddedList(data: JSON).reversed()
                    print("subCat:\(self.ratedAddedListArray)")
                    self.tableView.reloadData()
                    
                }
                else {
                    Helper.sharedInstance.showAlert(title: "Message", message:jsonResponse["message_code"].stringValue )
                }
            case.failure(let error)?:
                print("error: \(error)")
                
            case .none:
                print("error: ")
            }
        }
    }
    
    func getSamePriceForallFixedApi(rate_type:String,minimum_rate:String) {
        
        let api:APIEngine = APIEngine()
        api.getSamePriceForallFiexdListAPI(param:["subcategory_id" : (self.categoryID),
                                                  "rate_type":rate_type,
                                                  "minimum_rate":minimum_rate]) { responseObject, error in
                                                    print(Parameters.self)
                                                    switch responseObject?.result  {
                                                    case .success(let JSON)?:
                                                        print("Success with JSON: \(JSON)")
                                                        let jsonResponse = JSON
                                                        
                                                        if(jsonResponse["status"].bool == true) {
                                                            if (self.isForEdit != nil && self.isForEdit) || (self.isForEditFirstTime != nil && self.isForEditFirstTime){
                                                                self.navigationController?.popViewController(animated: true)
                                                            }
                                                            else {
                                                                self.navToTimeSlot()
                                                            }
                                                        }
                                                        else {
                                                            Helper.sharedInstance.showAlert(title: "Message", message:jsonResponse["message_code"].stringValue )
                                                        }
                                                    case.failure(let error)?:
                                                        print("error: \(error)")
                                                        
                                                    case .none:
                                                        print("error: ")
                                                    }
        }
    }
    
    func getSamePriceForallHourlyApi(rate_type:String,minimum_rate:String) {
        let api:APIEngine = APIEngine()
        api.getSamePriceForallHourlyListAPI(param:["subcategory_id" : (self.categoryID),
                                                   "rate_type":rate_type,
                                                   "minimum_rate":minimum_rate]) { responseObject, error in
                                                    print(Parameters.self)
                                                    switch responseObject?.result  {
                                                    case .success(let JSON)?:
                                                        print("Success with JSON: \(JSON)")
                                                        let jsonResponse = JSON
                                                        
                                                        if(jsonResponse["status"].bool == true) {
                                                            if (self.isForEdit != nil && self.isForEdit) || (self.isForEditFirstTime != nil && self.isForEditFirstTime){
                                                                self.navigationController?.popViewController(animated: true)
                                                            }
                                                            else {
                                                                self.navToTimeSlot()
                                                            }
                                                        }
                                                        else {
                                                            Helper.sharedInstance.showAlert(title: "Message", message:jsonResponse["message_code"].stringValue )
                                                        }
                                                    case.failure(let error)?:
                                                        print("error: \(error)")
                                                        
                                                    case .none:
                                                        print("error: ")
                                                    }
        }
    }
}
