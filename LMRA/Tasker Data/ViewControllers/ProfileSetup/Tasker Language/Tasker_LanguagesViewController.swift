//
//  Tasker_LanguagesViewController.swift
//  LMRA
//
//  Created by Mac on 04/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Toast_Swift
import FittedSheets
class Tasker_LanguagesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var lblViewTitle: UILabel!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var topLabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var otherLanguageView: UIView!
    @IBOutlet var otherLanguageBottomView: UIView!
    @IBOutlet weak var otherLanguageBottomBaseView: UIView!
    @IBOutlet weak var otherLanguageBackView: UIView!
    @IBOutlet weak var otherLanguageTextField: UITextField!
    @IBOutlet weak var dividerView: UIView!
    @IBOutlet weak var tblHighPriority: UITableView!
    @IBOutlet weak var dropDownBtn: UIButton!
    @IBOutlet weak var tblOtherLanguage: UITableView!
    @IBOutlet weak var tblPriorityLangHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var progressView: UIView!
    
    var isForEdit:Bool!
    var isForEditFirstTime:Bool!
    
    var authentication_key_val = ""
    var Language_Name = ""
    var Language_Id:Int!
    var languagePriority:String = ""
    
    var arrLanguage_priority:[LanguageResponseDataModel] = [LanguageResponseDataModel]()
    var arrLanguageOther:[LanguageResponseDataModel] = [LanguageResponseDataModel]()
    var temparrLanguageOther:[LanguageResponseDataModel] = [LanguageResponseDataModel]()
    
    var selectedTasker_languages:[TaskerProfileLanguageDataModel]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setStatusBarColor(color: .white, styleForStatus: .default)
        if isForEdit != nil && isForEdit{
            self.progressView.isHidden = true
            self.lblViewTitle.isHidden = false
            self.topLabelHeightConstraint.constant = 40
            self.topLabel.font = UIFont(name:"STCForward-Bold",size:16)
            self.topLabel.text = "Spoken languages"
            //Spoken languages
        }
        else {
            self.progressView.isHidden = false
            self.lblViewTitle.isHidden = true
            self.topLabelHeightConstraint.constant = 81
            self.topLabel.font = UIFont(name:"STCForward-Bold",size:30)
            self.topLabel.text = "Which languages do you speak ?"
        }
        
        self.isLanguageSelectedEnableContineBtn()
        
        self.tblHighPriority.tableFooterView = UIView()
        
        let nib = UINib(nibName: "LanguagesTableViewCell", bundle: nil)
        self.tblHighPriority.register(nib, forCellReuseIdentifier: "langCell")
        self.tblHighPriority.reloadData()
        self.authentication_key_val = UserDataManager.shared.getAccesToken()
        get_languages()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setStatusBarColor(color: .white, styleForStatus: .default)
        setNeedsStatusBarAppearanceUpdate()
        NotificationCenter.default.addObserver(self, selector: #selector(self.SelectedDataFromFilter), name: NSNotification.Name(rawValue: "SelectedFilterData"), object: nil)
    }
    
    override func viewDidLayoutSubviews() {
        otherLanguageBottomBaseView.roundCorners(corners: [.topLeft, .topRight], radius: 30)
    }
    
    //MARK:- ViewController IBAction's
    @IBAction func btnOtherLangauageCloseClickAction(_ sender: Any) {
        self.removeOtherLanguageViewWithoutSave()
    }
    
    @IBAction func btnOtherLanguageConfirmClickAction(_ sender: Any) {
        self.removeOtherLanguageView()
    }
    
    @IBAction func fullDropDownButtonAction(_ sender: Any) {
        temparrLanguageOther.append(contentsOf: arrLanguageOther)
        
        self.tblOtherLanguage.reloadData()
        self.otherLanguageBottomView.frame = self.view.frame
        self.view.addSubview(otherLanguageBottomView)
        
        let tapGesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.touchHappen(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        self.otherLanguageBackView.addGestureRecognizer(tapGesture)
        
        let panGesture:UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handleDismiss(_:)))
        panGesture.delegate = self
        self.otherLanguageBottomView.addGestureRecognizer(panGesture)
        
        //        let controller = BottomsheetViewController()
        //        strings.removeAll()
        //        stringsLang.removeAll()
        //        let sheetController = SheetViewController(controller: controller)
        //        self.present(sheetController, animated: true, completion: nil)
    }
    
    @IBAction func btnBackClickAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCloseClickAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func dropDownBtnAction(_ sender: Any) {
        //        let controller = BottomsheetViewController()
        //        strings.removeAll()
        //        //stringsLang.removeAll()
        //        let sheetController = SheetViewController(controller: controller)
        //        self.present(sheetController, animated: true, completion: nil)
    }
    
    @IBAction func continueBtnAction(_ sender: Any) {
        
        let selectedLang = self.getSelectedLangData()
        if selectedLang.count > 0 {
            self.updateLanguagesApi(arrayLang: selectedLang)
        }
        else {
            self.view.makeToast("please select Languages")
        }
        //        if strings.count > 0 {
        //            self.updateLanguagesApi(arrayLang: strings)
        //        }
        //        else if self.GetLangID_Array.count > 0 {
        //            self.updateLanguagesApi(arrayLang: self.GetLangID_Array)
        //        }
        //        else {
        //            self.view.makeToast("please select Languages")
        //        }
    }
    
    //MARK:- ViewController Own Defined Methods
    var viewTranslation = CGPoint(x: 0, y: 0)
    @objc func handleDismiss(_ sender: UIPanGestureRecognizer) {
        switch sender.state {
        case .changed:
            let velocity = sender.velocity(in: otherLanguageBottomView)
            if(velocity.y > 0) {
                viewTranslation = sender.translation(in: otherLanguageBottomView)
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.otherLanguageBottomView.transform = CGAffineTransform(translationX: 0, y: self.viewTranslation.y)
                })
            }
            
        case .ended:
            let velocity = sender.velocity(in: otherLanguageBottomView)
            if(velocity.y > 0) {
                if viewTranslation.y < 50 {
                    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                        self.otherLanguageBottomView.transform = .identity
                    })
                } else {
                    self.otherLanguageBottomView.transform = .identity
                    viewTranslation = CGPoint(x: 0, y: 0)
                    self.removeOtherLanguageViewWithoutSave()
                }
            }
        default:
            break
        }
    }
    
    @objc func touchHappen(_ sender: UITapGestureRecognizer) {
        self.removeOtherLanguageViewWithoutSave()
    }
    
    @objc func buttonPressed(sender:UIButton){
        var data = arrLanguage_priority[sender.tag]
        data.isSelected = !data.isSelected
        arrLanguage_priority[sender.tag] = data
        print("Selected item in row \(sender.tag)")
        self.tblHighPriority.reloadData()
        
        self.isLanguageSelectedEnableContineBtn()
    }
    
    @objc func otherlanguagebuttonPressed(sender:UIButton){
        var data = temparrLanguageOther[sender.tag]
        data.isSelected = !data.isSelected
        temparrLanguageOther[sender.tag] = data
        print("Selected item in row \(sender.tag)")
        self.tblOtherLanguage.reloadData()
    }
    
    @objc func SelectedDataFromFilter(_ notification: NSNotification) {
        //        self.GetLangNameArrayVal = notification.userInfo?["SelectedLangNameArrayValue"] as? [String] ?? [""]
        //        self.GetSelectedLangIDArray = notification.userInfo?["SelectedLangIDArrayValue"] as? [String] ?? [""]
        //
        //        print("GetLangNameArrayVal:",GetLangNameArrayVal)
        //        print("GetSelectedLangIDArray:",GetSelectedLangIDArray)
        //
        //        self.otherLanguageTextField.text =  GetLangNameArrayVal.joined(separator: ",")
        
    }
    
    func isLanguageSelectedEnableContineBtn() {
        let arrSelectedLanguage:[String] = getPriortySelectedLangData()
        
        if arrSelectedLanguage.count > 0 {
            self.continueBtn.isEnabled = true
            self.continueBtn.backgroundColor = UIColor.init(named: "colorTaskerGreen")
            self.continueBtn.setTitleColor(UIColor.white, for: .normal)
        }
        else {
            self.continueBtn.isEnabled = false
            self.continueBtn.backgroundColor = UIColor.init(named: "colorTaskerLightGray")
            self.continueBtn.setTitleColor(UIColor.white, for: .normal)
        }
    }
    
    func getPriortySelectedLangData()->[String] {
        var arrSelectedLanguage:[String] = [String]()
        for (_,element) in arrLanguage_priority.enumerated() {
            if element.isSelected == true {
                arrSelectedLanguage.append("\(element.language_id)")
            }
        }
        return arrSelectedLanguage
    }
    
    func getSelectedLangData()->[String] {
        var arrSelectedLanguage:[String] = [String]()
        for (_,element) in arrLanguage_priority.enumerated() {
            if element.isSelected == true {
                arrSelectedLanguage.append("\(element.language_id)")
            }
        }
        
        for (_,element) in arrLanguageOther.enumerated() {
            if element.isSelected == true {
                arrSelectedLanguage.append("\(element.language_id)")
            }
        }
        return arrSelectedLanguage
    }
    
    func removeOtherLanguageView() {
        self.otherLanguageTextField.text = ""
        self.arrLanguageOther.removeAll()
        
        self.arrLanguageOther.append(contentsOf: temparrLanguageOther)
        
        self.temparrLanguageOther.removeAll()
        self.setOtherSelectedLanguage()
        self.otherLanguageBottomView.removeFromSuperview()
        isLanguageSelectedEnableContineBtn()
    }
    
    func setOtherSelectedLanguage() {
        var strOtherLangSelect = ""
        for (_,element) in arrLanguageOther.enumerated() {
            if element.isSelected == true {
                if strOtherLangSelect.isBlank {
                    strOtherLangSelect = "\(element.language_name)"
                }
                else {
                    strOtherLangSelect = "\(strOtherLangSelect), \(element.language_name)"
                }
            }
        }
        otherLanguageTextField.text = strOtherLangSelect
        isLanguageSelectedEnableContineBtn()
    }
    
    func removeOtherLanguageViewWithoutSave() {
        self.temparrLanguageOther.removeAll()
        self.otherLanguageBottomView.removeFromSuperview()
    }
}

//MARK:- TableView Delegate Methods
extension Tasker_LanguagesViewController {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblOtherLanguage {
            return temparrLanguageOther.count
        }
        else {
            return arrLanguage_priority.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:LanguagesTableViewCell = self.tblHighPriority.dequeueReusableCell(withIdentifier: "langCell", for: indexPath) as! LanguagesTableViewCell
        if tableView == tblOtherLanguage {
            cell.languageNameLanel.text = self.temparrLanguageOther[indexPath.row].language_name
            cell.checkBoxButton.tag = indexPath.row
            cell.checkBoxButton.addTarget(self, action: #selector(otherlanguagebuttonPressed(sender:)), for: .touchUpInside)
            
            if self.temparrLanguageOther[indexPath.row].isSelected == true {
                cell.checkBoxButton.setImage(UIImage(named: "tasker_checked"), for: .normal)
            }
            else {
                cell.checkBoxButton.setImage(UIImage(named: "tasker_uncheck"), for: .normal)
            }
        }
        else {
            cell.languageNameLanel.text = self.arrLanguage_priority[indexPath.row].language_name
            cell.checkBoxButton.tag = indexPath.row
            cell.checkBoxButton.addTarget(self, action: #selector(buttonPressed(sender:)), for: .touchUpInside)
            
            if self.arrLanguage_priority[indexPath.row].isSelected == true {
                cell.checkBoxButton.setImage(UIImage(named: "tasker_checked"), for: .normal)
            }
            else {
                cell.checkBoxButton.setImage(UIImage(named: "tasker_uncheck"), for: .normal)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblOtherLanguage {
            var data = temparrLanguageOther[indexPath.row]
            data.isSelected = !data.isSelected
            temparrLanguageOther[indexPath.row] = data
            print("Selected item in row \(indexPath.row)")
            self.tblOtherLanguage.reloadData()
        }
        else if tableView == tblHighPriority {
            var data = arrLanguage_priority[indexPath.row]
            data.isSelected = !data.isSelected
            arrLanguage_priority[indexPath.row] = data
            print("Selected item in row \(indexPath.row)")
            self.tblHighPriority.reloadData()
            
            self.isLanguageSelectedEnableContineBtn()
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if let panGestureRecognizer = gestureRecognizer as? UIPanGestureRecognizer {
            let translation = panGestureRecognizer.translation(in: tblOtherLanguage)
            if abs(translation.x) > abs(translation.y) {
                return true
            }
            return false
        }
        return false
    }
}

//MARK:- ViewController API Call's
extension Tasker_LanguagesViewController {
    func updateLanguagesApi(arrayLang:[String]) {
        //update_Tasker_languages
        APICallManager.shared.updateTaskerLanguageAPI(subUrl: ConstantsClass.update_Tasker_languages, parameter: ["tasker_languages":arrayLang], isLoadingIndicatorShow: true) { (data) in
            DispatchQueue.main.async {
                if data.status {
                    if (self.isForEdit != nil && self.isForEdit) || (self.isForEditFirstTime != nil && self.isForEditFirstTime) {
                        self.navigationController?.popViewController(animated: true)
                    }
                    else {
                        let vc:TaskerProfileViewController = self.storyboard?.instantiateViewController(withIdentifier: "TaskerProfileViewController")as!TaskerProfileViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                else {
                    Helper.sharedInstance.showAlert(title: "Message", message:data.message_code)
                }
            }
        }
    }
    
    func get_languages() {
        APICallManager.shared.getLanguageAPI(subUrl: ConstantsClass.get_languages, parameter: [:], isLoadingIndicatorShow: true) { (data) in
            if data.status {
                DispatchQueue.main.async {
                    if let response:[LanguageResponseDataModel] = Optional.some(data.response) {
                        for (_,respelement) in response.enumerated() {
                            var langData = respelement
                            if langData.language_priority == "1" {
                                if (self.isForEdit != nil && self.isForEdit) || (self.isForEditFirstTime != nil && self.isForEditFirstTime) {
                                    for (_,element) in self.selectedTasker_languages.enumerated() {
                                        if element.language_name == langData.language_name {
                                            langData.isSelected = true
                                        }
                                    }
                                }
                                self.arrLanguage_priority.append(langData)
                            }
                            else {
                                if (self.isForEdit != nil && self.isForEdit) || (self.isForEditFirstTime != nil && self.isForEditFirstTime) {
                                    for (_,element) in self.selectedTasker_languages.enumerated() {
                                        if element.language_name == langData.language_name {
                                            langData.isSelected = true
                                        }
                                    }
                                }
                                self.arrLanguageOther.append(langData)
                            }
                        }
                    }
                    
                    self.tblHighPriority.reloadData()
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3, execute: {self.tblPriorityLangHeightConstraint.constant = self.tblHighPriority.contentSize.height}
                    )
                }
            }
        }
    }
}
