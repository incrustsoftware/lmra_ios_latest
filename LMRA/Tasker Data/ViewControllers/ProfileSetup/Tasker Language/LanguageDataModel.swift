//
//  LanguageDataModel.swift
//  LMRA
//
//  Created by Mac on 19/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
//{
//    code = 200;
//    "message_code" = "task_list_success";
//    response =     (
//                {
//            "language_id" = 1;
//            "language_name" = English;
//            "language_priority" = 1;
//        },
//                {
//            "language_id" = 2;
//            "language_name" = Arabic;
//            "language_priority" = 1;
//        }
//    );
//    status = 1;
//}
struct LanguageDataModel:Codable {
    var status:Bool
    var code:Int
    var message_code:String
    var response:[LanguageResponseDataModel]
    
    private enum LanguageDataModelKeys: Any, CodingKey {
        case status
        case code
        case message_code
        case response
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: LanguageDataModelKeys.self)
        self.status = try container.decode(Bool.self, forKey: .status)
        self.code = try container.decode(Int.self, forKey: .code)
        self.message_code = try container.decode(String.self, forKey: .message_code)
        self.response = try container.decode([LanguageResponseDataModel].self, forKey: .response)
    }
}

struct LanguageResponseDataModel:Codable {
    var language_id:Int
    var language_name:String
    var language_priority:String
    var isSelected:Bool
    
    private enum LanguageResponseDataModelKeys: Any, CodingKey {
        case language_id
        case language_name
        case language_priority
        case isSelected
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: LanguageResponseDataModelKeys.self)
        self.language_id = try container.decode(Int.self, forKey: .language_id)
        self.language_name = try container.decode(String.self, forKey: .language_name)
        self.language_priority = try container.decode(String.self, forKey: .language_priority)
        self.isSelected = false
    }
}
