//
//  LanguagesTableViewCell.swift
//  LMRA
//
//  Created by Mac on 05/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class LanguagesTableViewCell: UITableViewCell {
    @IBOutlet weak var checkBoxButton: UIButton!
    @IBOutlet weak var languageNameLanel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
