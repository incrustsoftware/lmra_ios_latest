//
//  TimeSlotCollectionViewCell.swift
//  LMRA
//
//  Created by Mac on 03/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import RadioButton

class TimeSlotCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var cellButton: UIButton!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var radioImage: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var daylabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
