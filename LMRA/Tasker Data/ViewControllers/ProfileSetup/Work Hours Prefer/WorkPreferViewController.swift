//
//  WorkPreferViewController.swift
//  LMRA
//
//  Created by Mac on 03/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import RadioButton
import Alamofire
import SwiftyJSON
import Toast_Swift

class WorkPreferViewController: UIViewController {
    @IBOutlet weak var lblViewTitle: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var weekView: UIView!
    @IBOutlet weak var dayView: UIView!
    @IBOutlet weak var weekendView: UIView!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var workDaysView: UIView!
    @IBOutlet weak var weekDaysRadioButton: RadioButton!
    @IBOutlet weak var weekDaysLabel: UILabel!
    @IBOutlet weak var monToFriLabel: UILabel!
    @IBOutlet weak var weekEndsView: UIView!
    @IBOutlet weak var weekEndsRadioButton: RadioButton!
    @IBOutlet weak var weekEndslabel: UILabel!
    @IBOutlet weak var friToSatLabel: UILabel!
    @IBOutlet weak var allWeekRadioBtn: RadioButton!
    @IBOutlet weak var allWeekLabel: UILabel!
    @IBOutlet weak var satToFriLabel: UILabel!
    @IBOutlet weak var selectWorkingHoursLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var topLabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblSelectWorkingDays: UILabel!
    
    var isForEdit:Bool!
    var isForEditFirstTime:Bool!
    
    var isWeekDay: Bool = false
    var isAllDay:Bool = false
    var isWeekEnds:Bool = false
    var working_day:String = ""
    var working_time:String = ""
    var authentication_key_val = ""
    var timeSlotArray:[TimeSlotesDataModel] = []
    var selectedWorkingDay:TaskerWorkingDayDataModel!
    var selectedWorkingTime:TaskerWorkingTimeDataModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setStatusBarColor(color: .white, styleForStatus: .default)
        if (isForEdit != nil && isForEdit) || (isForEditFirstTime != nil && isForEditFirstTime){
            self.progressView.isHidden = true
            self.topLabelHeightConstraint.constant = 0
            self.lblSelectWorkingDays.text = "Working days"
            self.lblViewTitle.isHidden = false
            
            self.isWeekDay = false
            self.isAllDay = false
            self.isWeekEnds = false
            
            self.continueButton.setTitle("Update", for: .normal)
            if selectedWorkingDay != nil {
                if selectedWorkingDay.day_name == "Week Days" {
                    self.isWeekDay = true
                    self.working_day = "Week Days"
                    weekDaysRadioButton.setImage(UIImage(named: "radioSelected"), for: .normal)
                    self.btncontinue(isEnable: true)
                }
                else if selectedWorkingDay.day_name == "All Week" {
                    self.isAllDay = true
                    self.working_day = "All Week"
                    allWeekRadioBtn.setImage(UIImage(named: "radioSelected"), for: .normal)
                    self.btncontinue(isEnable: true)
                }
                else {
                    self.isWeekEnds = true
                    self.working_day = "Weekends"
                    weekEndsRadioButton.setImage(UIImage(named: "radioSelected"), for: .normal)
                    self.btncontinue(isEnable: true)
                }
            }
        }
        else {
            self.progressView.isHidden = false
            self.lblSelectWorkingDays.text = "Select working days"
            self.topLabelHeightConstraint.constant = 81
            self.lblViewTitle.isHidden = true
        }
        
        let nib1 = UINib.init(nibName: "TimeSlotCollectionViewCell", bundle: nil)
        self.collectionView?.register(nib1, forCellWithReuseIdentifier: "timeslotCell")
        get_timeslots()
        
        self.collectionView.reloadData()
        Helper.sharedInstance.timeslotshadowView(view: self.dayView)
        Helper.sharedInstance.timeslotshadowView(view: self.weekendView)
        Helper.sharedInstance.timeslotshadowView(view: self.weekView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setStatusBarColor(color: .white, styleForStatus: .default)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillLayoutSubviews() {
        DispatchQueue.main.async {
            var contentRect = CGRect.zero
            
            for view in self.scrollView.subviews {
                contentRect = contentRect.union(view.frame)
            }
            
            self.scrollView.contentSize.height = contentRect.size.height
        }
    }
    
    //MARK:- ViewController IBAction's
    @IBAction func btnBackClickAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCloseClickAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func continueButtonAction(_ sender: Any) {
        if self.working_time == "" {
            self.view.makeToast("Please select time")
        }
        else if self.working_day == "" {
            self.view.makeToast("Please select working day")
        }
        else {
            if isForEdit != nil && isForEdit {
                updateWorkScheduleApi(working_day: self.working_day, working_time: self.working_time)
            }
            else {
                getAddWorkScheduleApi(working_day: self.working_day, working_time: self.working_time)
            }
        }
    }
    
    @IBAction func btnSelectWorkingDayClickAction(_ sender: Any) {
        if (sender as AnyObject).tag == 0 {//Week Days
            self.isWeekDay = true
            self.isAllDay = false
            self.isWeekEnds = false
            
            self.working_day = "Week Days"
            
            allWeekRadioBtn.setImage(UIImage(named: "radioUnselected"), for: .normal)
            weekEndsRadioButton.setImage(UIImage(named: "radioUnselected"), for: .normal)
            weekDaysRadioButton.setImage(UIImage(named: "radioSelected"), for: .normal)
        }
        else if (sender as AnyObject).tag == 1 {//Weekends
            self.isWeekDay = false
            self.isAllDay = false
            self.isWeekEnds = true
            
            self.working_day = "Weekends"
            
            allWeekRadioBtn.setImage(UIImage(named: "radioUnselected"), for: .normal)
            weekEndsRadioButton.setImage(UIImage(named: "radioSelected"), for: .normal)
            weekDaysRadioButton.setImage(UIImage(named: "radioUnselected"), for: .normal)
        }
        else if (sender as AnyObject).tag == 2 {//All Week
            self.isWeekDay = false
            self.isAllDay = true
            self.isWeekEnds = false
            
            self.working_day = "All Week"
            
            allWeekRadioBtn.setImage(UIImage(named: "radioSelected"), for: .normal)
            weekEndsRadioButton.setImage(UIImage(named: "radioUnselected"), for: .normal)
            weekDaysRadioButton.setImage(UIImage(named: "radioUnselected"), for: .normal)
        }
        
        self.btncontinue(isEnable: true)
    }
    
    func navtoLanguages() {
        if #available(iOS 12.0, *) {
            let vc:Tasker_LanguagesViewController = self.storyboard?.instantiateViewController(withIdentifier: "Tasker_LanguagesViewController")as!Tasker_LanguagesViewController
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
        }
    }
    
    @objc func buttonPressed(sender:UIButton){
        self.setTimeSlot(selectedIndex: sender.tag)
    }
    
    func setTimeSlot(selectedIndex:Int) {
        for(index,element) in self.timeSlotArray.enumerated() {
            var timeSlotData = element
            if index == selectedIndex {
                timeSlotData.isSelected = true
                self.working_time = "\(timeSlotData.timeslot_id)"
            }
            else {
                timeSlotData.isSelected = false
            }
            self.timeSlotArray[index] = timeSlotData
        }
        self.collectionView.reloadData()
    }
    
    func btncontinue(isEnable:Bool) {
        if isEnable == true {
            self.continueButton.isEnabled = true
            self.continueButton.backgroundColor = UIColor.init(named: "colorTaskerGreen")
            self.continueButton.setTitleColor(UIColor.white, for: .normal)
        }
        else {
            self.continueButton.isEnabled = false
            self.continueButton.backgroundColor = UIColor.init(named: "colorTaskerLightGray")
            self.continueButton.setTitleColor(UIColor.white, for: .normal)
        }
    }
}

//MARK:- CollectionView Delegate Methods
extension WorkPreferViewController:UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return timeSlotArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "timeslotCell", for: indexPath)as!TimeSlotCollectionViewCell
        Helper.sharedInstance.timeslotshadowView(view: cell.backView)
        cell.timeLabel.text = self.timeSlotArray[indexPath.row].timeslot_name
        cell.daylabel.text = self.timeSlotArray[indexPath.row].timeslot_daytime
        
        cell.cellButton.tag = indexPath.row
        cell.cellButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        
        if self.timeSlotArray[indexPath.row].isSelected == true {
            cell.radioImage.image = UIImage(named: "radioSelected")
        }
        else {
            cell.radioImage.image = UIImage(named: "radioUnselected")
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.setTimeSlot(selectedIndex: indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {       
        return CGSize (width: (collectionView.frame.size.width/2.5)-15, height: collectionView.frame.size.height)
    }
}

//MARK:- ViewController API Call's
extension WorkPreferViewController {
    func get_timeslots() {
        APICallManager.shared.getTimeSlotAPI(subUrl: ConstantsClass.get_timeslots, parameter: [:], isLoadingIndicatorShow: true) { (data) in
            if data.status {
                if let timeSlotData:[TimeSlotesDataModel] = Optional.some(data.response) {
                    DispatchQueue.main.async {
                        self.timeSlotArray = timeSlotData
                        if self.selectedWorkingTime != nil {
                            for(index,element) in self.timeSlotArray.enumerated() {
                                var timeSlotData = element
                                if self.selectedWorkingTime.timeslot_name == timeSlotData.timeslot_name {
                                    timeSlotData.isSelected = true
                                    self.working_time = "\(timeSlotData.timeslot_id)"
                                }
                                else {
                                    timeSlotData.isSelected = false
                                }
                                self.timeSlotArray[index] = timeSlotData
                            }
                        }
                        self.collectionView.reloadData()
                    }
                }
            }
        }
    }
    
    func updateWorkScheduleApi(working_day:String,working_time:String) {
        APICallManager.shared.updateTaskerWorkingScheduleAPI(subUrl: ConstantsClass.update_tasker_working_schedule, parameter: ["working_day":working_day,"working_time":working_time], isLoadingIndicatorShow: true) { (data) in
            if data.status {
                DispatchQueue.main.async {
                    if (self.isForEdit != nil && self.isForEdit) || (self.isForEditFirstTime != nil && self.isForEditFirstTime) {
                        self.navigationController?.popViewController(animated: true)
                    }
                    else {
                        self.navtoLanguages()
                    }
                }
            }
        }
    }
    
    func getAddWorkScheduleApi(working_day:String,working_time:String) {
        //self.Get_User_Type =  UserDefaults.standard.integer(forKey: UserDataManager_KEY.userType)
        //let param:[String:String] = ["user_id":"\(Get_User_Type!)"]
        
        let api:APIEngine = APIEngine()
        api.getUpdateTaskerWorkingScheduleAPI(param:["working_day":working_day,"working_time":working_time]){ responseObject, error in
            
            switch responseObject?.result  {
            case .success(let JSON)?:
                print("Success with JSON: \(JSON)")
                let jsonResponse = JSON
                
                if(jsonResponse["status"].bool == true) {
                    if (self.isForEdit != nil && self.isForEdit) || (self.isForEditFirstTime != nil && self.isForEditFirstTime) {
                        self.navigationController?.popViewController(animated: true)
                    }
                    else {
                        self.navtoLanguages()
                    }
                }
                else {
                    Helper.sharedInstance.showAlert(title: "Message", message:jsonResponse["message_code"].stringValue )
                }
            case.failure(let error)?:
                print("error: \(error)")
                
            case .none:
                print("error: ")
            }
        }
    }
}

