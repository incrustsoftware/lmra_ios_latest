//
//  UpdateTaskerWorkPrefernceDataModel.swift
//  LMRA
//
//  Created by Mac on 19/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
//{
//  "code" : 200,
//  "message_code" : "working_details_success",
//  "status" : true,
//  "response" : [
//    {
//      "tasker_cell_phone_number" : "97396969696",
//      "tasker_identity_card_number" : "969696969",
//      "tasker_languages" : [
//        {
//          "language_name" : "English",
//          "language_id" : "2313"
//        }
//      ],
//      "tasker_rates" : [
//        {
//          "is_fixed" : "Fixed Rate",
//          "rate_id" : "4210",
//          "subcategory_name" : "Pipe Fitting",
//          "rate" : "123"
//        },
//        {
//          "is_fixed" : "Fixed Rate",
//          "rate_id" : "4211",
//          "subcategory_name" : "Water Leakages",
//          "rate" : "123"
//        },
//        {
//          "is_fixed" : "Fixed Rate",
//          "rate_id" : "4212",
//          "subcategory_name" : "Installation Service",
//          "rate" : "123"
//        }
//      ],
//      "tasker_id" : "617",
//      "tasker_name" : "vaibhav",
//      "tasker_address" : [
//
//      ],
//      "tasker_email_id" : "vaibhavbhosale.4391@gmail.com"
//    }
//  ]
//}
struct UpdateTaskerWorkPrefernceDataModel:Codable {
    var status:Bool
    var code:Int
    var message_code:String
    var response:[TaskerUpdateProfileDetailResponseModel]
    
    private enum UpdateTaskerWorkPrefernceDataModelKeys: Any, CodingKey {
        case status
        case code
        case message_code
        case response
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: UpdateTaskerWorkPrefernceDataModelKeys.self)
        self.status = try container.decode(Bool.self, forKey: .status)
        self.code = try container.decode(Int.self, forKey: .code)
        self.message_code = try container.decode(String.self, forKey: .message_code)
        self.response = try container.decode([TaskerUpdateProfileDetailResponseModel].self, forKey: .response)
    }
}

struct TaskerUpdateProfileDetailResponseModel:Codable {
    var tasker_address:[String]
    var tasker_email_id:String
    var tasker_id:String
    var tasker_identity_card_number:String
    var tasker_languages:[TaskerUpdateProfileLanguageDataModel]
    var tasker_name:String
    var tasker_rates:[TaskerUpdateProfileRateDataModel]
    //var tasker_working_days:[TaskerUpdateWorkingDayDataModel]
    //var tasker_working_time:[TaskerUpdateWorkingTimeDataModel]
    
    private enum TaskerUpdateProfileDetailResponseModelKeys: Any, CodingKey {
        case tasker_address
        case tasker_email_id
        case tasker_id
        case tasker_identity_card_number
        case tasker_languages
        case tasker_name
        case tasker_rates
        //case tasker_working_days
        //case tasker_working_time
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: TaskerUpdateProfileDetailResponseModelKeys.self)
        self.tasker_address = try container.decode([String].self, forKey: .tasker_address)
        self.tasker_email_id = try container.decode(String.self, forKey: .tasker_email_id)
        self.tasker_id = try container.decode(String.self, forKey: .tasker_id)
        self.tasker_identity_card_number = try container.decode(String.self, forKey: .tasker_identity_card_number)
        self.tasker_languages = try container.decode([TaskerUpdateProfileLanguageDataModel].self, forKey: .tasker_languages)
        self.tasker_name = try container.decode(String.self, forKey: .tasker_name)
        self.tasker_rates = try container.decode([TaskerUpdateProfileRateDataModel].self, forKey: .tasker_rates)
        //self.tasker_working_days = try container.decode([TaskerWorkingDayDataModel].self, forKey: .tasker_working_days)
        //self.tasker_working_time = try container.decode([TaskerWorkingTimeDataModel].self, forKey: .tasker_working_time)
    }
}

struct TaskerUpdateProfileLanguageDataModel:Codable {
    var language_id:String
    var language_name:String
    
    private enum TaskerUpdateProfileLanguageDataModelKeys: Any, CodingKey {
        case language_id
        case language_name
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: TaskerUpdateProfileLanguageDataModelKeys.self)
        self.language_id = try container.decode(String.self, forKey: .language_id)
        self.language_name = try container.decode(String.self, forKey: .language_name)
    }
}

struct TaskerUpdateProfileRateDataModel:Codable {
    var is_fixed:String
    var rate:String
    var rate_id:String
    var subcategory_name:String
    
    private enum TaskerUpdateProfileRateDataModelKeys: Any, CodingKey {
        case is_fixed
        case rate
        case rate_id
        case subcategory_name
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: TaskerUpdateProfileRateDataModelKeys.self)
        self.is_fixed = try container.decode(String.self, forKey: .is_fixed)
        self.rate = try container.decode(String.self, forKey: .rate)
        self.rate_id = try container.decode(String.self, forKey: .rate_id)
        self.subcategory_name = try container.decode(String.self, forKey: .subcategory_name)
    }
}

struct TaskerUpdateWorkingDayDataModel:Codable {
    var day_id:String
    var day_name:String
    
    private enum TaskerUpdateWorkingDayDataModelKeys: Any, CodingKey {
        case day_id
        case day_name
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: TaskerUpdateWorkingDayDataModelKeys.self)
        self.day_id = try container.decode(String.self, forKey: .day_id)
        self.day_name = try container.decode(String.self, forKey: .day_name)
    }
}

struct TaskerUpdateWorkingTimeDataModel:Codable {
    var timeslot_id:String
    var timeslot_name:String
    
    private enum TaskerUpdateWorkingTimeDataModelKeys: Any, CodingKey {
        case timeslot_id
        case timeslot_name
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: TaskerUpdateWorkingTimeDataModelKeys.self)
        self.timeslot_id = try container.decode(String.self, forKey: .timeslot_id)
        self.timeslot_name = try container.decode(String.self, forKey: .timeslot_name)
    }
}
