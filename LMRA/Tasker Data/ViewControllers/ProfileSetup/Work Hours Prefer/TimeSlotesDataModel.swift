//
//  TimeSlotesDataModel.swift
//  LMRA
//
//  Created by Mac on 19/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
//["code": 200, "status": 1, "response": <__NSArrayI 0x600002549180>(
//{
//    "timeslot_daytime" = Morning;
//    "timeslot_id" = 1;
//    "timeslot_name" = "7 - 9 am";
//},
//{
//    "timeslot_daytime" = Morning;
//    "timeslot_id" = 2;
//    "timeslot_name" = "9 - 11 am";
//},
//{
//    "timeslot_daytime" = Morning;
//    "timeslot_id" = 3;
//    "timeslot_name" = "11 - 1 pm";
//},
//{
//    "timeslot_daytime" = Afternoon;
//    "timeslot_id" = 4;
//    "timeslot_name" = "1 - 3 pm";
//},
//{
//    "timeslot_daytime" = Afternoon;
//    "timeslot_id" = 5;
//    "timeslot_name" = "3 - 5 pm";
//},
//{
//    "timeslot_daytime" = Afternoon;
//    "timeslot_id" = 6;
//    "timeslot_name" = "5 - 7 pm";
//},
//{
//    "timeslot_daytime" = Evening;
//    "timeslot_id" = 7;
//    "timeslot_name" = "7 - 9 pm";
//},
//{
//    "timeslot_daytime" = Evening;
//    "timeslot_id" = 8;
//    "timeslot_name" = "9 - 11 pm";
//},
//{
//    "timeslot_daytime" = Evening;
//    "timeslot_id" = 9;
//    "timeslot_name" = "11 - 1 am";
//},
//{
//    "timeslot_daytime" = Overnight;
//    "timeslot_id" = 10;
//    "timeslot_name" = "1 - 3 am";
//},
//{
//    "timeslot_daytime" = Overnight;
//    "timeslot_id" = 11;
//    "timeslot_name" = "3 - 5 am";
//},
//{
//    "timeslot_daytime" = Overnight;
//    "timeslot_id" = 12;
//    "timeslot_name" = "5 - 7 am";
//}
//)
//, "message_code": timeslot_success]
struct TimeSlotDataModel:Codable {
    var status:Bool
    var code:Int
    var message_code:String
    var response:[TimeSlotesDataModel]
    
    private enum TimeSlotDataModelKeys: Any, CodingKey {
        case status
        case code
        case message_code
        case response
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: TimeSlotDataModelKeys.self)
        self.status = try container.decode(Bool.self, forKey: .status)
        self.code = try container.decode(Int.self, forKey: .code)
        self.message_code = try container.decode(String.self, forKey: .message_code)
        self.response = try container.decode([TimeSlotesDataModel].self, forKey: .response)
    }
}

struct TimeSlotesDataModel:Codable {
    var timeslot_id:Int
    var timeslot_name:String
    var timeslot_daytime:String
    var isSelected:Bool
    
    private enum TimeSlotesDataModelKeys: Any, CodingKey {
        case timeslot_id
        case timeslot_name
        case timeslot_daytime
        case isSelected
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: TimeSlotesDataModelKeys.self)
        self.timeslot_id = try container.decode(Int.self, forKey: .timeslot_id)
        self.timeslot_name = try container.decode(String.self, forKey: .timeslot_name)
        self.timeslot_daytime = try container.decode(String.self, forKey: .timeslot_daytime)
        self.isSelected = false
    }
}
