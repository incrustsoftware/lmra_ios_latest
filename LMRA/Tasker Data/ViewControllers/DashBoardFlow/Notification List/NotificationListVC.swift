//
//  NotificationListVC.swift
//  LMRA
//
//  Created by Mac on 21/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class NotificationListVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setStatusBarColor(color: .white, styleForStatus: .default)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    //MARK:- ViewController IBAction's
    @IBAction func btnBackClickAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
