//
//  SwitchToCustomerViewController.swift
//  LMRA
//
//  Created by Mac on 16/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Toast_Swift
import AlamofireSwiftyJSON

class SwitchToCustomerViewController: UIViewController {
    
    @IBOutlet weak var switchBtn: UIButton!
    @IBOutlet weak var nxtimageView: UIImageView!
    @IBOutlet weak var switchToCustomerLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titlelabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var middleView: UIView!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var topView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setStatusBarColor(color: UIColor.init(named: "colorTaskerIndigo")!, styleForStatus: .lightContent)
        setNeedsStatusBarAppearanceUpdate()
    }

    func switchToCustomer()
    {
        let urlString = ConstantsClass.baseUrl + ConstantsClass.tasker_switch_to_customer
        print("urlString: \(urlString)")
        guard let auth_key:String = Optional.some(UserDataManager.shared.getAccesToken()) else {
            self.view.makeToast("Something wents's wrong")
            return
        }
        let headers = [
            "Authorization": "\(auth_key)"
        ]
        Alamofire.request(urlString, method: .get, parameters: nil, encoding: URLEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if (response["status"] as? Int) != 0 {
                        if let respArray = response["response"] as? [String:Any] {
                            print("respArray: \(respArray)")
                            guard let authentication_key = respArray["customer_authentication_key"] else
                            {
                                return
                            }
                            guard let userType = Optional.some(respArray["user_type"]) else
                            {
                                return
                            }
                            UserDataManager.shared.change(authKey: "\(authentication_key)", userType: Int("\(userType ?? UserDataManager.UserType.CUSTOMER.rawValue)") ?? UserDataManager.UserType.CUSTOMER.rawValue)
  
                            guard let customer_profile_set = respArray["customer_profile_set"]as? Int else
                            {
                                return
                            }
                            
                            if customer_profile_set == 0 {
                                self.navTosetAddres()
                            }
                            else {
                                self.navToDashboard()
                            }
                            
                            
                            // }
                            
                        }
                    }
                    // Make sure to update UI in main thread
                    DispatchQueue.main.async {
                        //  self.tableView.reloadData()
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
        
        
    }
    
    func navTosetAddres()
    {
        let objVc:SetAddressViewController = Storyboard().mainStoryboard.instantiateViewController(withIdentifier: "SetAddressVC") as!
        SetAddressViewController
        self.navigationController?.pushViewController(objVc, animated: true)
    }
    func navToDashboard()
    {
        let stroyboard:UIStoryboard = UIStoryboard(name: "DashFlow", bundle: nil)
        let objVc:DashBoardTabViewController = stroyboard.instantiateViewController(withIdentifier: "DashBoardTabVC") as!
        DashBoardTabViewController
        self.navigationController?.pushViewController(objVc, animated: true)
    }
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func closeBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func switchBtnAction(_ sender: Any) {
        self.switchToCustomer()
    }
}
