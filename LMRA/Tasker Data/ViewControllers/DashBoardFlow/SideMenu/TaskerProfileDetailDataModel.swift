//
//  TaskerProfileDetailDataModel.swift
//  LMRA
//
//  Created by Mac on 18/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
//{
//    code = 200;
//    "message_code" = "tasker_profile_success";
//    response =     (
//                {
//            "tasker_address" =             (
//            );
//            "tasker_cell_phone_number" = 97396969696;
//            "tasker_email_id" = "vaibhavbhosale.4391@gmail.com";
//            "tasker_id" = 617;
//            "tasker_identity_card_number" = 969696969;
//            "tasker_languages" =             (
//                                {
//                    "language_id" = 2280;
//                    "language_name" = English;
//                },
//                                {
//                    "language_id" = 2281;
//                    "language_name" = Arabic;
//                },
//                                {
//                    "language_id" = 2282;
//                    "language_name" = Urdu;
//                },
//                                {
//                    "language_id" = 2283;
//                    "language_name" = French;
//                }
//            );
//            "tasker_name" = vaibhav;
//            "tasker_rates" =             (
//                                {
//                    "is_fixed" = "Fixed Rate";
//                    rate = 150;
//                    "rate_id" = 4161;
//                    "subcategory_name" = "Pipe Fitting";
//                },
//                                {
//                    "is_fixed" = "Fixed Rate";
//                    rate = 150;
//                    "rate_id" = 4162;
//                    "subcategory_name" = "Water Leakages";
//                },
//                                {
//                    "is_fixed" = "Fixed Rate";
//                    rate = 150;
//                    "rate_id" = 4163;
//                    "subcategory_name" = "Installation Service";
//                }
//            );
//            "tasker_working_days" =             (
//                                {
//                    "day_id" = 1263;
//                    "day_name" = "Week Days";
//                }
//            );
//            "tasker_working_time" =             (
//                                {
//                    "timeslot_id" = 1263;
//                    "timeslot_name" = "3 - 5 am";
//                }
//            );
//        }
//    );
//    status = 1;
//}
import Foundation
struct TaskerProfileDetailDataModel:Codable {
    var status:Bool
    var code:Int
    var message_code:String
    var response:[TaskerProfileDetailResponseModel]
    
    private enum TaskerProfileDetailDataModelKeys: Any, CodingKey {
        case status
        case code
        case message_code
        case response
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: TaskerProfileDetailDataModelKeys.self)
        self.status = try container.decode(Bool.self, forKey: .status)
        self.code = try container.decode(Int.self, forKey: .code)
        self.message_code = try container.decode(String.self, forKey: .message_code)
        self.response = try container.decode([TaskerProfileDetailResponseModel].self, forKey: .response)
    }
}

struct TaskerProfileDetailResponseModel:Codable {
    var tasker_address:[String]
    var tasker_email_id:String
    var tasker_id:String
    var tasker_identity_card_number:String
    var tasker_languages:[TaskerProfileLanguageDataModel]
    var tasker_name:String
    var tasker_rates:[TaskerProfileRateDataModel]
    var tasker_working_days:[TaskerWorkingDayDataModel]
    var tasker_working_time:[TaskerWorkingTimeDataModel]
    
    private enum TaskerProfileDetailResponseModelKeys: Any, CodingKey {
        case tasker_address
        case tasker_email_id
        case tasker_id
        case tasker_identity_card_number
        case tasker_languages
        case tasker_name
        case tasker_rates
        case tasker_working_days
        case tasker_working_time
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: TaskerProfileDetailResponseModelKeys.self)
        self.tasker_address = try container.decode([String].self, forKey: .tasker_address)
        self.tasker_email_id = try container.decode(String.self, forKey: .tasker_email_id)
        self.tasker_id = try container.decode(String.self, forKey: .tasker_id)
        self.tasker_identity_card_number = try container.decode(String.self, forKey: .tasker_identity_card_number)
        self.tasker_languages = try container.decode([TaskerProfileLanguageDataModel].self, forKey: .tasker_languages)
        self.tasker_name = try container.decode(String.self, forKey: .tasker_name)
        self.tasker_rates = try container.decode([TaskerProfileRateDataModel].self, forKey: .tasker_rates)
        self.tasker_working_days = try container.decode([TaskerWorkingDayDataModel].self, forKey: .tasker_working_days)
        self.tasker_working_time = try container.decode([TaskerWorkingTimeDataModel].self, forKey: .tasker_working_time)
    }
}

struct TaskerProfileLanguageDataModel:Codable {
    var language_id:String
    var language_name:String
    
    private enum TaskerProfileLanguageDataModelKeys: Any, CodingKey {
        case language_id
        case language_name
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: TaskerProfileLanguageDataModelKeys.self)
        self.language_id = try container.decode(String.self, forKey: .language_id)
        self.language_name = try container.decode(String.self, forKey: .language_name)
    }
}

struct TaskerProfileRateDataModel:Codable {
    var is_fixed:String
    var rate:String
    var rate_id:String
    var subcategory_name:String
    
    private enum TaskerProfileRateDataModelKeys: Any, CodingKey {
        case is_fixed
        case rate
        case rate_id
        case subcategory_name
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: TaskerProfileRateDataModelKeys.self)
        self.is_fixed = try container.decode(String.self, forKey: .is_fixed)
        self.rate = try container.decode(String.self, forKey: .rate)
        self.rate_id = try container.decode(String.self, forKey: .rate_id)
        self.subcategory_name = try container.decode(String.self, forKey: .subcategory_name)
    }
}

struct TaskerWorkingDayDataModel:Codable {
    var day_id:String
    var day_name:String
    
    private enum TaskerWorkingDayDataModelKeys: Any, CodingKey {
        case day_id
        case day_name
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: TaskerWorkingDayDataModelKeys.self)
        self.day_id = try container.decode(String.self, forKey: .day_id)
        self.day_name = try container.decode(String.self, forKey: .day_name)
    }
}

struct TaskerWorkingTimeDataModel:Codable {
    var timeslot_id:String
    var timeslot_name:String
    
    private enum TaskerWorkingTimeDataModelKeys: Any, CodingKey {
        case timeslot_id
        case timeslot_name
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: TaskerWorkingTimeDataModelKeys.self)
        self.timeslot_id = try container.decode(String.self, forKey: .timeslot_id)
        self.timeslot_name = try container.decode(String.self, forKey: .timeslot_name)
    }
}
