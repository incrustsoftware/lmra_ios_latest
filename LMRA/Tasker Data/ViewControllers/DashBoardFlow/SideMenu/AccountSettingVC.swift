//
//  AccountSettingVC.swift
//  LMRA
//
//  Created by Mac on 18/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class AccountSettingVC: UIViewController {
    
    @IBOutlet weak var tblServices: UITableView!
    @IBOutlet weak var lblWorkingHour: UILabel!
    @IBOutlet weak var lblSpokenLanguage: UILabel!
    
    var profileData:TaskerProfileDetailResponseModel!
    var selectedWorkingDay:TaskerWorkingDayDataModel!
    var selectedWorkingTime:TaskerWorkingTimeDataModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.getProfileData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setStatusBarColor(color: .white, styleForStatus: .default)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    //MARK:- ViewController IBAction's
    @IBAction func btnBackClickAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnEditServiceClickAction(_ sender: Any) {
        if self.profileData.tasker_rates.count > 0 {
            let vc:Tasker_AddRateViewController = self.storyboard?.instantiateViewController(withIdentifier: "Tasker_AddRateViewController")as!Tasker_AddRateViewController
            vc.isForEdit = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else {
            let objVc:Tasker_SelectCategoryViewController =
                Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "Tasker_SelectCategoryViewController") as!
            Tasker_SelectCategoryViewController
            self.navigationController?.pushViewController(objVc, animated: true)
        }
    }
    
    @IBAction func btnEditWorkingHoursClickAction(_ sender: Any) {
        let vc:WorkPreferViewController = self.storyboard?.instantiateViewController(withIdentifier: "WorkPreferViewController")as!WorkPreferViewController
        vc.isForEdit = true
        vc.selectedWorkingDay = self.selectedWorkingDay
        vc.selectedWorkingTime = self.selectedWorkingTime
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnEditLangClickAction(_ sender: Any) {
        let vc:Tasker_LanguagesViewController = self.storyboard?.instantiateViewController(withIdentifier: "Tasker_LanguagesViewController")as!Tasker_LanguagesViewController
        vc.isForEdit = true
        vc.selectedTasker_languages = self.profileData.tasker_languages
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension AccountSettingVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if profileData != nil {
            return profileData.tasker_rates.count
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:AccountSettingTVCell = tableView.dequeueReusableCell(withIdentifier: "AccountSettingTVCell", for: indexPath)as!AccountSettingTVCell
        cell.data = profileData.tasker_rates[indexPath.row]
        return cell
    }
}

//MARK:- ViewController IBAction's
extension AccountSettingVC {
    func getProfileData() {
        APICallManager.shared.getAccountProfileAPI(subUrl: ConstantsClass.get_Tasker_profileDetails, parameter: [:], isLoadingIndicatorShow: true) { (data) in
            if data.status {
                if data.response.count > 0 {
                    self.profileData = data.response[0]
                    DispatchQueue.main.async {
                        var strLanguage = ""
                        for (_,element) in self.profileData.tasker_languages.enumerated() {
                            if strLanguage == "" {
                                strLanguage = "\(element.language_name)"
                            }
                            else {
                                strLanguage = "\(strLanguage), \(element.language_name)"
                            }
                        }
                        
                        self.lblSpokenLanguage.text = strLanguage
                        
                        var strWorkingHr = ""
                        for(_,element) in self.profileData.tasker_working_days.enumerated() {
                            strWorkingHr = "\(element.day_name)"
                            self.selectedWorkingDay = element
                        }
                        
                        for(_,element) in self.profileData.tasker_working_time.enumerated() {
                            strWorkingHr = "\(strWorkingHr), \(element.timeslot_name)"
                            self.selectedWorkingTime = element
                        }
                        
                        self.lblWorkingHour.text = strWorkingHr
                        self.tblServices.reloadData()
                    }
                }
            }
        }
    }
}
