//
//  SideMenuViewController.swift
//  LMRA
//
//  Created by Mac on 14/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//


import UIKit
import Foundation
import Toast_Swift
import Alamofire


class SideMenuViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var switchToCustomerBtn: UIButton!
    @IBOutlet weak var switchLabel: UITableView!
    @IBOutlet weak var nxtBtn: UIImageView!
    @IBOutlet weak var switchView: UIView!
    @IBOutlet weak var editProfileBtn: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    var nameArray:[String] = ["Account Settings","How it works","FAQ","Help","About us","Log out"]
    var imageArray:[String] = ["PurpleUser","PurplePage","PurpleFAQ","PurpleHelp","PurpleAboutUs","PurpleLogOut"]
    var User_type:Int!
    var User_TypeString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "SideMenuTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "sideCell")
        self.tableView.reloadData()
        self.tableView.tableFooterView = UIView()
        setUI()
        guard let name = UserDefaults.standard.value(forKey: "NameKey") as? String else
        {
            return
        }
        
        self.nameLabel.text = name
        
//        self.navigationController?.navigationBar.isHidden = false
//        self.tabBarController?.navigationController?.navigationBar.isHidden = false
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //GLOBALVARIABLE.TaskerTabBarName = "ThirdTab"
        //self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func EditprofilebtnAction(_ sender: Any) {
        let navigationController = sideMenuController?.rootViewController as! UINavigationController
        let vc:Tasker_EditProfileViewController = self.storyboard?.instantiateViewController(withIdentifier: "Tasker_EditProfileViewController")as!Tasker_EditProfileViewController
        navigationController.pushViewController(vc, animated: true)
        sideMenuController?.hideRightView(animated: false) {}
    }
    
    @IBAction func closeBtnAction(_ sender: Any) {
        sideMenuController?.hideRightView(animated: true) {}
    }
    func setUI()
    {
        self.profileImageView.layer.cornerRadius = self.profileImageView.frame.height/2
        self.profileImageView.clipsToBounds = true
        self.profileImageView.contentMode = .scaleToFill
        get_profileDetails()
    }
    func get_profileDetails()
    {
        let urlString = ConstantsClass.baseUrl + ConstantsClass.get_tasker_basic_profile_details
        print("urlString: \(urlString)")
        guard let auth_key = UserDefaults.standard.string(forKey: "authentication_key") else {
            return
        }
        let headers = [
            "Authorization": "\(auth_key)"
        ]
        Alamofire.request(urlString, method: .get, parameters: nil, encoding: URLEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if (response["status"] as? Bool) != nil {
                        if let respArray = response["response"] as? [[String:Any]] {
                            print("respArray: \(respArray)")
                            
                            for Data in respArray
                            {
                                
                                guard let tasker_profile = Data["tasker_profile"]as? String else {
                                    return
                                }
                                self.profileImageView.sd_setImage(with: URL(string:(tasker_profile) ), placeholderImage: UIImage(named: "iconCustomer"))
                                
                                guard let tasker_name = Data["tasker_name"]as? String else {
                                    return
                                }
                                self.nameLabel.text = tasker_name
                            }
                            // print("Language_NameString_Array: \(self.taskArray)")
                            //    //
                        }
                    }
                    // Make sure to update UI in main thread
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
        
        
    }
    @available(iOS 12.0, *)
    func sign_out()
    {
        APICallManager.shared.logoutApi(subUrl: ConstantsClass.sign_out, parameter: [
            "user_type" : UserDataManager.shared.getUserType(),
        ], isLoadingIndicatorShow: true) { (response) in
            DispatchQueue.main.async {
                if let status = response["status"] as? Bool {
                    if status
                    {
                        print("sign_out_success")
                        UserDataManager.shared.clearUserDetails()
                        self.sideMenuController?.hideRightView(animated: true) {}
                        self.view.makeToast("sign_out_success", duration: 6.0, position: .bottom)
                        
                        let navigationController = self.sideMenuController?.rootViewController as! UINavigationController
                        var isPopView = false
                        for controller in navigationController.viewControllers as Array {
                            if controller.isKind(of: LoginViewController.self) {
                                isPopView = true
                                navigationController.popToViewController(controller, animated: true)
                                break
                            }
                        }
                        
                        if isPopView == false {
                            let vc = Storyboard().mainStoryboard.instantiateViewController(withIdentifier: "LoginVC")
                            navigationController.pushViewController(vc, animated: false)
                        }
                    }
                }
            }
        }
        //        let urlString = ConstantsClass.baseUrl + ConstantsClass.sign_out
        //        print("urlString: \(urlString)")
        //
        //        guard let auth_key:String = Optional.some(UserDataManager.shared.getAccesToken())  else {
        //            self.view.makeToast("Something went's wrong")
        //            return
        //        }
        ////        guard let auth_key = UserDefaults.standard.string(forKey: "authentication_key") else {
        ////            return
        ////        }
        //        let headers = [
        //            "Authorization": "\(auth_key)"
        //        ]
        //
        //        print("headers: \(headers)")
        //        self.User_type =  UserDefaults.standard.integer(forKey: UserDataManager_KEY.userType)
        //        self.User_TypeString = String(self.User_type)
        //        let parameters = [
        //            "user_type" : self.User_TypeString,
        //            ] as [String : Any]
        //        print("parameterssignOut: \(parameters)")
        //        Alamofire.request(urlString
        //            , method: .post, parameters: parameters, encoding:JSONEncoding.default,headers: headers).responseJSON
        //            {
        //                response in
        //                if let response = response.result.value as? [String:Any] {
        //                    print("responseSignOut: \(response)")
        //                    if let status = response["status"] as? Bool {
        //                        if status
        //                        {
        //                            print("sign_out_success")
        //                            UserDataManager.shared.clearUserDetails()
        //                            self.sideMenuController?.hideRightView(animated: true) {}
        //                            self.view.makeToast("sign_out_success", duration: 6.0, position: .bottom)
        //
        //                            let navigationController = self.sideMenuController?.rootViewController as! UINavigationController
        //                            var isPopView = false
        //                            for controller in navigationController.viewControllers as Array {
        //                                if controller.isKind(of: LoginViewController.self) {
        //                                    isPopView = true
        //                                    navigationController.popToViewController(controller, animated: true)
        //                                    break
        //                                }
        //                            }
        //
        //                            if isPopView == false {
        //                                let vc = Storyboard().mainStoryboard.instantiateViewController(withIdentifier: "LoginVC")
        //                                navigationController.pushViewController(vc, animated: false)
        //                            }
        //                        }
        //                    }
        //                    else {
        //
        //                    }
        //                    // Make sure to update UI in main thread
        //                    DispatchQueue.main.async {
        //                        print("waiting_task_details_list_success")
        //                    }
        //                }
        //                else {
        //                    print("Error occured") // serialized json response
        //                }
        //        }
    }
    
    @IBAction func switchTocustBtn(_ sender: Any) {
        let navigationController = sideMenuController?.rootViewController as! UINavigationController
        let vc:SwitchToCustomerViewController = self.storyboard?.instantiateViewController(withIdentifier: "SwitchToCustomerViewController")as!SwitchToCustomerViewController
        navigationController.pushViewController(vc, animated: true)
        sideMenuController?.hideRightView(animated: true) {}
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:SideMenuTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "sideCell", for: indexPath)as!SideMenuTableViewCell
        cell.nameLabel.text = nameArray[indexPath.row]
        cell.profileImageView.image = UIImage(named: imageArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc:AccountSettingVC = self.storyboard?.instantiateViewController(withIdentifier: "AccountSettingVC")as!AccountSettingVC
            //vc.fromAccountSetting = true
            navigationController.pushViewController(vc, animated: true)
            sideMenuController?.hideRightView(animated: true) {}
        }
        if indexPath.row == 5 {
            if #available(iOS 12.0, *) {
                self.sign_out()
            } else {
                // Fallback on earlier versions
            }
        }
        
        //sideMenuController?.hideRightView(animated: true) {}
    }
}
