//
//  AccountSettingTVCell.swift
//  LMRA
//
//  Created by Mac on 18/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class AccountSettingTVCell: UITableViewCell {

    @IBOutlet weak var lblServiceTitle: UILabel!
    @IBOutlet weak var lblServiceRate: UILabel!
    
    var data:TaskerProfileRateDataModel? {
        didSet {
            self.lblServiceTitle.text = data?.subcategory_name
            self.lblServiceRate.text = "\(data?.rate ?? "")/\(data?.is_fixed ?? "")"
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
