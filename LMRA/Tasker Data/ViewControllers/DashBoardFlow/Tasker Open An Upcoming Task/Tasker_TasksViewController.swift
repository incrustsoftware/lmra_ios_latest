//
//  Tasker_TasksViewController.swift
//  LMRA
//
//  Created by Mac on 12/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Toast_Swift
import AlamofireSwiftyJSON
import SDWebImage
class Tasker_TasksViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var taskLabel: UILabel!
    @IBOutlet weak var pagerview: UIView!
    @IBOutlet weak var openView: UIView!
    @IBOutlet weak var openLabel: UILabel!
    @IBOutlet weak var openDivderView: UIView!
    @IBOutlet weak var upcomingView: UIView!
    @IBOutlet weak var upcomingLabel: UILabel!
    @IBOutlet weak var upcomingDiverView: UIView!
    @IBOutlet weak var openButton: UIButton!
    
    var taskOpenArray:[OpenTask] = []
    var taskUpcomingArray:[OpenTask] = []
    
    var isOpen:Bool = false
    var index:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "TaskTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "openTaskCell")
        self.tableView.tableFooterView = UIView()
        self.tableView.estimatedRowHeight = 283
        self.tableView.rowHeight = UITableView.automaticDimension
        
        self.upcomingLabel.textColor = UIColor.white
        self.upcomingDiverView.backgroundColor = Helper.sharedInstance.hexStringToUIColor(hex: "#4F008C")
        self.openLabel.textColor = Helper.sharedInstance.hexStringToUIColor(hex: "#00C48C")
        self.openDivderView.backgroundColor = Helper.sharedInstance.hexStringToUIColor(hex: "#00C48C")
        NotificationCenter.default.addObserver(self,selector: #selector(self.onSuccessAcceptTask),name: NSNotification.Name(rawValue: UserDataManager.NotificationObserverKey.CANCEL_TASK_INVITATION_SUCCESS),object: nil)
        self.isOpen = true
        
        get_open_task_tasker()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        GLOBALVARIABLE.TaskerTabBarName = "SecondTab"
        self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK:- ViewController IBAction's
    @IBAction func openButtonAction(_ sender: Any) {
        self.isOpen = true
        self.tableView.reloadData()
        self.upcomingLabel.textColor = UIColor.white
        self.upcomingDiverView.backgroundColor = Helper.sharedInstance.hexStringToUIColor(hex: "#4F008C")
        self.openLabel.textColor = Helper.sharedInstance.hexStringToUIColor(hex: "#00C48C")
        self.openDivderView.backgroundColor = Helper.sharedInstance.hexStringToUIColor(hex: "#00C48C")
        
        self.get_open_task_tasker()
    }
    
    @IBAction func upcomingButtonAction(_ sender: Any) {
        self.isOpen = false
        self.tableView.reloadData()
        self.openLabel.textColor = UIColor.white
        self.openDivderView.backgroundColor = Helper.sharedInstance.hexStringToUIColor(hex: "#4F008C")
        self.upcomingLabel.textColor = Helper.sharedInstance.hexStringToUIColor(hex: "#00C48C")
        self.upcomingDiverView.backgroundColor = Helper.sharedInstance.hexStringToUIColor(hex: "#00C48C")
        
        self.get_upcomimg()
    }
    
    //MARK:- ViewController Own defined methods
    @objc func openCellClickAction(_ sender: Any) {
        if let opentask:OpenTask = Optional.some(self.taskOpenArray[(sender as AnyObject).tag]), let taskID:String = Optional.some(opentask.task_id) {
            let vc:TaskerAcceptedTaskDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "TaskerAcceptedTaskDetailVC")as!TaskerAcceptedTaskDetailVC
            vc.taskID = "\(taskID)"
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func upcomingCellClickAction(_ sender: Any) {
        if let opentask:OpenTask = Optional.some(self.taskUpcomingArray[(sender as AnyObject).tag]), let taskID:String = Optional.some(opentask.task_id) {
            let vc:TaskerAcceptedTaskDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "TaskerAcceptedTaskDetailVC")as!TaskerAcceptedTaskDetailVC
            vc.taskID = "\(taskID)"
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc private func onSuccessAcceptTask(notification: NSNotification){
        if isOpen == true {
            self.get_open_task_tasker()
        }
        else {
            self.get_upcomimg()
        }
    }
}

//MARK:- TableView Delegate Methods
extension Tasker_TasksViewController {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isOpen == true {
            return taskOpenArray.count
        }
        else {
            return taskUpcomingArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:TaskTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "openTaskCell")as!TaskTableViewCell
        //Helper.sharedInstance.shadowView(view: cell.backView)
        let temp:OpenTask = isOpen ? taskOpenArray[indexPath.row] : taskUpcomingArray[indexPath.row]
        cell.nameLabel.text = temp.customer_name
        cell.titleLabel.text = "Task"
        cell.decriptionLabel.text = temp.task_description
        
        cell.profilePicImageView.layer.cornerRadius = cell.profilePicImageView.frame.height/2
        cell.profilePicImageView.clipsToBounds = true
        cell.profilePicImageView.sd_setImage(with: URL(string:(temp.customer_user_image) ), placeholderImage: UIImage(named: "iconCustomer"))
        if isOpen == true {
            cell.upcomingTaskView.isHidden = true
            cell.openTaskView.isHidden = false
            //cell.calenderImageView.isHidden = true
            //cell.dateLabel.isHidden = true
            cell.calenderLabel.text = "you have been invited for task"
            cell.btnOpen.tag = indexPath.row
            cell.btnOpen.addTarget(self, action: #selector(openCellClickAction(_:)), for: .touchUpInside)
        }
        else {
            cell.upcomingTaskView.isHidden = false
            cell.openTaskView.isHidden = true
            //cell.calenderImageView.isHidden = false
            //cell.dateLabel.isHidden = false
            cell.dateLabel.text = "Date"
            if let date = temp.task_date.toDate(withFormat: "yyyy-MM-dd"), let strDate:String = Optional.some(date.toString(withFormat: "dd MMMM,yyyy")){
                cell.lblDate.text = strDate
            }
            else {
                cell.lblDate.text = temp.task_date
            }
            cell.btnUpcoming.tag = indexPath.row
            cell.btnUpcoming.addTarget(self, action: #selector(upcomingCellClickAction(_:)), for: .touchUpInside)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//MARK:- ViewController API Call's
extension Tasker_TasksViewController {
    func get_upcomimg() {
        APICallManager.shared.getUpcomingTaskTaskerApi(subUrl: ConstantsClass.get_upcoming_task_tasker, parameter: [:], isLoadingIndicatorShow: true) { (response) in
            DispatchQueue.main.async {
                if (response["status"] as? Bool) != nil {
                    if let respArray = response["response"] as? [[String:Any]] {
                        print("respArray: \(respArray)")
                        self.taskUpcomingArray.removeAll()
                        for Data in respArray
                        {
                            guard let customer_email_id = Data["customer_email_id"]as? String else {
                                return
                            }
                            guard let customer_cell_phone_number = Data["customer_cell_phone_number"]as? String else {
                                return
                            }
                            guard let customer_id = Data["customer_id"]as? String else {
                                return
                            }
                            guard let customer_identity_card_number = Data["customer_identity_card_number"]as? String else {
                                return
                            }
                            guard let customer_name = Data["customer_name"]as? String else {
                                return
                            }
                            guard let customer_user_image = Data["customer_user_image"]as? String else {
                                return
                            }
                            guard let task_date = Data["task_date"]as? String else {
                                return
                            }
                            guard let task_description = Data["task_description"]as? String else {
                                return
                            }
                            guard let task_id = Data["task_id"]as? String else {
                                return
                            }
                            guard let task_title = Data["task_title"]as? String else {
                                return
                            }
                            self.taskUpcomingArray.append(OpenTask(customer_cell_phone_number: customer_cell_phone_number, customer_email_id: customer_email_id, customer_id: customer_id, customer_identity_card_number: customer_identity_card_number, customer_name: customer_name, customer_user_image: customer_user_image, task_date: task_date, task_description: task_description, task_id: task_id, task_title: task_title))
                            
                        }
                        print("Language_NameString_Array: \(self.taskUpcomingArray)")
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                        //    //
                    }
                }
            }
        }
        //        let urlString = ConstantsClass.baseUrl + ConstantsClass.get_upcoming_task_tasker
        //        print("urlString: \(urlString)")
        //        guard let auth_key = UserDefaults.standard.string(forKey: "authentication_key") else {
        //            return
        //        }
        //        let headers = [
        //            "Authorization": "\(auth_key)"
        //        ]
        //        Alamofire.request(urlString, method: .get, parameters: nil, encoding: URLEncoding.default,headers: headers).responseJSON
        //            {
        //                response in
        //                if let response = response.result.value as? [String:Any] {
        //                    print("response: \(response)")
        //
        //                    if (response["status"] as? Bool) != nil {
        //                        if let respArray = response["response"] as? [[String:Any]] {
        //                            print("respArray: \(respArray)")
        //                            self.taskUpcomingArray.removeAll()
        //                            for Data in respArray
        //                            {
        //                                guard let customer_email_id = Data["customer_email_id"]as? String else {
        //                                    return
        //                                }
        //                                guard let customer_cell_phone_number = Data["customer_cell_phone_number"]as? String else {
        //                                    return
        //                                }
        //                                guard let customer_id = Data["customer_id"]as? String else {
        //                                    return
        //                                }
        //                                guard let customer_identity_card_number = Data["customer_identity_card_number"]as? String else {
        //                                    return
        //                                }
        //                                guard let customer_name = Data["customer_name"]as? String else {
        //                                    return
        //                                }
        //                                guard let customer_user_image = Data["customer_user_image"]as? String else {
        //                                    return
        //                                }
        //                                guard let task_date = Data["task_date"]as? String else {
        //                                    return
        //                                }
        //                                guard let task_description = Data["task_description"]as? String else {
        //                                    return
        //                                }
        //                                guard let task_id = Data["task_id"]as? String else {
        //                                    return
        //                                }
        //                                guard let task_title = Data["task_title"]as? String else {
        //                                    return
        //                                }
        //                                self.taskUpcomingArray.append(OpenTask(customer_cell_phone_number: customer_cell_phone_number, customer_email_id: customer_email_id, customer_id: customer_id, customer_identity_card_number: customer_identity_card_number, customer_name: customer_name, customer_user_image: customer_user_image, task_date: task_date, task_description: task_description, task_id: task_id, task_title: task_title))
        //
        //                            }
        //                            print("Language_NameString_Array: \(self.taskUpcomingArray)")
        //                            //    //
        //                        }
        //                    }
        //                }
        //                else {
        //                    print("Error occured") // serialized json response
        //                }
        //
        //                // Make sure to update UI in main thread
        //                DispatchQueue.main.async {
        //                    self.tableView.reloadData()
        //                }
        //        }
    }
    
    func get_open_task_tasker() {
        APICallManager.shared.getOpenTaskTaskerApi(subUrl: ConstantsClass.get_open_task_tasker, parameter: [:], isLoadingIndicatorShow: true) { (response) in
            DispatchQueue.main.async {
                if (response["status"] as? Bool) != nil {
                    if let respArray = response["response"] as? [[String:Any]] {
                        print("respArray: \(respArray)")
                        self.taskOpenArray.removeAll()
                        for Data in respArray {
                            guard let customer_email_id = Data["customer_email_id"]as? String else {
                                return
                            }
                            guard let customer_cell_phone_number = Data["customer_cell_phone_number"]as? String else {
                                return
                            }
                            guard let customer_id = Data["customer_id"]as? String else {
                                return
                            }
                            guard let customer_identity_card_number = Data["customer_identity_card_number"]as? String else {
                                return
                            }
                            guard let customer_name = Data["customer_name"]as? String else {
                                return
                            }
                            guard let customer_user_image = Data["customer_user_image"]as? String else {
                                return
                            }
                            guard let task_date = Data["task_date"]as? String else {
                                return
                            }
                            guard let task_description = Data["task_description"]as? String else {
                                return
                            }
                            guard let task_id = Data["task_id"]as? String else {
                                return
                            }
                            guard let task_title = Data["task_title"]as? String else {
                                return
                            }
                            self.taskOpenArray.append(OpenTask(customer_cell_phone_number: customer_cell_phone_number, customer_email_id: customer_email_id, customer_id: customer_id, customer_identity_card_number: customer_identity_card_number, customer_name: customer_name, customer_user_image: customer_user_image, task_date: task_date, task_description: task_description, task_id: task_id, task_title: task_title))
                            
                        }
                        print("Language_NameString_Array: \(self.taskOpenArray)")
                        
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                        //    //
                    }
                }
            }
        }
        
        //        let urlString = ConstantsClass.baseUrl + ConstantsClass.get_open_task_tasker
        //        print("urlString: \(urlString)")
        //        guard let auth_key = UserDefaults.standard.string(forKey: "authentication_key") else {
        //            return
        //        }
        //        let headers = [
        //            "Authorization": "\(auth_key)"
        //        ]
        //        Alamofire.request(urlString, method: .get, parameters: nil, encoding: URLEncoding.default,headers: headers).responseJSON
        //            {
        //                response in
        //                if let response = response.result.value as? [String:Any] {
        //                    print("response: \(response)")
        //
        //                    if (response["status"] as? Bool) != nil {
        //                        if let respArray = response["response"] as? [[String:Any]] {
        //                            print("respArray: \(respArray)")
        //                            self.taskOpenArray.removeAll()
        //                            for Data in respArray {
        //                                guard let customer_email_id = Data["customer_email_id"]as? String else {
        //                                    return
        //                                }
        //                                guard let customer_cell_phone_number = Data["customer_cell_phone_number"]as? String else {
        //                                    return
        //                                }
        //                                guard let customer_id = Data["customer_id"]as? String else {
        //                                    return
        //                                }
        //                                guard let customer_identity_card_number = Data["customer_identity_card_number"]as? String else {
        //                                    return
        //                                }
        //                                guard let customer_name = Data["customer_name"]as? String else {
        //                                    return
        //                                }
        //                                guard let customer_user_image = Data["customer_user_image"]as? String else {
        //                                    return
        //                                }
        //                                guard let task_date = Data["task_date"]as? String else {
        //                                    return
        //                                }
        //                                guard let task_description = Data["task_description"]as? String else {
        //                                    return
        //                                }
        //                                guard let task_id = Data["task_id"]as? String else {
        //                                    return
        //                                }
        //                                guard let task_title = Data["task_title"]as? String else {
        //                                    return
        //                                }
        //                                self.taskOpenArray.append(OpenTask(customer_cell_phone_number: customer_cell_phone_number, customer_email_id: customer_email_id, customer_id: customer_id, customer_identity_card_number: customer_identity_card_number, customer_name: customer_name, customer_user_image: customer_user_image, task_date: task_date, task_description: task_description, task_id: task_id, task_title: task_title))
        //
        //                            }
        //                            print("Language_NameString_Array: \(self.taskOpenArray)")
        //                            //    //
        //                        }
        //                    }
        //                }
        //                else {
        //                    print("Error occured") // serialized json response
        //                }
        //
        //                // Make sure to update UI in main thread
        //                DispatchQueue.main.async {
        //                    self.tableView.reloadData()
        //                }
        //        }
    }
}
