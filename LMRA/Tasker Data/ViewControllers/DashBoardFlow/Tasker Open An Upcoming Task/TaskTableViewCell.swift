//
//  TaskTableViewCell.swift
//  LMRA
//
//  Created by Mac on 12/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class TaskTableViewCell: UITableViewCell {

    
    @IBOutlet weak var calenderLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var calenderImageView: UIImageView!
    @IBOutlet weak var backView: UIView!
    
    @IBOutlet weak var profilePicImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var decriptionLabel: UILabel!
    @IBOutlet weak var upcomingTaskView: UIView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnUpcoming: UIButton!
    @IBOutlet weak var btnOpen: UIButton!
    
    @IBOutlet weak var openTaskView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
