//
//  TaskerListAcceptTVCell.swift
//  LMRA
//
//  Created by Mac on 16/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class TaskerListAcceptTVCell: UITableViewCell {
    
    @IBOutlet weak var lblTaskStatus: UILabel!
    @IBOutlet weak var lblTaskUpdatedTime: UILabel!
    @IBOutlet weak var lblHiringService: UILabel!
    @IBOutlet weak var lblTaskTitle: UILabel!
    @IBOutlet weak var lblTaskDesc: UILabel!
    @IBOutlet weak var lblCustName: UILabel!
    @IBOutlet weak var lblCustHiring: UILabel!
    @IBOutlet weak var btnSms: UIButton!
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var btnDetail: UIButton!
    
    var data:TaskerTaskResponseModel? {
        didSet {
            self.lblTaskStatus.text = "You accepted task invitation!"
            self.lblTaskUpdatedTime.text = data?.task_update_time
            self.lblHiringService.text = ""
            self.lblTaskTitle.text = data?.task_title
            self.lblTaskDesc.text = data?.task_description
            self.lblCustName.text = data?.customer_name
            self.lblCustHiring.text = "\(data?.customer_hire_count ?? 0)+ Hires"
            self.imgUser.layer.cornerRadius = self.imgUser.frame.size.width/2
            self.imgUser.layer.borderWidth = 0.3
            self.imgUser.layer.borderColor = UIColor.lightGray.cgColor
            self.imgUser.sd_setImage(with: URL(string:(data?.customer_profile) as! String), placeholderImage: UIImage(named:""))
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
