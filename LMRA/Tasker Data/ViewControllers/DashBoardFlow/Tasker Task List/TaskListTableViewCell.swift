//
//  TaskListTableViewCell.swift
//  LMRA
//
//  Created by Mac on 08/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
protocol TaskListTableViewCellDelegate: class {
    func didTapTaskButton(cell: TaskListTableViewCell)
     func didTapSmsButton(cell: TaskListTableViewCell)
     func didTapCallButton(cell: TaskListTableViewCell)
}
class TaskListTableViewCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var tasUpdateLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    
    @IBOutlet weak var taskStatusView: UIView!
    
    @IBOutlet weak var taskStatusimageView: UIImageView!
    
    @IBOutlet weak var taskStatusLabel: UILabel!
    
    @IBOutlet weak var taskTitleLabel: UILabel!
    
    @IBOutlet weak var taskTitleButton: UIButton!
    @IBOutlet weak var taskDescriptionLabel: UILabel!
    
    @IBOutlet weak var nxtArrowImagaView: UIImageView!
    @IBOutlet weak var taskTitleView: UIView!
    
    
    @IBOutlet weak var cutomerView: UIView!
    
    @IBOutlet weak var custTitleLabel: UILabel!
    
    @IBOutlet weak var cusImageView: UIImageView!
    
    
    @IBOutlet weak var custNameLabel: UILabel!
    
    @IBOutlet weak var hireCountLabel: UILabel!
    
    @IBOutlet weak var buttonView: UIView!
    
    
    @IBOutlet weak var smsBtn: UIButton!
    
    @IBOutlet weak var callBtn: UIButton!
    weak var cellDelegate: TaskListTableViewCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func selectTaskBtnClicked(_ sender: Any) {
         cellDelegate?.didTapTaskButton(cell: self)
    }
    
    @IBAction func smsBtnClicked(_ sender: Any) {
         cellDelegate?.didTapSmsButton(cell: self)
    }
    
    @IBAction func callBtnClicked(_ sender: Any) {
         cellDelegate?.didTapCallButton(cell: self)
    }
    
    
}
