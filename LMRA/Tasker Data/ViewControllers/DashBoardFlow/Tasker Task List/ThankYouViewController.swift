//
//  ThankYouViewController.swift
//  LMRA
//
//  Created by Mac on 10/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class ThankYouViewController: UIViewController {
    
    @IBOutlet weak var cancelImageView: UIImageView!
    @IBOutlet weak var thankYouLabel: UILabel!
    @IBOutlet weak var descriptinLabel: UILabel!
    @IBOutlet weak var homeBtn: UIButton!
    
    var isfromTaskDetail:Bool = false
    
    override func viewDidLoad() {
        setNeedsStatusBarAppearanceUpdate()
        super.viewDidLoad()
        if isfromTaskDetail == true {
            self.thankYouLabel.text = "Task Request Cancelled"
            self.descriptinLabel.isHidden = true
        }
        else {
            self.thankYouLabel.text = "Thank you"
            self.descriptinLabel.isHidden = false
            self.descriptinLabel.text = "Our team will review your feedback, we always aim to improve our services!"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setStatusBarColor(color: UIColor.init(named: "colorTaskerGreen")!, styleForStatus: .lightContent)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    //MARK:- ViewController IBAction's
    @IBAction func homeBtnAction(_ sender: Any) {
        var isPopView = false
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: TaskerDasboardVC.self) {
                isPopView = true
                self.navigationController?.popToViewController(controller, animated: true)
                break
            }
        }
        
        if isPopView == false {
            self.navigationController?.popViewController(animated: true)
        }
    }
}
