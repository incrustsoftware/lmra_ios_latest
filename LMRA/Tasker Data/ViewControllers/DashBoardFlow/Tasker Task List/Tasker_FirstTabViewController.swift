//
//  Tasker_FirstTabViewController.swift
//  LMRA
//
//  Created by Mac on 07/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import FittedSheets
import Alamofire
import AlamofireSwiftyJSON
import Toast_Swift
import SwiftyJSON
import MessageUI
import SDWebImage


class Tasker_FirstTabViewController: UIViewController,UIGestureRecognizerDelegate,OfflineModeVCDelegate {
    
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var switchBtn: UISwitch!
    @IBOutlet weak var rustemLabel: UILabel!
    @IBOutlet weak var swipeLabel: UILabel!
    @IBOutlet weak var notifLabel: UILabel!
    @IBOutlet weak var newTaskLabel: UILabel!
    @IBOutlet weak var notifImageView: UIImageView!
    @IBOutlet weak var notifBtn: UIButton!
    @IBOutlet weak var swipeView: UIView!
    @IBOutlet weak var tblTask: UITableView!
    
    var isSwitchOff:Bool = false
    var taskArrayList:[TaskerTaskResponseModel]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.taskArrayList = [TaskerTaskResponseModel]()
        self.tblTask.register(UINib(nibName: "TaskerListInviteTaskTVCell", bundle: nil), forCellReuseIdentifier: "TaskerListInviteTaskTVCell")
        self.tblTask.register(UINib(nibName: "TaskerListAcceptTVCell", bundle: nil), forCellReuseIdentifier: "TaskerListAcceptTVCell")
        self.tblTask.isHidden = true
        self.tblTask.tableFooterView = UIView()
        self.getActiveTaskApi()
        
        NotificationCenter.default.addObserver(self,selector: #selector(self.onSuccessAcceptTask),name: NSNotification.Name(rawValue: UserDataManager.NotificationObserverKey.ACCEPT_SUCCESS_TASK),object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(self.onSuccessAcceptTask),name: NSNotification.Name(rawValue: UserDataManager.NotificationObserverKey.CANCEL_TASK_INVITATION_SUCCESS),object: nil)

        self.rustemLabel.text = UserDataManager.shared.getUserName()
        //        tabBarController?.selectedIndex = 0
        //        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        //        swipeLeft.direction = .left
        //        self.view.addGestureRecognizer(swipeLeft)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //self.switchBtn.isOn = true
        //GLOBALVARIABLE.TaskerTabBarName = "FirstTab"
        //self.tabBarController?.tabBar.isHidden = false
        //NotificationCenter.default.addObserver(self, selector: #selector(self.incomingNotification(_:)), name:  NSNotification.Name(rawValue: "checkSwicth"), object: nil)
    }
    
    //MARK:- ViewController IBAction's
    @IBAction func notificationButtonAction(_ sender: Any) {
        let viewController : NotificationListVC = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "NotificationListVC") as! NotificationListVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func switchBtnAction(_ sender: Any) {
        if switchBtn.isOn == true {
            print("ON")
            self.get_online_mode(resonId: "")
        }
        else {
            let vc = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "OfflineModeVC") as! OfflineModeVC
            vc.modalPresentationStyle = .custom
            vc.delegate = self
            self.present(vc, animated: true) {}
            //
            //            let controller = OfflineReasonBottomsheetViewController()
            //            let sheetController = SheetViewController(controller: controller)
            //            self.present(sheetController, animated: true, completion: nil)
        }
    }
    
    //MARK:- ViewController Own defined methods
    func succefully(isOffline: Bool) {
        if isOffline == true {
            switchBtn.isOn = false
        }
        else {
            switchBtn.isOn = true
        }
    }
    
    @objc func SelectedDataFromFilter(_ notification: NSNotification) {
        self.isSwitchOff = (notification.userInfo?["SelectedLangNameArrayValue"] as? Bool)!
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case .right:
                print("Swiped right")
                self.tabBarController?.selectedIndex = 0;
            case .down:
                print("Swiped down")
                
            case .left:
                
                print("Swiped left")
                
                self.tabBarController?.selectedIndex = 2;
                
            case .up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    @objc func didTapTaskButton(_ sender: Any) {
        guard let temp:TaskerTaskResponseModel = Optional.some(taskArrayList[(sender as AnyObject).tag]) else {
            self.view.makeToast("Something went's wrong")
            return
        }
        
        if temp.task_invitation_status == "Waiting" {
            let vc:TaskerInviteTaskDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "TaskerInviteTaskDetailVC")as!TaskerInviteTaskDetailVC
            vc.taskID = "\(temp.task_id)"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if temp.task_invitation_status == "Accepted" {
            let vc:TaskerAcceptedTaskDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "TaskerAcceptedTaskDetailVC")as!TaskerAcceptedTaskDetailVC
            vc.taskID = "\(temp.task_id)"
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }   
    
    @objc func didTapSmsButton(_ sender: Any) {
        guard let temp:TaskerTaskResponseModel = Optional.some(taskArrayList[(sender as AnyObject).tag]) else {
            self.view.makeToast("Something went's wrong")
            return
        }
        
        guard MFMessageComposeViewController.canSendText() else {
            return
        }
        
        let messageVC = MFMessageComposeViewController()
        messageVC.body = "Test message from incrust";
        messageVC.recipients = [temp.customer_cell_phone_number]
        messageVC.messageComposeDelegate = self;
        
        self.present(messageVC, animated: false, completion: nil)
    }
    
    @objc func didTapCallButton(_ sender: Any) {
        guard let temp:TaskerTaskResponseModel = Optional.some(taskArrayList[(sender as AnyObject).tag]) else {
            self.view.makeToast("Something went's wrong")
            return
        }
        
        if let phoneCallURL = URL(string: "telprompt://\(temp.customer_cell_phone_number)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                } else {
                    application.openURL(phoneCallURL as URL)
                }
            }
        }
    }
    
    @objc private func onSuccessAcceptTask(notification: NSNotification){
        self.getActiveTaskApi()
    }
}

//MARK:- TableViewController Delegate methods
extension Tasker_FirstTabViewController:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return taskArrayList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let data:TaskerTaskResponseModel = Optional.some(taskArrayList[indexPath.row]) else {
            print("")
        }
        if let task_invitation_status:String = Optional.some(data.task_invitation_status) {
            if task_invitation_status == "Waiting" {
                let cell:TaskerListInviteTaskTVCell = tableView.dequeueReusableCell(withIdentifier: "TaskerListInviteTaskTVCell", for: indexPath)as!TaskerListInviteTaskTVCell
                cell.data = data
                
                cell.btnDetail.tag = indexPath.row
                cell.btnDetail.addTarget(self, action: #selector(didTapTaskButton(_:)), for: .touchUpInside)
                return cell
            }
            else {
                let cell:TaskerListAcceptTVCell = tableView.dequeueReusableCell(withIdentifier: "TaskerListAcceptTVCell", for: indexPath)as!TaskerListAcceptTVCell
                cell.data = data
                
                cell.btnSms.tag = indexPath.row
                cell.btnSms.addTarget(self, action: #selector(didTapSmsButton(_:)), for: .touchUpInside)
                
                cell.btnCall.tag = indexPath.row
                cell.btnCall.addTarget(self, action: #selector(didTapCallButton(_:)), for: .touchUpInside)
                
                cell.btnDetail.tag = indexPath.row
                cell.btnDetail.addTarget(self, action: #selector(didTapTaskButton(_:)), for: .touchUpInside)
                return cell
            }
        }
        
        
        //        cell.custTitleLabel.text = "custoumer"
        //        cell.tasUpdateLabel.text = temp.task_date
        //        cell.timeLabel.text = temp.task_update_time
        //        cell.taskTitleLabel.text = temp.task_title
        //        cell.custNameLabel.text = temp.customer_name
        //        cell.hireCountLabel.text = "\(temp.customer_hire_count)+Hires"
        //        cell.taskDescriptionLabel.text = temp.task_description
        //        cell.cellDelegate = self
        //        cell.cusImageView.layer.cornerRadius = cell.cusImageView.layer.frame.height/2
        //        cell.cusImageView.clipsToBounds = true
        //
        //        if temp.task_invitation_status == "Waiting" {
        //            cell.taskStatusimageView.image = UIImage(named: "iconTaskStatus")
        //            cell.taskStatusLabel.text = "You have been invited for task!"
        //            cell.buttonView.isHidden = true
        //            cell.cusImageView.sd_setImage(with: URL(string:(temp.customer_profile) ), placeholderImage: UIImage(named: "iconCustomer"))
        //        }
        //        else {
        //            cell.taskStatusimageView.image = UIImage(named: "accepted")
        //            cell.taskStatusLabel.text = "You accepted task invitation!"
        //            cell.buttonView.isHidden = false
        //            cell.smsBtn.layer.borderColor = #colorLiteral(red: 0, green: 0.7985872626, blue: 0.6180523038, alpha: 1)
        //            cell.smsBtn.layer.borderWidth = 2
        //            cell.smsBtn.clipsToBounds = true
        //            cell.cusImageView.sd_setImage(with: URL(string:(temp.tasker_profile) ), placeholderImage: UIImage(named: "iconCustomer"))
        //        }
        
    }
}

extension Tasker_FirstTabViewController:MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result) {
        case .cancelled:
            print("Message was cancelled")
            self.dismiss(animated: true, completion: nil)
        case .failed:
            print("Message failed")
            self.dismiss(animated: true, completion: nil)
        case .sent:
            print("Message was sent")
            self.dismiss(animated: true, completion: nil)
        default:
            break;
        }
    }
}

//MARK:- ViewController API Calls
extension Tasker_FirstTabViewController {
    func get_online_mode(resonId:String) {
        APICallManager.shared.offlineModeApi(subUrl: ConstantsClass.get_offline_mode, parameter: ["offline_reason" :"","is_offline":"0"], isLoadingIndicatorShow: true) { (offlineModeDataModel) in
            DispatchQueue.main.async {
                if offlineModeDataModel.status {
                    if let response:Bool = Optional.some(offlineModeDataModel.response) {
                        if response == true {
                            self.succefully(isOffline: false)
                        }
                    }
                }
            }
        }
    }
    
    func getActiveTaskApi() {
        APICallManager.shared.getActiveTaskAPI(subUrl: ConstantsClass.get_active_task, parameter: [:], isLoadingIndicatorShow: true) { (data) in
            DispatchQueue.main.async {
                if data.status {
                    if let taskData:[TaskerTaskResponseModel] = Optional.some(data.response) {
                        self.taskArrayList = taskData
                    }
                    else {
                        self.isTableView(hide: true)
                        self.view.makeToast("Something went's wrong")
                    }
                    
                    if self.taskArrayList != nil && self.taskArrayList.count > 0 {
                        self.isTableView(hide: false)
                        self.tblTask.reloadData()
                    }
                    else {
                        self.isTableView(hide: true)
                    }
                }
                else {
                    self.isTableView(hide: true)
                }
                
                if let isTaskOnline:String = Optional.some(data.tasker_offline_status) {
                    if isTaskOnline == "1" {
                        self.switchBtn.isOn = false
                    }
                    else {
                        self.switchBtn.isOn = true
                    }
                }
            }
        }
    }
    
    func isTableView(hide:Bool) {
        if hide {
            self.tblTask.isHidden = true
            self.notifImageView.isHidden = false
            self.newTaskLabel.isHidden = false
        }
        else {
            self.tblTask.isHidden = false
            self.notifImageView.isHidden = true
            self.newTaskLabel.isHidden = true
        }
    }
}
