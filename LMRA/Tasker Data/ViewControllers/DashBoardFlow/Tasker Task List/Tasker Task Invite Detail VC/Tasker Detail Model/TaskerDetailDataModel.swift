//
//  TaskerDetailDataModel.swift
//  LMRA
//
//  Created by Mac on 17/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
//{
//    code = 200;
//    "message_code" = "tasker_detail_success";
//    response =     {
//        "customer_cell_phone_number" = 97376767676;
//        "customer_family_name" = ae;
//        "customer_hire_count" = 1;
//        "customer_id" = 1008;
//        "customer_identity_card_number" = 767676767;
//        "customer_name" = te;
//        "customer_nationality" = India;
//        "customer_profile" = "";
//        "task_address" =         {
//            "additional_directions" = test;
//            "address_nickname" = test;
//            appartment = 3;
//            "appartment_no" = test;
//            area = 6;
//            "building_name" = test;
//            floor = test;
//            street = test;
//        };
//        "task_date" = "2020-10-17";
//        "task_day" = Saturday;
//        "task_description" = Test;
//        "task_id" = 2649;
//        "task_invitation_status" = Accepted;
//        "task_invitation_status_id" = 2;
//        "task_timeslot_daytime" = Afternoon;
//        "task_timeslot_id" = 5;
//        "task_timeslot_name" = "3 - 5 pm";
//        "task_title" = test;
//        "task_update_time" = "03:07 am";
//        "tasker_name" = vaibhav;
//    };
//    status = 1;
//}
struct TaskerDetailDataModel:Codable {
    var status:Bool
    var code:Int
    var message_code:String
    var response:TaskerDetailResponseModel
    
    private enum TaskerDetailDataModelKeys: Any, CodingKey {
        case status
        case code
        case message_code
        case response
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: TaskerDetailDataModelKeys.self)
        self.status = try container.decode(Bool.self, forKey: .status)
        self.code = try container.decode(Int.self, forKey: .code)
        self.message_code = try container.decode(String.self, forKey: .message_code)
        self.response = try container.decode(TaskerDetailResponseModel.self, forKey: .response)
    }
}

struct TaskerDetailResponseModel:Codable {
    var customer_cell_phone_number:String
    var customer_family_name:String
    var customer_hire_count:Int
    var customer_id:Int
    var customer_identity_card_number:String
    var customer_name:String
    var customer_nationality:String
    var customer_profile:String
    var task_date:String
    var task_description:String
    var task_id:Int
    var task_invitation_status:String
    var task_invitation_status_id:String
    var task_title:String
    var task_update_time:String
    var tasker_name:String
    var task_address:TaskAddressModel
    var task_day:String
    var task_timeslot_daytime:String
    var task_timeslot_id:String
    var task_timeslot_name:String
    
    private enum TaskerDetailResponseModelKey: Any, CodingKey {
        case customer_cell_phone_number
        case customer_family_name
        case customer_hire_count
        case customer_id
        case customer_identity_card_number
        case customer_name
        case customer_nationality
        case customer_profile
        case task_date
        case task_description
        case task_id
        case task_invitation_status
        case task_invitation_status_id
        case task_title
        case task_update_time
        case tasker_name
        case tasker_offline_status
        case task_address
        case task_day
        case task_timeslot_daytime
        case task_timeslot_id
        case task_timeslot_name
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: TaskerDetailResponseModelKey.self)
        self.customer_cell_phone_number  = try container.decode(String.self, forKey: .customer_cell_phone_number)
        self.customer_family_name  = try container.decode(String.self, forKey: .customer_family_name)
        self.customer_hire_count  = try container.decode(Int.self, forKey: .customer_hire_count)
        self.customer_id  = try container.decode(Int.self, forKey: .customer_id)
        self.customer_identity_card_number  = try container.decode(String.self, forKey: .customer_identity_card_number)
        self.customer_name  = try container.decode(String.self, forKey: .customer_name)
        self.customer_nationality  = try container.decode(String.self, forKey: .customer_nationality)
        self.customer_profile  = try container.decode(String.self, forKey: .customer_profile)
        self.task_date = try container.decode(String.self, forKey: .task_date)
        self.task_description = try container.decode(String.self, forKey: .task_description)
        self.task_id  = try container.decode(Int.self, forKey: .task_id)
        self.task_invitation_status  = try container.decode(String.self, forKey: .task_invitation_status)
        self.task_invitation_status_id  = try container.decode(String.self, forKey: .task_invitation_status_id)
        self.task_title  = try container.decode(String.self, forKey: .task_title)
        self.task_update_time  = try container.decode(String.self, forKey: .task_update_time)
        self.tasker_name = try container.decode(String.self, forKey: .tasker_name)
        
        self.task_address = try container.decode(TaskAddressModel.self, forKey: .task_address)
        self.task_day = try container.decode(String.self, forKey: .task_day)
        self.task_timeslot_daytime = try container.decode(String.self, forKey: .task_timeslot_daytime)
        self.task_timeslot_id = try container.decode(String.self, forKey: .task_timeslot_id)
        self.task_timeslot_name = try container.decode(String.self, forKey: .task_timeslot_name)
    }
}

struct TaskAddressModel:Codable {
    var additional_directions:String
    var address_nickname:String
    var appartment:String
    var appartment_no:String
    var area:String
    var building_name:String
    var floor:String
    var street:String
    
    private enum TaskAddressModelKey: Any, CodingKey {
        case additional_directions
        case address_nickname
        case appartment
        case appartment_no
        case area
        case building_name
        case floor
        case street
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: TaskAddressModelKey.self)
        self.additional_directions  = try container.decode(String.self, forKey: .additional_directions)
        self.address_nickname  = try container.decode(String.self, forKey: .address_nickname)
        self.appartment  = try container.decode(String.self, forKey: .appartment)
        self.appartment_no  = try container.decode(String.self, forKey: .appartment_no)
        self.area  = try container.decode(String.self, forKey: .area)
        self.building_name  = try container.decode(String.self, forKey: .building_name)
        self.floor  = try container.decode(String.self, forKey: .floor)
        self.street  = try container.decode(String.self, forKey: .street)
    }
}
