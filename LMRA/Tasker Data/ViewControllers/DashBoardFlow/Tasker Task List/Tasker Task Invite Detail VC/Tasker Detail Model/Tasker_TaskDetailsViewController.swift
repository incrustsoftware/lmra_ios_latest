//
//  Tasker_TaskDetailsViewController.swift
//  LMRA
//
//  Created by Mac on 09/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import FittedSheets
import Alamofire
import AlamofireSwiftyJSON
import Toast_Swift
import SwiftyJSON
import MessageUI
import SDWebImage
import MessageUI
class Tasker_TaskDetailsViewController: UIViewController {
    
    @IBOutlet weak var timingLabel: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var middleView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var taskStatusLabel: UILabel!
    @IBOutlet weak var dividerView: UIView!
    @IBOutlet weak var calenderImageView: UIImageView!
    @IBOutlet weak var appointmentLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var periodLabel: UILabel!
    @IBOutlet weak var taskDetailLabel: UIView!
    @IBOutlet weak var dropdownButton: UIButton!
    @IBOutlet weak var taskTitleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var profilePicImageview: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var hirecountLabel: UILabel!
    @IBOutlet weak var rightArrowBtn: UIButton!
    @IBOutlet weak var acceptBtn: UIButton!
    @IBOutlet weak var declinebtn: UIButton!
    
    var id:Int = 0
    var taskDetail:TaskDetail = TaskDetail()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getTaskDetailsApi(task_id:"\(id)")
        declinebtn.layer.borderColor = #colorLiteral(red: 0, green: 0.7985872626, blue: 0.6180523038, alpha: 1)
        declinebtn.layer.borderWidth = 2
        declinebtn.clipsToBounds = true
    }
    
    //MARK:- ViewController IBAction's
    @IBAction func dropDownBtnAction(_ sender: Any) {
        
    }
    
    @IBAction func backArrowBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func rightArrowBtnAction(_ sender: Any) {
    }
    
    @IBAction func acceptBtnAction(_ sender: Any) {
        let vc:Tasker_CodoOfConductViewController = self.storyboard?.instantiateViewController(withIdentifier: "Tasker_CodoOfConductViewController")as!Tasker_CodoOfConductViewController
        //vc.id = self.id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func callBtnAction(_ sender: Any) {
        let temp:TaskDetail = taskDetail
        if let phoneCallURL = URL(string: "telprompt://\(temp.customer_cell_phone_number)") {
            
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                    application.openURL(phoneCallURL as URL)
                    
                }
            }
        }
        print("call")
    }
    
    @IBAction func smsBtnAction(_ sender: Any) {
        let temp:TaskDetail = taskDetail
        guard MFMessageComposeViewController.canSendText() else {
            return
        }
        
        let messageVC = MFMessageComposeViewController()
        
        messageVC.body = "Test message from incrust";
        messageVC.recipients = [temp.customer_cell_phone_number]
        messageVC.messageComposeDelegate = self;
        
        self.present(messageVC, animated: false, completion: nil)
        print("sms")
    }
    
    @IBAction func declineBtnAction(_ sender: Any) {
        
    }
    
    //MARK:- ViewController Own defined mehods
    func setUI() {
        let temp:TaskDetail = taskDetail
        self.timingLabel.text = temp.task_update_time
        self.taskStatusLabel.text = "You have been invited for task"
        self.dayLabel.text = temp.task_timeslot_daytime
        self.timeLabel.text = temp.task_update_time
        self.dateLabel.text = temp.task_date
        self.periodLabel.text = temp.task_timeslot_daytime
        self.taskTitleLabel.text = temp.task_title
        self.title = temp.task_title
        self.descriptionLabel.text = temp.task_description
        self.nameLabel.text = temp.tasker_name
        self.hirecountLabel.text = "\(temp.customer_hire_count)+hires"
    }
}

extension Tasker_TaskDetailsViewController:MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result) {
        case .cancelled:
            print("Message was cancelled")
            self.dismiss(animated: true, completion: nil)
        case .failed:
            print("Message failed")
            self.dismiss(animated: true, completion: nil)
        case .sent:
            print("Message was sent")
            self.dismiss(animated: true, completion: nil)
        default:
            break;
        }
    }
}

//MARK:- ViewController API Call's
extension Tasker_TaskDetailsViewController {
    func getTaskDetailsApi(task_id:String) {
        let api:APIEngine = APIEngine()
        api.getTaskDetailsAPI(param:["task_id":task_id]) { responseObject, error in
            print(Parameters.self)
            switch responseObject?.result  {
            case .success(let JSON)?:
                print("Success with JSON: \(JSON)")
                let jsonResponse = JSON
                if(jsonResponse["status"].bool == true) {
                    //   self.navToTimeSlot()
                    let parser:Parser = Parser()
                    self.taskDetail = parser.taskDetail(data: JSON)
                    print("taskk:\(self.taskDetail)")
                    self.setUI()
                }
                else {
                    Helper.sharedInstance.showAlert(title: "Message", message:jsonResponse["message_code"].stringValue )
                }
            case.failure(let error)?:
                print("error: \(error)")
            case .none:
                print("error: ")
            }
        }
    }
}
