//
//  Tasker_CodoOfConductViewController.swift
//  LMRA
//
//  Created by Mac on 09/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class Tasker_CodoOfConductViewController: UIViewController {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var textView: UITextView!
    
    var taskID:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK:- ViewController IBAction's
    @IBAction func closeBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func acceptBtnAction(_ sender: Any) {
        if taskID != nil {
            getTaskInviteApi(task_id:taskID)
        }
    }
    
    @IBAction func declineBtnAction(_ sender: Any) {
        if taskID != nil {
            getTaskCancelApi(task_id: taskID)
        }
    }
}

//MARK:- ViewController API Call's
extension Tasker_CodoOfConductViewController {
    func getTaskInviteApi(task_id:String) {
        let api:APIEngine = APIEngine()
        api.getTaskInviteAPI(param:["task_id":task_id]) { responseObject, error in
            
            switch responseObject?.result  {
            case .success(let JSON)?:
                print("Success with JSON: \(JSON)")
                let jsonResponse = JSON
                if(jsonResponse["status"].bool == true) {
                    let vc:TaskerAcceptedTaskDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "TaskerAcceptedTaskDetailVC")as!TaskerAcceptedTaskDetailVC
                    vc.isFromAcceptTask = true
                    vc.taskID = self.taskID
                    NotificationCenter.default.post(name: Notification.Name(UserDataManager.NotificationObserverKey.ACCEPT_SUCCESS_TASK), object: nil)
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else {
                    Helper.sharedInstance.showAlert(title: "Message", message:jsonResponse["message_code"].stringValue )
                }
            case.failure(let error)?:
                print("error: \(error)")
            case .none:
                print("error: ")
            }
        }
    }
    
    func getTaskCancelApi(task_id:String) {
        let api:APIEngine = APIEngine()
        api.getTaskCancelAPI(param:["task_id":task_id]) { responseObject, error in
            
            switch responseObject?.result  {
            case .success(let JSON)?:
                print("Success with JSON: \(JSON)")
                let jsonResponse = JSON
                if(jsonResponse["status"].bool == true) {
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    Helper.sharedInstance.showAlert(title: "Message", message:jsonResponse["message_code"].stringValue )
                }
            case.failure(let error)?:
                print("error: \(error)")
            case .none:
                print("error: ")
            }
        }
    }
}
