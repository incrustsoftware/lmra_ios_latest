//
//  TaskerInviteTaskDetailVC.swift
//  LMRA
//
//  Created by Mac on 16/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire

class TaskerInviteTaskDetailVC: UIViewController {

    @IBOutlet weak var lblHeaderTaskTitle: UILabel!
    @IBOutlet weak var lblTaskUpdateTime: UILabel!
    @IBOutlet weak var lblAppDay: UILabel!
    @IBOutlet weak var lblAppTime: UILabel!
    @IBOutlet weak var lblAppDayAnDate: UILabel!
    @IBOutlet weak var lblAppAMPM: UILabel!
    @IBOutlet weak var lblTaskTitle: UILabel!
    @IBOutlet weak var lblTaskDesc: UILabel!
    @IBOutlet weak var imgCust: UIImageView!
    @IBOutlet weak var lblCustName: UILabel!
    @IBOutlet weak var lblCustHires: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var taskID:String!
    override func viewDidLoad() {
        super.viewDidLoad()
        if let id = taskID {
            self.getTaskDetailsApi(task_id: id)
        }
    }
    
    //MARK:- ViewController IBAction's
    @IBAction func btnBackClickAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCancelClickAction(_ sender: Any) {
        let vc:TaskDeclineReasoneVC = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "TaskDeclineReasoneVC") as! TaskDeclineReasoneVC
        vc.taskID = self.taskID
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func btnAccpetClickAction(_ sender: Any) {
        let vc:Tasker_CodoOfConductViewController = self.storyboard?.instantiateViewController(withIdentifier: "Tasker_CodoOfConductViewController")as!Tasker_CodoOfConductViewController
        vc.taskID = self.taskID
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
   //MARK:- ViewController Own Define Methods
    func setTaskInviteData(taskerData:TaskerDetailResponseModel) {
        self.taskID = "\(taskerData.task_id)"
        self.lblHeaderTaskTitle.text = taskerData.task_title
        self.lblTaskUpdateTime.text = taskerData.task_update_time
        self.lblAppDay.text = taskerData.task_day
        self.lblAppTime.text = taskerData.task_timeslot_name
        if let date = taskerData.task_date.toDate(withFormat: "yyyy-mm-dd"),let strDate:String = Optional.some(date.toString(withFormat: "dd MMM yyyy"))  {
            self.lblAppDayAnDate.text = "\(taskerData.task_day) \(strDate)"
        }
       
        self.lblAppAMPM.text = taskerData.task_timeslot_daytime
        self.lblTaskTitle.text = taskerData.task_title
        self.lblTaskDesc.text = taskerData.task_description
        
        self.lblCustName.text = taskerData.customer_name
        self.lblCustHires.text = "\(taskerData.customer_hire_count)+ Hires"
        self.imgCust.layer.cornerRadius = self.imgCust.frame.size.width/2
        self.imgCust.layer.borderWidth = 0.3
        self.imgCust.layer.borderColor = UIColor.lightGray.cgColor
        self.imgCust.sd_setImage(with: URL(string:(taskerData.customer_profile) as! String), placeholderImage: UIImage(named:""))
    }
    
}

//MARK:- ViewController API Call's
extension TaskerInviteTaskDetailVC {
    func getTaskDetailsApi(task_id:String) {
        APICallManager.shared.getTaskDetailsApi(subUrl: ConstantsClass.get_task_detail, parameter: ["task_id":task_id], isLoadingIndicatorShow: true) { (taskerTaskDataModel) in
            if taskerTaskDataModel.status {
                if let response:TaskerDetailResponseModel = Optional.some(taskerTaskDataModel.response) {
                    print(response.customer_id)
                    DispatchQueue.main.async {
                        self.setTaskInviteData(taskerData: response)
                    }
                }
            }
        }
    }
}
