//
//  DeclineTaskResponseModel.swift
//  LMRA
//
//  Created by Mac on 17/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
//{
//    code = 200;
//    "message_code" = "decline_task_success";
//    status = 1;
//}
struct DeclineTaskResponseModel:Codable {
    var status:Bool
    var code:Int
    var message_code:String
    
    private enum DeclineTaskResponseModelKeys: Any, CodingKey {
        case status
        case code
        case message_code
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: DeclineTaskResponseModelKeys.self)
        self.status = try container.decode(Bool.self, forKey: .status)
        self.code = try container.decode(Int.self, forKey: .code)
        self.message_code = try container.decode(String.self, forKey: .message_code)
    }
}
