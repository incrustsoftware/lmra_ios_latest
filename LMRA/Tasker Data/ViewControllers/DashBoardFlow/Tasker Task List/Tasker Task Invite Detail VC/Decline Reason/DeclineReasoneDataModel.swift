//
//  DeclineReasoneDataModel.swift
//  LMRA
//
//  Created by Mac on 17/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
//{
//    code = 200;
//    "message_code" = "decline_reasons_list_success";
//    response =     (
//                {
//            "decline_reason" = Busy;
//            id = 1;
//        },
//                {
//            "decline_reason" = "Not avaialble";
//            id = 2;
//        },
//                {
//            "decline_reason" = "Personal Reason";
//            id = 3;
//        },
//                {
//            "decline_reason" = "Some Reason";
//            id = 4;
//        }
//    );
//    status = 1;
//}
struct DeclineReasoneDataModel:Codable {
    var status:Bool
    var code:Int
    var message_code:String
    var response:[DeclineReasoneResponseModel]
    
    private enum DeclineReasoneDataModelKeys: Any, CodingKey {
        case status
        case code
        case message_code
        case response
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: DeclineReasoneDataModelKeys.self)
        self.status = try container.decode(Bool.self, forKey: .status)
        self.code = try container.decode(Int.self, forKey: .code)
        self.message_code = try container.decode(String.self, forKey: .message_code)
        self.response = try container.decode([DeclineReasoneResponseModel].self, forKey: .response)
    }
}

struct DeclineReasoneResponseModel:Codable {
    var decline_reason:String
    var id:Int
    var isSelected:Bool
    
    private enum DeclineReasoneResponseModelKeys: Any, CodingKey {
        case decline_reason
        case id
        case isSelected
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: DeclineReasoneResponseModelKeys.self)
        self.decline_reason = try container.decode(String.self, forKey: .decline_reason)
        self.id = try container.decode(Int.self, forKey: .id)
        self.isSelected = false
    }
}
