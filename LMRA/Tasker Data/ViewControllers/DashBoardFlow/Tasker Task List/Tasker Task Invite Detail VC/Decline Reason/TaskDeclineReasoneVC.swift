//
//  TaskDeclineReasoneVC.swift
//  LMRA
//
//  Created by Mac on 17/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class TaskDeclineReasoneVC: UIViewController {
    
    @IBOutlet weak var tblReason: UITableView!
    
    var taskID:String!
    var selectedData :DeclineReasoneResponseModel!
    var arrReason:[DeclineReasoneResponseModel]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.arrReason = [DeclineReasoneResponseModel]()
        let nib = UINib(nibName: "ReasonsTableViewCell", bundle: nil)
        self.tblReason.register(nib, forCellReuseIdentifier: "reasonCell")
        self.get_reasons()
        // Do any additional setup after loading the view.
    }
    //MARK:- ViewController IBAction's
    @IBAction func btnContineClickAction(_ sender: Any) {
        if selectedData != nil && self.taskID != nil {
            self.declineTaskApi(decline_reason_id: "\(selectedData.id)", task_id: self.taskID)
        }
    }
    
    @IBAction func btnBackClickAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
}

//MARK:- TableView Delegate Methods
extension TaskDeclineReasoneVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrReason.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ReasonsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "reasonCell", for: indexPath)as!ReasonsTableViewCell
        let data:DeclineReasoneResponseModel = arrReason[indexPath.row]
        cell.radioBtn.isUserInteractionEnabled = false
        if data.isSelected {
            cell.radioBtn.setImage(UIImage(named: "radioSelected"), for: .normal)
        }
        else {
            cell.radioBtn.setImage(UIImage(named: "radioUnselected"), for: .normal)
        }
        cell.reasonLabel.text = data.decline_reason
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for (index,element) in arrReason.enumerated() {
            var data = element
            if index == indexPath.row {
                data.isSelected = true
                selectedData = data
            }
            else {
                data.isSelected = false
            }
            self.arrReason[index] = data
        }
        self.tblReason.reloadData()
    }
}

//MARK:- ViewController API Call's
extension TaskDeclineReasoneVC {
    func get_reasons() {
        APICallManager.shared.decline_task_reasonApi(subUrl: ConstantsClass.decline_task_reason, parameter: ["":""], isLoadingIndicatorShow: true) { (taskerTaskDataModel) in
            if taskerTaskDataModel.status {
                if let response:[DeclineReasoneResponseModel] = Optional.some(taskerTaskDataModel.response) {
                    DispatchQueue.main.async {
                        self.arrReason = response
                        self.tblReason.reloadData()
                    }
                }
            }
        }
    }
    
    func declineTaskApi(decline_reason_id:String,task_id:String) {
        APICallManager.shared.decline_task_invitationAPI(subUrl: ConstantsClass.decline_task_invitation, parameter: ["decline_reason_id":decline_reason_id,"task_id":task_id], isLoadingIndicatorShow: true) { (decline_taskModel) in
            if decline_taskModel.status {
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: Notification.Name(UserDataManager.NotificationObserverKey.CANCEL_TASK_INVITATION_SUCCESS), object: nil)
                    let vc:ThankYouViewController = self.storyboard?.instantiateViewController(withIdentifier: "ThankYouViewController")as!ThankYouViewController
                    vc.isfromTaskDetail = false
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        //        let api:APIEngine = APIEngine()
        //        api.declineReasonAPI(param:["decline_reason_id" :"self.GetLangID","task_id":"\(self.id)"]) { responseObject, error in
        //            print(Parameters.self)
        //            switch responseObject?.result  {
        //            case .success(let JSON)?:
        //                print("Success with JSON: \(JSON)")
        //                let jsonResponse = JSON
        //
        //                if(jsonResponse["status"].bool == true) {
        //                    let vc:ThankYouViewController = self.storyboard?.instantiateViewController(withIdentifier: "ThankYouViewController")as!ThankYouViewController
        //                    vc.isfromTaskDetail = false
        //                    self.navigationController?.pushViewController(vc, animated: true)
        //                }
        //                else {
        //                    Helper.sharedInstance.showAlert(title: "Message", message:jsonResponse["message_code"].stringValue )
        //                }
        //            case.failure(let error)?:
        //                print("error: \(error)")
        //
        //            case .none:
        //                print("error: ")
        //            }
        //        }
    }
}
