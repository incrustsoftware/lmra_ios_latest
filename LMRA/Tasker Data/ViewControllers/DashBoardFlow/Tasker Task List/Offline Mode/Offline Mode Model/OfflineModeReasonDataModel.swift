//
//  OfflineModeDataModel.swift
//  LMRA
//
//  Created by Mac on 15/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
struct OfflineModeReasonDataModel:Codable {
    var response:[OfflineModeReasonResponseModel]!
}

struct OfflineModeReasonResponseModel:Codable {
    var id:Int
    var offline_reason:String
    var isSelected:Bool
    
    private enum OfflineModeKeys: Any, CodingKey {
        case id
        case offline_reason
        case isSelected
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: OfflineModeKeys.self)
        self.id = try container.decode(Int.self, forKey: .id)
        self.offline_reason = try container.decode(String.self, forKey: .offline_reason)
        self.isSelected = false
    }
}
