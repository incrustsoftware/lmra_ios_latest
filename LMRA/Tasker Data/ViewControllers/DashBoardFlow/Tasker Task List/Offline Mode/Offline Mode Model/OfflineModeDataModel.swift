//
//  OfflineModeDataModel.swift
//  LMRA
//
//  Created by Mac on 17/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
struct OfflineModeDataModel:Codable {
    var status:Bool
    var code:Int
    var message_code:String
    var response:Bool
    
    private enum OfflineModeDataModelKeys: Any, CodingKey {
        case status
        case code
        case message_code
        case response
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: OfflineModeDataModelKeys.self)
        self.status = try container.decode(Bool.self, forKey: .status)
        self.code = try container.decode(Int.self, forKey: .code)
        self.message_code = try container.decode(String.self, forKey: .message_code)
        self.response = try container.decode(Bool.self, forKey: .response)
    }
}
