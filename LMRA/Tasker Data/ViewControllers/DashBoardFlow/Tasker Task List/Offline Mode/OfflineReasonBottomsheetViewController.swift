//
//  OfflineReasonBottomsheetViewController.swift
//  LMRA
//
//  Created by Mac on 07/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift
import SwiftyJSON
import AlamofireSwiftyJSON
class OfflineReasonBottomsheetViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, ReasonsTableViewCellDelegate {
    
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    var reasonName = [String]()
    var reasonID = [Int]()
    var selectedIdArray = [String]()
     var GetLangID_Array:[String] = []
    var BtnSelctedTag:Int!
    var GetLangID:String = ""
    var selected_Id:Int!
    var isSwitch:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "ReasonsTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "reasonCell")
        self.cancelButton.layer.borderColor = #colorLiteral(red: 0, green: 0.7985872626, blue: 0.6180523038, alpha: 1)
        cancelButton.layer.borderWidth = 2
        self.cancelButton.clipsToBounds = true
        self.tableView.tableFooterView = UIView()
       get_languages()
    }
    func get_languages()
        {
            let urlString = ConstantsClass.baseUrl + ConstantsClass.get_offline_reasons
            print("urlString: \(urlString)")
             guard let auth_key = UserDefaults.standard.string(forKey: "authentication_key") else {
                       return
                   }
                  let headers = [
                       "Authorization": "\(auth_key)"
                   ]
            Alamofire.request(urlString, method: .get, parameters: nil, encoding: URLEncoding.default,headers: headers).responseJSON
                {
                    response in
                    if let response = response.result.value as? [String:Any] {
                        print("response: \(response)")
                        
                        if (response["status"] as? Bool) != nil {
                            if let respArray = response["response"] as? [[String:Any]] {
                                print("respArray: \(respArray)")
                                
                                for Data in respArray
                                {

                                    self.reasonName.append(Data["offline_reason"] as! String)
                                    self.selected_Id = Data["id"]as? Int
                                    self.GetLangID = String(self.selected_Id)
                                    //self.reasonID.append(Data["id"]as!Int)
                                    self.selectedIdArray.append(self.GetLangID)
                                  
                                }
                                print("Language_NameString_Array: \(self.reasonName)\(self.reasonID)")
//
                            }
                        }
                        // Make sure to update UI in main thread
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                    }
                    else
                    {
                        print("Error occured") // serialized json response
                    }
            }
            
            
        }

    func addReasonApi()
        {
            
            let api:APIEngine = APIEngine()
            api.addReasonAPI(param:["offline_reason" :self.GetLangID,"is_offline":"1"])
                       { responseObject, error in
                        print(Parameters.self)
                                       switch responseObject?.result  {
                                       case .success(let JSON)?:
                                           print("Success with JSON: \(JSON)")
                                           let jsonResponse = JSON
                                          
                                           if(jsonResponse["status"].bool == true) {
                                            self.dismiss(animated: true, completion: nil)
                                           }
                                           else
                                           {
                                               Helper.sharedInstance.showAlert(title: "Message", message:jsonResponse["message_code"].stringValue )
                                          }
                                       case.failure(let error)?:
                                           print("error: \(error)")
                                           
                                       case .none:
                                           print("error: ")
                                       }
                                   }
        }


    @IBAction func confirmButtonAction(_ sender: Any) {
       // post a notification
        self.isSwitch = false
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "checkSwicth"), object: nil, userInfo: ["text":"false"])

        print(self.isSwitch) // textValue printing
        self.addReasonApi()
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        self.isSwitch = true
               NotificationCenter.default.post(name: NSNotification.Name(rawValue: "checkSwicth"), object: nil, userInfo: ["text":"true"])

               print(self.isSwitch) // textValue printing
        self.dismiss(animated: true, completion: nil)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reasonName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ReasonsTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "reasonCell", for: indexPath)as!ReasonsTableViewCell
        //let temp:[String] = reasonName[indexPath.row]
        cell.reasonLabel.text = reasonName[indexPath.row]
       // cell.cellDelegate = self
        cell.radioBtn.tag = indexPath.row
        cell.radioBtn.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        if self.BtnSelctedTag == indexPath.row
    {
        self.GetLangID = self.selectedIdArray[indexPath.row]
        print("GetLangID :\(self.GetLangID)")
        cell.radioBtn.setImage(UIImage(named: "radioSelected"), for: .normal)
       
    }
    if self.GetLangID_Array.contains(self.GetLangID) {
    if let itemToRemoveIndex = GetLangID_Array.firstIndex(of: self.GetLangID) {
    //GetLangID_Array.remove(at: itemToRemoveIndex)
    cell.radioBtn.setImage(UIImage(named: "radioUnselected"), for: .normal)
        }
        } else {
        self.GetLangID_Array.removeAll()
        self.GetLangID_Array.append(self.GetLangID)
        }
        print(self.GetLangID_Array)
                   //}
        return cell
    }
    @objc func buttonPressed(sender:UIButton,cell:ReasonsTableViewCell){
        self.BtnSelctedTag = sender.tag
        print("Selected item in row \(sender.tag)")
    let indexPath = tableView.indexPath(for: cell)
        self.tableView.reloadData()
        if sender.isSelected{
          // self.selectedIdArray.append(sender.tag)
            // self.selectedIdArray.remove(at:sender.tag)
            sender.isSelected = false
        }
        else
        {
          //  self.selectedIdArray.append(sender.tag)
            sender.isSelected = true
        }
    }
        func didTapButton(cell: ReasonsTableViewCell) {
             if let indexPath = tableView.indexPath(for: cell) {
                   print(indexPath.row)
                   if #available(iOS 12.0, *) {
                    if cell.radioBtn.isSelected == false
                    {
                        cell.radioBtn.setImage(UIImage(named: "radioUnselected"), for: .normal)
                        cell.radioBtn.isSelected = true
                    }
                    else
                    {
                        cell.radioBtn.setImage(UIImage(named: "radioSelected"), for: .normal)
                        cell.radioBtn.isSelected = false
                    }

                   } else {
                       // Fallback on earlier versions
                   }
                   
                  }
        }
    
    
}
