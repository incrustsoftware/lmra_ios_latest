//
//  ReasonsTableViewCell.swift
//  LMRA
//
//  Created by Mac on 07/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import RadioButton
protocol ReasonsTableViewCellDelegate: class {
    func didTapButton(cell: ReasonsTableViewCell)
}
class ReasonsTableViewCell: UITableViewCell {

    @IBOutlet weak var radioBtn: RadioButton!
    weak var cellDelegate: ReasonsTableViewCellDelegate?

    @IBOutlet weak var reasonLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func radioAction(_ sender: Any) {
        cellDelegate?.didTapButton(cell: self)
    }
    
    
}
