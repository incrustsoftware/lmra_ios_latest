//
//  OfflineModeVC.swift
//  LMRA
//
//  Created by Mac on 15/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire

protocol OfflineModeVCDelegate: class {
    func succefully(isOffline:Bool)
}

class OfflineModeVC: UIViewController,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var tblOfflineMode: UITableView!
    @IBOutlet weak var offlineModeBaseView: UIView!
    @IBOutlet weak var offlineModeBackView: UIView!
    var arrReasons : [OfflineModeReasonResponseModel]!
    var selectedData :OfflineModeReasonResponseModel!
    
    weak var delegate: OfflineModeVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.arrReasons = [OfflineModeReasonResponseModel]()
        let nib = UINib(nibName: "ReasonsTableViewCell", bundle: nil)
        self.tblOfflineMode.register(nib, forCellReuseIdentifier: "reasonCell")
        
        let panGesture:UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.handleDismiss(_:)))
        panGesture.delegate = self
        self.view.addGestureRecognizer(panGesture)
        
        let tapGesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.touchHappen(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.delegate = self
        self.offlineModeBackView.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setStatusBarColor(color: .clear, styleForStatus: .lightContent)
        setNeedsStatusBarAppearanceUpdate()
        self.get_offline_reasons()
    }
    
    @objc func touchHappen(_ sender: UITapGestureRecognizer) {
        self.delegate?.succefully(isOffline: false)
        self.dismiss(animated: true) {}
    }
    
    override func viewDidLayoutSubviews() {
        offlineModeBaseView.roundCorners(corners: [.topLeft, .topRight], radius: 30)
    }
    
    @IBAction func btnConfirmOfflineModeClickAction(_ sender: Any) {
        if selectedData != nil {
            self.get_offline_mode(resonId:"\(selectedData.id)")
        }
        else {
            self.view.makeToast("Please select reason...")
        }
        //self.dismiss(animated: true) {}
    }
    @IBAction func btnCancelOfflineModeClickAction(_ sender: Any) {
        self.delegate?.succefully(isOffline: false)
        self.dismiss(animated: true) {}
    }
    
    var viewTranslation = CGPoint(x: 0, y: 0)
    @objc func handleDismiss(_ sender: UIPanGestureRecognizer) {
        switch sender.state {
        case .changed:
            let velocity = sender.velocity(in: self.view)
            if(velocity.y > 0) {
                viewTranslation = sender.translation(in: self.view)
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.view.transform = CGAffineTransform(translationX: 0, y: self.viewTranslation.y)
                })
            }
            
        case .ended:
            let velocity = sender.velocity(in: self.view)
            if(velocity.y > 0) {
                if viewTranslation.y < 50 {
                    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                        self.view.transform = .identity
                    })
                } else {
                    self.view.transform = .identity
                    viewTranslation = CGPoint(x: 0, y: 0)
                    self.delegate?.succefully(isOffline: false)
                    self.dismiss(animated: true) {}
                }
            }
        default:
            break
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if let panGestureRecognizer = gestureRecognizer as? UIPanGestureRecognizer {
            let translation = panGestureRecognizer.translation(in: tblOfflineMode)
            if abs(translation.x) > abs(translation.y) {
                return true
            }
            return false
        }
        return false
    }
    
}

//MARK:- TableViewController Delegate methods
extension OfflineModeVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrReasons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ReasonsTableViewCell = self.tblOfflineMode.dequeueReusableCell(withIdentifier: "reasonCell", for: indexPath)as!ReasonsTableViewCell
        cell.radioBtn.isUserInteractionEnabled = false
        if arrReasons[indexPath.row].isSelected == true {
            cell.radioBtn.setImage(UIImage(named: "radioSelected"), for: .normal)
        }
        else {
            cell.radioBtn.setImage(UIImage(named: "radioUnselected"), for: .normal)
        }
        cell.reasonLabel.text = arrReasons[indexPath.row].offline_reason
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for (index,element) in arrReasons.enumerated() {
            var data = element
            if index == indexPath.row {
                data.isSelected = true
                selectedData = data
            }
            else {
                data.isSelected = false
            }
            self.arrReasons[index] = data
        }
        self.tblOfflineMode.reloadData()
    }
}

//MARK:- ViewController API Call's
extension OfflineModeVC {
    func get_offline_reasons() {
//        let urlString = ConstantsClass.baseUrl + ConstantsClass.get_offline_reasons
//        print("urlString: \(urlString)")
//        guard let auth_key:String = Optional.some(UserDataManager.shared.getAccesToken()) else {
//            self.view.makeToast("Something wents's wrong")
//            return
//        }
//        let headers = [
//            "Authorization": "\(auth_key)"
//        ]
//        Alamofire.request(urlString, method: .get, parameters: nil, encoding: URLEncoding.default,headers: headers).responseJSON
//            {
//                response in
//                if let data = response.data {
//                    do {
//                        let responseData = try JSONDecoder().decode(OfflineModeReasonDataModel.self, from: data)
//                        if responseData.response.count > 0 {
//                            self.arrReasons = responseData.response
//                            
//                            DispatchQueue.main.async {
//                                self.tblOfflineMode.reloadData()
//                            }
//                        }
//                    }
//                    catch {
//                        print(error.localizedDescription)
//                    }
//                }
//        }
        APICallManager.shared.getOfflineReasonsApi(subUrl: ConstantsClass.get_offline_reasons, parameter: [:], isLoadingIndicatorShow: true) { (responseData) in
            DispatchQueue.main.async {
                if responseData.response.count > 0 {
                    self.arrReasons = responseData.response
                    
                    DispatchQueue.main.async {
                        self.tblOfflineMode.reloadData()
                    }
                }
            }
        }
    }
    
    func get_offline_mode(resonId:String) {
        APICallManager.shared.offlineModeApi(subUrl: ConstantsClass.get_offline_mode, parameter: ["offline_reason" :resonId,"is_offline":"1"], isLoadingIndicatorShow: true) { (offlineModeDataModel) in
            DispatchQueue.main.async {
                if offlineModeDataModel.status {
                    if let response:Bool = Optional.some(offlineModeDataModel.response) {
                        if response == true {
                            self.delegate?.succefully(isOffline: true)
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                }
            }
        }
    }
}
