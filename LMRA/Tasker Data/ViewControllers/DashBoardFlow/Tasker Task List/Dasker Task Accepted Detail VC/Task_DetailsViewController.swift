//
//  Task_DetailsViewController.swift
//  LMRA
//
//  Created by Mac on 10/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import FittedSheets
import Alamofire
import AlamofireSwiftyJSON
import Toast_Swift
import SwiftyJSON
import MessageUI
import SDWebImage
class Task_DetailsViewController: UIViewController {
    
    @IBOutlet weak var topview: UIView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var titlelabel: UILabel!
    @IBOutlet weak var taskUpdateLabel: UILabel!
    @IBOutlet weak var timingLabel: UILabel!
    @IBOutlet weak var middleView: UIView!
    @IBOutlet weak var taskStatusImageView: UIImageView!
    @IBOutlet weak var dividerView: UIView!
    @IBOutlet weak var taskStatusLabel: UILabel!
    @IBOutlet weak var calenderImageView: UIImageView!
    @IBOutlet weak var appointmentLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var periodLabel: UILabel!
    @IBOutlet weak var superView: UIView!
    @IBOutlet weak var dropDownBtn: UIButton!
    @IBOutlet weak var taskDetailView: UIView!
    @IBOutlet weak var taskDetailLabel: UILabel!
    @IBOutlet weak var taskTitleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var locationImageView: UIImageView!
    @IBOutlet weak var addresLabel: UILabel!
    @IBOutlet weak var addresDetailLabel: UILabel!
    @IBOutlet weak var custoumerView: UIView!
    @IBOutlet weak var custmerImageView: UIImageView!
    @IBOutlet weak var customerNameLabel: UILabel!
    @IBOutlet weak var cusDropDownBtn: UIButton!
    @IBOutlet weak var hirecountLabel: UILabel!
    @IBOutlet weak var smsBtn: UIButton!
    @IBOutlet weak var callBtn: UIButton!
    @IBOutlet weak var acceptBtn: UIButton!
    @IBOutlet weak var declineBtn: UIButton!
    
    var id:Int = 0
    var taskDetail:TaskDetail = TaskDetail()
    var isSucces:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getTaskDetailsApi(task_id:"\(id)")
        declineBtn.layer.borderColor = #colorLiteral(red: 0, green: 0.7985872626, blue: 0.6180523038, alpha: 1)
        declineBtn.layer.borderWidth = 2
        declineBtn.clipsToBounds = true
        smsBtn.layer.borderColor = #colorLiteral(red: 0, green: 0.7985872626, blue: 0.6180523038, alpha: 1)
        smsBtn.layer.borderWidth = 2
        smsBtn.clipsToBounds = true
        if isSucces == false {
            self.declineBtn.setTitle("Decline", for: .normal)
            self.addressView.isHidden = true
            self.smsBtn.isHidden = true
            self.callBtn.isHidden = true
            self.taskStatusImageView.image = UIImage(named: "iconTaskStatus")
        }
        else {
            self.taskStatusImageView.image = UIImage(named: "accepted")
            self.declineBtn.setTitle("Cancel", for: .normal)
            self.addressView.isHidden = false
            self.smsBtn.isHidden = false
            self.callBtn.isHidden = false
            self.acceptBtn.isHidden = true
        }
        self.taskUpdateLabel.text = "Task Update"
    }
    
    //MARK:- ViewController IBAction's
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func callBtnAction(_ sender: Any) {
    }
    
    @IBAction func smsBtnAction(_ sender: Any) {
    }
    
    @IBAction func acceptBtnAction(_ sender: Any) {
        if self.isSucces == false {
            let vc:Tasker_CodoOfConductViewController = self.storyboard?.instantiateViewController(withIdentifier: "Tasker_CodoOfConductViewController")as!Tasker_CodoOfConductViewController
            //vc.id = self.id
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func declineBtnAction(_ sender: Any) {
        if isSucces == true {
            getTaskCancelApi(task_id:"\(self.id)")
        }
        else {
            let vc:Tasker_DeclineReasonViewController = self.storyboard?.instantiateViewController(withIdentifier: "Tasker_DeclineReasonViewController")as!Tasker_DeclineReasonViewController
            vc.id = self.id
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //MARK:- ViewController Own defined mehods
    func setUI() {
        Helper.sharedInstance.shadowView(view: self.taskDetailView)
        Helper.sharedInstance.shadowView(view: self.middleView)
        Helper.sharedInstance.shadowView(view: self.addressView)
        Helper.sharedInstance.shadowView(view: self.custoumerView)
        let temp:TaskDetail = taskDetail
        self.timingLabel.text = temp.task_update_time
        self.titlelabel.text = temp.task_title
        self.custmerImageView.sd_setImage(with: URL(string:(temp.customer_profile) ), placeholderImage: UIImage(named: "iconCustomer"))
        self.custmerImageView.layer.cornerRadius = self.custmerImageView.frame.height/2
        self.custmerImageView.clipsToBounds = true
        if self.isSucces == false {
            self.taskStatusLabel.text = "You have been invited for task!"
        }
        else {
            self.taskStatusLabel.text = "You accepted Task!"
            self.addresDetailLabel.text = "\(temp.appartment_no) \(temp.appartment) \(temp.building_name) \(temp.area)\(temp.address_nickname)"
        }
        self.dayLabel.text = temp.task_timeslot_daytime
        self.timeLabel.text = temp.task_update_time
        self.dateLabel.text = temp.task_date
        self.periodLabel.text = temp.task_timeslot_daytime
        self.taskTitleLabel.text = temp.task_title
        self.descriptionLabel.text = temp.task_description
        self.customerNameLabel.text = temp.tasker_name
        self.hirecountLabel.text = "\(temp.customer_hire_count)+hires"
    }
}

//MARK:- ViewController IBAction's
extension Task_DetailsViewController {
    func getTaskCancelApi(task_id:String) {
        let api:APIEngine = APIEngine()
        api.getTaskCancelAPI(param:["task_id":task_id]) { responseObject, error in
            
            switch responseObject?.result  {
            case .success(let JSON)?:
                print("Success with JSON: \(JSON)")
                let jsonResponse = JSON
                if(jsonResponse["status"].bool == true) {
                    let vc:ThankYouViewController = self.storyboard?.instantiateViewController(withIdentifier: "ThankYouViewController")as!ThankYouViewController
                    vc.isfromTaskDetail = true
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }
                else {
                    Helper.sharedInstance.showAlert(title: "Message", message:jsonResponse["message_code"].stringValue )
                }
            case.failure(let error)?:
                print("error: \(error)")
            case .none:
                print("error: ")
            }
        }
    }
    
    func getTaskDetailsApi(task_id:String) {
        let api:APIEngine = APIEngine()
        api.getTaskDetailsAPI(param:["task_id":task_id]) { responseObject, error in
            
            switch responseObject?.result  {
            case .success(let JSON)?:
                print("Success with JSON: \(JSON)")
                let jsonResponse = JSON
                if(jsonResponse["status"].bool == true) {
                    //   self.navToTimeSlot()
                    let parser:Parser = Parser()
                    self.taskDetail = parser.taskDetail(data: JSON)
                    print("taskk:\(self.taskDetail)")
                    self.setUI()
                    self.navigationController?.navigationBar.isHidden = true
                }
                else {
                    Helper.sharedInstance.showAlert(title: "Message", message:jsonResponse["message_code"].stringValue )
                }
            case.failure(let error)?:
                print("error: \(error)")
            case .none:
                print("error: ")
            }
        }
    }
}
