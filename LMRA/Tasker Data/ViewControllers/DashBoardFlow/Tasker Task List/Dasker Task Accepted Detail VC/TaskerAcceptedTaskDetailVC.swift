//
//  TaskerAcceptedTaskDetailVC.swift
//  LMRA
//
//  Created by Mac on 16/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire
import MessageUI

class TaskerAcceptedTaskDetailVC: UIViewController {
    
    @IBOutlet weak var lblHeaderTaskTitle: UILabel!
    @IBOutlet weak var lblTaskUpdateTime: UILabel!
    @IBOutlet weak var lblAppDay: UILabel!
    @IBOutlet weak var lblAppTime: UILabel!
    @IBOutlet weak var lblAppDayAnDate: UILabel!
    @IBOutlet weak var lblAppAMPM: UILabel!
    @IBOutlet weak var lblTaskTitle: UILabel!
    @IBOutlet weak var lblTaskDesc: UILabel!
    @IBOutlet weak var imgCust: UIImageView!
    @IBOutlet weak var lblCustName: UILabel!
    @IBOutlet weak var lblCustHires: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var taskID:String!
    var isFromAcceptTask:Bool!
    var taskerData:TaskerDetailResponseModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let id = taskID {
            self.getTaskDetailsApi(task_id: id)
        }
    }
    
    
    //MARK:- ViewController IBAction's
    @IBAction func btnBackClickAction(_ sender: Any) {
        if isFromAcceptTask != nil && isFromAcceptTask == true {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: TaskerDasboardVC.self) {
                    self.navigationController?.popToViewController(controller, animated: true)
                    break
                }
            }
        }
        else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnSMSClickAction(_ sender: Any) {
        if taskerData != nil {
            guard MFMessageComposeViewController.canSendText() else {
                return
            }
            
            let messageVC = MFMessageComposeViewController()
            messageVC.body = "Test message from incrust";
            messageVC.recipients = [taskerData.customer_cell_phone_number]
            messageVC.messageComposeDelegate = self;
            
            self.present(messageVC, animated: false, completion: nil)
        }
    }
    
    @IBAction func btnCALLClickAction(_ sender: Any) {
        if taskerData != nil {
            if let phoneCallURL = URL(string: "telprompt://\(taskerData.customer_cell_phone_number)") {
                
                let application:UIApplication = UIApplication.shared
                if (application.canOpenURL(phoneCallURL)) {
                    if #available(iOS 10.0, *) {
                        application.open(phoneCallURL, options: [:], completionHandler: nil)
                    } else {
                        // Fallback on earlier versions
                        application.openURL(phoneCallURL as URL)
                        
                    }
                }
            }
        }
    }
    @IBAction func btnCancelClickAction(_ sender: Any) {
        if taskID != nil {
            self.cancelTaskApi(task_id: taskID)
        }
        else if taskerData != nil, let taskId:Int = Optional.some(taskerData.task_id) {
            self.cancelTaskApi(task_id: "\(taskId)")
        }
    }
    
    //MARK:- ViewController Own Define Methods
    func setTaskInviteData(taskerData:TaskerDetailResponseModel) {
        self.lblHeaderTaskTitle.text = taskerData.task_title 
        self.lblTaskUpdateTime.text = taskerData.task_update_time  
        self.lblAppDay.text = taskerData.task_day  
        self.lblAppTime.text = taskerData.task_timeslot_name  
        if let date = taskerData.task_date.toDate(withFormat: "yyyy-mm-dd"),let strDate:String = Optional.some(date.toString(withFormat: "dd MMM yyyy"))  {
            self.lblAppDayAnDate.text = "\(taskerData.task_day) \(strDate)"
        }
        
        self.lblAppAMPM.text = taskerData.task_timeslot_daytime
        self.lblTaskTitle.text = taskerData.task_title
        self.lblTaskDesc.text = taskerData.task_description
        
        self.lblCustName.text = taskerData.customer_name
        self.lblCustHires.text = "\(taskerData.customer_hire_count)+ Hires"
        if let address:TaskAddressModel = Optional.some(taskerData.task_address)  {
            self.lblAddress.text = "\(address.appartment_no) \(address.appartment) \(address.building_name) \(address.area)\(address.address_nickname)"
        }
        self.imgCust.layer.cornerRadius = self.imgCust.frame.size.width/2
        self.imgCust.layer.borderWidth = 0.3
        self.imgCust.layer.borderColor = UIColor.lightGray.cgColor
        self.imgCust.sd_setImage(with: URL(string:(taskerData.customer_profile) as! String), placeholderImage: UIImage(named:""))
    }
    
}

//MARK:- ViewController API Call's
extension TaskerAcceptedTaskDetailVC {
    func getTaskDetailsApi(task_id:String) {
        APICallManager.shared.getTaskDetailsApi(subUrl: ConstantsClass.get_task_detail, parameter: ["task_id":task_id], isLoadingIndicatorShow: true) { (taskerTaskDataModel) in
            if taskerTaskDataModel.status {
                if let response:TaskerDetailResponseModel = Optional.some(taskerTaskDataModel.response) {
                    print(response.customer_id)
                    DispatchQueue.main.async {
                        self.taskerData = response
                        self.setTaskInviteData(taskerData: response)
                    }
                }
            }
        }
    }
    
    func cancelTaskApi(task_id:String) {
        APICallManager.shared.cancelTaskApi(subUrl: ConstantsClass.cancel_task_invitation, parameter: ["task_id":task_id], isLoadingIndicatorShow: true) { (taskerTaskDataModel) in
            if taskerTaskDataModel.status {
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: Notification.Name(UserDataManager.NotificationObserverKey.CANCEL_TASK_INVITATION_SUCCESS), object: nil)
                    let vc:ThankYouViewController = self.storyboard?.instantiateViewController(withIdentifier: "ThankYouViewController")as!ThankYouViewController
                    vc.isfromTaskDetail = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
}

extension TaskerAcceptedTaskDetailVC:MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result) {
        case .cancelled:
            print("Message was cancelled")
            self.dismiss(animated: true, completion: nil)
        case .failed:
            print("Message failed")
            self.dismiss(animated: true, completion: nil)
        case .sent:
            print("Message was sent")
            self.dismiss(animated: true, completion: nil)
        default:
            break;
        }
    }
}
