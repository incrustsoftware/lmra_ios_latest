//
//  Tasker_AccountSettingViewController.swift
//  LMRA
//
//  Created by Mac on 15/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import Toast_Swift
import SwiftyJSON
class Tasker_AccountSettingViewController: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var servicesVew: UIView!
    @IBOutlet weak var serviceTopView: UIView!
    @IBOutlet weak var serviceimageView: UIImageView!
    @IBOutlet weak var editServiceBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var timingView: UIView!
    @IBOutlet weak var timingTopView: UIView!
    @IBOutlet weak var timingImageView: UIImageView!
    @IBOutlet weak var timingLabel: UILabel!
    @IBOutlet weak var editTimingBtn: UIButton!
    @IBOutlet weak var editTimingLabel: UILabel!
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var languageView: UIView!
    @IBOutlet weak var editLanguageView: UIView!
    @IBOutlet weak var langimageview: UIImageView!
    @IBOutlet weak var langLabel: UILabel!
    @IBOutlet weak var editLangBtn: UIButton!
    @IBOutlet weak var langlabel: UILabel!
    @IBOutlet weak var languagesLabel: UILabel!
    
    var rateArray:[Rates] = []
    var workArray:[WorkDays] = []
    var langArray:[Languages] = []
    var timeslotArray:[WorkTime] = []
    var rate_id:String = ""
    var is_fixed :String = ""
    var rate :String = ""
    var AllDataDictionary = [[String:Any]]()
    var subcategory_name :String = ""
    var tasker_id:String = ""
    var tasker_name:String  = ""
    var tasker_cell_phone_number:String = ""
    var  tasker_identity_card_number:String = ""
    var  tasker_email_id:String = ""
    var tasker_working_time:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "ProfileServicesTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "serviceCell")
        self.tableView.tableFooterView = UIView()
        self.tableView.reloadData()
        getProileApi()
        Helper.sharedInstance.shadowView(view: self.servicesVew)
        Helper.sharedInstance.shadowView(view: self.timingView)
        Helper.sharedInstance.shadowView(view: self.languageView)
        Helper.sharedInstance.shadowView(view: self.tableView)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getProileApi()
    }
    
    //MARK:- ViewController IBAction's
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func closeBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func editLangBtnAction(_ sender: Any) {
        let vc:Tasker_LanguagesViewController = self.storyboard?.instantiateViewController(withIdentifier: "Tasker_LanguagesViewController")as!Tasker_LanguagesViewController
        vc.isForEdit = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func backbutton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func editServicesBtnAction(_ sender: Any) {
        let vc:Tasker_SelectCategoryViewController = self.storyboard?.instantiateViewController(withIdentifier: "Tasker_SelectCategoryViewController")as!Tasker_SelectCategoryViewController
        vc.isFromEditProfile = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func editTmingbtnAction(_ sender: Any) {
        let vc:WorkPreferViewController = self.storyboard?.instantiateViewController(withIdentifier: "WorkPreferViewController")as!WorkPreferViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func confrimBtnAction(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let rootViewController = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "TaskerDasboardVC")
        
        let navigationController = UINavigationController(rootViewController: rootViewController)
        let rightViewController = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "SideMenuViewController")
        let sideMenuController = LGSideMenuController(rootViewController: navigationController,
                                                      leftViewController: nil,
                                                      rightViewController: rightViewController)
        sideMenuController.leftViewWidth = 0
        sideMenuController.leftViewPresentationStyle = .slideBelow;
        
        sideMenuController.rightViewWidth = (appDelegate?.window?.bounds.size.width ?? 320) - 20
        sideMenuController.leftViewPresentationStyle = .slideBelow
        navigationController.isNavigationBarHidden = true
        navigationController.interactivePopGestureRecognizer?.delegate = self
        //self.interactivePopGestureRecognizer.enabled = NO;
        appDelegate?.window?.rootViewController = sideMenuController
        appDelegate?.window?.makeKeyAndVisible()
    }
    
    //MARK:- ViewController Own Defined methods
    func SetUI() {
        for i in langArray {
            print(i.language_name)
            self.languagesLabel.text = "\(i.language_name),"
        }
    }
    
    func setDayUI() {
        for i in workArray {
            print(i.day_name)
            self.editTimingLabel.text = "\(i.day_name)"
        }
    }
}

//MARK:- TableView Delegate Methods
extension Tasker_AccountSettingViewController:UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rateArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ProfileServicesTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "serviceCell", for: indexPath)as!ProfileServicesTableViewCell
        let temp:Rates = rateArray[indexPath.row]
        cell.serviceNameLabel.text = temp.subcategory_name
        cell.rateLabel.text = "\(temp.rate) BHD/\(temp.is_fixed)"
        return cell
    }
}

//MARK:- ViewController API Call's
extension Tasker_AccountSettingViewController {
    func getProileApi() {
        let urlString = ConstantsClass.baseUrl + ConstantsClass.get_Tasker_profileDetails
        print("urlString: \(urlString)")
        guard let auth_key = UserDefaults.standard.string(forKey: "authentication_key") else {
            return
        }
        let headers = [
            "Authorization": "\(auth_key)"
        ]
        Alamofire.request(urlString, method: .get, parameters: nil, encoding: URLEncoding.default,headers: headers).responseJSON { response in
            if let response = response.result.value as? [String:Any] {
                print("response: \(response)")
                
                if (response["status"] as? Bool) != nil {
                    if let respArray = response["response"] as? [[String:Any]] {
                        print("respArray: \(respArray)")
                        self.AllDataDictionary = respArray
                        //    for k in respArray
                        //    {
                        //    guard let tasker_id = k["tasker_id"] as? String else
                        //    {
                        //    return
                        //    }
                        //    guard let tasker_name = k["tasker_name"] as? String else
                        //    {
                        //    return
                        //    }
                        //    guard let tasker_cell_phone_number = k["tasker_cell_phone_number"] as? String else
                        //    {
                        //    return
                        //    }
                        //    guard let tasker_identity_card_number = k["tasker_identity_card_number"] as? String else
                        //    {
                        //    return
                        //    }
                        //    guard let tasker_email_id = k["tasker_email_id"] as? String else
                        //    {
                        //    return
                        //    }
                        //    guard let tasker_working_time = k["tasker_working_time"] as? String else
                        //    {
                        //    return
                        //    }
                        //    self.tasker_id = tasker_id
                        //    self.tasker_name = tasker_name
                        //    self.tasker_email_id = tasker_email_id
                        //    self.tasker_working_time = tasker_working_time
                        //    self.tasker_cell_phone_number = tasker_cell_phone_number
                        //    self.tasker_identity_card_number = tasker_identity_card_number
                        //    print("\(tasker_id)\(tasker_name)\(tasker_email_id)\(tasker_working_time)\(tasker_cell_phone_number)\(tasker_identity_card_number)")
                        //    }
                        for i in self.AllDataDictionary {
                            if let rate = i["tasker_rates"] as? [[String:Any]] {
                                for ratesData in rate {
                                    guard let rate_id = ratesData["rate_id"] as? String else {
                                        return
                                    }
                                    guard let subcategory_name = ratesData["subcategory_name"]as? String else {
                                        return
                                    }
                                    guard let rate = ratesData["rate"] as? String else{
                                        return
                                    }
                                    guard let is_fixed = ratesData["is_fixed"] as? String else {
                                        return
                                    }
                                    self.rateArray.append(Rates(rate_id: rate_id, subcategory_name: subcategory_name, rate: rate, is_fixed: is_fixed))
                                }
                                print("rateasrray \(self.rateArray)")
                            }
                            if let tasker_languages = i["tasker_languages"] as? [[String:Any]] {
                                for langData in tasker_languages {
                                    guard let language_id = langData["language_id"] as? String else {
                                        return
                                    }
                                    guard let language_name = langData["language_name"] as? String else {
                                        return
                                    }
                                    self.langArray.append(Languages(language_id: language_id, language_name: language_name))
                                    print("langrray \(self.langArray)")
                                    self.SetUI()
                                }
                            }
                            if let timingArray = i["tasker_working_days"] as? [[String:Any]] {
                                for time in timingArray {
                                    guard let day_id = time["day_id"] as? String else {
                                        return
                                    }
                                    guard let day_name = time["day_name"] as? String else {
                                        return
                                    }
                                    self.workArray.append(WorkDays(day_id: day_id, day_name: day_name))
                                }
                                print("DayArray:\(self.workArray)")
                                self.setDayUI()
                            }
                            //        if let timingSlotArray = i["tasker_working_time"] as? [[String:Any]]
                            //               {
                            //                   for timeSlot in timingSlotArray
                            //                   {
                            //                       guard let timeslot_id = timeSlot["timeslot_id"] as? Int else
                            //                       {
                            //                           return
                            //                       }
                            //                       guard let timeslot_name = timeSlot["timeslot_name"] as? String else
                            //                       {
                            //                           return
                            //                       }
                            //                    self.timeslotArray.append(WorkTime(timeslot_id: timeslot_id, timeslot_name: timeslot_name))
                            //                   }
                            //                   print("TimeslotArray:\(self.timeslotArray)")
                            //                   //self.setDayUI()
                            //               }
                            //  }
                            
                            
                        }
                        
                    }
                }
                // Make sure to update UI in main thread
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
            else {
                print("Error occured") // serialized json response
            }
        }
    }
}
