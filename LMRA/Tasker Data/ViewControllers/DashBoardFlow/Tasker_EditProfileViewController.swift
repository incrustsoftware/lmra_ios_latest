//
//  Tasker_EditProfileViewController.swift
//  LMRA
//
//  Created by Mac on 14/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import TextFieldEffects
import Alamofire
import SDWebImage
class Tasker_EditProfileViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIView!
    @IBOutlet weak var profileImageview: UIImageView!
    @IBOutlet weak var imageButton: UIButton!
    @IBOutlet weak var imageClickButton: UIButton!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var nameTextField: HoshiTextField!
    @IBOutlet weak var fullNameView: UIView!
    @IBOutlet weak var fullNameTextField: HoshiTextField!
    @IBOutlet weak var nationalityView: UIView!
    @IBOutlet weak var nationalityTextfield: HoshiTextField!
    @IBOutlet weak var phoneNumberView: UIView!
    @IBOutlet weak var phoneNumberTextField: HoshiTextField!
    @IBOutlet weak var placeholderImage: UIImageView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailTextField: HoshiTextField!
    @IBOutlet weak var deleteView: UIView!
    @IBOutlet weak var userCautionLabel: UILabel!
    @IBOutlet weak var deletLabel: UILabel!
    @IBOutlet weak var deletButton: UIButton!
    @IBOutlet weak var uploadBtn: UIButton!
    
    let picker = UIImagePickerController()
    var imagePicker = UIImagePickerController()
    var imageString = ""
    var trimedString:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        profileImageview.layer.cornerRadius = profileImageview.frame.height / 2
        imageButton.layer.cornerRadius = imageButton.frame.height / 2
        imageClickButton.layer.cornerRadius = imageClickButton.frame.height / 2
        // Helper.sharedInstance.timeslotshadowView(view: self.deleteView)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setStatusBarColor(color: .white, styleForStatus: .default)
        setNeedsStatusBarAppearanceUpdate()
        
        get_profileDetails()
        
        DispatchQueue.main.async {
            var contentRect = CGRect.zero
            
            for view in self.scrollView.subviews {
                contentRect = contentRect.union(view.frame)
            }
            
            self.scrollView.contentSize.height = contentRect.size.height
        }
    }
    
    //MARK:- ViewController IBAction's
    @IBAction func emailTextDidEnd(_ sender: Any) {
        if (emailTextField.text != "") {
            uploadBtn.isEnabled = true
            self.uploadBtn.setTitleColor(UIColor.white, for: .normal)
            self.uploadBtn.backgroundColor = Helper.sharedInstance.hexStringToUIColor(hex: "#00C48C")                }
        else {
            self.uploadBtn.backgroundColor = Helper.sharedInstance.hexStringToUIColor(hex: "#F3F3F1")
            self.uploadBtn.setTitleColor(Helper.sharedInstance.hexStringToUIColor(hex: "#8E9AA0"), for: .normal)
        }
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func imageClickBtnAction(_ sender: Any) {
        print("camera button click")
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as! UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func phoneNumberEditingDidBegin(_ sender: Any) {
        if self.phoneNumberTextField.text!.count > 11 {
            phoneNumberTextField.deleteBackward()
        }
    }
    
    //MARK:- ViewController Own Defined Method's
    func validate()->Bool {
        self.trimedString = self.emailTextField.text!
        self.trimedString =  self.trimedString.replacingOccurrences(of: " ", with: "")
        if (self.nameTextField.text == nil) {
            self.view.makeToast("Name text should not be blank")
            return false
        }
        else if (self.fullNameTextField.text == nil) {
            self.view.makeToast("full name should not be blank")
            return false
        }
        else if self.phoneNumberTextField.text == nil {
            self.view.makeToast("Please enter the phone number")
            return false
        }
        else if self.emailTextField.text == nil {
            self.view.makeToast("Please enter the Email ID")
            return false
        }
        else if self.nationalityTextfield.text == nil {
            self.view.makeToast("Please enter Nationality")
            return false
        }
        else if (Helper.sharedInstance.isValidEmail(testStr: trimedString) == false) {
            //BtnConfirm.backgroundColor = colorWithHexString(hexString: "#F3F3F1")
            self.view.makeToast("Please enter valid email address")
        }
        return true
    }
    
    func openCamera() {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary() {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func updateProfileApi(email_id:String, bitmap_image:String) {
        let api:APIEngine = APIEngine()
        api.getupdateProfileAPI(param:["email_id":email_id,"bitmap_image":bitmap_image]) { responseObject, error in
            Helper.sharedInstance.showLoader()
            switch responseObject?.result  {
            case .success(let JSON)?:
                print("Success with JSON: \(JSON)")
                let jsonResponse = JSON
                if(jsonResponse["status"].bool == true) {
                    self.get_profileDetails()
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    Helper.sharedInstance.showAlert(title: "Message", message:jsonResponse["message_code"].stringValue )
                }
                Helper.sharedInstance.hideLoader()
            case.failure(let error)?:
                print("error: \(error)")
                Helper.sharedInstance.hideLoader()
            case .none:
                print("error: ")
            }
        }
    }
    
    @available(iOS 12.0, *)
    @IBAction func deletButtonAction(_ sender: Any) {
        // Create the alert controller
        let alertController = UIAlertController(title: "Alert", message: "Are you sure to delete Account", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.deleteAccount()
            NSLog("OK Pressed")
        }
        let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func uploadButtonAction(_ sender: Any) {
        if !validate() {
            // self.uploadBtn.backgroundColor = Helper.sharedInstance.hexStringToUIColor(hex:"#F3F3F1")
            return
        }
        else {
            //self.uploadBtn.backgroundColor = Helper.sharedInstance.hexStringToUIColor(hex:"#00C48C")
            updateProfileApi(email_id: self.trimedString, bitmap_image: self.imageString)
        }
    }
}

//MARK:- ImagePickar Delegate methods
extension Tasker_EditProfileViewController:UIImagePickerControllerDelegate ,UINavigationControllerDelegate {
    //What to do when the picker returns with a photo
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let chosenImage = info[UIImagePickerController.InfoKey.originalImage]
            as? UIImage //2
        self.placeholderImage.isHidden = true
        profileImageview.image = chosenImage //4
        dismiss(animated: true, completion: nil) //5
        
        let scaledImage = UIImage.scaleImageWithDivisor(img: chosenImage!, divisor: 3)
        print("scaledImage:",scaledImage)
        
        //let imageData:NSData = scaledImage.pngData()! as NSData
        let imageData = scaledImage.lowestQualityJPEGNSData
        self.imageString = imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        print("imageString....\(imageString)")
        uploadBtn.isEnabled = true
        self.uploadBtn.setTitleColor(UIColor.white, for: .normal)
        self.uploadBtn.backgroundColor = Helper.sharedInstance.hexStringToUIColor(hex: "#00C48C")
    }
    
    //What to do if the image picker cancels.
    private func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

//MARK:- ViewController API Call's
extension Tasker_EditProfileViewController {
    func get_profileDetails() {
        let urlString = ConstantsClass.baseUrl + ConstantsClass.get_tasker_basic_profile_details
        print("urlString: \(urlString)")
        guard let auth_key = UserDefaults.standard.string(forKey: "authentication_key") else {
            return
        }
        let headers = [
            "Authorization": "\(auth_key)"
        ]
        Alamofire.request(urlString, method: .get, parameters: nil, encoding: URLEncoding.default,headers: headers).responseJSON {
            response in
            if let response = response.result.value as? [String:Any] {
                print("response: \(response)")
                Helper.sharedInstance.showLoader()
                if (response["status"] as? Bool) != nil {
                    if let respArray = response["response"] as? [[String:Any]] {
                        print("respArray: \(respArray)")
                        
                        for Data in respArray
                        {
                            guard let tasker_id = Data["tasker_id"]as? String else {
                                return
                            }
                            guard let tasker_cell_phone_number = Data["tasker_cell_phone_number"]as? String else {
                                return
                            }
                            guard let tasker_name = Data["tasker_name"]as? String else {
                                return
                            }
                            guard let tasker_email_id = Data["tasker_email_id"]as? String else {
                                return
                            }
                            guard let tasker_identity_card_number = Data["tasker_identity_card_number"]as? String else {
                                return
                            }
                            guard let tasker_profile = Data["tasker_profile"]as? String else {
                                return
                            }
                            guard let tasker_nationality = Data["tasker_nationality"]as? String else {
                                return
                            }
                            guard let tasker_family_name = Data["tasker_family_name"]as? String else {
                                return
                            }
                            self.nameTextField.text = tasker_name
                            self.fullNameTextField.text = tasker_family_name
                            self.emailTextField.text = tasker_email_id
                            self.phoneNumberTextField.text = tasker_cell_phone_number
                            self.nationalityTextfield.text = tasker_nationality
                            
                            //self.profileImageview.sd_setImage(with: URL(string:(tasker_profile) ), placeholderImage: UIImage(named:""))
                            self.placeholderImage.isHidden = true
                            
                        }
                        Helper.sharedInstance.hideLoader()
                        // print("Language_NameString_Array: \(self.taskArray)")
                        //    //
                    }
                }
                // Make sure to update UI in main thread
                DispatchQueue.main.async {
                    // self.tableView.reloadData()
                }
            }
            else {
                self.placeholderImage.isHidden = false
                print("Error occured") // serialized json response
            }
        }
    }
    
    @available(iOS 12.0, *)
    func deleteAccount() {
        let urlString = ConstantsClass.baseUrl + ConstantsClass.delete_tasker_account
        print("urlString: \(urlString)")
        guard let auth_key = UserDefaults.standard.string(forKey: "authentication_key") else {
            return
        }
        let headers = [
            "Authorization": "\(auth_key)"
        ]
        Alamofire.request(urlString, method: .get, parameters: nil, encoding: URLEncoding.default,headers: headers).responseJSON { response in
            if let response = response.result.value as? [String:Any] {
                print("response: \(response)")
                
                if (response["status"] as? Bool) != nil {
                    if let respArray = response["response"] as? [[String:Any]] {
                        print("respArray: \(respArray)")
                        self.view.makeToast("Delete Account Succesfully", duration: 6.0, position: .bottom)
                        UserDataManager.shared.clearUserDetails()
                    }
                }
                // Make sure to update UI in main thread
                DispatchQueue.main.async {
                    let nextViewController = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "GetStartedViewController") as! GetStartedViewController
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                }
            }
            else {
                print("Error occured") // serialized json response
            }
        }
    }
}
