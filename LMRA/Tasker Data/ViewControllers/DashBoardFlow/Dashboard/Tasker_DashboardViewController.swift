//
//  Tasker_DashboardViewController.swift
//  LMRA
//
//  Created by Mac on 07/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class Tasker_DashboardViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //
        //        let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! Tasker_ConfirmDetailsViewController
        //        let rightViewController = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
        //        let rootViewController = homeVC
        //
        //        let navigationController = UINavigationController(rootViewController: rootViewController)
        //        navigationController.isNavigationBarHidden = true
        //
        //
        //        let sideMenuController = LGSideMenuController(rootViewController: navigationController,
        //                                                      leftViewController: nil,
        //                                                      rightViewController:rightViewController)
        //
        //        sideMenuController.rightViewWidth = 250.0;
        //        sideMenuController.tableViewHeaderHeight = 75.0
        //        sideMenuController.rightViewBackgroundAlpha = 0
        //        sideMenuController.rightViewPresentationStyle = .slideAbove;
        self.navigationController?.navigationBar.barTintColor = Helper.sharedInstance.hexStringToUIColor(hex: "#4f008c")
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

