//
//  TaskerTaskDataModel.swift
//  LMRA
//
//  Created by Mac on 16/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
//({
//    code = 200;
//    "message_code" = "tasker_task_list_success";
//    response =     (
//                {
//            "customer_cell_phone_number" = 97389898989;
//            "customer_family_name" = bhosale;
//            "customer_hire_count" = 1;
//            "customer_id" = 1006;
//            "customer_identity_card_number" = 898989898;
//            "customer_name" = Vishal;
//            "customer_nationality" = "Indian ";
//            "customer_profile" = "";
//            "task_date" = "2020-10-17";
//            "task_description" = Test;
//            "task_id" = 2632;
//            "task_invitation_status" = Accepted;
//            "task_invitation_status_id" = 2;
//            "task_title" = "test pipe fitting ";
//            "task_update_time" = "08:19 am";
//            "tasker_name" = vaibhav;
//            "tasker_offline_status" = 0;
//            "tasker_profile" = "";
//        }
//    );
//    status = 1;
//    "tasker_offline_status" = 0;
//})

struct TaskerTaskDataModel:Codable {
    var status:Bool
    var code:Int
    var message_code:String
    var tasker_offline_status:String
    var response:[TaskerTaskResponseModel]
    
    private enum TaskerTaskDataModelKeys: Any, CodingKey {
        case status
        case code
        case tasker_offline_status
        case message_code
        case response
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: TaskerTaskDataModelKeys.self)
        if container.contains(TaskerTaskDataModelKeys.response) {
            self.response = try container.decode([TaskerTaskResponseModel].self, forKey: .response)
        }
        else {
            self.response = []
        }
        self.status = try container.decode(Bool.self, forKey: .status)
        self.code = try container.decode(Int.self, forKey: .code)
        self.message_code = try container.decode(String.self, forKey: .message_code)
        
        self.tasker_offline_status = try container.decode(String.self, forKey: .tasker_offline_status)
        
//        do {
//            self.response = try container.decode([TaskerTaskResponseModel].self, forKey: .response)
//        } catch DecodingError.typeMismatch {
//            self.response = [TaskerTaskResponseModel]()
//        }
    }
    
//    func decode(_ type: Bool.Type, forKey key: KeyedDecodingContainer.Key) throws -> Bool {
//        guard encodedObject.contain(key.stringValue) else {
//            throw ... //key not found
//        }
//
//        guard let decodedValue = encodedObject[key.stringValue] else {
//            throw ... //value not found
//        }
//
//        guard let typedValue = decodedValue as? Bool else {
//            throw ... //type mismatch
//        }
//
//        return typedValue
//    }
}
//
//public struct OptionalObject<TaskerTaskResponseModel: Decodable>: Decodable {
//    let response: [TaskerTaskResponseModel]?
//
//    public init(from decoder: Decoder) throws {
//        do {
//            let container = try decoder.singleValueContainer()
//            self.response = try container.decode([TaskerTaskResponseModel].self)
//        } catch {
//            self.response = []
//        }
//    }
//}

struct TaskerTaskResponseModel:Codable {
    var customer_cell_phone_number:String
    var customer_family_name:String
    var customer_hire_count:Int
    var customer_id:Int
    var customer_identity_card_number:String
    var customer_name:String
    var customer_nationality:String
    var customer_profile:String
    var task_date:String
    var task_description:String
    var task_id:Int
    var task_invitation_status:String
    var task_invitation_status_id:String
    var task_title:String
    var task_update_time:String
    var tasker_name:String
    var tasker_offline_status:String
    var tasker_profile:String
    
    private enum TaskerTaskResponseModelKeys: Any, CodingKey {
        case customer_cell_phone_number
        case customer_family_name
        case customer_hire_count
        case customer_id
        case customer_identity_card_number
        case customer_name
        case customer_nationality
        case customer_profile
        case task_date
        case task_description
        case task_id
        case task_invitation_status
        case task_invitation_status_id
        case task_title
        case task_update_time
        case tasker_name
        case tasker_offline_status
        case tasker_profile
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: TaskerTaskResponseModelKeys.self)
        self.customer_cell_phone_number  = try container.decode(String.self, forKey: .customer_cell_phone_number)
        self.customer_family_name  = try container.decode(String.self, forKey: .customer_family_name)
        self.customer_hire_count  = try container.decode(Int.self, forKey: .customer_hire_count)
        self.customer_id  = try container.decode(Int.self, forKey: .customer_id)
        self.customer_identity_card_number  = try container.decode(String.self, forKey: .customer_identity_card_number)
        self.customer_name  = try container.decode(String.self, forKey: .customer_name)
        self.customer_nationality  = try container.decode(String.self, forKey: .customer_nationality)
        self.customer_profile  = try container.decode(String.self, forKey: .customer_profile)
        self.task_date = try container.decode(String.self, forKey: .task_date)
        self.task_description = try container.decode(String.self, forKey: .task_description)
        self.task_id  = try container.decode(Int.self, forKey: .task_id)
        self.task_invitation_status  = try container.decode(String.self, forKey: .task_invitation_status)
        self.task_invitation_status_id  = try container.decode(String.self, forKey: .task_invitation_status_id)
        self.task_title  = try container.decode(String.self, forKey: .task_title)
        self.task_update_time  = try container.decode(String.self, forKey: .task_update_time)
        self.tasker_name = try container.decode(String.self, forKey: .tasker_name)
        self.tasker_offline_status  = try container.decode(String.self, forKey: .tasker_offline_status)
        self.tasker_profile = try container.decode(String.self, forKey: .tasker_profile)
    }
}


