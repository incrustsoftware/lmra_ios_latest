//
//  TaskerDashboardTabbarVC.swift
//  LMRA
//
//  Created by Mac on 14/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class TaskerDashboardTabbarVC: UITabBarController, UITabBarControllerDelegate {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
        let item1 = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "Tasker_FirstTabViewController")
        let icon1 = UITabBarItem(title: "", image: UIImage(named: "Home"), selectedImage: UIImage(named: "selectedHome"))
        item1.tabBarItem.isSpringLoaded = true
        item1.tabBarItem = icon1
        
        let item2 = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "Tasker_TasksViewController")
        let icon2 = UITabBarItem(title: "", image: UIImage(named: "Diamond"), selectedImage: UIImage(named: "selectedDiamond"))
        item2.tabBarItem = icon2
        item2.tabBarItem.isSpringLoaded = true
        let item3 = SideMenuViewController()
        let icon3 = UITabBarItem(title: "", image: UIImage(named: "hamburger"), selectedImage: UIImage(named: "hamburger"))
        item3.tabBarItem = icon3
        item3.tabBarItem.isSpringLoaded = true
        let controllers = [item1,item2,item3]  //array of the root view controllers displayed by the tab bar interface
        self.viewControllers = controllers
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //        if let item1 = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "Tasker_FirstTabViewController") as? Tasker_FirstTabViewController {
        //
        //            let navgitaionController1 = UINavigationController(rootViewController: item1)
        //            navgitaionController1.navigationBar.isHidden = true
        //            let icon1 = UITabBarItem(title: "", image: UIImage(named: "Home"), selectedImage: UIImage(named: "selectedHome"))
        //            navgitaionController1.tabBarItem = icon1
        //            navgitaionController1.tabBarItem.isSpringLoaded = true
        //            array.append(navgitaionController1)
        //        }
        //
        //        if let item2 = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "Tasker_TasksViewController") as? Tasker_TasksViewController {
        //
        //            let navgitaionController2 = UINavigationController(rootViewController: item2)
        //            navgitaionController2.navigationBar.isHidden = true
        //            let icon2 = UITabBarItem(title: "", image: UIImage(named: "Diamond"), selectedImage: UIImage(named: "selectedDiamond"))
        //            navgitaionController2.tabBarItem = icon2
        //            navgitaionController2.tabBarItem.isSpringLoaded = true
        //            array.append(navgitaionController2)
        //        }
        //
        //
        //        let navgitaionController3 = UINavigationController(rootViewController: SideMenuViewController())
        //        navgitaionController3.navigationBar.isHidden = true
        //        let icon2 = UITabBarItem(title: "", image: UIImage(named: "hamburger"), selectedImage: UIImage(named: "hamburger"))
        //        navgitaionController3.tabBarItem = icon2
        //
        //        array.append(navgitaionController3)
        //
        //        self.viewControllers = array
        
    }
    
    @objc func loadSideMenu(_ sender: Any) {
        sideMenuController?.showRightView(animated: true, completionHandler: {
        })
    }
    
    //Delegate methods
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        //let navigationController = sideMenuController?.rootViewController as! UINavigationController
        //navigationController.navigationBar.isHidden = true
        //self.navigationController?.navigationBar.isHidden = true
        
        if viewController.isKind(of: SideMenuViewController.self) {
            sideMenuController?.showRightView(animated: true, completionHandler: {
                //let navigationController = self.sideMenuController?.rootViewController as! UINavigationController
                //navigationController.navigationBar.isHidden = true
                //self.navigationController?.navigationBar.isHidden = true
            })
            return false
        }
        
        print("Should select viewController: \(viewController.title ?? "") ?")
        return true;
    }
}
