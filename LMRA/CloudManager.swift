//
//  CloudeManager.swift
//  LMRA
//
//  Created by Mac on 17/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import Alamofire
//import ObjectMapper

class BackendAPIManager: NSObject {
    static let sharedInstance = BackendAPIManager()
    
    var alamoFireManager : Alamofire.SessionManager!
    
    var backgroundCompletionHandler: (() -> Void)? {
        get {
            return alamoFireManager?.backgroundCompletionHandler
        }
        set {
            alamoFireManager?.backgroundCompletionHandler = newValue
        }
    }
    
    fileprivate override init() {
        let configuration = URLSessionConfiguration.background(withIdentifier: "com.url.background")
        configuration.timeoutIntervalForRequest = 200 // seconds
        configuration.timeoutIntervalForResource = 200
        self.alamoFireManager = Alamofire.SessionManager(configuration: configuration)
    }
}

class CloudManager:NSObject {
    
    let errorResponse:DataResponse<Any>? = nil
    let errorResponseString:DataResponse<String>? = nil
    let apiOperationQueue = OperationQueue()
    
    func makeCloudRequest(subUrl:String,parameter:[String:Any],requestType:HTTPMethod ,completionHandler: @escaping (DataResponse<Any>?) -> Void ){
        guard NetworkCheck.isNetwork() else {
            self.getcurrentControllerview().makeToast("Network Connectivity Problem")
            return
        }
        
        let Baseurl = ConstantsClass.baseUrl + subUrl
        
        Helper.sharedInstance.showLoader()
        DispatchQueue.global(qos: .background).async {
            print("This is run on the background queue")
            BackendAPIManager.sharedInstance.alamoFireManager.request(Baseurl, method: requestType, parameters: parameter, encoding: JSONEncoding.default, headers: RequestHeader.getHeadersWithAuth()).responseJSON { response in
                print("This is run on the main queue, after the previous code in outer block")
                DispatchQueue.main.async {
                    Helper.sharedInstance.hideLoader()
                }
                guard response.error == nil else {
                    print("New Error:\(response.error.debugDescription)")
                    //self.showTostMessage(strMessage: Str_Constant.ToastMsg().somethingwentwrong)
                    return
                }
                
                print(NSString(data: (response.request?.httpBody)!, encoding: String.Encoding.utf8.rawValue) ?? "fail")
                print(response)
                print(response.result.value!)
                switch response.result {
                case .success:
                    print("Validation Successful")
                case .failure(let error):
                    print(error)
                }
                print(response.result.value ?? "")
                completionHandler(response)
            }
        }
    }
    
    fileprivate func convertToJSONString(parameter:[String:Any])->String{
        if let json = try? JSONSerialization.data(withJSONObject: parameter, options: []) {
            // here `json` is your JSON data
            if let content = String(data: json, encoding: String.Encoding.utf8) {
                // here `content` is the JSON data decoded as a String
                print(content)
                return content
            }
            else {
                return ""
            }
        }
        else {
            return ""
        }
    }
}

class NetworkCheck {
    //Wrapper for internet Check
    class func isNetwork()->Bool{
        return NetworkReachabilityManager()!.isReachable
    }
}



