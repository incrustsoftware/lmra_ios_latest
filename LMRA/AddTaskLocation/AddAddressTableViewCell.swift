//
//  AddAddressTableViewCell.swift
//  LMRA
//
//  Created by Mac on 21/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class AddAddressTableViewCell: UITableViewCell {
@IBOutlet weak var AddressView: UIView!
@IBOutlet weak var LblFullAddress: UILabel!
@IBOutlet weak var BtnEdit: UIButton!
@IBOutlet weak var LblAddressType: UILabel!
       
    @IBOutlet weak var AddressNameLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
//        AddressView.layer.cornerRadius = 5;
//        AddressView.layer.masksToBounds = true;
//        self.AddressView.layer.borderWidth = 1
//        self.AddressView.layer.borderColor = UIColor(red:221/255, green:224/255, blue:226/255, alpha: 1).cgColor
        AddressView.layer.shadowColor = UIColor.gray.cgColor
        AddressView.layer.shadowOpacity = 0.5
        AddressView.layer.shadowOffset = CGSize.zero
        AddressView.layer.shadowRadius = 6
        AddressView.layer.cornerRadius = 5
        clipsToBounds = false
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
