//
//  AddTaskLocationViewController.swift
//  LMRA
//
//  Created by Mac on 19/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire

class AddTaskLocationViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var AddAddressTableView: UITableView!
    @IBOutlet weak var LblAddAddress: UILabel!
    @IBOutlet weak var BtnAddAddress: UIButton!
    @IBOutlet weak var BtnContinue: UIButton!
    @IBOutlet weak var LblMainTaskLocation: UILabel!
    @IBOutlet weak var LblInnerTaskLocation: UILabel!
    @IBOutlet weak var ToolView: UIView!
  
    
    var GetTaskID = ""
    var GetAddress_Id:Int!
    var GetAddress_IdString = ""
    var GetCustomerID = ""
    var authentication_key_val = ""
    var FullAddress = ""
    var Address_IdString = ""
    var Address_Id:Int!
    var GetAddressNickName = ""
    var GetFullAddress = ""
    
     var isCalledFromSearchResult:Bool = false
    var selectedTag:Int!
    var AddressDict = [NSDictionary]()
    var lastContentOffset: CGFloat = 0
    var IsSelectedRow:Bool = false
    var isFromLocation:Bool = false
     var BtnSelctedTag:Int!
     var GetSelectedIndex:Int!
    let defaults = UserDefaults.standard
    
    var Customer_IdInt:Int!
    var Customer_IdString = ""
    var task_IdInt:Int!
    var task_IdString = ""
    var GetTaskDate = ""
    var ConvertedGetTaskDate = ""
    var GetTaskTime = ""
    var GetTaskDay = ""
    var GetTaskDaytime = ""
    var GetTaskTitle = ""
    var GetTaskDescription = ""
    var Language_Name = ""
    var Language_Id:Int!
    var LangIDString = ""
    var LangIDDict = [NSDictionary]()
    var LangIDString_Array = [String] ()
    var GetLangName_Array = [String] ()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.ToolView.isHidden = true
       //remove tableview lines
        self.AddAddressTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        
        // Do any additional setup after loading the view.
        
        self.GetTaskID = UserDefaults.standard.string(forKey: "TaskIDKey") ?? ""//get
        self.GetCustomerID = UserDefaults.standard.string(forKey: "CustomerIDKey") ?? ""//get
        self.authentication_key_val = UserDefaults.standard.string(forKey: "authentication_key")!//get
        print("authentication_key_val \(authentication_key_val)")
        print("GetTaskIDAddLocation: \(self.GetTaskID)")
        
        //        self.manage_addresses_customer()
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = AddAddressTableView.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.lastContentOffset < AddAddressTableView.contentOffset.y {
            // did move up
            self.ToolView.isHidden = false
            self.LblMainTaskLocation.isHidden = true
        } else if self.lastContentOffset > AddAddressTableView.contentOffset.y {
            // did move down
        } else {
            // didn't move
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.manage_addresses_customer()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.AddressDict.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = AddAddressTableView.dequeueReusableCell(withIdentifier: "AddAddressCell", for: indexPath as IndexPath) as! AddAddressTableViewCell
        
//      // then adjust the color for cells since they will be reused
//        if cell.isSelected {
//          cell.LblAddressType.textColor = COLOR.PinkColor
//           cell.LblFullAddress.textColor = COLOR.PinkColor
//           cell.AddressNameLbl.textColor = COLOR.PinkColor
//       } else {
//           // change color back to whatever it was
//            cell.LblAddressType.textColor = .black
//            cell.LblFullAddress.textColor = .black
//            cell.AddressNameLbl.textColor = .black
//       }
        
        self.FullAddress = ""
        let AllData =  self.AddressDict[indexPath.row]
        print("AllDataCellForRow:",AllData)
        
        let Area = AllData["area"] as? String ?? ""
        let Appartment = AllData["appartment"] as? String ?? ""
        let Street = AllData["street"] as? String ?? ""
        let BuildingName = AllData["building_name"] as? String ?? ""
        let Floor = AllData["floor"] as? String ?? ""
        let AppartmentNo = AllData["appartment_no"] as? String ?? ""
        let AdditionalDirections = AllData["additional_directions"] as? String ?? ""
 
        
          self.FullAddress += Street
          self.FullAddress += ", "
          self.FullAddress += BuildingName
          self.FullAddress += ", "
          self.FullAddress += Floor
          self.FullAddress += ", "
          self.FullAddress += AppartmentNo
          self.FullAddress += ", "
          self.FullAddress += Area
               
         
        
        cell.LblFullAddress.text = self.FullAddress
        cell.LblAddressType.text = AllData["address_nickname"] as? String ?? ""
        
        cell.BtnEdit.tag = indexPath.row
        cell.BtnEdit.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        
    
        if self.BtnSelctedTag == indexPath.item
        {
            self.BtnSelctedTag = nil
            let AllData =  self.AddressDict[indexPath.row]
            self.GetAddress_Id = AllData["id"] as? Int
            self.GetAddress_IdString = String(self.GetAddress_Id)

            let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "EditAddressDetailsVC") as! EditAddressDetailsViewController

            self.navigationController?.pushViewController(nextViewController, animated: true)
            
            print("GetAddress_IdStringforslectedIndex :\(self.GetAddress_IdString)")
            UserDefaults.standard.set(self.GetAddress_IdString,forKey: "SelectedGetAddressIdKey")
            self.isFromLocation = true
            UserDefaults.standard.set(self.isFromLocation,forKey: "EditFromAddLocation")

        }
        
        if selectedTag == indexPath.row
        {
            cell.AddressNameLbl.textColor = COLOR.PinkColor
            cell.LblAddressType.textColor = COLOR.PinkColor
            cell.LblFullAddress.textColor = COLOR.PinkColor
        }
        else
        {
            cell.AddressNameLbl.textColor = .black
            cell.LblAddressType.textColor = .black
            cell.LblFullAddress.textColor = .black
        }
        
        
        ////////////////////////////////////////////////////////////////////
        self.isCalledFromSearchResult = UserDefaults.standard.bool(forKey: "isEditSearchResult")// this is how you retrieve the bool value
        print("isCalledFromSearchResult......:",isCalledFromSearchResult)
        if (isCalledFromSearchResult)//value from search result
        {
            self.GetSelectedIndex = UserDefaults.standard.integer(forKey: "SelectedTaskLocationAddress")
            print("GetSelectedIndex......:",GetSelectedIndex)
          
            if self.GetSelectedIndex == cell.BtnEdit.tag
            {
                IsSelectedRow = true
                let AllData =  self.AddressDict[indexPath.row]
                self.GetAddress_Id = AllData["id"] as? Int
                self.GetAddress_IdString = String(self.GetAddress_Id)
                 print("GetAddress_IdString......:",GetAddress_IdString)
                cell.AddressNameLbl.textColor = COLOR.PinkColor
                cell.LblAddressType.textColor = COLOR.PinkColor
                cell.LblFullAddress.textColor = COLOR.PinkColor
            }
            else
            {
                cell.AddressNameLbl.textColor = .black
                cell.LblAddressType.textColor = .black
                cell.LblFullAddress.textColor = .black
            }
        }
        
        return cell
    }
    
    @objc func buttonPressed(sender:UIButton){

        self.BtnSelctedTag = sender.tag
        print("Selected item in row \(sender.tag)")
        self.AddAddressTableView.reloadData()
        sender.isSelected = true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = AddAddressTableView.cellForRow(at: indexPath) as! AddAddressTableViewCell


        self.selectedTag = indexPath.row
        IsSelectedRow = true

        
        let AllData =  self.AddressDict[indexPath.row]
        self.GetAddress_Id = AllData["id"] as? Int
        self.GetAddress_IdString = String(self.GetAddress_Id)
  
        self.GetAddressNickName = cell.LblAddressType.text!
        self.GetFullAddress = cell.LblFullAddress.text!
        
        UserDefaults.standard.set(self.GetAddress_IdString,forKey: "SelectedGetAddressIdKey")
        UserDefaults.standard.set(self.GetAddressNickName,forKey: "SelectedAddressNickNameKey")
        UserDefaults.standard.set(self.GetFullAddress,forKey: "SelectedFullAddressKey")
       
        print("GetAddress_IdString :\(self.GetAddress_IdString)")
        print("SelectedIndex \(indexPath.row)")
        print("GetFullAddress :\(self.GetFullAddress)")
         self.AddAddressTableView.reloadData()
        
        UserDefaults.standard.set(self.selectedTag,forKey: "SelectedTaskLocationAddress")//set slected index of address
     
    }
    func tableView(tableView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
        let cell = AddAddressTableView.cellForRow(at: indexPath as IndexPath) as! AddAddressTableViewCell

        // change color back to whatever it was
      
//        cell.LblAddressType.textColor = .black
//        cell.LblFullAddress.textColor = .black
//        cell.AddressNameLbl.textColor = .black

    }
    func manage_addresses_customer()
    {

        let urlString = ConstantsClass.baseUrl + ConstantsClass.manage_addresses_customer
        print("urlString: \(urlString)")
        
        let headers = [
            "Authorization": "\(self.authentication_key_val)"
        ]
            Alamofire.request(urlString, method: .get, parameters: nil, encoding: URLEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any] {
                    print("responseLocation: \(response)")
                    
                    if (response["status"] as? Bool) != nil {
                        if let respArray = response["response"] as? [[String:Any]] {
                            print("respArray: \(respArray)")
                            
                            
                            self.AddressDict = response["response"] as! [NSDictionary]
                            print("AddressDict:",self.AddressDict)
                            
                        }
                    }
                    // Make sure to update UI in main thread
                    DispatchQueue.main.async {
                        self.AddAddressTableView.reloadData()
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
    }
    
    
    func add_customer_task_location()
    {
        let url = ConstantsClass.baseUrl + ConstantsClass.add_customer_task_location
        print("url: \(url)")
        
        let headers = [
            "Authorization": "\(self.authentication_key_val)"
        ]
         print("headers: \(headers)")
        let parameters = [
            "address_id" : "\(self.GetAddress_IdString)",
            "task_id" : "\(self.GetTaskID)"
            ] as [String : Any]
        
        print("parametersUpdateAddress: \(parameters)")
        
        Alamofire.request(url
            , method: .post, parameters: parameters, encoding:JSONEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any]
                {
                    print("response: \(response)")
                    if let responseDict = response["response"]as? [String:Any]
                    {
                        if let status = response["status"] as? Bool {
                            
                            if status
                            {
                                
                                self.Customer_IdInt = responseDict["customer_id"] as? Int
                                print("Customer_IdInt: \(String(describing: self.Customer_IdInt))")
                                self.Customer_IdString = String(self.Customer_IdInt)
                                
                                self.task_IdInt = responseDict["task_id"] as? Int
                                self.task_IdString = String(self.task_IdInt)
                                
                                self.GetTaskDate = responseDict["task_date"] as! String
                                self.ConvertedGetTaskDate =  StaticUtility.convertDateFormater9( self.GetTaskDate)
                                self.GetTaskTime = responseDict["task_time"] as! String
                                self.GetTaskDay = responseDict["task_day"] as! String
                                self.GetTaskDaytime = responseDict["task_daytime"] as! String
                                
                                self.GetTaskTitle = responseDict["task_title"] as! String
                                self.GetTaskDescription = responseDict["description"] as! String
                                
                                 self.LangIDDict = responseDict["task_languages"] as! [NSDictionary]
                                 print("LangIDDict:",self.LangIDDict)
                                self.LangIDString_Array.removeAll()
                                self.GetLangName_Array.removeAll()
                                 for Data in self.LangIDDict
                                 {
                                     self.Language_Name = Data["language_name"]as? String ?? ""
                                     self.LangIDString = Data["language_id"]as? String ?? ""
                                    
                                    self.LangIDString_Array.append(self.LangIDString)
                                    self.GetLangName_Array.append(self.Language_Name)
                                    
                                    
                                    UserDefaults.standard.set(self.LangIDString_Array,forKey: "SelectedGetLangIDArrayKey")//set array
                                    UserDefaults.standard.set(self.GetLangName_Array,forKey: "SelectedTaskLangKey")//set array
                                }
                                print("Language_Name: \(self.Language_Name)")
                                print("LangIDString: \(self.LangIDString)")
                                print("GetLangName_Array: \(self.GetLangName_Array)")
                                print("LangIDString_Array: \(self.LangIDString_Array)")
                                
                                
                                DispatchQueue.main.async
                                    {
                                        print("Address Updated successfully")
                                        let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
                                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ReviewAndSearchVC") as! ReviewAndSearchViewController
                                        
                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                }
                            }
                            else
                            {
                                self.view.makeToast("Something went wrong", duration: 3.0, position: .bottom)
                                print("Something went wrong")
                            }
                        }
                    }
                }
        }
    }
  
    
    @IBAction func BtnEditAddressClick(_ sender: Any) {
    }
    
    @IBAction func BtnBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func BtnCancelClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func BtnContinueClick(_ sender: Any) {
        if IsSelectedRow == false
        {
            self.view.makeToast("Please Select Address", duration: 3.0, position: .bottom)
        }
        else
        {
            self.add_customer_task_location()
        }
//        self.add_customer_task_location()
    }
    
    @IBAction func BtnAddAddress(_ sender: Any) {
        UserDefaults.standard.set(true,forKey: "isAddAdress")
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SetAddressVC") as! SetAddressViewController
        
        self.navigationController?.pushViewController(nextViewController, animated: true)
    
    }
    
    
    @IBAction func BtnUseAnotherAddress(_ sender: Any) {
        UserDefaults.standard.set(true,forKey: "isAddAdress")
              
              let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
              let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SetAddressVC") as! SetAddressViewController
              
              self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
}
