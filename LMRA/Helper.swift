//
//  Helper.swift
//  Food Delivery
//
//  Created by QualtiasIT on 25/07/19.
//  Copyright © 2019 Aniket. All rights reserved.
//

import Foundation
import ReachabilitySwift
import SVProgressHUD
import AlamofireSwiftyJSON
import SwiftyJSON

class Helper: NSObject {
    
    static let sharedInstance = Helper()
    //let imageBaseUrl = "http://qualitasitproducts.com/akash/fooddelivery/mobileapp_api/"
    let imageBaseUrl = "https://www.qitstaging.com/food_delivery/api/"
//    func isLoggedIN()->Bool
//    {
//        var token : String?
//
//        token = UserDefaults.standard.string(forKey: UD_TOKEN)
//
//        if let token = token {
//
//            if (token == "1") {
//
//                return true
//            }
//            else if token.count > 0
//            {
//                return true
//            }
//            else
//            {
//                return false
//            }
//        }
//        else
//        {
//            return false
//        }
//
//    }
//    func unsuscribeNotif()
//    {
//        guard let userId : Int = UserDefaults.standard.value(forKey: UD_USER_ID) as? Int else {
//
//            return
//        }
//        let topic = "ios\(userId)"
//
//        Messaging.messaging().unsubscribe(fromTopic: topic)
//
//    }
//    func subscribeNotif()
//    {
//        guard let userId : Int = UserDefaults.standard.value(forKey: UD_USER_ID) as? Int else {
//
//            return
//        }
//        let topic = "ios\(userId)"
//
//        Messaging.messaging().subscribe(toTopic: topic)
//
//    }

    func getCurrencySymbol(code: String) -> String?
    {

        let locale = NSLocale(localeIdentifier: "NG")
        return locale.displayName(forKey: NSLocale.Key.currencySymbol, value: "NG")
    }



    func networkIsAvailable() -> Bool {

        if (Reachability()?.currentReachabilityStatus == .notReachable) {
            return false
        }
        else {
            return true
        }
    }
    func hexStringToUIColor (hex:String) -> UIColor {
           var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
           
           if (cString.hasPrefix("#")) {
               cString.remove(at: cString.startIndex)
           }
           
           if ((cString.count) != 6) {
               return UIColor.gray
           }
           
           var rgbValue:UInt32 = 0
           Scanner(string: cString).scanHexInt32(&rgbValue)
           
           return UIColor(
               red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
               green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
               blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
               alpha: CGFloat(1.0)
           )
       }
    func colorWithHexString(hexString: String, alpha:CGFloat = 1.0) -> UIColor {
        
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
    func networkIsAvailableMessage() -> Bool {

        if (Reachability()?.currentReachabilityStatus == .notReachable) {

            Helper.sharedInstance.showAlert(title: "Network Problem!", message: "Please check your network connection")
            return false
        }
        else {
            return true
        }
    }

    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }


    func showAlert(title : String, message : String) -> Void {

        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        var topVC = UIApplication.shared.keyWindow?.rootViewController
        while((topVC!.presentedViewController) != nil){
            topVC = topVC!.presentedViewController
        }
        topVC?.present(alert, animated: true, completion: nil)
    }

    func showLoader() {
        DispatchQueue.main.async(execute: {() -> Void in
            SVProgressHUD.setForegroundColor(COLOR.GreenColor)
            SVProgressHUD.setBackgroundColor(UIColor.clear)
            SVProgressHUD.setRingThickness(3)
            //SVProgressHUD.st = SVProgressHUDStyleCustom
            SVProgressHUD.show()
        })
    }

    func hideLoader() {
        DispatchQueue.main.async(execute: {() -> Void in
            SVProgressHUD.dismiss()
        })
    }
   
    

//    func logout() -> Void {
//
//       let defaults = UserDefaults.standard
////        defaults.set("", forKey: UD_USER_ID)
//        defaults.set("", forKey: UD_TOKEN)
////        defaults.set("", forKey: UD_STATUS)
////        defaults.set("", forKey: UD_NAME)
////        defaults.set("", forKey: UD_LASTNAME)
////        defaults.set("", forKey: UD_DOB)
////        defaults.set("", forKey: UD_GENDER)
////        defaults.set("", forKey: UD_EMAIL)
////        defaults.set("", forKey: UD_MOBILE)
////        defaults.set("", forKey: UD_PHOTO)
////        defaults.set("", forKey: UD_CITY)
////        defaults.set("", forKey: UD_CITY_ID)
////        defaults.set("", forKey: UD_TYPE_ID)
////        defaults.set("", forKey: UD_LIMS_USER_ID)
////        defaults.set("", forKey: UD_FACEBOOK_ID)
////        defaults.set("", forKey: UD_UPDATED_ON)
////        defaults.set("", forKey: UD_REFERRAL_CODE)
////        defaults.set("", forKey: UD_REFERRER_CODE)
////        defaults.set("", forKey: UD_FCM_TOKEN)
////        defaults.set("", forKey: UD_NAME)
////        defaults.set("", forKey: UD_TYPE)
////        defaults.set("", forKey: UD_RATE)
////        defaults.set("", forKey: UD_LAT)
////        defaults.set("", forKey: UD_LONG)
////        defaults.set("", forKey: UD_SPECIALITY)
////
////       // defaults.set("", forKey: UD_ANNIVERSARY)
////
////
//        defaults.synchronize()
//
//
//        if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
//
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//
//            let navigationController = storyboard.instantiateViewController(withIdentifier: "TabbarViewController")
//
//            window.rootViewController = navigationController
//            window.makeKeyAndVisible()
//
//        }
//    }


    func retriveDataForUD(key : String) -> JSON?
    {
        let userDefaults = UserDefaults.standard
        let string = userDefaults.value(forKey: key)

        if string != nil {
            let encode : String = string as! String

            let encodedString : NSData = (encode as NSString).data(using: String.Encoding.utf8.rawValue)! as NSData
            do {
                return try JSON(data: encodedString as Data)
            } catch {
                return nil
            }
        } else {
            return nil
        }

    }

    func saveDataForUD(data : JSON, key : String)
    {
        let userDefaults = UserDefaults.standard
        let string = data.rawString()
        userDefaults.set(string, forKey: key)
        userDefaults.synchronize()
    }
    @available(iOS 12.0, *)
    func TabbarViewIn() -> Void {
        
        if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let navigationController = storyboard.instantiateViewController(withIdentifier: "TabbarViewController")
            
            window.rootViewController = navigationController
            window.makeKeyAndVisible()
            
        }
        
    }


    
   
//    func checkNavigation()-> Void
//    {
//
//
//        if isLoggedIN() {
//
//            loggedIn()
//           // self.postFCMAPI()
//            //checkAppVersion()
//        }
//        else
//        {
//            login()
//           // logout()
//
//        }
//    }
    
    

    //    func checkAppVersion()-> Void
    //    {
    //        if isLoggedIN() {
    //
    //            let api : APIEngine = APIEngine()
    //
    //            api.getAppVersionAPI(param: [:]) { (response, error) in
    //
    //                switch response?.result  {
    //                case .success(let JSON)?:
    //                    print("Success with JSON: \(JSON)")
    //                    let appVersioniOS : String = JSON["data"]["app_version"]["ios"].stringValue
    //
    //                    let appVersioniOSMandatory : String = JSON["data"]["app_version"]["ios_mandatory"].stringValue
    //
    //
    //                    let dictionary = Bundle.main.infoDictionary!
    //                    let version = dictionary["CFBundleShortVersionString"] as! String
    //
    //                    let appVersioniOSFloat : Float = Float(appVersioniOS)!
    //
    //                    let versionFlat : Float = Float(version)!
    //
    //                    if (appVersioniOSFloat > versionFlat)
    //                    {
    //
    //                        let alert = UIAlertController(title: "New Update Available!", message: "Please update the app to the latest version.", preferredStyle: UIAlertControllerStyle.alert)
    //
    //                        if (appVersioniOSMandatory == "0")
    //                        {
    //                            alert.addAction(UIAlertAction(title: "Later", style: UIAlertActionStyle.default, handler: nil))
    //                        }
    //
    //                        alert.addAction(UIAlertAction(title: "Update Now", style: UIAlertActionStyle.default, handler: { (action) in
    //                            //
    //                            //UIApplication.shared.openURL()
    //                            UIApplication.shared.open(NSURL(string:kDownloadAppURL)! as URL, options: [:], completionHandler: { (check) in
    //                                //
    //
    //                            })
    //
    //                        }))
    //
    //                        var topVC = UIApplication.shared.keyWindow?.rootViewController
    //                        while((topVC!.presentedViewController) != nil){
    //                            topVC = topVC!.presentedViewController
    //                        }
    //                        topVC?.present(alert, animated: true, completion: nil)
    //
    //                    }
    //                    //completionHandler(count)
    //
    //                case.failure(let error)?:
    //                    print("error: \(error)")
    //
    //                case .none:
    //                    print("error: ")
    //                }
    //
    //            }
    //        }
    //    }




//    func loggedIn() -> Void {
//        login()
//        if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
//
//
//            //Helper.sharedInstance.updateFCMCall()
//
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//
//            let leftViewController = storyboard.instantiateViewController(withIdentifier: "MenuTableViewController")
//
//            let rightViewController = UITableViewController()
//
//            let navigationController = storyboard.instantiateViewController(withIdentifier: "NavDashboard")
//
//            let sideMenuController = LGSideMenuController(rootViewController: navigationController,
//                                                          leftViewController: leftViewController,
//                                                          rightViewController: rightViewController)
//
//            sideMenuController.leftViewWidth = 280.0;
//            sideMenuController.leftViewPresentationStyle = .scaleFromLittle;
//
//            sideMenuController.rightViewWidth = 0.0;
//            sideMenuController.leftViewPresentationStyle = .slideBelow;
//
//            window.rootViewController = sideMenuController
//            window.makeKeyAndVisible()
//
//        }

  //  }


    //    func updateFCMCall() -> Void {
    //
    //        if !Helper.sharedInstance.isLoggedIN() {
    //
    //            return
    //        }
    //
    //        let defaults = UserDefaults.standard
    //
    //        var fcmkey = ""
    //
    //        if let fcmkey_key = defaults.value(forKey: UD_FCM_TOKEN)
    //        {
    //            fcmkey = fcmkey_key as! String
    //        }
    //
    //        let api : APIEngine = APIEngine()
    //
    //        let dictLogin: [String :String] = ["fcm" : fcmkey]
    //
    //        api.updateFCMAPI(param:dictLogin, completionHandler: { responseObject, error in
    //
    //        })
    //    }
//
//    func postFCMAPI(){
//        let defaults = UserDefaults.standard
//        var fcmkey = ""
//        if let fcmkey_key = defaults.value(forKey: UD_FCM_TOKEN)
//        {
//            fcmkey = fcmkey_key as! String
//        }
//        let param:[String:String] = ["fcm":fcmkey]
//
//        let api : APIEngine = APIEngine()
//        api.fcmAPI(param:param) { (responseObject, error) in
//
//            switch responseObject?.result  {
//            case .success(let JSON)?:
//                print("Success with JSON: \(JSON)")
//                if (JSON["status"].intValue == 1) {
//
//                }
//            case.failure(let error)?:
//                print("error: \(error)")
//
//            case .none:
//                print("error: ")
//            }
//
//        }
//    }

    func timeAgoSinceDate(date:Date, numericDates:Bool) -> String {
        let calendar = NSCalendar.current
        let unitFlags: NSCalendar.Unit = [.second, .minute, .hour, .day, .weekOfYear, .month, .year]
        let now = NSDate()
        let components = (calendar as NSCalendar).components(unitFlags, from: date, to: now as Date, options: [])

        if (components.year! >= 2) {
            return "\(components.year!)" + " years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!)" + " months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!)" + " weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!)" + " days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!)" + " hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!)" + " minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!)" + " seconds ago"
        } else {
            return "Just now"
        }
    }

    func shadowView(view:UIView)
    {
       
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = .zero
        view.layer.shadowRadius = 1
        view.layer.cornerRadius = 6
    }
    func timeslotshadowView(view:UIView)
       {
          
           view.layer.shadowColor = UIColor.lightGray.cgColor
           view.layer.shadowOpacity = 1
           view.layer.shadowOffset = .zero
           view.layer.shadowRadius = 2
           view.layer.cornerRadius = 2
       }
    func cellShadowView(view:UIView)
    {
       
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = .zero
        view.layer.shadowRadius = 16
        view.layer.cornerRadius = 1
    }
    
    //    func shareApp(fromVC: UIViewController) -> Void {
    //
    //        let shareText = "\(kiOSAppStoreLink)"
    //
    //        let vc = UIActivityViewController(activityItems: [shareText], applicationActivities: [])
    //        fromVC.present(vc, animated: true, completion: nil)
    //    }
}
extension UIView
{
    //common for adding activity Indicator
    
    func activityStartAnimating(activityColor: UIColor, backgroundColor: UIColor) {
        let backgroundView = UIView()
        backgroundView.frame = CGRect.init(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height)
        //backgroundView.frame = CGRect.init(x: 0, y: 0, width: 50, height: 50)
        backgroundView.backgroundColor = backgroundColor
        backgroundView.tag = 475647
        
        var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
        activityIndicator = UIActivityIndicatorView(frame: CGRect.init(x: 0, y: 0, width: 50, height: 50))
        activityIndicator.center = self.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
        activityIndicator.color = activityColor
        activityIndicator.startAnimating()
        self.isUserInteractionEnabled = false
        
        backgroundView.addSubview(activityIndicator)
        
        self.addSubview(backgroundView)
    }
    
    func activityStopAnimating() {
        if let background = viewWithTag(475647){
            background.removeFromSuperview()
        }
        self.isUserInteractionEnabled = true
    }
    
    
}
