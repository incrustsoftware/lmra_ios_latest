//
//  ConstantsClass.swift
//  LMRA
//
//  Created by Mac on 12/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class ConstantsClass: NSObject {
    static let baseUrl = "http://lmra.incrustsoftware.com/index.php/"
    //Tasker API
    static let send_otp_to_create_tasker_account = "tasker/send_otp_to_create_tasker_account"
    static let verify_otp_tasker_mobile_account = "tasker/verify_otp_tasker_account"
    static let resend_otp_to_create_tasker_account = "tasker/resend_otp_to_create_tasker_account"
    static let change_cell_phone_number_tasker = "tasker/change_cell_phone_number_tasker"
    static let confirm_personal_details_tasker = "tasker/confirm_personal_details_tasker"
    static let send_otp_onemail_tasker = "tasker/send_otp_on_email_tasker"
    static let verify_otp_tasker_account = "tasker/verify_otp_tasker_account"
    static let add_service_tasker_subcateogories = "tasker/add_service_subcategories_tasker"
    static let add_tasker_rate = "tasker/add_tasker_rate"
    static let get_tasker_subcategories = "tasker/get_tasker_subcategories"
    static let add_tasker_samepriceforall = "tasker/same_price_forall_fixed"
    static let same_price_forall = "tasker/same_price_forall_hourly"
    static let update_rate = "tasker/update_tasker_rate"
    static let add_Working_schedule = "tasker/add_tasker_working_schedule"
    static let update_Tasker_languages = "tasker/update_tasker_languages"
    static let get_Tasker_profileDetails = "tasker/get_tasker_profile_details"
    static let get_offline_reasons = "tasker_main_flow/get_tasker_offline_reasons"
    static let get_offline_mode = "tasker_main_flow/tasker_offline_mode"
    static let get_active_task = "tasker_main_flow/get_tasker_active_task"
    static let get_task_detail = "tasker_main_flow/get_tasker_task_detail"
    static let accept_task_invitation = "tasker_main_flow/accept_task_invitation"
    static let cancel_task_invitation = "tasker_main_flow/cancel_task_invitation"
    static let decline_task_reason = "tasker_main_flow/get_tasks_decline_reasons"
    static let decline_task_invitation = "tasker_main_flow/decline_task_invitation"
    static let get_open_task_tasker = "tasker_main_flow/get_open_task_tasker"
    static let get_upcoming_task_tasker = "tasker_main_flow/get_upcoming_task_tasker"
    static let tasker_switch_to_customer = "tasker_main_flow/tasker_switch_to_customer"
    
    static let get_tasker_basic_profile_details = "tasker/get_tasker_basic_profile_details"
    static let update_tasker_basic_profile_details = "tasker/update_tasker_basic_profile_details"
    static let delete_tasker_account = "tasker/delete_tasker_account"
    static let update_tasker_working_schedule = "tasker/update_tasker_working_schedule"
    //Customer
    static let get_category = "customer/get_categories"
    static let get_service_subcategories = "customer_service/get_service_subcategories"
    static let send_otp_to_create_customer_account = "customer/send_otp_to_create_customer_account"
    static let verify_otp_customer_account = "customer/verify_otp_customer_account"
    static let resend_otp_to_create_customer_account = "customer/resend_otp_to_create_customer_account"
    static let change_cell_phone_number_customer = "customer/change_cell_phone_number_customer"
    static let confirm_personal_details_customer = "customer/confirm_personal_details_customer"
    static let set_address_customer = "customer/set_address_customer"
    static let get_timeslots = "customer_service/get_timeslots"
    static let get_languages = "customer_service/get_languages"
    static let add_customer_task = "customer_service/add_customer_task"
    static let add_customer_task_details = "customer_service/add_customer_task_details"
    static let manage_addresses_customer = "customer/manage_addresses_customer"
    static let add_customer_task_location = "customer_service/add_customer_task_location"
    static let get_tasker_by_search = "customer_service/get_tasker_by_search"
    static let get_tasker_by_filter = "customer_service/get_tasker_by_filter"
    static let invite_tasker = "customer_service/invite_tasker"
    static let get_customer_task_list = "customer_service/get_customer_task_list"
    static let waiting_accepted_task_details = "customer_service/waiting_accepted_task_details"
    static let review_task_details = "customer_service/review_task_details"
    static let open_tasks_customer = "customer_service/open_tasks_customer"
    static let upcommimg_tasks_customer = "customer_service/upcommimg_tasks_customer"
    static let customer_sign_in = "customer/customer_sign_in"
    static let get_customer_basic_profile_details = "customer/get_customer_basic_profile_details"
    static let get_address_by_ids = "customer/get_address_by_id"
    static let delete_customer_account = "customer/delete_customer_account"
    static let get_adress_type = "customer/get_adress_type"
    static let sign_out = "customer/sign_out"
    static let get_services_subcategories = "customer_service/get_service_subcategories"
    static let update_current_address_customer = "customer/update_current_address_customer"
    static let get_all_notifications = "tasker_main_flow/get_all_notifications"
    static let customer_switch_to_tasker = "customer_service/customer_switch_to_tasker"
    static let cancel_task_customer = "customer_service/cancel_task_customer"
    static let confirm_review_address_customer = "customer/confirm_review_address_customer"
    static let send_otp_on_email_customer = "customer/send_otp_on_email_customer"
    static let get_tasker_details = "customer_service/get_tasker_details"
}

struct AlertMessages {
    static let NETWORK_ERROR_MESSAGE = "No Network. Please try again later."
    static let ENTER_AN_KEY = "Please enter key."
    static let ENTER_CONTACT_NUMBER = "Please enter valid contact number."
    static let INVALID_RESPONSE = "Oops! Something went wrong. Please try again in a bit."
    static let INVALID_RESPONSE_CHECKSTATUS = "Something went wrong.Please try again later"
    static let Task_Completed = "Task completed successfully."
    static let Email_Invalid = "Email id is not register with us"
}
