//
//  CreateYourAccountVC.swift
//  LMRA
//
//  Created by Mac on 02/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Foundation
import Toast_Swift
import Alamofire

@available(iOS 12.0, *)
class CreateYourAccountVC: UIViewController,UIGestureRecognizerDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var lblCellPhoneNumber: UILabel!
    @IBOutlet weak var lblIdentityCard: UILabel!
    @IBOutlet weak var txtFieldCellPhoneNumberHeight: NSLayoutConstraint!
    @IBOutlet weak var txtFieldIdentityCardHeight: NSLayoutConstraint!
    @IBOutlet weak var txtfieldCellNumber: UITextField!
    @IBOutlet weak var txtFieldidCard: UITextField!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var BtnCheckbox: UIButton!
    
    var iconClick = false
    var maxLenCellNo:Int = 11
    var maxLenCPR:Int = 9
    
    var Cell_Number = ""
    var CPR_Number = ""
    var GetRelayID:Int!
    var StringGetRelayID = ""
    var SMS_OTP:Int = 0
    var User_type:Int!
    var User_TypeString = ""
    var GetUserDeviceType = ""
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtfieldCellNumber.delegate = self
        self.txtFieldidCard.delegate = self
        
        btnContinue.layer.cornerRadius = 2;
        //when you click on txtCellNo
        txtfieldCellNumber.addTarget(self, action: #selector(CreateYourAccountVC.txtFieldChangedForCellNo(_:)),
                                     for: .editingChanged)
        
        //when you click on cpr no
        txtFieldidCard.addTarget(self, action: #selector(CreateYourAccountVC.txtFieldChangedForCPRNo(_:)),
                                 for: .editingChanged)
        
        let cellnumberTap = UITapGestureRecognizer(target:self,action:#selector(self.cellnumberTapTapped))
        
        // add it to the label
        lblCellPhoneNumber.addGestureRecognizer(cellnumberTap)
        lblCellPhoneNumber.isUserInteractionEnabled = true
        
        let idCardTap = UITapGestureRecognizer(target:self,action:#selector(self.idCardTapTapped))
        
        // add it to the label
        lblIdentityCard.addGestureRecognizer(idCardTap)
        lblIdentityCard.isUserInteractionEnabled = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        addDoneButtonOnKeyboard()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == txtfieldCellNumber){
            let currentText = textField.text! + string
            return currentText.count <= maxLenCellNo
        }
        if(textField == txtFieldidCard){
            let currentText = textField.text! + string
            return currentText.count <= maxLenCPR
        }
        return true;
    }
    
    @objc func cellnumberTapTapped()
    {
        txtFieldCellPhoneNumberHeight.constant = 30
        txtfieldCellNumber.becomeFirstResponder()
    }
    
    @objc func idCardTapTapped()
    {
        txtFieldIdentityCardHeight.constant = 30
        txtFieldidCard.becomeFirstResponder()
    }
    
    @IBAction func OnCheckboxClick(_ sender: AnyObject) {
        
        if(iconClick == true)
        {
            BtnCheckbox.setImage(UIImage(named: "Uncheck"), for: .normal)
            self.btnContinue.setTitleColor(COLOR.GrayColor, for: .normal)
            btnContinue.backgroundColor = COLOR.LightGrayColor
            
        }
        else {
            BtnCheckbox.setImage(UIImage(named: "Check"), for: .normal)
//            if (txtfieldCellNumber.text!.count == 11) && (txtFieldidCard.text!.count == 9)
            if (txtfieldCellNumber.text!.count > 0) && (txtFieldidCard.text!.count > 0)
            {
                self.btnContinue.setTitleColor(COLOR.WhiteColor, for: .normal)
                btnContinue.backgroundColor = COLOR.PinkColor
            }
            else
            {
                self.btnContinue.setTitleColor(COLOR.GrayColor, for: .normal)
                btnContinue.backgroundColor = COLOR.LightGrayColor
            }
        }
        iconClick = !iconClick
    }
    
    
    
    
    @IBAction func btnBackClicked(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnContinueClicked(_ sender: Any) {
        let CheckOTPFromChnageCellNo: Bool = false
        UserDefaults.standard.set(CheckOTPFromChnageCellNo,forKey: "isCellNoChange")
        
        checkValidation()
    }
    
    func checkValidation()  {
        if (txtfieldCellNumber.text! == "" ) || (txtfieldCellNumber.text!.count != 11 ){
            self.btnContinue.setTitleColor(COLOR.GrayColor, for: .normal)
            btnContinue.backgroundColor = COLOR.LightGrayColor
            self.view.makeToast("Please enter valid cell phone number")
        }
        else if (!txtfieldCellNumber.text!.hasPrefix("973"))
        {
            self.view.makeToast("Cell phone number should start with 973")
            self.btnContinue.setTitleColor(COLOR.GrayColor, for: .normal)
            btnContinue.backgroundColor = COLOR.LightGrayColor
        }
        else if (txtFieldidCard.text?.isEmpty ?? true) || (txtFieldidCard.text!.count != 9)
        {
            self.btnContinue.setTitleColor(COLOR.GrayColor, for: .normal)
            btnContinue.backgroundColor = COLOR.LightGrayColor
            self.view.makeToast("Please enter valid Identity card number")
        }
        else if(iconClick == false)
        {
            
            self.view.makeToast("Please accept the Terms & Conditions")
        }
        else
        {
            self.Cell_Number =  txtfieldCellNumber.text!
            self.CPR_Number = txtFieldidCard.text!
            
            defaults.set(self.Cell_Number, forKey: "cellNoKey")//set
            defaults.set(self.CPR_Number, forKey: "CPRNoKey")//set
            
            print("Cell_Number: \(self.Cell_Number)")
            print("CPR_Number: \(self.self.CPR_Number)")
            
            if (txtfieldCellNumber.text!.count == 11 ) && (txtFieldidCard.text!.count == 9 ) && (iconClick == true)
            {

                self.SentOTPSMS()
            }
        }
    }
    
    @available(iOS 12.0, *)
    func SentOTPSMS()
    {
        
        let url = ConstantsClass.baseUrl + ConstantsClass.send_otp_to_create_customer_account
        print("url: \(url)")
        self.User_type =  UserDefaults.standard.integer(forKey: UserDataManager_KEY.userType)
        self.GetRelayID = UserDefaults.standard.integer(forKey: "RelayIDKey")
        self.GetUserDeviceType = UserDefaults.standard.string(forKey: "UserDeviceTypeIDKey") ?? "2"
        self.StringGetRelayID =  String(self.GetRelayID)
        self.User_TypeString = String(self.User_type)
        let parameters = [
            "cell_no" : "\(self.Cell_Number)",
            "cpr_no" : "\(self.CPR_Number)",
            "user_type" : "\(self.User_TypeString)",
            "relay_id" : "\(self.StringGetRelayID)",
            "user_device_type" : "\(self.GetUserDeviceType)"
            ] as [String : Any]
        
        print("parameters: \(parameters)")
        
        Alamofire.request(url
            , method: .post, parameters: parameters, encoding:JSONEncoding.default).responseJSON
            {
                response in
                
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if let code = response["code"] as? Int {
                        
                        if code == 402
                        {
                            self.view.makeToast("User already exist", duration: 3.0, position: .bottom)
                        }
                    }
                    
                    if let responseDict = response["response"]as? [String:Any]
                    {
                        self.SMS_OTP = responseDict["otp"]as! Int
                        print("SMS_OTP: \(self.SMS_OTP)")
                        UserDefaults.standard.set(self.SMS_OTP, forKey: "SMS_OTPKey")
                        
                    
                        
                        if let status = response["status"] as? Bool {
                            if status{
                                DispatchQueue.main.async {
                                    
                                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                                    
                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                }
                            }
                            else
                            {
                                self.view.makeToast("Something went wrong", duration: 3.0, position: .bottom)
                                print("Something went wrong")
                            }
                        }
                        else
                        {
                            print("Error occured") // serialized json response
                            
                        }
                    }
                }
        }
    }
    
    
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 20))
        doneToolbar.barStyle = .default
        let Cancel: UIBarButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(self.doneButtonAction))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [Cancel,flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.txtfieldCellNumber.inputAccessoryView = doneToolbar
        self.txtFieldidCard.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.txtfieldCellNumber.resignFirstResponder()
        self.txtFieldidCard.resignFirstResponder()
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        self.txtfieldCellNumber.resignFirstResponder()
        self.txtFieldidCard.resignFirstResponder()
        
        return Int(string) != nil
    }
    
    //when textfield click action method
    
    @objc func txtFieldChangedForCellNo(_ textField: UITextField) {
         if (txtfieldCellNumber.text!.count > 0) && (txtFieldidCard.text!.count > 0) && (iconClick == true)
//        if (txtfieldCellNumber.text!.count == 11) && (txtFieldidCard.text!.count == 9) && (iconClick == true)
        {
            self.btnContinue.setTitleColor(COLOR.WhiteColor, for: .normal)
            btnContinue.backgroundColor = COLOR.PinkColor
        }
        else
        {
            self.btnContinue.setTitleColor(COLOR.GrayColor, for: .normal)
            btnContinue.backgroundColor = COLOR.LightGrayColor
        }
    }
    
    @objc func txtFieldChangedForCPRNo(_ textField: UITextField) {
         if (txtfieldCellNumber.text!.count > 0) && (txtFieldidCard.text!.count > 0) && (iconClick == true)
//        if (txtfieldCellNumber.text!.count == 11) && (txtFieldidCard.text!.count == 9) && (iconClick == true)
        {
            self.btnContinue.setTitleColor(COLOR.WhiteColor, for: .normal)
            btnContinue.backgroundColor = COLOR.PinkColor
        }
        else
        {
            self.btnContinue.setTitleColor(COLOR.GrayColor, for: .normal)
            btnContinue.backgroundColor = COLOR.LightGrayColor
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                //                        self.view.frame.origin.y -= keyboardSize.height
                self.view.frame.origin.y -= 150
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            //                    self.view.frame.origin.y = 0
            self.view.frame.origin.y += 150
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
}
