//
//  WelcomeViewController.swift
//  LMRA
//
//  Created by Mac on 03/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController,UIGestureRecognizerDelegate{
    
    @IBOutlet weak var MainView: UIView!
    @IBOutlet var ContentView: UIView!
    
    @IBOutlet weak var BtnContinue: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
         BtnContinue.layer.cornerRadius = 2;
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        swipeLeft.direction = .left
        self.MainView.addGestureRecognizer(swipeLeft)
    }
    
    @IBAction func btnContinueClicked(_ sender: Any) {
        let stroyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let objVc:ConfirmYourDetailsVC =
            storyboard?.instantiateViewController(withIdentifier: "ConfirmYourDetailsVC") as!
        ConfirmYourDetailsVC
        // objVc.venderId = VendorId
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(objVc, animated: true)
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case .right:
                 print("Swiped right")
            case .down:
                print("Swiped down")
            case .left:
                let stroyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let objVc:ConfirmYourDetailsVC =
                    storyboard?.instantiateViewController(withIdentifier: "ConfirmYourDetailsVC") as!
                ConfirmYourDetailsVC
                // objVc.venderId = VendorId
                self.navigationController?.isNavigationBarHidden = true
                self.navigationController?.pushViewController(objVc, animated: true)
                print("Swiped left")
            case .up:
                print("Swiped up")
            default:
                break
            }
        }
    }
}
