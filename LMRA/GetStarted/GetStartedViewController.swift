//
//  GetStartedViewController.swift
//  LMRA
//
//  Created by Mac on 24/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

@available(iOS 12.0, *)
@available(iOS 12.0, *)
@available(iOS 12.0, *)
class GetStartedViewController: UIViewController,UIGestureRecognizerDelegate {
    
    //Outlets
    //Outlets
    @IBOutlet weak var taskerView: UIView!
    @IBOutlet weak var customerView: UIView!
    @IBOutlet weak var taskerForwordButton: UIButton!
    @IBOutlet weak var customerForwordButton: UIButton!
    @IBOutlet weak var btnAlreadyHaveAnAccount: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var taskerImageViewHeight: NSLayoutConstraint!
    
    let defaults = UserDefaults.standard
    var CustomerUser_Type:Int!
    var User_Type:Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customerView.layer.cornerRadius = 2;
        taskerView.layer.cornerRadius = 2;
        
        //    let isfirstLogin = UserDefaults.standard.string(forKey: "FirstLogin") ?? ""
        //
        //    let appDelegate = UIApplication.shared.delegate as? AppDelegate
        //
        //
        //    if isfirstLogin == "0" //login
        //    {
        //        let sb = UIStoryboard(name: "DashFlow", bundle: nil)
        //        let vc = sb.instantiateViewController(withIdentifier: "DashBoardTabVC") as! DashBoardTabViewController
        //
        //        if let delegate = UIApplication.shared.delegate as?AppDelegate {
        //            delegate.window?.rootViewController = vc
        //
        //        }
        //
        //    }
        //
        //    else
        //    {
        //        let sb = UIStoryboard(name: "Main", bundle: nil)
        //        let vc = sb.instantiateViewController(withIdentifier: "GetStartedViewController") as! GetStartedViewController
        //
        //        if let delegate = UIApplication.shared.delegate as? AppDelegate {
        //            delegate.window?.rootViewController = vc
        //
        //        }
        //
        //    }
        
        
        
        // create the gesture recognizer
        let TapGesture = UITapGestureRecognizer(target:self,action:#selector(self.customerViewTapped))
        
        // add it to the label
        customerView.addGestureRecognizer(TapGesture)
        //set constraint
        setConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UserDefaults.standard.set(false, forKey: "isFromSubcategory")
    }
    
    @IBAction func TaskerButtonClicked(_ sender: Any) {
        self.User_Type = 2
        UserDefaults.standard.set(self.User_Type,forKey: UserDataManager_KEY.userType)//set
        
        let stroyboard:UIStoryboard = UIStoryboard(name: "Tasker", bundle: nil)
        let objVc:TaskerCreateAccountViewController =
            stroyboard.instantiateViewController(withIdentifier: "TaskerCreateAccountViewController") as!
        TaskerCreateAccountViewController
        // objVc.venderId = VendorId
        
        self.navigationController?.pushViewController(objVc, animated: true)
    }
    
    func setConstraints() {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                taskerImageViewHeight.constant = 90
            case 1334:
                taskerImageViewHeight.constant = 100
            case 1920, 2208:
                taskerImageViewHeight.constant = 115
            case 2436:
                taskerImageViewHeight.constant = 140
            case 1792, 2688:
                taskerImageViewHeight.constant = 140
            default:
                UtilityLog.sharedInstant.printLog(value:"Unknown")
            }
        }
    }
    
    @available(iOS 12.0, *)
    @objc func customerViewTapped()
    {
        self.CustomerUser_Type = 1 // set customerUserType
        UserDefaults.standard.set(self.CustomerUser_Type,forKey: UserDataManager_KEY.userType)//set
        print("setCustomerUser_Type:",CustomerUser_Type)
        
        let stroyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let objVc:BookServiceViewController =
            storyboard?.instantiateViewController(withIdentifier: "BookServiceViewController") as!
        BookServiceViewController
        // objVc.venderId = VendorId
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(objVc, animated: true)
    }
    
    @IBAction func CustomerForwardBtnClick(_ sender: Any) {
        self.CustomerUser_Type = 1
        UserDefaults.standard.set(self.CustomerUser_Type,forKey: UserDataManager_KEY.userType)//set
        
        let stroyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let objVc:BookServiceViewController =
            storyboard?.instantiateViewController(withIdentifier: "BookServiceViewController") as!
        BookServiceViewController
        // objVc.venderId = VendorId
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(objVc, animated: true)
        
    }
    
    @IBAction func btnAlreadyHaveanAccountClicked(_ sender: Any) {
        //        let stroyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //        let objVc:LoginViewController =
        //            storyboard?.instantiateViewController(withIdentifier: "LoginVC") as!
        //        LoginViewController
        //        // objVc.venderId = VendorId
        //        self.navigationController?.isNavigationBarHidden = true
        //        self.navigationController?.pushViewController(objVc, animated: true)
        
    }
    
    @IBAction func btnBackClicked(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnLoginClicked(_ sender: Any) {
        let stroyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let objVc:LoginViewController =
            storyboard?.instantiateViewController(withIdentifier: "LoginVC") as!
        LoginViewController
        // objVc.venderId = VendorId
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(objVc, animated: true)
    }
    
}
