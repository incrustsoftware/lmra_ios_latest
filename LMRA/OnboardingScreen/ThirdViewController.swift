//
//  ThirdViewController.swift
//  User_Queue_App
//
//  Created by Mac on 05/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

@available(iOS 12.0, *)
@available(iOS 12.0, *)
class ThirdViewController: UIViewController,CAAnimationDelegate {

    @IBOutlet weak var btnSkip: UIButton!
    
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    @IBOutlet weak var lblShortDescription: UILabel!
    
    
    
    @IBOutlet weak var lblLongDescription: UILabel!
    
    
    
    
    @available(iOS 12.0, *)
    @IBAction func btnSkipClicked(_ sender: Any) {
        
      
        
        
        if let presentVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GetStartedViewController") as? GetStartedViewController {
            self.navigationController?.isNavigationBarHidden = true
            self.navigationController?.pushViewController(presentVC, animated: true)
        }
    }
    
 
    
    @IBAction func btnSignupClicked(_ sender: Any)
    {
        /*if let presentVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
            self.navigationController?.isNavigationBarHidden = true
            self.navigationController?.pushViewController(presentVC, animated: true)
        }*/
        
    }
    
    @IBOutlet weak var btnSignUp: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
       //btnSignUp.layer.cornerRadius = 10.0
       let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        
        view.addGestureRecognizer(leftSwipe)
        view.addGestureRecognizer(rightSwipe)
        
        
    //  changeLabel()
        
      //  btnSkip.setTitle(getLabel(langId: "1", labelId: "Skip"), for: .normal)
        
    }
   /* func changeLabel() {
        lblShortDescription.text = getLabel(langId: UserDefaults.standard.value(forKey: "LangId") as! String, labelId: "Multiple Service Providers")
        // print(lblJoinQueue.text as!String)
        
        
        lblLongDescription.text = getLabel(langId: UserDefaults.standard.value(forKey: "LangId") as! String, labelId: "Browse the alternate vendor having less wasting time")
        btnNext.setTitle(getLabel(langId: UserDefaults.standard.value(forKey: "LangId") as! String, labelId: "Next"), for: .normal)
    }*/
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer)
    {
        if (sender.direction == .left)
        {
            print("Swipe Left")
            
            // show the view from the right side
            
         //   self.performSegue(withIdentifier: "goToSecpndPage", sender:self)
            
            if let presentVC = UIStoryboard(name: "Onborading", bundle: nil).instantiateViewController(withIdentifier: "FourthViewController") as? FourthViewController {
                self.navigationController?.isNavigationBarHidden = true
                self.navigationController?.pushViewController(presentVC, animated: true)
            }
        }
        
        if (sender.direction == .right)
        {
            print("Swipe Right")
            let transition = CATransition()
            transition.duration = 0.45
            transition.timingFunction = CAMediaTimingFunction(name: .default)
            transition.type = .push
            transition.subtype = .fromLeft
            transition.delegate = self
            self.navigationController?.view.layer.add(transition, forKey: kCATransition)
            
            
            
            if let presentVC = UIStoryboard(name: "Onborading", bundle: nil).instantiateViewController(withIdentifier: "SecondViewController") as? SecondViewController {
                self.navigationController?.isNavigationBarHidden = true
                self.navigationController?.pushViewController(presentVC, animated: true)
            }
            // show the view from the left side
        }
    }

}
