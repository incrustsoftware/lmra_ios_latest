//
//  FourthViewController.swift
//  LMRA
//
//  Created by Mac on 29/07/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

@available(iOS 12.0, *)
@available(iOS 12.0, *)
class FourthViewController: UIViewController,CAAnimationDelegate {


    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
       //btnSignUp.layer.cornerRadius = 10.0
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        
        view.addGestureRecognizer(leftSwipe)
        view.addGestureRecognizer(rightSwipe)
        
        
    //  changeLabel()
        
      //  btnSkip.setTitle(getLabel(langId: "1", labelId: "Skip"), for: .normal)
        
    }
    @available(iOS 12.0, *)
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer)
       {
           if (sender.direction == .left)
           {
               print("Swipe Left")
               
               // show the view from the right side
               
            //   self.performSegue(withIdentifier: "goToSecpndPage", sender:self)
               
                 if let presentVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GetStartedViewController") as? GetStartedViewController {
                     self.navigationController?.isNavigationBarHidden = true
                     self.navigationController?.pushViewController(presentVC, animated: true)
                 }
             
           }
           
           if (sender.direction == .right)
           {
               print("Swipe Right")
               let transition = CATransition()
               transition.duration = 0.45
               transition.timingFunction = CAMediaTimingFunction(name: .default)
               transition.type = .push
               transition.subtype = .fromLeft
               transition.delegate = self
               self.navigationController?.view.layer.add(transition, forKey: kCATransition)
               
               
               
               if let presentVC = UIStoryboard(name: "Onborading", bundle: nil).instantiateViewController(withIdentifier: "ThirdViewController") as? ThirdViewController {
                   self.navigationController?.isNavigationBarHidden = true
                   self.navigationController?.pushViewController(presentVC, animated: true)
               }
               // show the view from the left side
           }
       }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @available(iOS 12.0, *)
    @IBAction func btnSkipClicked(_ sender: Any)
    {
        
            if let presentVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GetStartedViewController") as? GetStartedViewController {
                self.navigationController?.isNavigationBarHidden = true
                self.navigationController?.pushViewController(presentVC, animated: true)
    }
    }
    
    
    @IBAction func btnGetStartedClicked(_ sender: Any)
    {
        
                   if let presentVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GetStartedViewController") as? GetStartedViewController {
                    self.navigationController?.isNavigationBarHidden = true
                    self.navigationController?.pushViewController(presentVC, animated: true)
        }
        
        
    }
    
    @IBAction func GetStartedClick(_ sender: Any) {
        
          if let presentVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GetStartedViewController") as? GetStartedViewController {
                    self.navigationController?.isNavigationBarHidden = true
                    self.navigationController?.pushViewController(presentVC, animated: true)
        }
        
    }
    
}
