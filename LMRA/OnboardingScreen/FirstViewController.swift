//
//  FirstViewController.swift
//  User_Queue_App
//
//  Created by Mac on 05/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import CoreLocation

final class FirstLaunch {
    
    let userDefaults: UserDefaults = .standard
    
    let wasLaunchedBefore: Bool
    var isFirstLaunch: Bool {
        return !wasLaunchedBefore
    }
    init() {
        let key = "WasLaunchedBefore"
        let wasLaunchedBefore = userDefaults.bool(forKey: key)
        self.wasLaunchedBefore = wasLaunchedBefore
        if !wasLaunchedBefore {
            userDefaults.set(true, forKey: key)
        }
    }
}
var locationManager: CLLocationManager?
@available(iOS 12.0, *)
class FirstViewController: UIViewController ,UINavigationControllerDelegate, CLLocationManagerDelegate{

    
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var btnSkip: UIButton!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var LblWelcome: UILabel!
    
    @IBOutlet weak var lblJoinQueue: UILabel!
   
    
    @IBOutlet weak var lblDiscription: UILabel!
    
    @available(iOS 12.0, *)
    @IBAction func btnSkipClicked(_ sender: Any) {
        
      
        
        
        if let presentVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GetStartedViewController") as? GetStartedViewController {
            self.navigationController?.isNavigationBarHidden = true
            self.navigationController?.pushViewController(presentVC, animated: true)
        }
    }
    
    
    @IBAction func btnNextClicked(_ sender: Any) {
        

        
       /* if let presentVC = UIStoryboard(name: "Onborading", bundle: nil).instantiateViewController(withIdentifier: "SecondViewController") as? SecondViewController {
            self.navigationController?.isNavigationBarHidden = true
            self.navigationController?.pushViewController(presentVC, animated: true)
        }*/
    }
    
    var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        let firstLaunch = FirstLaunch()
//        if firstLaunch.isFirstLaunch {
//        
//            
//        }
//        else
//        {
//            let sb = UIStoryboard(name: "Main", bundle: nil)
//            let vc = sb.instantiateViewController(withIdentifier: "GetStartedViewController") as! GetStartedViewController
//            
//            if let delegate = UIApplication.shared.delegate as? AppDelegate {
//                delegate.window?.rootViewController = vc
//                
//            }
//            
//            
//        }
        
        
      locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        
        self.navigationController?.navigationBar.isHidden = true

        //Left swipe
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))

        leftSwipe.direction = .left
        rightSwipe.direction = .right

        view.addGestureRecognizer(leftSwipe)
        view.addGestureRecognizer(rightSwipe)


        LblWelcome.font = UIFont(name:"STCForward-Bold",size:14)
        
       // changeLabel()
        
        
        
        
        
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways {
            if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) {
                if CLLocationManager.isRangingAvailable() {
                    // do stuff
                    print("popup location")
                }
            }
        }
    }
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer)
    {
        if (sender.direction == .left)
        {
            print("Swipe Left")
            
            // show the view from the right side
            
            // self.performSegue(withIdentifier: "goTOSecondPage", sender:self)
            
            if let presentVC = UIStoryboard(name: "Onborading", bundle: nil).instantiateViewController(withIdentifier: "SecondViewController") as? SecondViewController {
                self.navigationController?.isNavigationBarHidden = true
                self.navigationController?.pushViewController(presentVC, animated: true)
            }
        }
        
        if (sender.direction == .right)
        {
            print("Swipe Right")
            
            // show the view from the left side
            
            
        }
    }

}
class DefaultPageControl: UIPageControl {
    
    override var currentPage: Int {
        didSet {
            updateDots()
        }
    }
    
    override func sendAction(_ action: Selector, to target: Any?, for event: UIEvent?) {
        super.sendAction(action, to: target, for: event)
        updateDots()
    }
    
    private func updateDots() {
        let currentDot = subviews[currentPage]
        let largeScaling = CGAffineTransform(scaleX: 2.0, y: 2.0)
        let smallScaling = CGAffineTransform(scaleX: 1.5, y: 1.5)
        
        subviews.forEach {
            // Apply the large scale of newly selected dot.
            // Restore the small scale of previously selected dot.
            $0.transform = $0 == currentDot ? largeScaling : smallScaling
        }
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        // We rewrite all the constraints
        rewriteConstraints()
    }
    
    private func rewriteConstraints() {
        let systemDotSize: CGFloat = 7.0
        let systemDotDistance: CGFloat = 30.0
        
        let halfCount = CGFloat(subviews.count) / 2
        subviews.enumerated().forEach {
            let dot = $0.element
            dot.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.deactivate(dot.constraints)
            NSLayoutConstraint.activate([
                dot.widthAnchor.constraint(equalToConstant: systemDotSize),
                dot.heightAnchor.constraint(equalToConstant: systemDotSize),
                dot.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0),
                dot.centerXAnchor.constraint(equalTo: centerXAnchor, constant: systemDotDistance * (CGFloat($0.offset) - halfCount))
                ])
        }
    }
}

