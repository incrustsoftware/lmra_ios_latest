//
//  Constant.swift
//
//
//  Created by Ashwini Chougale on 30/01/18.
//  Copyright © 2018 Tata Technologies. All rights reserved.
//

import Foundation
import UIKit

/**
 *  Here we are set the "isPrintable" instant value "true" or "false"
 *  for to manage the all print statemets in overall project
 *  - if app is in development state then,
 *    "isPrintable = true"
 *  - if app goes to live then, here we need to set the
 *    "isPrintable = false"
 */
let isPrintable = true
var lastConnectionStatusConnected = true

//MARK: - WebService -
struct BaseURL {
    //TODO: - QA Release Check URL
    // Prod URL
    //public static let mainBaseURL = "https://mmcdevapp01.tatatechnologies.com/"
    
    // QA URL
    static let mainBaseURL = "https://mmpcqaapp.tatatechnologies.com/"
    
    //Dev URL
    static let host = "http://35.176.9.83/"
}

struct URLConstants {
    static let FETCH_VENDOR_QUEUE = BaseURL.host + "Vendors/GetVendorAllQueues"
}

struct Storyboard {
    let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    let taskerStoryboard = UIStoryboard(name: "Tasker", bundle: nil)
    let dashFlowStoryboard = UIStoryboard(name: "DashFlow", bundle: nil)
    let onboradingStoryboard = UIStoryboard(name: "Onborading", bundle: nil)
}

//MARK: - View -
//struct Storyboard
//{
//    static let Main = "Main"
//    static let Tabbar = "TabBar"
//    static let Dashboard = "Dashboard"
//    static let StartingPages = "StartingPages"
//    static let OTP = "OTP"
//    static let Business = "BusinessStoryboard"
//    static let Service = "ServiceStoryboard"
//    static let MenuItems = "MenuItems"
//    static let AddCustomer = "AddCustomer"
//    static let Settings = "Settings"
//}

struct VCIdentifier {
    
    static let TabbarViewController = "TabbarViewController"
    static let DashboardViewController = "DashboardViewController"
    
    //StartingPages ViewControllers
    static let FirstViewController = "FirstViewController"
    static let SecondViewController = "SecondViewController"
    static let ThirdViewController = "ThirdViewController"
    static let FourthViewController = "FourthViewController"
    static let FifthViewController = "FifthViewController"
    
    //Business profile viewcontrollers
    static let BusinessProfileViewController = "BusinessProfileViewController"
    static let BuisnessprofilemapViewController = "BuisnessprofilemapViewController"
    static let ContactDetailsViewController = "ContactDetailsViewController"
    
    //OTP verification ViewControllers
    static let SignUpViewController = "SignUpViewController"
    static let PhoneVerificationViewController = "PhoneVerificationViewController"
    
    //service viewcontroller
    static let SetupProfileViewController = "SetupProfileViewController"
    static let AddQueueViewController = "AddQueueViewController"
    static let AddQueueCompletedViewController = "AddQueueCompletedViewController"
    static let AddScheduleViewController = "AddScheduleViewController"
    static let AddScheduleCompletedViewController = "AddScheduleCompletedViewController"
    static let AddServiceViewController = "AddServiceViewController"
    static let AddServiceCompletedViewController = "AddServiceCompletedViewController"
    static let GetQueueViewController = "GetQueueViewController"
    static let GetServiceListViewController = "GetServiceListViewController"
    static let AddHolidaysViewController = "AddHolidaysViewController"
    
    //Queue details view controller
    static let BottomSheetViewController = "BottomSheetViewController"
    static let QueueDetailsViewController = "QueueDetailsViewController"
    static let ServeAppointmentViewController = "ServeAppointmentViewController"
    static let MessageViewController = "MessageViewController"
    
    // Menu Items ViewController
    static let BoradcastMessageViewController = "BoradcastMessageViewController"
    static let StopQueueViewController = "StopQueueViewController"
    static let BlcokCalendarViewController = "BlcokCalendarViewController"
    static let ApproveDeclineCondensedViewController = "ApproveDeclineCondensedViewController"
    static let BlockCalendarPopupViewController = "BlockCalendarPopupViewController"
    static let ApproveDeclineViewController = "ApproveDeclineViewController"
    
    // Add Customer
    static let AddAppointmentViewController = "AddAppointmentViewController"
    
    // Settings
    static let SettingsViewController = "SettingsViewController"
    static let ContactFeedbackViewController = "ContactFeedbackViewController"
}


struct MenuItems {
    static let Analytics = "Analytics"
    static let StopQueue = "Stop Queue"
    static let ApproveDeclineAppo = "Approve/Decline Appointment"
    static let BlockCalendar = "Block Calendar"
    static let BroadcastMessage = "Broadcast Message"
    static let EditQueue = "Edit Queue Details"
    static let Help = "Help"
}

struct UserDefaultsKeys {
    static let IS_LOGIN = "isLogin"
}

struct StringConstants {
    static let ALERT_TITLE_INFORMATION = "Information"
    static let ALERT_NO_INTERNET = "Please check your network condition."
    static let ALERT_DELETE_VIN_CONFIRM = ""
}

//struct MMPCDatabase {
//    //static let DBpassword = "Manik"
//    static let DBpassword = "AwGz0aeKLGqkfMWfTugvH7xGdIKdqt8AqSPuW5kJStCG1eag7dT6J30fh833GsGlEU4gKgmaKlWPtLm1A5HT5IBfqcHcDRdF4U9cli+ameRFHwfyB1Gn70dqjrpGr6+B7Yhz7tKJAbJPBeLtz4XpPH7a"
//    static let DBFileName = "MMPC.sqlite"
//}

struct ENTITIES {
    static let message = "message"
    static let status = "status"
}

//struct DATEFORMATTERS {
//    static let YYYYMMDDTHHMMSSZZZZZZ: String = "yyyy-MM-dd HH:mm:ss.zzzzzz"
//    static let YYYYMMDDTHHMMSS: String = "yyyy-MM-dd HH:mm:ss"
//    static let MMDDYYYY: String = "MM/dd/yyyy"
//    static let MMDDYY: String = "MM/dd/yy"
//    static let YYYYMMDD: String = "yyyy-MM-dd"
//    static let DDMMYYYY: String = "dd-MM-yyyy"
//    static let MMMMDDYYYY: String =  "MMMM dd, yyyy"
//    static let YYYYMMMMDD: String = "yyyy-MMMM-dd"
//    static let HHMM: String = "HH:mm"
//    static let EMMMMDDYYYY: String = "E, MMMM dd, yyyy"
//    static let DDMMMYYYY: String = "dd MMM yyyy"
//    static let MMMYYYY: String = "MMM yyyy"
//    static let MMYY = "MMMM yy"
//    static let DDMMYY = "dd/MM/yy"
//    static let DD = "dd"
//    static let MMMDD: String = "MMM dd"
//    static let HHMMA: String = "HH:mm a"
//    static let DDMMM: String = "dd MMM"
//
//}

struct COLOR {
    static let selQueue = UIColor(red: 87/255, green: 150/255, blue: 139/255, alpha: 1)
    static let unSelCalender = UIColor(red: 90/255, green: 88/255, blue: 88/255, alpha: 1)
    static let unSelCalenderBackground = UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1)
    
    //Dashboard
    static let ongoingGradient_1 = UIColor(red: 87.0 / 255.0, green: 150 / 255.0, blue: 139 / 255.0, alpha: 1.0).cgColor
    static let ongoingGradient_2 = UIColor(red: 66 / 255.0, green: 126.0 / 255.0, blue: 115 / 255.0, alpha: 1.0).cgColor
    static let ongoingQueueLabel = UIColor(red: 172/255, green: 219/255, blue: 210/255, alpha: 1)
    static let ongoingQueueBtnTitle = UIColor(red: 87/255, green: 150/255, blue: 139/255, alpha: 1)
    static let ongoingQueueBtnBack = UIColor(red: 226/255, green: 250/255, blue: 245/255, alpha: 1)
    static let yetToStartQueueGradient_1 = UIColor(red: 111/255, green: 132/255, blue: 150/255, alpha: 1).cgColor
    static let yetToStartQueueGradient_2 = UIColor(red: 91/255, green: 114/255, blue: 133/255, alpha: 1).cgColor
    static let yetToStartQueueLabel = UIColor(red: 201/255, green: 214/255, blue: 227/255, alpha: 1)
    static let yetToStartQueueBtnTitle = UIColor(red: 91/255, green: 114/255, blue: 133/255, alpha: 1)
    static let yetToStartQueueBtnBack = UIColor(red: 234/255, green: 243/255, blue: 251/255, alpha: 1)
    static let closedQueueGradient_1 = UIColor(red: 178/255, green: 116/255, blue: 100/255, alpha: 1).cgColor
    static let closedQueueGradient_2 = UIColor(red: 169/255, green: 94/255, blue: 77/255, alpha: 1).cgColor
    static let closedQueueLabel = UIColor(red: 247/255, green: 209/255, blue: 200/255, alpha: 1)
    static let closedQueueBtnTitle = UIColor(red: 171/255, green: 98/255, blue: 81/255, alpha: 1)
    static let closedQueueBtnBack = UIColor(red: 253/255, green: 226/255, blue: 220/255, alpha: 1)
    static let declineHeading = UIColor(red: 235/255, green: 64/255, blue: 64/255, alpha: 1)
    static let grayLbl = UIColor(red: 111/255, green: 132/255, blue: 150/255, alpha: 1)
    static let pinkCgColor = UIColor(red: 255/255, green: 55/255, blue: 94/255, alpha: 1).cgColor
    
    //QUEUE DETAILS
    static let pendingTokenLbl = UIColor(red: 218/255, green: 147/255, blue: 81/255, alpha: 1)
    static let pendingTokenBack = UIColor(red: 255/255, green: 242/255, blue: 23/255, alpha: 1)
    static let lightGrey = UIColor(red: 126/255, green: 149/255, blue: 171/255, alpha: 1)
    static let cardOutlineGreen = UIColor(red: 67/255, green: 127/255, blue: 116/255, alpha: 1)
    static let cardGreen = UIColor(red: 227/255, green: 246/255, blue: 243/255, alpha: 1)
    static let statusBtnApproved = UIColor(red: 240/255, green: 255/255, blue: 252/255, alpha: 1)
    static let cardOutlineGray = UIColor(red: 118/255, green: 118/255, blue: 118/255, alpha: 1)
    static let cardGray = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
    static let statusBtnGray = UIColor(red: 236/255, green: 255/255, blue: 255/255, alpha: 1)
    static let cardOutlineRed = UIColor(red: 235/255, green: 64/255, blue: 64/255, alpha: 1)
    static let cardRed = UIColor(red: 255/255, green: 225/255, blue: 225/255, alpha: 1)
    static let cardOutlineOrange = UIColor(red: 218/255, green: 147/255, blue: 81/255, alpha: 1)
    static let cardOrange = UIColor(red: 255/255, green: 242/255, blue: 223/255, alpha: 1)
    static let lateGray = UIColor(red: 111/255, green: 132/255, blue: 150/255, alpha: 1)
    static let BaseGray = UIColor(red: 106/255, green: 125/255, blue: 148/255, alpha: 1)
    static let WhiteColor = UIColor(red:243.0, green:243.0, blue:241.0, alpha:1.0)//whilte
    static let PinkColor = UIColor(red:255.0/255.0, green:55.0/255.0, blue:94.0/255.0, alpha:1.0)//pink
    static let GrayColor = UIColor(red: 142.0/255.0, green: 154.0/255.0, blue: 160.0/255.0, alpha: 1.0)//gray for button title
    static let GreenColor = UIColor(red: 0.0/255.0, green: 196.0/255.0, blue: 140.0/255.0, alpha: 1.0)//Green for Notification view
    static let LightGrayColor = UIColor(red: 243.0/255.0, green: 242.0/255.0, blue: 241.0/255.0, alpha: 1.0)//Background Gray color for button
    static let PurpleColor = UIColor(red: 79.0/255.0, green: 0.0/255.0, blue: 140.0/255.0, alpha: 1.0)//Background purple for cell selection
    
}

struct CHARACTERS
{
  static let  ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',.-&_ "
}

struct DEVICES {
    static let iPhoneSE: String = "iPhoneSE"
    static let iPhone6: String = "iPhone6"
    static let iPhonePlus: String = "iPhonePlus"
    static let iPhoneX: String = "iPhoneX"
    static let iPhoneXR: String = "iPhoneXR"
    static let iPad: String = "iPad"
    static let iPadPro_12_9: String = "iPadPro 12.9"
    static let iPadPro_10_5: String = "iPadPro 10.5"
}

struct GLOBALVARIABLE {
   static var TabBarName = ""
   static var TaskerTabBarName = ""
}

extension Date {
    var millisecondsSince1970:Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
        //RESOLVED CRASH HERE
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
}

extension String {
    var stripped: String {
        let okayChars = Set("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890-")
        return self.filter {okayChars.contains($0)}
    }
    
    var num_stripped: String {
        let okayChars = Set("1234567890.")
        return self.filter {okayChars.contains($0)}
    }
}

extension UIView {
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}

extension String {
    
    enum RegularExpressions: String {
        case phone = "^\\s*(?:\\+?(\\d{1,3}))?([-. (]*(\\d{3})[-. )]*)?((\\d{3})[-. ]*(\\d{2,4})(?:[-.x ]*(\\d+))?)\\s*$"
    }
    
    func isValid(regex: RegularExpressions) -> Bool {
        return isValid(regex: regex.rawValue)
    }
    
    func isValid(regex: String) -> Bool {
        let matches = range(of: regex, options: .regularExpression)
        return matches != nil
    }
    
    func onlyDigits() -> String {
        let filtredUnicodeScalars = unicodeScalars.filter{CharacterSet.decimalDigits.contains($0)}
        return String(String.UnicodeScalarView(filtredUnicodeScalars))
    }
    
    func makeAColl() {
        if let url = URL(string: "tel://\(self.onlyDigits())"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    public var isBlank: Bool {
        get {
            let trimmed = trimmingCharacters(in: .whitespacesAndNewlines)
            return trimmed.isEmpty
        }
    }
    
    public var isEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func dialNumber() {
        let strNumber = self
        
        if let url = URL(string: "tel://\(strNumber)") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url as URL)
            }
        }
    }
}

extension String {
    mutating func insert(string:String,ind:Int) {
        self.insert(contentsOf: string, at:self.index(self.endIndex, offsetBy: ind) )
    }
}

extension TimeInterval {
    private var milliseconds: Int {
        return Int((truncatingRemainder(dividingBy: 1)) * 1000)
    }
    
    private var seconds: Int {
        return Int(self) % 60
    }
    
    private var minutes: Int {
        return (Int(self) / 60 ) % 60
    }
    
    private var hours: Int {
        return Int(self) / 3600
    }
    
    var stringTime: String {
        if hours != 0 {
            return "\(hours): \(minutes)"
        } else if minutes != 0 {
            return "\(minutes): \(seconds)"
        } else if milliseconds != 0 {
            return "\(seconds): \(milliseconds)"
        } else {
            return "\(seconds)"
        }
    }
}

extension NSLayoutConstraint {
    func setMultiplier(multiplier:CGFloat) -> NSLayoutConstraint {
        NSLayoutConstraint.deactivate([self])
        let newConstraint = NSLayoutConstraint(
            item: firstItem ?? AnyObject.self,
            attribute: firstAttribute,
            relatedBy: relation,
            toItem: secondItem,
            attribute: secondAttribute,
            multiplier: multiplier,
            constant: constant)
        newConstraint.priority = priority
        newConstraint.shouldBeArchived = shouldBeArchived
        newConstraint.identifier = identifier
        NSLayoutConstraint.activate([newConstraint])
        return newConstraint
    }
}

extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}

extension Array where Element:Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()
        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }
        return result
    }
}

extension UISearchBar {

    // Due to searchTextField property who available iOS 13 only, extend this property for iOS 13 previous version compatibility
    var compatibleSearchTextField: UITextField {
        guard #available(iOS 13.0, *) else { return legacySearchField }
        return self.searchTextField
    }

    private var legacySearchField: UITextField {
        if let textField = self.subviews.first?.subviews.last as? UITextField {
            // Xcode 11 previous environment
            return textField
        } else if let textField = self.value(forKey: "searchField") as? UITextField {
            // Xcode 11 run in iOS 13 previous devices
            return textField
        } else {
            // exception condition or error handler in here
            return UITextField()
        }
    }
}

extension UINavigationController {

    func backToViewController(viewController: Swift.AnyClass) {

            for element in viewControllers as Array {
                if element.isKind(of: viewController) {
                    self.popToViewController(element, animated: true)
                break
            }
        }
    }
}
extension UIView {

    func addTapGesture(action : @escaping ()->Void ){
        let tap = MyTapGestureRecognizer(target: self , action: #selector(self.handleTap(_:)))
        tap.action = action
        tap.numberOfTapsRequired = 1

        self.addGestureRecognizer(tap)
        self.isUserInteractionEnabled = true

    }
    @objc func handleTap(_ sender: MyTapGestureRecognizer) {
        sender.action!()
    }
}

class MyTapGestureRecognizer: UITapGestureRecognizer {
    var action : (()->Void)? = nil
}

extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}
