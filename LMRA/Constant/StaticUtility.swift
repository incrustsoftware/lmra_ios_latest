//
//  StaticUtility.swift
//  MMPC
//
//  Created by Saurabh Patil on 13/09/19.
//  Copyright © 2019 Vinod. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class StaticUtility {
    
    class func getDeviceId() -> String {
        let uuid = UIDevice.current.identifierForVendor?.uuidString
        return uuid ?? UUID().uuidString
    }
    
    class func setBorderToSegment(segmentControl : UIView){
        segmentControl.layer.borderColor = UIColor.gray.cgColor
        //segmentControl.layer.borderWidth = 1
        let segmentHeight = self.getCalculated(segmentControl.frame.height)
        segmentControl.layer.cornerRadius = segmentHeight / 2
    }
    
    class func getCalculated(_ value: CGFloat) -> CGFloat {
        if isDevice() == DEVICES.iPhoneX || isDevice() == DEVICES.iPhoneXR {
            return safeAreaHeight() * (value / 568.0)
        } else {
            return screenHeight * (value / 568.0)
        }
    }
    
    //Distance Calculation
    class func calculateDistance(lat1: Double, long1: Double, lat2: Double, long2: Double) -> Double {
        let coordinate0 = CLLocation(latitude: lat1, longitude: long1)
        let coordinate1 = CLLocation(latitude: lat2, longitude: long2)
        let distanceInMeters = coordinate0.distance(from: coordinate1)
        return distanceInMeters / 1000
        //        let theta = long1 - long2
        //        var dist = sin(deg2rad(deg: lat1)) * sin(deg2rad(deg: lat2)) + cos(deg2rad(deg: lat1)) * cos(deg2rad(deg: lat2)) * cos(deg2rad(deg: theta))
        //        dist = acos(dist)
        //        dist = rad2deg(rad: dist)
        //        dist = dist * 60 * 1.1515
        //        return dist
    }
    
    class func deg2rad(deg: Double) -> Double {
        return (deg * .pi / 180.0)
    }
    class func rad2deg(rad: Double) -> Double {
        return (rad * 180.0 / .pi)
    }
    
    class func mandatoryLabelAttributedString(inputString : String, label : UILabel) {
        
        //        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: inputString)
        //        attributedString.setColorForText(textForAttribute: inputString, withColor: UIColor.darkGray)
        //        attributedString.setColorForText(textForAttribute: "*", withColor: UIColor.red)
        //        print(attributedString.description)
        //        label.attributedText = attributedString
    }
    
    class func timeConversionInDesiredFormat(inputDate: String, inputFormat: String, outputFormat: String)-> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = inputFormat
        // set locale to reliable US_POSIX
        if let date = dateFormatter.date(from: inputDate) {
            dateFormatter.dateFormat = outputFormat
            return dateFormatter.string(from: date)
        } else {
            dateFormatter.dateFormat = outputFormat
            return dateFormatter.string(from: Date())
        }
    }
  
    class func convertDateFormater4(_ date: String) -> String
      {
          let dateFormatter = DateFormatter()
          dateFormatter.dateFormat = "yyyy-MM-dd"
          let date = dateFormatter.date(from: date)
          dateFormatter.dateFormat = "EEEE, dd MMM"
          
          return dateFormatter.string(from: date!)
          
      }
    class func convertDateFormater5(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, dd MMM"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "EEEE"
        
        return dateFormatter.string(from: date!)
        
    }
    class func convertDateFormater6(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "EEEE dd, MMMM, yyyy"
        
        return dateFormatter.string(from: date!)
        
    }
    class func convertDateFormater7(_ date: String) -> String
       {
           let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = "yyyy-MM-dd"
           let date = dateFormatter.date(from: date)
           dateFormatter.dateFormat = "EEEE, yyyy"
           
           return dateFormatter.string(from: date!)
           
       }
    
    class func convertDateFormater8(_ date: String) -> String
       {
           let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = "dd-MM-yyyy"
           let date = dateFormatter.date(from: date)
           dateFormatter.dateFormat = "EEEE, dd MMM"
           
           return dateFormatter.string(from: date!)
           
       }
    
    class func convertDateFormater9(_ date: String) -> String
       {
           let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = "yyyy-MM-dd"
           let date = dateFormatter.date(from: date)
           dateFormatter.dateFormat = "dd-MM-yyyy"
           
           return dateFormatter.string(from: date!)
           
       }
    
    class func getDayOfWeek(_ today: String) -> String? {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        guard let todayDate = formatter.date(from: today) else { return "Wrong Format" }
        let myCalendar = Calendar(identifier: .indian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        switch (weekDay) {
        case 1:
            return "Sun"
        case 2:
            return "Mon"
        case 3:
            return "Tue"
        case 4:
            return "Wed"
        case 5:
            return "Thu"
        case 6:
            return "Fri"
        case 7:
            return "Sat"
        default:
            return ""
        }
    }
    
    class func getNameIntervalOfWeek(from: String, to: String) -> String? {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        guard let fromDate = formatter.date(from: from) else { return "Wrong Format" }
        guard let toDate = formatter.date(from: to) else { return "Wrong Format" }
        
        formatter.dateFormat = "dd MMM"
        let finalFrom = formatter.string(from: fromDate)
        let finalTo = formatter.string(from: toDate)
        
        return "\(finalFrom)\n - \(finalTo)"
    }
}

class UtilityLog {
    static let sharedInstant = UtilityLog()
    private init(){}
    func printLog(value:Any){
        if isPrintable{
            print("\(value)")
        }
    }
}

