//
//  SetAddressViewController.swift
//  LMRA
//
//  Created by Mac on 24/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//


import UIKit
import iOSDropDown
import Alamofire

class SetAddressViewController: UIViewController ,UITextFieldDelegate{
    
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var LblNickName: UILabel!
    @IBOutlet weak var TFNickName: UITextField!
    @IBOutlet weak var LblArea: UILabel!
    @IBOutlet weak var LblAddressType: UILabel!
    @IBOutlet weak var DropdownArea: DropDown!
    @IBOutlet weak var DropDownAddressType: DropDown!
    @IBOutlet weak var LblStreet: UILabel!
    @IBOutlet weak var TFStreet: UITextField!
    @IBOutlet weak var LblBuildingNm: UILabel!
    @IBOutlet weak var TFBuildingNm: UITextField!
    @IBOutlet weak var LblFloor: UILabel!
    @IBOutlet weak var TFFloor: UITextField!
    @IBOutlet weak var LblAprtNo: UILabel!
    @IBOutlet weak var TFAprtNo: UITextField!
    @IBOutlet weak var LblAdditnDir: UILabel!
    @IBOutlet weak var TFAdditnDir: UITextField!
    @IBOutlet weak var LblNickNmHt: NSLayoutConstraint!
    @IBOutlet weak var LblAreaHT: NSLayoutConstraint!
    @IBOutlet weak var LblAddressTypeHt: NSLayoutConstraint!
    @IBOutlet weak var LblStreetHt: NSLayoutConstraint!
    @IBOutlet weak var LblBuildingnNmht: NSLayoutConstraint!
    @IBOutlet weak var LblFloorNmht: NSLayoutConstraint!
    @IBOutlet weak var LblAprtNoHt: NSLayoutConstraint!
    @IBOutlet weak var LblAddtnDirHt: NSLayoutConstraint!
    @IBOutlet weak var Btncontiue: UIButton!
    @IBOutlet weak var MainView: UIView!
    
    var maxLenAddNickName:Int = 50
    var maxLenStreet:Int = 50
    var maxLenBuildNM:Int = 50
    var maxLenFloor:Int = 50
    var maxLenAprtNo:Int = 50
    var maxLenAddDir:Int = 50
    let defaults = UserDefaults.standard
    var BtnContinueColor = UIColor(red:243, green:243, blue:241, alpha:1.0)
    var Cell_Number = ""
    var Get_CPR_No = ""
    var AddrNickName = ""
    var Area = ""
    var AddressType = ""
    var Street = ""
    var BuildingName = ""
    var Floor = ""
    var AprtNumber = ""
    var AddDir = ""
    var Current_cell_number = ""
    var authentication_key_val = ""
    var FullAddress = ""
    var GetAccountSettingAddressID = ""
    var StringGetAreaID = ""
    var StringGetAddressTypeId = ""
    var AuthKeyFromSetAddress = ""
    var AllDataDict = [[String:Any]]()
    var AddressTypeArray = [String]()
    var AddressAreaArray = [String]()
    var AddressTypeIDArray = [String]()
    var AddressAreaIDArray = [String]()
    var isCalledFromAccountSetting:Bool!
    
    var charactersSet = CharacterSet(charactersIn: CHARACTERS.ACCEPTABLE_CHARACTERS)
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DropdownArea.delegate = self
        DropDownAddressType.delegate = self
        self.TFNickName.delegate = self
        self.TFFloor.delegate = self
        self.TFStreet.delegate = self
        self.TFBuildingNm.delegate = self
        self.TFAdditnDir.delegate = self
        self.TFAprtNo.delegate = self
        self.DropdownArea.tintColor = .clear
        self.DropDownAddressType.tintColor = .clear
        
        Btncontiue.layer.cornerRadius = 2;
        self.get_adress_type()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        addDoneButtonOnKeyboard()
        
//        TFAdditnDir.addTarget(self, action: #selector(SetAddressViewController.txtFieldChanged(_:)),
//                              for: .editingChanged)
//        TFNickName.addTarget(self, action: #selector(SetAddressViewController.txtFieldChanged(_:)),
//                             for: .editingChanged)
        TFStreet.addTarget(self, action: #selector(SetAddressViewController.txtFieldChanged(_:)),
                           for: .editingChanged)
        TFBuildingNm.addTarget(self, action: #selector(SetAddressViewController.txtFieldChanged),
                               for: .editingChanged)
        TFFloor.addTarget(self, action: #selector(SetAddressViewController.txtFieldChanged),
                          for: .editingChanged)
        TFAprtNo.addTarget(self, action: #selector(SetAddressViewController.txtFieldChanged),
                           for: .editingChanged)
        DropdownArea.addTarget(self, action: #selector(SetAddressViewController.txtFieldChanged),
                               for: .editingChanged)
        
        DropDownAddressType.addTarget(self, action: #selector(SetAddressViewController.txtFieldChanged),
                                      for: .editingChanged)
        
        let NickNameTapped = UITapGestureRecognizer(target:self,action:#selector(self.NickNameTapped))
        
        // add it to the label
        LblNickName.addGestureRecognizer(NickNameTapped)
        LblNickName.isUserInteractionEnabled = true
        
        let StreetTapped = UITapGestureRecognizer(target:self,action:#selector(self.StreetTapped))
        
        // add it to the label
        LblStreet.addGestureRecognizer(StreetTapped)
        LblStreet.isUserInteractionEnabled = true
        
        let BuildingTapped = UITapGestureRecognizer(target:self,action:#selector(self.BuildingTapped))
        
        // add it to the label
        LblBuildingNm.addGestureRecognizer(BuildingTapped)
        LblBuildingNm.isUserInteractionEnabled = true
        
        let FloorTapped = UITapGestureRecognizer(target:self,action:#selector(self.FloorTapped))
        
        // add it to the label
        LblFloor.addGestureRecognizer(FloorTapped)
        LblFloor.isUserInteractionEnabled = true
        
        let ApprtNoTapped = UITapGestureRecognizer(target:self,action:#selector(self.ApprtNoTapped))
        
        // add it to the label
        LblAprtNo.addGestureRecognizer(ApprtNoTapped)
        LblAprtNo.isUserInteractionEnabled = true
        
        let AddtlDirTapped = UITapGestureRecognizer(target:self,action:#selector(self.AddtlDirTapped))
        
        // add it to the label
        LblAdditnDir.addGestureRecognizer(AddtlDirTapped)
        LblAdditnDir.isUserInteractionEnabled = true
        
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
//        scrollview.addGestureRecognizer(tap)
        
        MainView.addTapGesture{
            self.dismissKeyboard()
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.DropdownArea.text = "Choose Area"
        self.DropDownAddressType.text = "Choose Address type"
        DropdownArea.didSelect{(selectedText , index ,id) in
            self.DropdownArea.text =  selectedText
            print("self.DropdownArea.text: \( self.DropdownArea.text ?? "")")
            
            let GetAreaID = index
            self.StringGetAreaID = String(GetAreaID)
            print("self.DropdownArea.id:",self.StringGetAreaID)
            
        }
        
        DropDownAddressType.didSelect{(selectedText , index ,id) in
            self.DropDownAddressType.text = selectedText
            print("self.DropDownAddressType.text: \(self.DropDownAddressType.text ?? "")")
            
            let GetAddressTypeId = index
            self.StringGetAddressTypeId = String(GetAddressTypeId)
            print("self.GetAddressType.id:",self.StringGetAddressTypeId)
        }
    }
    
//    @objc func dismissKeyboard() {
//        scrollview.contentOffset.y = 0
//        view.endEditing(true)
//    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    @objc func NickNameTapped()
    {
        LblNickNmHt.constant = 30
        TFNickName.becomeFirstResponder()
    }
    
    @objc func StreetTapped()
    {
        LblStreetHt.constant = 30
        TFStreet.becomeFirstResponder()
    }
    
    @objc func BuildingTapped()
    {
        LblBuildingnNmht.constant = 30
        TFBuildingNm.becomeFirstResponder()
    }
    
    @objc func FloorTapped()
    {
        LblFloorNmht.constant = 30
        TFFloor.becomeFirstResponder()
    }
    
    @objc func ApprtNoTapped()
    {
        LblAprtNoHt.constant = 30
        TFAprtNo.becomeFirstResponder()
    }
    
    @objc func AddtlDirTapped()
    {
        LblAddtnDirHt.constant = 30
        TFAdditnDir.becomeFirstResponder()
    }
    
    @IBAction func BachBtnClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ContinueBtnCick(_ sender: Any) {
        SetAddrcheckValidation()
//        Btncontiue.backgroundColor = COLOR.PinkColor//UIColor(red: 255/255, green: 55/255, blue: 94/255, alpha: 1.0)
    }
    
    func SetAddrcheckValidation()
    {
        if (self.DropdownArea.text == "Choose Area")
        {
            
            self.view.makeToast("Please select valid area")
        }
        else if (self.DropDownAddressType.text == "Choose Address type")
        {
            
            self.view.makeToast("Please select valid address type")
        }
        else if (TFStreet.text == "")
        {
            
            self.view.makeToast("Please enter street")
        }
        else if (TFBuildingNm.text == "")
        {
            
            self.view.makeToast("Please enter building name")
        }
        else if (TFFloor.text == "")
        {
            
            self.view.makeToast("Please enter floor")
        }
        else if (TFAprtNo.text == "")
        {
            
            self.view.makeToast("Please enter apartment number")
        }
        else if TFNickName.text?.rangeOfCharacter(from: charactersSet.inverted) != nil {
            self.view.makeToast("Special character not allowed &,.space allowed between characters allowed")
            
        }
        else if TFAdditnDir.text?.rangeOfCharacter(from: charactersSet.inverted) != nil {
            self.view.makeToast("Special character not allowed &,.space allowed between characters allowed")
            
        }
        else
        {
            self.AddrNickName =  TFNickName.text!
            self.Area = self.DropdownArea.text!
            self.AddressType =  self.DropDownAddressType.text!
            self.Street = TFStreet.text!
            self.BuildingName = TFBuildingNm.text!
            self.Floor = TFFloor.text!
            self.AprtNumber = TFAprtNo.text!
            self.AddDir = TFAdditnDir.text!
            self.Get_CPR_No = UserDefaults.standard.string(forKey: "CPRNoKey")!//get
            self.Current_cell_number = UserDefaults.standard.string(forKey: "CurrentCellNoKey")!//get
            self.authentication_key_val = UserDefaults.standard.string(forKey: "authentication_key")!//get
            
            self.FullAddress = ""
            self.FullAddress =  self.AddrNickName + "," + "" +  self.Area + "," + "" +
                self.Street + "," + "" +  self.BuildingName + "," + "" + self.Floor + "," + "" + self.AprtNumber + "," + "" + self.AddDir
            
            print("authentication_key_val: \(self.authentication_key_val)")
            print("Current_cell_number: \(self.Current_cell_number)")
            print("Get_CPR_No: \(self.Get_CPR_No)")
            print("AddDir: \(self.AddDir)")
            print("AprtNumber: \(self.AprtNumber)")
            print("Floor: \(self.Floor)")
            print("BuildingName: \(self.BuildingName)")
            print("Street: \(self.Street)")
            print("AddressType: \(self.AddressType)")
            print("Area: \(self.Area)")
            print("AddrNickName: \(self.AddrNickName)")
            print("FullAddress: \(self.FullAddress)")
            
            defaults.set(self.AddrNickName, forKey: "AddrNickNameKey")//set
            defaults.set(self.Area, forKey: "AreaKey")//set
            defaults.set(self.AddressType, forKey: "AddressTypeKey")//set
            defaults.set(self.Street, forKey: "StreetKey")//set
            defaults.set(self.BuildingName, forKey: "BuildingNameKey")//set
            defaults.set(self.Floor, forKey: "FloorKey")//set
            defaults.set(self.AprtNumber, forKey: "AprtNumberKey")//set
            defaults.set(self.AddDir, forKey: "AddDirKey")//set
            defaults.set(self.FullAddress, forKey: "FullAddressKey")//set
            
            let isCalledFromAddNewAddress = UserDefaults.standard.bool(forKey: "isAddAdress")// this is how you retrieve the bool value
            if (isCalledFromAddNewAddress == true)//value from add task location
            {
                self.Verify_set_address_customer()
                let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "AddTaskLocation") as! AddTaskLocationViewController
                
                self.navigationController?.pushViewController(nextViewController, animated: true)
                
            }
            else
            {
                
                self.Verify_set_address_customer()
                
                self.view.makeToast("Set Address Verified successfully", duration: 6.0, position: .bottom)
                
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ReviewNdConfirmVC") as! ReviewNdConfirmViewController
                
                self.navigationController?.pushViewController(nextViewController, animated: true)
                
            }
            
        }
    }
    
    
    func Verify_set_address_customer()
    {
        
        print("self.DropdownArea.id:",self.StringGetAreaID)
        print("self.GetAddressType.id:",self.StringGetAddressTypeId)
        
        let url = ConstantsClass.baseUrl + ConstantsClass.set_address_customer
        print("url: \(url)")
        let headers = [
            "Authorization": "\(self.authentication_key_val)"
        ]
        print("headers: \(headers)")
        let parameters = [
            "address_nickname" : "\(self.AddrNickName)",
            "area" : "\(self.StringGetAreaID)",//sending Id
            "appartment" : "\(self.StringGetAddressTypeId)",// sending id
            "street" : "\(self.Street)",
            "building_name" : "\(self.BuildingName)",
            "floor" : "\(self.Floor)",
            "appartment_no" : "\(self.AprtNumber)",
            "additional_directions" : "\(self.AddDir)",
            "cell_no" : "\(self.Current_cell_number)",
            "cpr_no" : "\(self.Get_CPR_No)"
            ] as [String : Any]
        
        print("parametersSetAddress: \(parameters)")
        
        Alamofire.request(url
            , method: .post, parameters: parameters, encoding:JSONEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any]
                {
                    print("response: \(response)")
                    if let responseDict = response["response"]as? [String:Any]
                    {
                        if let status = response["status"] as? Bool {
                            
                            if status
                            {
                                
                                
                            }
                            else
                            {
                                self.view.makeToast("Something went wrong", duration: 3.0, position: .bottom)
                                print("Something went wrong")
                            }
                        }
                    }
                }
        }
    }
    
    
    
    func get_adress_type()
    {
        self.authentication_key_val = UserDefaults.standard.string(forKey: "authentication_key")!//get
        
        let urlString = ConstantsClass.baseUrl + ConstantsClass.get_adress_type
        print("urlString: \(urlString)")
        
        let headers = [
            "Authorization": "\(self.authentication_key_val)"
        ]
        Helper.sharedInstance.showLoader()
        Alamofire.request(urlString, method: .post, parameters: nil, encoding: URLEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any] {
                    print("responseLocation: \(response)")
                    
                    if (response["status"] as? Bool) != nil {
                        if let respArrayGetAddress = response["response"] as? [String:Any] {
                            print("respArrayGetAddress: \(respArrayGetAddress)")
                            
                            print("respArrayGetAddress: \(respArrayGetAddress)")
                            
                            if let AddressType = respArrayGetAddress["address_master_data"]as? [[String:Any]]
                            {
                                for TypeData in AddressType
                                {
                                    let AddressTypeName = TypeData["adrress_master_name"]as? String ?? ""
                                    let AddressTypeID = TypeData["adrress_master_id"]as? String ?? ""
                                    self.AddressTypeArray.append(AddressTypeName)
                                    self.AddressTypeIDArray.append(AddressTypeID)
                                }
                            }
                            print("AddressTypeArray: \(self.AddressTypeArray)")
                            
                            
                            
                            if let AddressArea = respArrayGetAddress["area_master_data"]as? [[String:Any]]
                            {
                                for TypeData in AddressArea
                                {
                                    let AddressAreaName = TypeData["area_master_name"]as? String ?? ""
                                    let AddressAreaID = TypeData["area_master_id"]as? String ?? ""
                                    self.AddressAreaArray.append(AddressAreaName)
                                    self.AddressAreaIDArray.append(AddressAreaID)
                                }
                            }
                            print("AddressAreaArray: \(self.AddressAreaArray)")
                            
                        }
                    }
                    // Make sure to update UI in main thread
                    DispatchQueue.main.async {
                        Helper.sharedInstance.hideLoader()
                        print("address_data_success")
                        self.AddressAreaArray.insert("Choose Area", at:0)
                        self.AddressTypeArray.insert("Choose Address type", at:0)
                        self.DropdownArea.optionArray = self.AddressAreaArray
                        self.DropDownAddressType.optionArray = self.AddressTypeArray
                        
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 20))
        doneToolbar.barStyle = .default
        let Cancel: UIBarButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(self.doneButtonAction))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [Cancel,flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.TFNickName.inputAccessoryView = doneToolbar
        self.TFStreet.inputAccessoryView = doneToolbar
        self.TFBuildingNm.inputAccessoryView = doneToolbar
        self.TFFloor.inputAccessoryView = doneToolbar
        self.TFAprtNo.inputAccessoryView = doneToolbar
        self.TFAdditnDir.inputAccessoryView = doneToolbar
        self.DropdownArea.inputAccessoryView = doneToolbar
        self.DropDownAddressType.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.TFNickName.resignFirstResponder()
        self.TFStreet.resignFirstResponder()
        self.TFBuildingNm.resignFirstResponder()
        self.TFFloor.resignFirstResponder()
        self.TFAprtNo.resignFirstResponder()
        self.TFAdditnDir.resignFirstResponder()
         self.DropdownArea.resignFirstResponder()
         self.DropDownAddressType.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.TFNickName.resignFirstResponder()
        self.TFStreet.resignFirstResponder()
        self.TFBuildingNm.resignFirstResponder()
        self.TFFloor.resignFirstResponder()
        self.TFAprtNo.resignFirstResponder()
        self.TFAdditnDir.resignFirstResponder()
       self.DropdownArea.resignFirstResponder()
      self.DropDownAddressType.resignFirstResponder()
        
        if textField == DropdownArea
        {

            return false
        }
        else if textField == DropDownAddressType
        {

            return false
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
         
        if(textField == TFNickName){
            let currentText = textField.text! + string
            return currentText.count <= maxLenAddNickName
        }
        if(textField == TFStreet){
            let currentText = textField.text! + string
            return currentText.count <= maxLenStreet
        }
        if(textField == TFBuildingNm){
            let currentText = textField.text! + string
            return currentText.count <= maxLenBuildNM
        }
        if(textField == TFFloor){
            let currentText = textField.text! + string
            return currentText.count <= maxLenFloor
        }
        if(textField == TFAprtNo){
            let currentText = textField.text! + string
            return currentText.count <= maxLenAprtNo
        }
         if(textField == TFAdditnDir){
            let currentText = textField.text! + string
            return currentText.count <= maxLenAddDir
        }
        if textField == DropdownArea
        {
            return false
        }
        else if textField == DropDownAddressType
        {
            return false
        }
        
          return true;
     }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
             
            if self.view.frame.origin.y == 0
            {
              
                self.view.frame.origin.y -= 150
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            //            self.view.frame.origin.y = 0
            self.view.frame.origin.y += 150
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
        DropdownArea.resignFirstResponder()
        DropDownAddressType.resignFirstResponder()
    
    }
    
    
    @objc func txtFieldChanged(_ textField: UITextField) {
        if (TFStreet.text!.count == 0) || (TFBuildingNm.text!.count == 0) || (TFFloor.text!.count == 0) || (TFAprtNo.text!.count == 0) || (self.DropdownArea.text == "Choose Area") || (self.DropDownAddressType.text == "Choose Address type")
      
        {
            self.Btncontiue.setTitleColor(COLOR.GrayColor, for: .normal)
            Btncontiue.backgroundColor = COLOR.LightGrayColor
        }
        else
        {
            self.Btncontiue.setTitleColor(COLOR.WhiteColor, for: .normal)
            Btncontiue.backgroundColor = COLOR.PinkColor
        }
    }
    
    //for now
//    //do not allow the textfield to edit
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        return false
//    }
    
  // do not allow to open keyboard
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == DropdownArea || textField == DropDownAddressType
        {
            textField.endEditing(true)
            DropdownArea.resignFirstResponder()
            DropDownAddressType.resignFirstResponder()
        }
    }


    
}
