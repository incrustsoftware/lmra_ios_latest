//
//  Parser.swift
//  LMRA
//
//  Created by Mac on 28/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import Alamofire
import Toast_Swift
import SwiftyJSON
var UD_LANGARRAY = [String]()
var strings: [String] = []
var stringsLang: [String] = []
class Parser: NSObject {
    static let sharedInstance = Parser()
    func parseCategoryList(data:JSON)->[MainCategory]
    {
        var lists:[MainCategory] = []
        
        let jsonResponse = JSON(data)
        jsonResponse["msg"].array?.forEach({ (item) in
            
            let list = MainCategory(id: item["response"]["id"].intValue, user_type: item["response"]["user_type"].stringValue, category: item["response"]["category"].stringValue)
            lists.append(list)
            
        })
        return lists
        
    }
    func parseSubCategoryList(data:JSON)->[SubCategory]
    {
        var lists:[SubCategory] = []
        
        let jsonResponse = JSON(data)
        jsonResponse["response"].array?.forEach({ (item) in
            
            let list = SubCategory(category_id: item["category_id"].intValue, subcategory_id: item["subcategory_id"].intValue, subcategory_name: item["subcategory_name"].stringValue, subcategory_image: item["subcategory_image"].stringValue)
            lists.append(list)
            
        })
        return lists
        
    }
    func parseTaskList(data:JSON)->[TaskList]
    {
        var lists:[TaskList] = []
        
        let jsonResponse = JSON(data)
        jsonResponse["response"].array?.forEach({ (item) in
            
            let list = TaskList(task_id: item["task_id"].intValue, task_title: item["task_title"].stringValue, task_date: item["task_date"].stringValue, task_description: item["task_description"].stringValue, customer_id: item["customer_id"].intValue, customer_name: item["customer_name"].stringValue, customer_family_name: item["customer_family_name"].stringValue, customer_nationality: item["customer_nationality"].stringValue, customer_cell_phone_number: item["customer_cell_phone_number"].stringValue, customer_profile: item["customer_profile"].stringValue, customer_identity_card_number: item["customer_identity_card_number"].stringValue, customer_hire_count: item["customer_hire_count"].intValue, task_invitation_status_id: item["task_invitation_status_id"].stringValue, task_invitation_status: item["task_invitation_status"].stringValue, tasker_offline_status: item["tasker_offline_status"].stringValue, task_update_time: item["task_update_time"].stringValue, tasker_name: item["tasker_name"].stringValue, tasker_profile: item["tasker_profile"].stringValue)
            lists.append(list)
            
        })
        return lists
        
    }
    func parseAddSubCategoryList(data:JSON)->[AddSubCategory]
       {
           var lists:[AddSubCategory] = []
           
           let jsonResponse = JSON(data)
           jsonResponse["response"].array?.forEach({ (item) in
               
               let list = AddSubCategory(subcategory_id: item["subcategory_id"].stringValue, subcategory_name: item["subcategory_name"].stringValue)
               lists.append(list)
               
           })
           return lists
           
       }
    func parseGetSubCategoryList(data:JSON)->[GetSubCategoryList]
          {
              var lists:[GetSubCategoryList] = []
              
              let jsonResponse = JSON(data)
              jsonResponse["response"].array?.forEach({ (item) in
                  
                let list = GetSubCategoryList(subcategory_id: item["subcategory_id"].stringValue, subcategory_name: item["subcategory_name"].stringValue)
                  lists.append(list)
                  
              })
              return lists
              
          }
    
    func parseRateAddedList(data:JSON)->[RateAddedList]
             {
                var lists:[RateAddedList] = [].reversed()
                 
                 let jsonResponse = JSON(data)
                 jsonResponse["response"].array?.forEach({ (item) in
                     
                    let list = RateAddedList(rate_id: item["rate_id"].stringValue, is_fixed: item["is_fixed"].stringValue, rate: item["rate"].stringValue, subcategory_name: item["subcategory_name"].stringValue)
                     lists.append(list)
                     
                 })
                 return lists
                 
             }
    func parseTimeSlotList(data:JSON)->[TimeSlot]
    {
        var lists:[TimeSlot] = []
        
        let jsonResponse = JSON(data)
        jsonResponse["response"].array?.forEach({ (item) in
            
            let list = TimeSlot(timeslot_name: item["timeslot_name"].stringValue, timeslot_id: item["timeslot_id"].intValue, timeslot_daytime: item["timeslot_daytime"].stringValue)
            lists.append(list)
            
        })
        return lists
        
    }
    func taskDetail(data:JSON) -> TaskDetail {
         let task:TaskDetail = TaskDetail()
        let jsonResponce = data
        if (jsonResponce["status"].stringValue == "true")
        {
            task.customer_id = jsonResponce["response"]["customer_id"].intValue; task.task_description = jsonResponce["response"]["task_description"].stringValue;
            task.customer_name = jsonResponce["response"]["customer_name"].stringValue;
            task.task_title =  jsonResponce["response"]["task_title"].stringValue; task.task_invitation_status_id = jsonResponce["response"]["task_invitation_status_id"].stringValue; task.customer_nationality = jsonResponce["response"]["customer_nationality"].stringValue; task.task_update_time = jsonResponce["response"]["task_update_time"].stringValue; task.customer_profile = jsonResponce["response"]["customer_profile"].stringValue; task.task_timeslot_name = jsonResponce["response"]["task_timeslot_name"].stringValue; task.customer_hire_count = jsonResponce["response"]["customer_hire_count"].intValue
            task.building_name = jsonResponce["response"]["task_address"]["building_name"].stringValue
            task.additional_directions =  jsonResponce["response"]["task_address"]["additional_directions"].stringValue
            task.floor = jsonResponce["response"]["task_address"]["floor"].stringValue
            task.area = jsonResponce["response"]["task_address"]["area"].stringValue
            task.appartment_no = jsonResponce["response"]["task_address"]["appartment_no"].stringValue
            task.appartment = jsonResponce["response"]["task_address"]["appartment"].stringValue
            task.street =  jsonResponce["response"]["task_address"]["street"].stringValue
            task.address_nickname =  jsonResponce["response"]["task_address"]["address_nickname"].stringValue
            task.customer_family_name = jsonResponce["response"]["customer_family_name"].stringValue
            task.tasker_name = jsonResponce["response"]["tasker_name"].stringValue; task.task_timeslot_daytime = jsonResponce["response"]["task_timeslot_daytime"].stringValue
            task.task_date = jsonResponce["response"]["task_date"].stringValue; task.customer_cell_phone_number = jsonResponce["response"]["customer_cell_phone_number"].stringValue; task.task_invitation_status = jsonResponce["response"]["task_invitation_status"].stringValue; task.task_timeslot_id = jsonResponce["response"]["task_timeslot_id"].stringValue; task.customer_identity_card_number =  jsonResponce["response"]["customer_identity_card_number"].stringValue; task.task_id = jsonResponce["response"]["task_id"].intValue
          
        }
        return task
    }
         
}
