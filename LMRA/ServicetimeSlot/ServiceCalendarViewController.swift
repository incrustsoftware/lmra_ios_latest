//
//  ServiceCalendarViewController.swift
//  LMRA
//
//  Created by Mac on 26/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import FSCalendar

class ServiceCalendarViewController: UIViewController,FSCalendarDelegate,FSCalendarDataSource{

    //@IBOutlet weak var MainView: UIView!
    @IBOutlet weak var CalendarView: FSCalendar!
    @IBOutlet weak var BtnConfirm: UIButton!
    var getselectedDate = ""
    
    
    fileprivate let gregorian = Calendar(identifier: .gregorian)
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd" //"EEEE, d MMM yyyy"
        return formatter
    }()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.CalendarView.delegate = self
        self.CalendarView.dataSource = self
         BtnConfirm.layer.cornerRadius = 4;
        CalendarView.roundCorners(corners: [.topLeft, .topRight], radius: 30)
        // Do any additional setup after loading the view.
    }

    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        self.getselectedDate = self.formatter.string(from: date)
        self.configureVisibleCells()
    }
    
       private func configureVisibleCells() {
           CalendarView.visibleCells().forEach { (cell) in
               let date = CalendarView.date(for: cell)
               let position = CalendarView.monthPosition(for: cell)
               
           }
       }
       
       //disable past date function
       
       func minimumDate(for calendar: FSCalendar) -> Date
       {
           return Date()
       }
    
    
    @IBAction func BtnConfirmClick(_ sender: Any) {

        let SelectedDate:[String: String] = ["SelectedDate": "\(self.getselectedDate)"  ]
      
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SelectedDateKey"), object: nil, userInfo: SelectedDate)
        print("getselectedDate:",self.getselectedDate)
            
        self.dismiss(animated: true, completion: nil)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
