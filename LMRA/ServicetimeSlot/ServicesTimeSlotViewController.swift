//
//  ServicesTimeSlotViewController.swift
//  LMRA
//
//  Created by Mac on 20/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire
import FSCalendar
import FittedSheets

class ServicesTimeSlotViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,FSCalendarDataSource,FSCalendarDelegate, FSCalendarDelegateAppearance {
    
    @IBOutlet weak var CalendarView: FSCalendar!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var SelctDateView: UIView!
    @IBOutlet weak var RadioBtnTomorrow: UIButton!
    @IBOutlet weak var RadioBtnSelectDate: UIButton!
    @IBOutlet weak var TimeSlotCollectionView: UICollectionView!
    @IBOutlet weak var RadioBtnToday: UIButton!
    
    @IBOutlet weak var EditBtnView: UIView!
    @IBOutlet weak var BtnEditate: UIButton!
    @IBOutlet weak var LblTomarrowsDate: UILabel!
    @IBOutlet weak var LblTodaysDate: UILabel!
    
    @IBOutlet weak var LabelChooseDate: UILabel!
    @IBOutlet weak var LabelTommorow: UILabel!
    @IBOutlet weak var LabelToday: UILabel!
    var isTimeSelected:Bool = false
    var isDateSelected:Bool = false
    var getSelectedDate = ""
    var authentication_key_val = ""
    var currentDate = ""
    var currentDateString = ""
    var currentDate_Day = ""
    var ConvertedTommarowDate = ""
    var Tommarow = ""
    var TommarowDate = ""
    var TommarowDateString = ""
    var TommarowDate_Day = ""
    var ConvertedgetSelectedDate = ""
    var getSelectedDate_Day = ""
    var service_date = ""
    var service_day = ""
    var TaskID:Int!
    var TaskIDString = ""
    var CustomerID:Int!
    var CustomerIDString = ""
    var GetSeletedTimeSlot = ""
    var GetSeletedTimeDay = ""
    var ConvertedFinalServiceDate = ""
    var selectedIndex:NSIndexPath?
    var BtnSelctedTag:Int!
    
    var TimeSlot = ""
    var TimeSlotString_Array = [String] ()
    var TimeSlotIDInteger:Int!
    var TimeSlotID = ""
    var TimeSlotIDString_Array = [String] ()
    var TimeSlotDayTime_Array = [String] ()
    var GetTimeID = ""
    var TimeSlotDayTime = ""
    var GetTaskID = ""
    var Task_TaskerTimeSlot = ""
    var AllDataDict = [[String:Any]]()
    var lastSelectedPosition:Int = -1;
    var isEditTimeSlot:Bool = true
    var isCalledFromSearchResult:Bool = false
    let defaults = UserDefaults.standard
    fileprivate let gregorian = Calendar(identifier: .gregorian)
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, d MMM yyyy"
        return formatter
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.CalendarView.isHidden = true
        CalendarView.dataSource = self
        CalendarView.delegate = self
        EditBtnView.isHidden = true
        
        UserDefaults.standard.set(false,forKey: "isEditSearchResult")
        UserDefaults.standard.set("",forKey: "PreviousTaskIDKey")
        
        self.authentication_key_val = UserDefaults.standard.string(forKey: "authentication_key") ?? ""//get
        print("authentication_key_val:",self.authentication_key_val)
        
        //        self.GetTaskID = UserDefaults.standard.string(forKey: "TaskIDKey") ?? "" //get
        //        print("GetTaskID:",self.GetTaskID)
        //
        
        self.get_timeslots()// first call get time slot //for now
        
        
        // todays date from calendar
        let date = Date()
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        
        let tommorow = Calendar.current.date(byAdding: .day, value: 1, to: date)
        print("tommorow:",tommorow)
        self.TommarowDateString = convertToString1(date: tommorow ?? Date(), dateformat:"EEEE, d MMM yyyy")
        self.TommarowDate_Day = convertDateFormater(self.TommarowDateString)
        
        print("TommarowDateString:",self.TommarowDateString)
        print("TommarowDate_Day:",self.TommarowDate_Day)
        
        let day1 = String(day)
        let month1 = String(month)
        let year1 = String(year)
        
        print("year:",year)
        print("month:",month)
        print("day:",day)
        
        self.currentDate = year1 + "-" + month1 + "-" + day1
        print("currentDate is", self.currentDate)
        
        self.TommarowDate = convertDateFormater3(self.TommarowDateString)
        self.LblTomarrowsDate.text = self.TommarowDate
        
        
        let TodayTapped = UITapGestureRecognizer(target:self,action:#selector(self.TodayTapped))
        
        //add it to the label
        LabelToday.addGestureRecognizer(TodayTapped)
        LabelToday.isUserInteractionEnabled = true
        
        let TommorowTapped = UITapGestureRecognizer(target:self,action:#selector(self.TommorowTapped))
        
        //add it to the label
        LabelTommorow.addGestureRecognizer(TommorowTapped)
        LabelTommorow.isUserInteractionEnabled = true
        
        let ChooseDateTapped = UITapGestureRecognizer(target:self,action:#selector(self.ChooseDateTapped))
        
        //add it to the label
        LabelChooseDate.addGestureRecognizer(ChooseDateTapped)
        LabelChooseDate.isUserInteractionEnabled = true
        
    }
    
    @objc func TodayTapped()
    {
        isDateSelected = true
        RadioBtnToday.isSelected = true
        RadioBtnTomorrow.isSelected = false
        RadioBtnSelectDate.isSelected = false
        self.CalendarView.isHidden = true
        self.currentDateString = String(self.currentDate)
        print("currentDateString is", self.currentDateString)
        self.currentDate_Day = convertDateFormater(currentDateString)
        print("currentDate_Day is", self.currentDate_Day)
        self.service_date = self.currentDateString
        self.service_day = self.currentDate_Day
        EditBtnView.isHidden = true
        
        if (!isTimeSelected)
        {
            self.btnContinue.setTitleColor(COLOR.GrayColor, for: .normal)
            btnContinue.backgroundColor = COLOR.LightGrayColor
        }
        else
        {
            self.btnContinue.setTitleColor(COLOR.WhiteColor, for: .normal)
            btnContinue.backgroundColor = COLOR.PinkColor
        }
        
    }
    
    @objc func TommorowTapped()
    {
        isDateSelected = true
        RadioBtnTomorrow.isSelected = true
        RadioBtnToday.isSelected = false
        RadioBtnSelectDate.isSelected = false
        self.CalendarView.isHidden = true
        self.TommarowDate = convertDateFormater3(self.TommarowDateString)
        self.LblTomarrowsDate.text = self.TommarowDate
        self.service_date = self.TommarowDateString
        self.service_day = self.TommarowDate_Day
        EditBtnView.isHidden = true
        //        self.LblTodaysDate.isHidden = true
        //        self.BtnEditate.isHidden = true
        if (!isTimeSelected)
        {
            self.btnContinue.setTitleColor(COLOR.GrayColor, for: .normal)
            btnContinue.backgroundColor = COLOR.LightGrayColor
        }
        else
        {
            self.btnContinue.setTitleColor(COLOR.WhiteColor, for: .normal)
            btnContinue.backgroundColor = COLOR.PinkColor
        }
    }
    
    @objc func ChooseDateTapped()
    {
        isDateSelected = true
        RadioBtnSelectDate.isSelected = true
        RadioBtnToday.isSelected = false
        RadioBtnTomorrow.isSelected = false
        //bottom sheet implementation for calendar
        
        let controller = ServiceCalendarViewController()
        let sheetController = SheetViewController(controller: controller)
        self.present(sheetController, animated: true, completion: nil)
        
        LblTodaysDate.font = UIFont(name:"STCForward-Bold",size:14)
        EditBtnView.isHidden = false
        self.LblTodaysDate.isHidden = false
        if (!isTimeSelected)
        {
            self.btnContinue.setTitleColor(COLOR.GrayColor, for: .normal)
            btnContinue.backgroundColor = COLOR.LightGrayColor
        }
        else
        {
            self.btnContinue.setTitleColor(COLOR.WhiteColor, for: .normal)
            btnContinue.backgroundColor = COLOR.PinkColor
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true //for now
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.SelectedDateFromCalendar(_:)), name: NSNotification.Name(rawValue: "SelectedDateKey"), object: nil)
        
        
    }
    
    @objc func SelectedDateFromCalendar(_ notification: NSNotification) {
        var GetDate = notification.userInfo?["SelectedDate"] as? String ?? ""
        print("GetDate:",GetDate)
        if GetDate == ""
        {
            GetDate =  self.currentDate
            self.LblTodaysDate.text = convertDateFormater3(GetDate)
        }
        else
        {
            self.LblTodaysDate.text = convertDateFormater3(GetDate)
        }
        
        self.service_date = GetDate
        self.getSelectedDate_Day = convertDateFormater(GetDate)
        self.service_day = self.getSelectedDate_Day
        
        print("self.service_date:", self.service_date)
        
    }
    
    func get_timeslots()
    {
        let urlString = ConstantsClass.baseUrl + ConstantsClass.get_timeslots
        print("urlString: \(urlString)")
        
        let headers = [
            "Authorization": "\(self.authentication_key_val)"
        ]
        Alamofire.request(urlString, method: .get, parameters: nil, encoding: URLEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if (response["status"] as? Bool) != nil {
                        if let respArray = response["response"] as? [[String:Any]] {
                            print("respArray: \(respArray)")
                            
                            for Data in respArray
                            {
                                self.TimeSlot = Data["timeslot_name"]as! String
                                self.TimeSlotDayTime = Data["timeslot_daytime"]as! String
                                self.TimeSlotIDInteger = Data["timeslot_id"] as! Int
                                self.TimeSlotID = String(self.TimeSlotIDInteger)
                                self.TimeSlotString_Array.append(self.TimeSlot)
                                self.TimeSlotIDString_Array.append(self.TimeSlotID)
                                self.TimeSlotDayTime_Array.append(self.TimeSlotDayTime)
                            }
                            print("TimeSlotString_Array: \(self.TimeSlotString_Array)")
                            print("TimeSlotIDString_Array: \(self.TimeSlotIDString_Array)")
                        }
                    }
                    // Make sure to update UI in main thread
                    DispatchQueue.main.async {
                        self.TimeSlotCollectionView.reloadData()
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
        
        
    }
    
    
    //    func review_task_details()
    //    {
    //
    //        let urlString = ConstantsClass.baseUrl + ConstantsClass.review_task_details
    //        print("urlString: \(urlString)")
    //
    //        let headers = [
    //            "Authorization": "\(self.authentication_key_val)"
    //        ]
    //        print("headers: \(headers)")
    //
    //        let parameters = [
    //            "task_id" : "\(self.GetTaskID)",
    //            ] as [String : AnyObject]
    //        print("parametersaddtaskdetails: \(parameters)")
    //        Alamofire.request(urlString
    //            , method: .post, parameters: parameters, encoding:JSONEncoding.default,headers: headers).responseJSON
    //            {
    //                response in
    //                if let response = response.result.value as? [String:Any] {
    //                    print("response: \(response)")
    //
    //                    if (response["status"] as? Bool) != nil {
    ////                        self.isEditTimeSlot = true
    //                        if let respArrayGetTasker = response["response"] as? [[String:Any]] {
    //                            print("respArrayTaskerDetails: \(respArrayGetTasker)")
    //                            self.AllDataDict = respArrayGetTasker
    //                            print("AllDataDict:",self.AllDataDict)
    //                            for AllData in self.AllDataDict
    //                            {
    //
    ////                                self.Task_TaskerDayTime = AllData["day_time"]as! String
    //                                self.Task_TaskerTimeSlot = AllData["timeslot"]as! String
    //
    ////                                self.Task_TaskerDate = AllData["date"]as! String
    ////                                self.convertedate = StaticUtility.convertDateFormater4(self.Task_TaskerDate)
    ////                                self.Task_TaskerDay = StaticUtility.convertDateFormater5(self.convertedate)
    //
    //                            }
    //
    //
    //                              print("Task_TaskerTimeSlot:",self.Task_TaskerTimeSlot)
    //                        }
    //                    }
    //
    //                    else
    //                    {
    //
    //                    }
    //                    // Make sure to update UI in main thread
    //                    DispatchQueue.main.async {
    //                        self.TimeSlotCollectionView.reloadData()
    //                        print("review_task_details_list_success")
    //
    //
    //
    //                    }
    //                }
    //                else
    //                {
    //                    print("Error occured") // serialized json response
    //                }
    //        }
    //
    //    }
    
    
    func add_customer_task()
    {
        if self.service_date == ""
        {
             self.ConvertedFinalServiceDate = ""
        }
        else
        {
             self.ConvertedFinalServiceDate = convertDateFormater3(self.service_date)
        }
       
        print("ConvertedFinalServiceDate: \(self.ConvertedFinalServiceDate)")
        UserDefaults.standard.set(self.service_day,forKey: "SelectedserviceDayKey")
        UserDefaults.standard.set(self.ConvertedFinalServiceDate,forKey: "SelectedServiceDateKy")
        let GetSubCategoryID = UserDefaults.standard.string(forKey: "SelectedSubCategoryID")
        self.isCalledFromSearchResult = UserDefaults.standard.bool(forKey: "isEditSearchResult")// this is how you retrieve the bool value
        print("isCalledFromSearchResult:",isCalledFromSearchResult)
        var PreviousTaskID = ""
        if self.isCalledFromSearchResult
        {
            PreviousTaskID = UserDefaults.standard.string(forKey: "PreviousTaskIDKey") ?? ""//get created task id here
        }
        
        let urlString = ConstantsClass.baseUrl + ConstantsClass.add_customer_task
        print("urlString: \(urlString)")
        
        let headers = [
            "Authorization": "\(self.authentication_key_val)"
        ]
        
        let parameters = [
            "service_date" : "\(self.service_date)",
            "service_day" : "\(self.service_day)",
            "service_time" : "\(self.GetTimeID)",
            "subcategory_id" : "\(GetSubCategoryID ?? "0")",
            "task_id":"\(PreviousTaskID ?? "")"  //add extra parameter when customer rechaed in search details page
            ] as [String : Any]
        
        print("parametersaddtask: \(parameters)")
        Helper.sharedInstance.showLoader()
        Alamofire.request(urlString
            , method: .post, parameters: parameters, encoding:JSONEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any]
                {
                    print("response: \(response)")
                    if let responseDict = response["response"]as? [String:Any]
                    {
                        self.TaskID = responseDict["task_id"] as? Int
                        self.TaskIDString = String(self.TaskID)
                        print("TaskIDString: \(self.TaskIDString)")
                        
                        self.CustomerID = responseDict["customer_id"] as? Int
                        self.CustomerIDString = String(self.CustomerID)
                        print("CustomerIDString: \(self.CustomerIDString)")
                        
                        UserDefaults.standard.set(self.TaskIDString, forKey: "TaskIDKey")
                        UserDefaults.standard.set(self.CustomerIDString, forKey: "CustomerIDKey")
                        if let status = response["status"] as? Bool {
                            if status{
                                DispatchQueue.main.async {
                                    
                                    Helper.sharedInstance.hideLoader()
                                    
                                    let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
                                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "AddNewTaskVC") as! AddNewTaskDetailsViewController
                                    
                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                    
                                    
                                }
                            }
                            else
                            {
                                self.view.makeToast("Something went wrong", duration: 3.0, position: .bottom)
                                print("Something went wrong")
                            }
                        }
                        else
                        {
                            print("Error occured") // serialized json response
                            
                        }
                    }
                    
                    
                }
        }
    }
    
    @IBAction func BtnEditDateClick(_ sender: Any) {
        print("btn edit click")
        //        self.getSelectedDate = self.formatter.string(from: date)
        //              print("getSelectedDate:",self.getSelectedDate)
        
        
        //bottom sheet implementation for calendar
        
        let controller = ServiceCalendarViewController()
        let sheetController = SheetViewController(controller: controller)
        self.present(sheetController, animated: true, completion: nil)
        
        LblTodaysDate.font = UIFont(name:"STCForward-Bold",size:14)
        EditBtnView.isHidden = false
        
    }
    
    @IBAction func TodayBtnClick(_ sender: UIButton) {
        isDateSelected = true
        sender.isSelected = true
        RadioBtnTomorrow.isSelected = false
        RadioBtnSelectDate.isSelected = false
        self.CalendarView.isHidden = true
        self.currentDateString = String(self.currentDate)
        print("currentDateString is", self.currentDateString)
        self.currentDate_Day = convertDateFormater(currentDateString)
        print("currentDate_Day is", self.currentDate_Day)
        self.service_date = self.currentDateString
        self.service_day = self.currentDate_Day
        EditBtnView.isHidden = true
        //        self.LblTodaysDate.isHidden = true
        //        self.BtnEditate.isHidden = true
        
        if (!isTimeSelected)
        {
            self.btnContinue.setTitleColor(COLOR.GrayColor, for: .normal)
            btnContinue.backgroundColor = COLOR.LightGrayColor
        }
        else
        {
            self.btnContinue.setTitleColor(COLOR.WhiteColor, for: .normal)
            btnContinue.backgroundColor = COLOR.PinkColor
        }
    }
    
    @IBAction func TommorowBtnClick(_ sender: UIButton) {
        isDateSelected = true
        sender.isSelected = true
        RadioBtnToday.isSelected = false
        RadioBtnSelectDate.isSelected = false
        self.CalendarView.isHidden = true
        self.TommarowDate = convertDateFormater3(self.TommarowDateString)
        self.LblTomarrowsDate.text = self.TommarowDate
        self.service_date = self.TommarowDateString
        self.service_day = self.TommarowDate_Day
        EditBtnView.isHidden = true
        //        self.LblTodaysDate.isHidden = true
        //        self.BtnEditate.isHidden = true
        if (!isTimeSelected)
        {
            self.btnContinue.setTitleColor(COLOR.GrayColor, for: .normal)
            btnContinue.backgroundColor = COLOR.LightGrayColor
        }
        else
        {
            self.btnContinue.setTitleColor(COLOR.WhiteColor, for: .normal)
            btnContinue.backgroundColor = COLOR.PinkColor
        }
    }
    
    @IBAction func SelectDateBtClick(_ sender: UIButton) {
        isDateSelected = true
        sender.isSelected = true
        RadioBtnToday.isSelected = false
        RadioBtnTomorrow.isSelected = false
        //bottom sheet implementation for calendar
        
        let controller = ServiceCalendarViewController()
        let sheetController = SheetViewController(controller: controller)
        self.present(sheetController, animated: true, completion: nil)
        
        LblTodaysDate.font = UIFont(name:"STCForward-Bold",size:14)
        EditBtnView.isHidden = false
        self.LblTodaysDate.isHidden = false
        if (!isTimeSelected)
        {
            self.btnContinue.setTitleColor(COLOR.GrayColor, for: .normal)
            btnContinue.backgroundColor = COLOR.LightGrayColor
        }
        else
        {
            self.btnContinue.setTitleColor(COLOR.WhiteColor, for: .normal)
            btnContinue.backgroundColor = COLOR.PinkColor
        }
    }
    
    @IBAction func BackBtnClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.tabBarController?.tabBar.isHidden = false //for now
    }
    
    @IBAction func BtnContinueClick(_ sender: Any) {
        if (RadioBtnSelectDate.isSelected && LblTodaysDate.text == "")
        {
            self.view.makeToast("Please select the date from calendar", duration: 3.0, position: .bottom)
        }
        
        else if (!isDateSelected) || (!isTimeSelected)
        {
            self.view.makeToast("Time or Date is not selected", duration: 3.0, position: .bottom)
        }
       else if(isDateSelected && isTimeSelected)
        {
            self.add_customer_task()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.TimeSlotString_Array.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = TimeSlotCollectionView.dequeueReusableCell(withReuseIdentifier: "SelectServiceTimeSlotVC", for: indexPath as IndexPath) as! SelectServiceTimeSlotTimeCollectionViewCell
        
        cell.MainView.layer.shadowColor = UIColor.gray.cgColor
        cell.MainView.layer.shadowOpacity = 0.5
        cell.MainView.layer.shadowOffset = CGSize.zero
        cell.MainView.layer.shadowRadius = 6
        cell.MainView.layer.cornerRadius = 5
        cell.clipsToBounds = false
        cell.LblTimeSlot.text = self.TimeSlotString_Array[indexPath.item]
        cell.LblDayTime.text = self.TimeSlotDayTime_Array[indexPath.item]
        cell.BtnRadio.tag = indexPath.row
        cell.BtnRadio.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        
        //        if isEditTimeSlot == true
        //        {
        //            if self.Task_TaskerTimeSlot == self.TimeSlotString_Array[indexPath.item]
        //            {
        //                cell.BtnRadio.setImage(UIImage(named: "FillRadioBtn"), for: .normal)
        //            }
        //
        //        }
        //      else
        if self.BtnSelctedTag == indexPath.item
        {
            print("Task_TaskerTimeSlotforcell:",self.Task_TaskerTimeSlot)
            
            cell.BtnRadio.setImage(UIImage(named: "FillRadioBtn"), for: .normal)
            self.GetTimeID = self.TimeSlotIDString_Array[indexPath.item]
            print("GetTimeID :\(self.GetTimeID)!")
            
            self.GetSeletedTimeSlot = self.TimeSlotString_Array[indexPath.item]
            self.GetSeletedTimeDay = self.TimeSlotDayTime_Array[indexPath.item]
            print("GetSeletedTimeSlot... :\(self.GetSeletedTimeSlot)")
            print("GetSeletedTimeDay.... :\(self.GetSeletedTimeDay)")
            UserDefaults.standard.set(self.GetSeletedTimeSlot,forKey: "SelectedTimeSlotKey")
            UserDefaults.standard.set(self.GetSeletedTimeDay,forKey: "SelectedTimeDayKey")
            
            //                let indexPath = IndexPath(item: self.BtnSelctedTag, section: 0)
            //                self.TimeSlotCollectionView.scrollToItem(at: indexPath, at: [.centeredVertically,   .centeredHorizontally], animated: false)
            
        }
        else
        {
            cell.BtnRadio.setImage(UIImage(named: "emptyRadioBtn"), for: .normal)
            
        }
        
        
        return cell
    }
    
    @objc func buttonPressed(sender:UIButton){
        isTimeSelected = true
        self.BtnSelctedTag = sender.tag
        print("Selected item in row \(sender.tag)")
        self.TimeSlotCollectionView.reloadData()
        sender.isSelected = true
        
        if (!isDateSelected)
        {
            self.btnContinue.setTitleColor(COLOR.GrayColor, for: .normal)
            btnContinue.backgroundColor = COLOR.LightGrayColor
        }
        else
        {
            self.btnContinue.setTitleColor(COLOR.WhiteColor, for: .normal)
            btnContinue.backgroundColor = COLOR.PinkColor
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("You selected cell :\(indexPath.item)!")
        isTimeSelected = true
        if let cell = TimeSlotCollectionView.cellForItem(at: indexPath) as? SelectServiceTimeSlotTimeCollectionViewCell
        {
            cell.BtnRadio.setImage(UIImage(named: "FillRadioBtn"), for: .normal)
        }
        
        self.GetTimeID = self.TimeSlotIDString_Array[indexPath.item]
        print("GetTimeID :\(self.GetTimeID)!")
        
        self.GetSeletedTimeSlot = self.TimeSlotString_Array[indexPath.item]
        self.GetSeletedTimeDay = self.TimeSlotDayTime_Array[indexPath.item]
        print("GetSeletedTimeSlot :\(self.GetSeletedTimeSlot)")
        print("GetSeletedTimeDay :\(self.GetSeletedTimeDay)")
        UserDefaults.standard.set(self.GetSeletedTimeSlot,forKey: "SelectedTimeSlotKey")
        UserDefaults.standard.set(self.GetSeletedTimeDay,forKey: "SelectedTimeDayKey")
        
        if (!isDateSelected)
        {
            self.btnContinue.setTitleColor(COLOR.GrayColor, for: .normal)
            btnContinue.backgroundColor = COLOR.LightGrayColor
        }
        else
        {
            self.btnContinue.setTitleColor(COLOR.WhiteColor, for: .normal)
            btnContinue.backgroundColor = COLOR.PinkColor
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if let cell = TimeSlotCollectionView.cellForItem(at: indexPath) as? SelectServiceTimeSlotTimeCollectionViewCell
        {
            cell.BtnRadio.setImage(UIImage(named: "emptyRadioBtn"), for: .normal)
        }
        print("You disselected cell :\(indexPath.item)!")
        
        
    }
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        self.getSelectedDate = self.formatter.string(from: date)
        self.LblTodaysDate.text = self.getSelectedDate
        
        self.ConvertedgetSelectedDate = convertDateFormater1(self.getSelectedDate)
        self.getSelectedDate_Day = convertDateFormater(self.ConvertedgetSelectedDate)
        
        print("getSelectedDate:",self.getSelectedDate)
        print("ConvertedgetSelectedDate is", self.ConvertedgetSelectedDate)
        print("getSelectedDate_Day:",self.getSelectedDate_Day)
        
        LblTodaysDate.font = UIFont(name:"STCForward-Bold",size:14)
        self.CalendarView.isHidden = true
        self.LblTodaysDate.isHidden = false
        //self.BtnEditate.isHidden = false
        self.service_date = self.ConvertedgetSelectedDate
        self.service_day = self.getSelectedDate_Day
        self.configureVisibleCells()
    }
    
    private func configureVisibleCells() {
        CalendarView.visibleCells().forEach { (cell) in
            let date = CalendarView.date(for: cell)
            let position = CalendarView.monthPosition(for: cell)
            //            self.configure(cell: cell, for: date!, at: position)
        }
    }
    
    //disable past date function
    
    func minimumDate(for calendar: FSCalendar) -> Date
    {
        return Date()
    }
    
    func switchDateComponent(component: Calendar.Component, isNextDirection: Bool) {
        if let nextDate = Calendar.current.date(byAdding: component, value: isNextDirection ? -1 : 1, to: CalendarView.selectedDate ?? Date()) {
            CalendarView.select(nextDate, scrollToDate: true)
            self.TommarowDate = self.convertToString1(date: nextDate, dateformat:"EEEE, d MMM yyyy")
            
            //            self.ConvertedTommarowDate = convertDateFormater1(self.TommarowDate)
            //            self.TommarowDate_Day = convertDateFormater(self.ConvertedTommarowDate)
            
            print("TommarowDate is", self.TommarowDate)
            //            print("ConvertedTommarowDate is", self.ConvertedTommarowDate)
            //            print("TommarowDate_Day:",self.TommarowDate_Day)
            
        }
    }
    
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "EEEE"
        
        return dateFormatter.string(from: date!)
        
    }
    
    func convertDateFormater1(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, d MMM yyyy"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        return dateFormatter.string(from: date!)
        
    }
    func convertDateFormater2(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "EEEE, d MMM yyyy"
        
        return dateFormatter.string(from: date!)
        
    }
    func convertDateFormater3(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "EEEE, d MMM yyyy"
        
        return dateFormatter.string(from: date!)
        
    }
    func convertToString1(date: Date, dateformat formatType: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"// Your New Date format as per requirement change it own
        
        let newDate: String = dateFormatter.string(from: date) // pass Date here
        //        print("newDate:",newDate) // New formatted Date string
        
        return newDate
    }
    
    
}
