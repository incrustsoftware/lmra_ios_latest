//
//  SelectServiceTimeSlotTimeCollectionViewCell.swift
//  LMRA
//
//  Created by Mac on 18/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class SelectServiceTimeSlotTimeCollectionViewCell: UICollectionViewCell {
  
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var LblTimeSlot: UILabel!
    @IBOutlet weak var LblDayTime: UILabel!
    @IBOutlet weak var BtnRadio: UIButton!
    
 
}
