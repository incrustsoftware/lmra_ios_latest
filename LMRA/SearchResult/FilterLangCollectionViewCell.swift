//
//  FilterLangCollectionViewCell.swift
//  LMRA
//
//  Created by Mac on 30/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class FilterLangCollectionViewCell: UICollectionViewCell {
    
   
     @IBOutlet weak var FilterInnerLangView: UIView!
     @IBOutlet weak var FilterSelectedLangLbl: UILabel!
    
}
