//
//  SearchResultTableViewCell.swift
//  LMRA
//
//  Created by Mac on 19/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class SearchResultTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var View3: UIView!
    @IBOutlet weak var View2: UIView!
    @IBOutlet weak var View1: UIView!
    @IBOutlet weak var Mainview: UIView!
    @IBOutlet weak var ImgCustomer: UIImageView!
    @IBOutlet weak var LblCustomerName: UILabel!
    @IBOutlet weak var LblSubName: UILabel!
    @IBOutlet weak var LblCustomerCompletion: UILabel!
    
    @IBOutlet weak var LblTaskerCategory1: UILabel!
    @IBOutlet weak var LblTaskerCategory2: UILabel!
    @IBOutlet weak var LblTaskerCategory3: UILabel!
    
    @IBOutlet weak var LblTaskerRate1: UILabel!
    @IBOutlet weak var LblTaskerRate2: UILabel!
    @IBOutlet weak var LblTaskerRate3: UILabel!
    
    var lang_NameArray = [String]()
    var lang_IdArray = [String]()
    var subcategory_NameArray = [String]()
    var subcategory_IdArray = [String]()
    var subcategory_RateArray = [String]()
    var AllSubCategories = ""
    var TaskerCompletionCount = ""
    var AmountType = ""
    var TaskerId = ""
   
    override func awakeFromNib() {
        super.awakeFromNib()
        
        Mainview.layer.shadowColor = UIColor.gray.cgColor
        Mainview.layer.shadowOpacity = 0.5
        Mainview.layer.shadowOffset = CGSize.zero
        Mainview.layer.shadowRadius = 6
        Mainview.layer.cornerRadius = 5
        Mainview.clipsToBounds = false
        
        ImgCustomer.layer.cornerRadius = ImgCustomer.frame.size.width/2
        ImgCustomer.clipsToBounds = true
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
  
    
    func bindCelldata(AllDataDict:[String:Any]){
        
//        self.lang_NameArray.removeAll()
//        self.lang_IdArray.removeAll()
//        self.subcategory_NameArray.removeAll()
//        self.subcategory_RateArray.removeAll()
//        self.subcategory_IdArray.removeAll()
//        self.LblCustomerName.text = ""
//        self.TaskerCompletionCount = ""
//        self.AmountType = ""
//        self.AllSubCategories = ""
//        self.ImgCustomer.image = nil

        
        let url = AllDataDict["tasker_user_profile"] as? String ?? ""
        print("url.....:\(url)")
        UserDefaults.standard.set(url, forKey:"SelectedTaskerImageKey")
        self.ImgCustomer?.downloaded(from: url)
        self.ImgCustomer.contentMode = .scaleAspectFill //3
        let TaskerName = AllDataDict["tasker_name"] as? String ?? ""
        let TaskerFamilyName = AllDataDict["tasker_family_name"] as? String ?? ""
        self.LblCustomerName.text = TaskerName + " " + TaskerFamilyName
        self.TaskerCompletionCount = AllDataDict["tasker_completed_tasks_count"] as? String ?? "0"
        self.LblCustomerCompletion.text = self.TaskerCompletionCount + " completed tasks"
        self.TaskerId = AllDataDict["tasker_id"] as? String ?? ""
       
         print("urTaskerIdl.....:",self.TaskerId)
        
        if let tasker_Rate = AllDataDict["tasker_rate"]as? [[String:Any]]
        {
            self.subcategory_RateArray.removeAll()
            for rateData in tasker_Rate
            {
                
                self.AmountType = rateData["rate"] as? String ?? ""
                self.subcategory_RateArray.append(self.AmountType)
               
            }
            print ("AmountType.....: \(self.AmountType)")
            print ("subcategory_RateArray.....: \(self.subcategory_RateArray)")
            
        }
        
        if let tasker_Languages = AllDataDict["tasker_languages"]as? [[String:Any]]
        {
            for lang  in tasker_Languages
            {
                self.lang_NameArray.append(lang["language_name"] as? String ?? "")
                self.lang_IdArray.append(lang["language_id"] as? String ?? "")
            }
        }
        if let tasker_Subcategories = AllDataDict["tasker_subcategories"]as? [[String:Any]]
        {
            self.subcategory_NameArray.removeAll()
            self.AllSubCategories = ""
            for subcategory  in tasker_Subcategories
            {
                
//                print("subcategory:\(subcategory)")
                
                self.subcategory_NameArray.append(subcategory["subcategory_name"] as? String ?? "")
                self.subcategory_IdArray.append(subcategory["subcategory_id"] as? String ?? "")
                self.AllSubCategories =  "\(self.AllSubCategories) \(subcategory["subcategory_name"] as? String ?? "") ,"
                
                var All_Subcatgory = self.AllSubCategories
                All_Subcatgory.removeLast()
                
            }
            print ("subcategory_NameArray.....: \(self.subcategory_NameArray)")
            print ("subcategory_NameArraycount.....: \(self.subcategory_NameArray.count)")
            print ("AllSubCategories.....: \(self.AllSubCategories)")
        }
        
        //implementation for rate wise display of data
        if (self.subcategory_RateArray.count > 0) && (self.subcategory_NameArray.count > 0)
        {
            if self.subcategory_RateArray.count == 1 && self.subcategory_NameArray.count == 1
            {
                self.LblTaskerRate1.text = "BHD " + self.subcategory_RateArray[0]
                self.LblTaskerCategory1.text = self.subcategory_NameArray[0]
                self.View1.isHidden = false
                self.View2.isHidden = true
                self.View3.isHidden = true
            }
            else if self.subcategory_RateArray.count == 2 && self.subcategory_NameArray.count == 2
            {
                if (self.subcategory_RateArray[0] == self.subcategory_RateArray[1])
                {
                    self.LblTaskerRate1.text = "BHD " + self.subcategory_RateArray[0]
                    self.LblTaskerCategory1.text = self.subcategory_NameArray[0] + "," + self.subcategory_NameArray[1]
                    self.View1.isHidden = false
                    self.View2.isHidden = true
                    self.View3.isHidden = true
                }
                else
                {
                    self.LblTaskerRate1.text = "BHD " + self.subcategory_RateArray[0]
                    self.LblTaskerRate2.text = "BHD " + self.subcategory_RateArray[1]
                    self.LblTaskerCategory1.text = self.subcategory_NameArray[0]
                    self.LblTaskerCategory2.text = self.subcategory_NameArray[1]
                    self.View1.isHidden = false
                    self.View2.isHidden = false
                    self.View3.isHidden = true
                }
            }
            else if self.subcategory_RateArray.count == 3 && self.subcategory_NameArray.count == 3
            {
                if (self.subcategory_RateArray[0] == self.subcategory_RateArray[1]) && (self.subcategory_RateArray[1] == self.subcategory_RateArray[2]) && (self.subcategory_RateArray[0] == self.subcategory_RateArray[2])
                {
                    self.LblTaskerRate1.text = "BHD " + self.subcategory_RateArray[0]
                    self.LblTaskerCategory1.text = self.subcategory_NameArray[0] + "," + self.subcategory_NameArray[1] + "," + self.subcategory_NameArray[2]
                    self.View1.isHidden = false
                    self.View2.isHidden = true
                    self.View3.isHidden = true
                }
                else
                {
                    self.LblTaskerRate1.text = "BHD " + self.subcategory_RateArray[0]
                    self.LblTaskerRate2.text = "BHD " + self.subcategory_RateArray[1]
                    self.LblTaskerRate3.text = "BHD " + self.subcategory_RateArray[2]
                    self.LblTaskerCategory1.text = self.subcategory_NameArray[0]
                    self.LblTaskerCategory2.text = self.subcategory_NameArray[1]
                    self.LblTaskerCategory3.text = self.subcategory_NameArray[2]
                    self.View1.isHidden = false
                    self.View2.isHidden = false
                    self.View3.isHidden = false
                }
            }
        }
        else if self.subcategory_RateArray.count > 0
        {
            if self.subcategory_RateArray.count == 1
            {
                self.LblTaskerRate1.text = "BHD " + self.subcategory_RateArray[0]
                self.LblTaskerCategory1.text = ""
                self.View1.isHidden = false
                self.View2.isHidden = true
                self.View3.isHidden = true
            }
            else if self.subcategory_RateArray.count == 2
            {
                if (self.subcategory_RateArray[0] == self.subcategory_RateArray[1])
                {
                    self.LblTaskerRate1.text = "BHD " + self.subcategory_RateArray[0]
                    self.LblTaskerCategory1.text = ""
                    self.View1.isHidden = false
                    self.View2.isHidden = true
                    self.View3.isHidden = true
                }
                else
                {
                    self.LblTaskerRate1.text = "BHD " + self.subcategory_RateArray[0]
                    self.LblTaskerRate2.text = "BHD " + self.subcategory_RateArray[1]
                    self.LblTaskerCategory1.text = ""
                    self.LblTaskerCategory2.text = ""
                    self.View1.isHidden = false
                    self.View2.isHidden = false
                    self.View3.isHidden = true
                }
            }
            else if self.subcategory_RateArray.count == 3
            {
                if (self.subcategory_RateArray[0] == self.subcategory_RateArray[1]) && (self.subcategory_RateArray[1] == self.subcategory_RateArray[2]) && (self.subcategory_RateArray[0] == self.subcategory_RateArray[2])
                {
                    self.LblTaskerRate1.text = "BHD " + self.subcategory_RateArray[0]
                    self.LblTaskerCategory1.text = ""
                    self.View1.isHidden = false
                    self.View2.isHidden = true
                    self.View3.isHidden = true
                }
                else
                {
                    self.LblTaskerRate1.text = "BHD " + self.subcategory_RateArray[0]
                    self.LblTaskerRate2.text = "BHD " + self.subcategory_RateArray[1]
                    self.LblTaskerRate3.text = "BHD " + self.subcategory_RateArray[2]
                    self.LblTaskerCategory1.text = ""
                    self.LblTaskerCategory2.text = ""
                    self.LblTaskerCategory3.text = ""
                    self.View1.isHidden = false
                    self.View2.isHidden = false
                    self.View3.isHidden = false
                }
            }
        }
        else if self.subcategory_NameArray.count > 0
        {
            if self.subcategory_NameArray.count == 1
            {
                self.LblTaskerRate1.text = "BHD"
                self.LblTaskerCategory1.text = self.subcategory_NameArray[0]
                self.View1.isHidden = false
                self.View2.isHidden = true
                self.View3.isHidden = true
            }
            else if self.subcategory_NameArray.count == 2
            {
                self.LblTaskerRate1.text = "BHD"
                self.LblTaskerCategory1.text = self.subcategory_NameArray[0] + "," + self.subcategory_NameArray[1]
                self.View1.isHidden = false
                self.View2.isHidden = true
                self.View3.isHidden = true
            }
            else if self.subcategory_NameArray.count == 3
            {
                self.LblTaskerRate1.text = "BHD"
                self.LblTaskerCategory1.text = self.subcategory_NameArray[0] + "," + self.subcategory_NameArray[1] + "," + self.subcategory_NameArray[2]
                self.View1.isHidden = false
                self.View2.isHidden = true
                self.View3.isHidden = true
            }
        }
    }
}


    

