//
//  SearchResultViewController.swift
//  LMRA
//
//  Created by Mac on 19/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire
import FittedSheets

class SearchResultViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate {
    
    @IBOutlet weak var LblTaskDay: UILabel!
    
    @IBOutlet weak var BtnEditSearch: UIButton!
    @IBOutlet weak var LblTaskTime: UILabel!
    @IBOutlet weak var BtnBack: UIButton!
    @IBOutlet weak var LblMainSearchResult: UILabel!
    @IBOutlet weak var SearchResultView: UIView!
    
    @IBOutlet weak var SearchTableView: UITableView!
    //variabeele for filter data
    var GetRateVal = ""
    var GetSliderMinValInt:Int!
    var GetSliderResultValInt:Int!
    var GetNationaliyVal = ""
    var GetLangIndexArrayVal = [String] ()
    var GetLangNameArrayVal = [String] ()
    var TaskerCompletionCount = ""
    //pagination variable
    var currentPage : Int = 1
    var TotalTaskerCount:Int!
    var LastIndexpath:Int!
    var isLoadingList : Bool = false
    var IsCalledFromInviteTasker:Bool = false
    ///////////////////////
    var authentication_key_val = ""
    var GetTaskID = ""
    var GetCustomerID = ""
    var SelectedGetAddressId = ""
    var SelectedTaskTime = ""
    var SelectedTaskDay = ""
    var SelectedTaskDate = ""
    var SelectedTimeSlot = ""
    var GetSelectedTaskDate = ""
    var TaskerID = ""
    var GetTaskerID = ""
    var Tasker_Name_Array = [String] ()
    var Tasker_Subcategory_NameArray = [[String:Any]]()
    var Tasker_Subcategory_IDArray = [[String:Any]]()
    var GetSelectedLangIDArray2 = [String] ()
    var GetSelectedLangIDArray = [String] ()
    
    var AllDataDict = [[String:Any]]()
    var Tasker_Subcategories = [[String:Any]]()
    var WhiteColor = UIColor(red:255, green:255, blue:255, alpha:1.0)
    var GrayColor = UIColor(red:0.142, green:0.154, blue:0.160, alpha:1.0)
    var lastContentOffset: CGFloat = 0
    var GetIsFromFilterResult:Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        SearchTableView.delegate = self
        SearchTableView.dataSource = self
 
        
        self.authentication_key_val = UserDefaults.standard.string(forKey: "authentication_key") ?? ""//get
        print("authentication_key_val :\(String(describing: self.authentication_key_val))") //get
        self.GetTaskID = UserDefaults.standard.string(forKey: "TaskIDKey") ?? ""//get
        self.GetCustomerID = UserDefaults.standard.string(forKey: "CustomerIDKey") ?? ""//get
        print("GetTaskIDSearchResult: \(self.GetTaskID)")
        self.SelectedTaskTime = UserDefaults.standard.string(forKey: "TaskTimeKey") ?? ""//get
        self.SelectedTaskDay = UserDefaults.standard.string(forKey: "TaskDayKey") ?? ""//get
        self.GetSelectedTaskDate = UserDefaults.standard.string(forKey: "TaskDateKey") ?? ""//get
        self.SelectedTimeSlot = UserDefaults.standard.string(forKey: "SelectedTimeSlotKey") ?? ""//get
        self.SelectedGetAddressId = UserDefaults.standard.string(forKey: "SelectedGetAddressIdKey") ?? ""//get
        
        self.LblTaskDay.text = self.SelectedTaskDay
        self.LblTaskTime.text = self.SelectedTimeSlot
        
        self.GetSelectedLangIDArray = UserDefaults.standard.object(forKey:"SelectedGetLangIDArrayKey" ) as? [String] ?? [String]()
        print("GetSelectedLangIDArray :\(String(describing: self.GetSelectedLangIDArray))") //get
        
        self.GetSelectedLangIDArray2 = self.GetSelectedLangIDArray
        //call search first time
        self.get_tasker_by_search()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
        NotificationCenter.default.addObserver(self, selector: #selector(self.SelectedDataFromFilter), name: NSNotification.Name(rawValue: "SelectedFilterData"), object: nil)
        
    }
    
    
    @objc func SelectedDataFromFilter(_ notification: NSNotification) {
        self.GetRateVal = notification.userInfo?["SelectedRateValue"] as? String ?? ""
        self.GetSliderMinValInt = notification.userInfo?["SelectedSliderMinValue"] as? Int ?? 0
        self.GetSliderResultValInt = notification.userInfo?["SliderResultValue"] as? Int ?? 0
        self.GetNationaliyVal = notification.userInfo?["SelectedNationalityValue"] as? String ?? ""
        self.GetLangIndexArrayVal = notification.userInfo?["SelectedLangIndexArrayValue"] as? [String] ?? [""]
        self.GetLangNameArrayVal = notification.userInfo?["SelectedLangNameArrayValue"] as? [String] ?? [""]
        self.GetSelectedLangIDArray = notification.userInfo?["SelectedLangIDArrayValue"] as? [String] ?? [""]
        self.GetIsFromFilterResult = notification.userInfo?["IsfromFliterResultValue"] as? Bool ?? false
        
        print("GetRateVal:",GetRateVal)
        print("GetSliderMinVal:",GetSliderMinValInt)
        print("GetSliderResultVal:",GetSliderResultValInt)
        print("GetNationaliyVal:",GetNationaliyVal)
        print("GetLangIndexArrayVal:",GetLangIndexArrayVal)
        print("GetLangNameArrayVal:",GetLangNameArrayVal)
        print("GetSelectedLangIDArray:",GetSelectedLangIDArray)
        print("GetIsFromFilterResult...:",GetIsFromFilterResult)
        //call filter first and then insert search data
        self.AllDataDict.removeAll()
         print("Alldata....:",self.AllDataDict.count)
        self.get_tasker_by_filter()
        
    }
    
        func getListFromServer(_ pageNumber: Int){
            self.isLoadingList = false
            self.SearchTableView.reloadData()
        }

        func loadMoreItemsForList(){
            currentPage += 1
            print("currentPage:",currentPage)
//            self.get_tasker_by_search()
            
             print("GetIsFromFilterResultload:",GetIsFromFilterResult)
            if self.GetIsFromFilterResult
            {
                self.get_tasker_by_filter()
            }
            else
            {
                self.get_tasker_by_search()
            }
    
        }
    


    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) && !isLoadingList){
            if self.AllDataDict.count < self.TotalTaskerCount
            {
            self.isLoadingList = true
            self.loadMoreItemsForList()
            }

        }
    }


    
    
    func get_tasker_by_search()
    {
        
        let urlString = ConstantsClass.baseUrl + ConstantsClass.get_tasker_by_search
        print("urlString: \(urlString)")
        
        let headers = [
            "Authorization": "\(self.authentication_key_val)"
        ]
        print("headers: \(headers)")
        
        let parameters = [
            "task_id" : "\(self.GetTaskID)",
            "task_date" : "\(self.GetSelectedTaskDate)",
            "task_time" : "\(self.SelectedTaskTime)",
            "task_day" : "\(self.SelectedTaskDay)",
            "task_address" : "\(self.SelectedGetAddressId)",
            "task_languages" :(self.GetSelectedLangIDArray2),
            "page":"\(self.currentPage)"
            
            ] as [String : Any]
        print("parametersaddSearchTasker: \(parameters)")
        Helper.sharedInstance.showLoader()
        Alamofire.request(urlString
            , method: .post, parameters: parameters, encoding:JSONEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    self.TotalTaskerCount = response["total_tasker_count"] as? Int ?? 0
//                    print("response:",self.TotalTaskerCount ?? 0)
                    
                    if let status = response["status"] as? Bool {
                        print("Get status true here")
                        
                        if status
                        {
                            if let respArray = response["response"] as? [[String:Any]] {
                                print("respArraySearchTasker: \(respArray)")
                                
                                self.AllDataDict.append(contentsOf: respArray)
                              
                                
                            }
                        }
                        else
                        {
                            self.SearchTableView.isHidden = true
                        }
                        
                    }
                    
                    
                    // Make sure to update UI in main thread
                    DispatchQueue.main.async {
                        Helper.sharedInstance.hideLoader()
                        self.SearchTableView.reloadData()
                        print("Tasker Search successfully.")
                    }
                }
                else
                {
                    Helper.sharedInstance.hideLoader()
                    print("Error occured") // serialized json response
                }
        }
    }
    
    
    func get_tasker_by_filter()
    {
        print("currentPage......:",currentPage)
        let urlString = ConstantsClass.baseUrl + ConstantsClass.get_tasker_by_filter
        print("urlString: \(urlString)")
        
        let headers = [
            "Authorization": "\(self.authentication_key_val)"
        ]
        print("headers: \(headers)")
        
        print("GetRateVal:",GetRateVal)
        print("GetSliderMinVal:",GetSliderMinValInt)
        print("GetSliderResultVal:",GetSliderResultValInt)
        print("GetNationaliyVal:",GetNationaliyVal)
        print("GetLangIndexArrayVal:",GetLangIndexArrayVal)
        print("GetLangNameArrayVal:",GetLangNameArrayVal)
        print("GetSelectedLangIDArray:",GetSelectedLangIDArray)
        
        
        let parameters = [
            "tasker_available_rate" : "\(self.GetRateVal)",
            "tasker_rates_min" : (self.GetSliderMinValInt),
            "tasker_rates_max" : (self.GetSliderResultValInt),
            "tasker_languages" : (self.GetSelectedLangIDArray),
            "tasker_nationality" :"\(self.GetNationaliyVal)",
            "page":"\(self.currentPage)"
            
            ] as [String : Any]
        print("parametersFilterTasker: \(parameters)")
        Helper.sharedInstance.showLoader()
        Alamofire.request(urlString
            , method: .post, parameters: parameters, encoding:JSONEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if let status = response["status"] as? Bool {
                        print("Get status true here")
                        
                        if status
                        {
                            if let respArrayTaskerFilter = response["response"] as? [[String:Any]] {
                                print("respArrayTaskerFilter: \(respArrayTaskerFilter)")
                                
                                self.AllDataDict.append(contentsOf: respArrayTaskerFilter)
                                Helper.sharedInstance.hideLoader()
                            }
                        }
                        //                    else
                        //                    {
                        //                        Helper.sharedInstance.hideLoader()
                        //                         self.SearchTableView.isHidden = true
                        //                    }
                    }
                    // Make sure to update UI in main thread
                    DispatchQueue.main.async {
                        Helper.sharedInstance.hideLoader()
                        self.SearchTableView.reloadData()
                        print("Filter Tasker successfully.")
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("Alldata:",self.AllDataDict.count)
        return self.AllDataDict.count
       
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = SearchTableView.dequeueReusableCell(withIdentifier: "SearchResultCell", for: indexPath as IndexPath) as! SearchResultTableViewCell
        
        cell.bindCelldata(AllDataDict: self.AllDataDict[indexPath.row])
         let DisplayData = indexPath.row + 1
        let defaults = UserDefaults.standard
        defaults.setValue(DisplayData, forKey: "DisplayData") //set
        print("DisplayData:",DisplayData)
       
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = SearchTableView.dequeueReusableCell(withIdentifier: "SearchResultCell", for: indexPath as IndexPath) as! SearchResultTableViewCell
        
        cell.bindCelldata(AllDataDict: self.AllDataDict[indexPath.row])
        
        print("cell.GetTaskerID:",cell.TaskerId)
        UserDefaults.standard.set(cell.TaskerId, forKey: "SelectedTaskerID")
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TaskerProfileDetailsVC") as! TaskerProfileDetailsViewController
        
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    

    
    
    @IBAction func BackBtnClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func EditBtnClick(_ sender: Any) {
        UserDefaults.standard.set(true,forKey: "isEditSearchResult")
        UserDefaults.standard.set(self.GetTaskID,forKey: "PreviousTaskIDKey")
        self.navigationController?.backToViewController(viewController: ServicesTimeSlotViewController.self)//pop to specifica view controller
        if IsCalledFromInviteTasker
        {
            let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ServiceTimetVC") as! ServicesTimeSlotViewController
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        
    }
    
    @IBAction func BtnFilterClick(_ sender: Any) {
        print("click on filter button")
  
        let myViewController = FilterResultViewController(nibName: "FilterResultViewController", bundle: nil)
        self.present(myViewController, animated: true, completion: nil)
        
    

    }
   
}


