//
//  FilterNationalityCollectionViewCell.swift
//  LMRA
//
//  Created by Mac on 30/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class FilterNationalityCollectionViewCell: UICollectionViewCell {
  
       @IBOutlet weak var FilterInnerNationalityView: UIView!
       @IBOutlet weak var FilterSelectedNationalityLbl: UILabel!

}
