//
//  UIViewExtension.swift
//  LMRA
//
//  Created by Mac on 16/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class CardView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 2
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 1
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.5
    
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
    }
}

@IBDesignable extension UIButton {
    
    @IBInspectable var borderWidth1: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var cornerRadius1: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var borderColor1: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}


extension String {
    
    func toDate(withFormat format: String)-> Date?{
        let dateFormatter = DateFormatter()
        //dateFormatter.timeZone = TimeZone(identifier: "Asia/Tehran")
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: self)
        return date
    }
}

extension Date {
    func toString(withFormat format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        //dateFormatter.timeZone = TimeZone(identifier: "Asia/Tehran")
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = format
        let str = dateFormatter.string(from: self)
        return str
    }
}

extension NSObject {
    func getcurrentControllerview() -> UIView {
        guard var topController = UIApplication.shared.keyWindow?.rootViewController else {
            // topController should now be your topmost view controller
            return UIView()
        }
        
        while let presentedViewController = topController.presentedViewController {
            topController = presentedViewController
        }
        return (topController as UIViewController).view
    }
}

extension UIView {
    func setCardView(cornerRadius:CGFloat,shadowColor:UIColor,shadowOffsetHeight:Int,shadowOffsetWidth:Int,shadowOpacity:Float) {
        self.layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: cornerRadius)
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        self.layer.shadowOpacity = shadowOpacity
        self.layer.shadowPath = shadowPath.cgPath
    }
}
extension UIViewController {
    func setStatusBarColor(color:UIColor,styleForStatus:UIStatusBarStyle) {
        //navigationController?.navigationBar.barStyle = .blackTranslucent
        if #available(iOS 13.0, *) {
            let statusBar1 =  UIView()
            statusBar1.frame = UIApplication.shared.keyWindow?.windowScene?.statusBarManager!.statusBarFrame as! CGRect
            statusBar1.backgroundColor = color
            UIApplication.shared.statusBarStyle = styleForStatus
            UIApplication.shared.keyWindow?.addSubview(statusBar1)
        }
        else {
            let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView
            if statusBar?.responds(to: #selector(setter: UIView.backgroundColor)) ?? false {
                statusBar?.backgroundColor = color
                UIApplication.shared.statusBarStyle = styleForStatus
            }
        }
        
        //UIApplication.shared.statusBarStyle = styleForStatus
        //setNeedsStatusBarAppearanceUpdate()
    }
    //        override var preferredStatusBarStyle: UIStatusBarStyle {
    //            .default
    //        }
    //    var className = NSStringFromClass(self.classForCoder)
    //    class className {
    //        override var preferredStatusBarStyle : UIStatusBarStyle {
    //            return .lightContent
    //        }
    //    }
}
//extension UIApplicationDelegate {
//    static var shared: Self {
//        return UIApplication.shared.delegate! as! Self
//    }
//}
