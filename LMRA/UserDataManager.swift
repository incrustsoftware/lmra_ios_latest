//
//  UserDefaultsUtil.swift
//  LMRA
//
//  Created by Mac on 14/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit


struct UserDataManager_KEY {
    static let isUSERLoggedIN = "isUSERLoggedIN"
    static let userDetails = "userDetails"
    static let userType = "userType"
    static let authentication_key = "authentication_key"
}

class UserDataManager: NSObject {
    
    //USER Type
    enum UserType:Int {
        case CUSTOMER = 1
        case TASKER = 2
        case NA = 0
    }
    
    struct NotificationObserverKey {
        static let ACCEPT_SUCCESS_TASK = "acceptedSuccessTask"
        static let CANCEL_TASK_INVITATION_SUCCESS = "cancel_task_Invitation"
    }
    
    //MARK:- Shared Property
    static let shared = UserDataManager()

    //MARK:- Properties
    private var userLoggedIn    : Bool?
    private var token           : String?
    private var user_name       : String?
    private var family_name     : String?
    private var user_Type       : Int?
    
    
    //MARK:- Life Cycle Methods
    private override init() {
        super.init()
        self.userLoggedIn = UserDefaults.standard.bool(forKey: UserDataManager_KEY.isUSERLoggedIN)
        //User Login Data
        let user = UserDefaults.standard.value(forKey: UserDataManager_KEY.userDetails)
        if user != nil{
            do {
                let loginModelData = try JSONDecoder().decode(Verify_OTP_DataModel.self, from: user as! Data)
                mapUserDetails(userDetails: loginModelData.response)
            }
            catch{}
        }
        
        //User Type
        let userType = UserDefaults.standard.value(forKey: UserDataManager_KEY.userType)
        self.user_Type = userType as? Int
    }
    
    //MARK:- Set & Clear Data Methods
    private func mapUserDetails(userDetails:Verify_OTP_ResponseModel){
        self.token          = userDetails.authentication_key
        self.user_name      = userDetails.name
        
        UserDefaults.standard.set(self.token, forKey: UserDataManager_KEY.authentication_key)
        UserDefaults.standard.synchronize()
    }
    
    //MARK:- Public setters
    public func clearUserDetails(){
        self.userLoggedIn   = false
        self.token          = nil
        self.user_Type      = UserType.NA.rawValue
        self.user_name      = nil
        self.family_name    = nil
        
        UserDefaults.standard.removeObject(forKey: UserDataManager_KEY.isUSERLoggedIN)
        UserDefaults.standard.removeObject(forKey: UserDataManager_KEY.authentication_key)
        UserDefaults.standard.synchronize()
    }
    
    public func isUserLoggedIn() -> Bool{
        return self.userLoggedIn!
    }
    
    public func setTaskerOrCustomer(userType:UserType) {
        self.user_Type = userType.rawValue
        UserDefaults.standard.set(userType.rawValue, forKey: UserDataManager_KEY.userType)
    }
    
    public func getUserType()->Int {
        return self.user_Type ?? 0
    }
    
    public func getAccesToken()->String {
        if let token = self.token {
            return token
        }
        else {
            return ""
        }
    }
    
    //Called every time user data refreshes :- like profile update,login,registration
    public func setUserDetails(userDetails:Data){
        self.userLoggedIn = true
        UserDefaults.standard.setValue(userLoggedIn, forKey: UserDataManager_KEY.isUSERLoggedIN)
        
        do {
            let loginDataModel = try JSONDecoder().decode(Verify_OTP_DataModel.self, from: userDetails)
            self.mapUserDetails(userDetails: loginDataModel.response)
            UserDefaults.standard.setValue(userDetails, forKey: UserDataManager_KEY.userDetails)
            UserDefaults.standard.synchronize()
        }
        catch {
            print(error)
        }
        
        
//        do {
//            //Model to JsonData
//            let jsonBody = try JSONEncoder().encode(LoginDataModel(status: 1, response: LoginResponse(cell_phone_number: "String", email_id: "", family_name: "", identity_card_number: 1, name: "", nationality: "", otp: 1, token: "", user_type: 1), code: 1, message_code: ""))
//
//            //From JsonData to Model
//            let jsonData = try JSONDecoder().decode(LoginDataModel.self, from: jsonBody)
//        }
//        catch {
//
//        }
        
    }
    
    func change(authKey:String,userType:Int) {
        let user = UserDefaults.standard.value(forKey: UserDataManager_KEY.userDetails)
        if user != nil{
            do {
                var loginModelData = try JSONDecoder().decode(Verify_OTP_DataModel.self, from: user as! Data)
                loginModelData.response.authentication_key = authKey
                
                let jsonBody = try JSONEncoder().encode(Verify_OTP_DataModel.init(status: loginModelData.status, response: loginModelData.response, code: loginModelData.code, message_code: loginModelData.message_code))
                self.setTaskerOrCustomer(userType: UserDataManager.UserType(rawValue: userType)!)
                self.setUserDetails(userDetails: jsonBody)
            }
            catch{}
        }
    }
    
    
    public func getUserDetails(userDetails:Data)->Login_DataModel{
        let user = UserDefaults.standard.value(forKey: UserDataManager_KEY.userDetails)
        if user != nil{
            do {
                let loginModelData = try JSONDecoder().decode(Login_DataModel.self, from: user as! Data)
                return loginModelData
            }
            catch{
                return Login_DataModel()
            }
        }
        else {
            return Login_DataModel()
        }
    }

    public func getUserName() -> String{
        if let user_name = self.user_name{
            return user_name
        }
        return ""
    }
}

