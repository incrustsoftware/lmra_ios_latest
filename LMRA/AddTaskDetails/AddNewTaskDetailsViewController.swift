//
//  AddNewTaskDetailsViewController.swift
//  LMRA
//
//  Created by Mac on 19/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire

class AddNewTaskDetailsViewController: UIViewController,UITextFieldDelegate ,UITextViewDelegate,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var Scrollview: UIScrollView!
    @IBOutlet weak var BtnBack: UIButton!
    @IBOutlet weak var TFTaskTitleHt: NSLayoutConstraint!
    @IBOutlet weak var LblTitle: UILabel!
    @IBOutlet weak var BtnCancel: UIButton!
    @IBOutlet weak var TitleView: UIView!
    @IBOutlet weak var LblInnerTaskTitle: UILabel!
    @IBOutlet weak var LblMainTitle: UILabel!
    @IBOutlet weak var Tftasktitle: UITextField!
    @IBOutlet weak var DescriptionTextview: UITextView!
    @IBOutlet weak var BtnContinue: UIButton!
    @IBOutlet weak var LangTableView: UITableView!
    
    
    var authentication_key_val = ""
    var Language_Name = ""
    var Language_Id:Int!
    var LanguageSelectedID:Int!
    var Language_NameString_Array = [String] ()
    var Language_TagArray = [Int] ()
    var GetTaskTitle = ""
    var GetTaskDescription = ""
    var GetTaskID = ""
    var GetCustomerID = ""
    var LangIDString = ""
    var LangPriority = ""
    var Task_TaskerLang = ""
    var Task_TaskerLangAray = [String] ()
    var LangIDDict = [NSDictionary]()
    var LangIDString_Array = [String] ()
    var GetLangName_Array = [String] ()
    var NewGetLangName_Array = [String] ()
    var GetLangID_Array = [String] ()
    var LangPriority_Array = [String] ()
    var LangPriorityOne_Array = [String] ()
    var LangPriorityZero_Array = [String] ()
    var IndexpathArray = [Int] ()
    var GetLangID = ""
    var task_IdInt:Int!
    var Customer_IdInt:Int!
    var task_IdString = ""
    var Customer_IdString = ""
    var Task_Date = ""
    var Task_time = ""
    var GetTaskDate = ""
    var GetTaskTime = ""
    var GetTaskDay = ""
    var GetSelectedTaskLang = ""
    var ConvertedGetTaskDate = ""
    var Task_TaskerTitle = ""
    var Task_TaskerDesc = ""
    var BtnSelctedTag:Int!
    var maxLenTaskTitle:Int = 100
    var maxLenTaskDesc:Int = 800
    var AllDataDict = [[String:Any]]()
    let defaults = UserDefaults.standard
    var charactersSet = CharacterSet(charactersIn: CHARACTERS.ACCEPTABLE_CHARACTERS)
    var isCalledFromSearchResult:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        //        self.TitleView.isHidden = true
        //        self.LblMainTitle.isHidden = false
        
        self.Tftasktitle.delegate = self
        self.DescriptionTextview.delegate = self
        
        LangTableView.delegate = self
        LangTableView.dataSource = self
        self.Scrollview.delegate = self
        
        
        TitleView.layer.shadowColor = UIColor.gray.cgColor
        TitleView.layer.shadowOpacity = 0.5
        TitleView.layer.shadowOffset = CGSize.zero
        TitleView.layer.shadowRadius = 6
        TitleView.layer.cornerRadius = 5
        TitleView.clipsToBounds = false
        
        //          Tftasktitle.becomeFirstResponder()
        
        //let TaskTitleTap = UITapGestureRecognizer(target:self,action:#selector(self.TaskTitleTapped))
        //
        Tftasktitle.addTarget(self, action: #selector(AddNewTaskDetailsViewController.textFieldDidChange(_:)),
                              for: .editingChanged)
        
        
        //add it to the label
        //        LblTitle.addGestureRecognizer(TaskTitleTap)
        //        LblTitle.isUserInteractionEnabled = true
        //
        
        self.authentication_key_val = UserDefaults.standard.string(forKey: "authentication_key") ?? ""//get
        self.GetTaskID = UserDefaults.standard.string(forKey: "TaskIDKey") ?? ""//get
        self.GetCustomerID = UserDefaults.standard.string(forKey: "CustomerIDKey") ?? ""//get
        print("GetTaskID:",GetTaskID)
        self.get_languages()
        
        
        
        self.DescriptionTextview.layer.borderColor = UIColor.lightGray.cgColor
        self.DescriptionTextview.layer.borderWidth = 2
        DescriptionTextview.text = "Type task description"
        DescriptionTextview.textColor = UIColor.black
        DescriptionTextview.font = UIFont(name:"STCForward-Regular",size:18)
        
        //        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        //        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        addDoneButtonOnKeyboard()
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.isCalledFromSearchResult = UserDefaults.standard.bool(forKey: "isEditSearchResult")// this is how you retrieve the bool value
        print("isCalledFromSearchResult......:",isCalledFromSearchResult)
        if (isCalledFromSearchResult)//value from search result
        {
            print("from search result")
            self.review_task_details()
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //        return self.Language_NameString_Array.count
        //        return self.LangIDDict.count
        return self.LangPriorityOne_Array.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = LangTableView.dequeueReusableCell(withIdentifier: "LanguageCell", for: indexPath as IndexPath) as! LanguageTableViewCell
        
        let AllData =  self.LangIDDict[indexPath.row]
        print("AllDataCellForRow:",AllData)
        
        let language_priority = AllData["language_priority"] as? String
        let Language_Name = AllData["language_name"] as? String
        let Language_ID = AllData["language_id"] as? String
        
        if(language_priority == "0")
        {
            cell.LblLanguage.text = Language_Name
        }
        if(language_priority == "1")
        {
            cell.LblLanguage.text = Language_Name
            
        }
        
        
        cell.LangCheckBox.tag = indexPath.row
        
        self.isCalledFromSearchResult = UserDefaults.standard.bool(forKey: "isEditSearchResult")// this is how you retrieve the bool value
        print("isCalledFromSearchResult......:",isCalledFromSearchResult)
        if (isCalledFromSearchResult)//value from search result
        {
            self.IndexpathArray = UserDefaults.standard.array(forKey: "SelectedLangIndexArray") as? [Int] ?? [Int]()
            self.GetLangID_Array = UserDefaults.standard.array(forKey: "SelectedLangIDArray")as? [String] ?? [String]()
            self.NewGetLangName_Array = UserDefaults.standard.array(forKey: "SelectedLangNameArray")as? [String] ?? [String]()
            print("IndexpathArrayforselected:",self.IndexpathArray)
            print("GetLangID_ArraySelected... :\(self.GetLangID_Array)")
            print("NewGetLangName_ArraySelected... :\(self.NewGetLangName_Array)")
            
            
            for selectedIndex in self.IndexpathArray
            {
                
                //                print("cell.LangCheckBox.tag:",cell.LangCheckBox.tag)
                if cell.LangCheckBox.tag == selectedIndex
                {
                    cell.LangCheckBox.isSelected = true
                    
                }
                
                
                //                cell.LangCheckBox.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
                //                if self.BtnSelctedTag == indexPath.row
                //
                //                {
                //
                //                    if cell.LangCheckBox.isSelected == false
                //                    {
                //                        cell.LangCheckBox.setImage(UIImage(named: "emptyCheck"), for: .normal)
                //                    }
                //
                //                    else
                //                    {
                //                        cell.LangCheckBox.setImage(UIImage(named: "activeCheck"), for: .normal)
                //
                //                    }
                //
                //
                //                }
                
                print("operations here")
            }
        }
        
        
        
        
        print("cell.LangCheckBox.tag:",cell.LangCheckBox.tag)
        //        cell.LangCheckBox.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        //
        //        if self.BtnSelctedTag == indexPath.row
        //        {
        //
        //            //check for indepath
        //            if self.IndexpathArray.contains(self.BtnSelctedTag) {
        //                if let itemToRemoveIndex1 = IndexpathArray.firstIndex(of: self.BtnSelctedTag) {
        //                    IndexpathArray.remove(at: itemToRemoveIndex1)
        //                }
        //            } else {
        //                self.IndexpathArray.append(self.BtnSelctedTag)
        //            }
        //              print("IndexpathArray....:",self.IndexpathArray)
        //           //////////////////////////////////////////////////////////////////////
        //
        //            self.GetLangID = self.LangIDString_Array[indexPath.row]
        //
        //            //check for id
        //            if self.GetLangID_Array.contains(self.GetLangID) {
        //                if let itemToRemoveIndex = GetLangID_Array.firstIndex(of: self.GetLangID) {
        //                    GetLangID_Array.remove(at: itemToRemoveIndex)
        //                }
        //            } else {
        //                self.GetLangID_Array.append(self.GetLangID)
        //            }
        //
        //            self.GetSelectedTaskLang = self.GetLangName_Array[indexPath.row]
        //
        //            //check for lang
        //            if self.NewGetLangName_Array.contains(self.GetSelectedTaskLang) {
        //                if let itemToRemoveIndex1 = NewGetLangName_Array.firstIndex(of: self.GetSelectedTaskLang) {
        //                    NewGetLangName_Array.remove(at: itemToRemoveIndex1)
        //                }
        //            } else {
        //                self.NewGetLangName_Array.append(self.GetSelectedTaskLang)
        //            }
        //            print("GetLangID_Array :\(self.GetLangID_Array)")
        //            print("NewGetLangName_Array :\(self.NewGetLangName_Array)")
        //
        //            UserDefaults.standard.set(self.GetLangID_Array,forKey: "SelectedGetLangIDArrayKey")//set array
        //            UserDefaults.standard.set(self.NewGetLangName_Array,forKey: "SelectedTaskLangKey")//set array
        //        }
        
        
        //        if(isRequiredInformationProvided() ==  true)
        //        {
        //            self.BtnContinue.setTitleColor(COLOR.WhiteColor, for: .normal)
        //            BtnContinue.backgroundColor = COLOR.PinkColor
        //        }
        //        else
        //        {
        //            self.BtnContinue.setTitleColor(COLOR.GrayColor, for: .normal)
        //            BtnContinue.backgroundColor = COLOR.LightGrayColor
        //        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("You selected cell :\(indexPath.row)!")
        self.BtnSelctedTag = indexPath.row
        
        if let cell = LangTableView.cellForRow(at: indexPath) as? LanguageTableViewCell
        {
            if cell.LangCheckBox.isSelected
            {
                cell.LangCheckBox.isSelected = false
                //check for indepath
                if self.IndexpathArray.contains(self.BtnSelctedTag) {
                    if let itemToRemoveIndex1 = IndexpathArray.firstIndex(of: self.BtnSelctedTag) {
                        IndexpathArray.remove(at: itemToRemoveIndex1)
                    }
                }
                
                self.GetSelectedTaskLang = self.GetLangName_Array[indexPath.row]
                
                //check for lang
                if self.NewGetLangName_Array.contains(self.GetSelectedTaskLang) {
                    if let itemToRemoveIndex1 = NewGetLangName_Array.firstIndex(of: self.GetSelectedTaskLang) {
                        NewGetLangName_Array.remove(at: itemToRemoveIndex1)
                    }
                }
                
                self.GetLangID = self.LangIDString_Array[indexPath.row]
                
                //check for id
                if self.GetLangID_Array.contains(self.GetLangID) {
                    if let itemToRemoveIndex = GetLangID_Array.firstIndex(of: self.GetLangID) {
                        GetLangID_Array.remove(at: itemToRemoveIndex)
                    }
                }
                
            }
            else
            {
                cell.LangCheckBox.isSelected = true
                
                if !(self.IndexpathArray.contains(self.BtnSelctedTag))
                {
                    self.IndexpathArray.append(self.BtnSelctedTag)
                }
                print("IndexpathArray....:",self.IndexpathArray)
                //////////////////////////////////////////////////////////////////////
                self.GetLangID = self.LangIDString_Array[indexPath.row]
                if !(self.GetLangID_Array.contains(self.GetLangID))
                {
                    self.GetLangID_Array.append(self.GetLangID)
                }
                
                print("GetLangID_Array :\(self.GetLangID_Array)")
                
                self.GetSelectedTaskLang = self.GetLangName_Array[indexPath.row]
                if !(self.NewGetLangName_Array.contains(self.GetSelectedTaskLang))
                {
                    self.NewGetLangName_Array.append(self.GetSelectedTaskLang)
                }
            }
            print("NewGetLangName_Array :\(self.NewGetLangName_Array)")
            
            
            UserDefaults.standard.set(self.GetLangID_Array,forKey: "SelectedGetLangIDArrayKey")//set array
            UserDefaults.standard.set(self.NewGetLangName_Array,forKey: "SelectedTaskLangKey")//set array
            
            
        }
        if(isRequiredInformationProvided() ==  true)
        {
            self.BtnContinue.setTitleColor(COLOR.WhiteColor, for: .normal)
            BtnContinue.backgroundColor = COLOR.PinkColor
        }
        else
        {
            self.BtnContinue.setTitleColor(COLOR.GrayColor, for: .normal)
            BtnContinue.backgroundColor = COLOR.LightGrayColor
        }
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        if let cell = LangTableView.cellForRow(at: indexPath) as? LanguageTableViewCell
        {
            if cell.LangCheckBox.isSelected
            {
                cell.LangCheckBox.isSelected = false
                
                
                //check for indepath
                if self.IndexpathArray.contains(self.BtnSelctedTag) {
                    if let itemToRemoveIndex1 = IndexpathArray.firstIndex(of: self.BtnSelctedTag) {
                        IndexpathArray.remove(at: itemToRemoveIndex1)
                    }
                }
                
                self.GetSelectedTaskLang = self.GetLangName_Array[indexPath.row]
                
                //check for lang
                if self.NewGetLangName_Array.contains(self.GetSelectedTaskLang) {
                    if let itemToRemoveIndex1 = NewGetLangName_Array.firstIndex(of: self.GetSelectedTaskLang) {
                        NewGetLangName_Array.remove(at: itemToRemoveIndex1)
                    }
                }
                
                self.GetLangID = self.LangIDString_Array[indexPath.row]
                
                //check for id
                if self.GetLangID_Array.contains(self.GetLangID) {
                    if let itemToRemoveIndex = GetLangID_Array.firstIndex(of: self.GetLangID) {
                        GetLangID_Array.remove(at: itemToRemoveIndex)
                    }
                }
            }
            else
            {
                cell.LangCheckBox.isSelected = true
                if !(self.IndexpathArray.contains(self.BtnSelctedTag))
                {
                    self.IndexpathArray.append(self.BtnSelctedTag)
                }
                // print("IndexpathArray....:",self.IndexpathArray)
                //////////////////////////////////////////////////////////////////////
                self.GetLangID = self.LangIDString_Array[indexPath.row]
                if !(self.GetLangID_Array.contains(self.GetLangID))
                {
                    self.GetLangID_Array.append(self.GetLangID)
                }
                
                //print("GetLangID_Array :\(self.GetLangID_Array)")
                
                self.GetSelectedTaskLang = self.GetLangName_Array[indexPath.row]
                if !(self.NewGetLangName_Array.contains(self.GetSelectedTaskLang))
                {
                    self.NewGetLangName_Array.append(self.GetSelectedTaskLang)
                }
            }
            
            if(isRequiredInformationProvided() ==  true)
            {
                self.BtnContinue.setTitleColor(COLOR.WhiteColor, for: .normal)
                BtnContinue.backgroundColor = COLOR.PinkColor
            }
            else
            {
                self.BtnContinue.setTitleColor(COLOR.GrayColor, for: .normal)
                BtnContinue.backgroundColor = COLOR.LightGrayColor
            }
        }
        print("IndexpathArrayRemove....:",self.IndexpathArray)
        print("GetLangID_ArrayRemove :\(self.GetLangID_Array)")
        print("NewGetLangName_ArrayRemove :\(self.NewGetLangName_Array)")
        
    }
    
    @objc func buttonPressed(sender:UIButton){
        self.BtnSelctedTag = sender.tag
        print("Selected item in row... \(sender.tag)")
        self.LangTableView.reloadData()
        if sender.isSelected{
            sender.isSelected = false
        }
        else
        {
            sender.isSelected = true
            
        }
        
    }
    
    //    @objc func buttonPressed1(sender:UIButton){
    //           self.BtnSelctedTag = sender.tag
    //           print("Selected item in row \(sender.tag)")
    //           self.LangTableView.reloadData()
    //
    //        if sender.isSelected{
    //            sender.isSelected = false
    //        }
    //        else
    //        {
    //            sender.isSelected = true
    //
    //        }
    //
    //
    //
    //       }
    
    
    //horizontal scrollview lock
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x != 0 {
            scrollView.contentOffset.x = 0
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if (DescriptionTextview.text == "Type task description" && DescriptionTextview.textColor == .black)
        {
            DescriptionTextview.text = ""
            DescriptionTextview.textColor = .black
            DescriptionTextview.font = UIFont(name:"STCForward-Regular",size:18)
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if (DescriptionTextview.text == "")
        {
            DescriptionTextview.text = "Type task description"
            DescriptionTextview.textColor = .black
            DescriptionTextview.font = UIFont(name:"STCForward-Regular",size:18)
        }
        
        if(isRequiredInformationProvided() ==  true)
        {
            self.BtnContinue.setTitleColor(COLOR.WhiteColor, for: .normal)
            BtnContinue.backgroundColor = COLOR.PinkColor
        }
        else
        {
            self.BtnContinue.setTitleColor(COLOR.GrayColor, for: .normal)
            BtnContinue.backgroundColor = COLOR.LightGrayColor
        }
    }
    
    func get_languages()
    {
        let urlString = ConstantsClass.baseUrl + ConstantsClass.get_languages
        print("urlString: \(urlString)")
        
        let headers = [
            "Authorization": "\(self.authentication_key_val)"
        ]
        print("headers: \(headers)")
        Alamofire.request(urlString, method: .get, parameters: nil, encoding: URLEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if (response["status"] as? Bool) != nil {
                        if let respArray = response["response"] as? [[String:Any]] {
                            print("respArray: \(respArray)")
                            
                            self.LangIDDict = response["response"] as! [NSDictionary]
                            print("LangIDDict:",self.LangIDDict)
                            for Data in self.LangIDDict
                            {
                                self.Language_Name = Data["language_name"]as! String
                                self.Language_Id = Data["language_id"]as? Int
                                self.LangIDString = String( self.Language_Id)
                                self.LangPriority = Data["language_priority"] as! String
                                if self.LangPriority == "1"
                                {
                                    self.LangPriorityOne_Array.append(self.LangPriority)
                                    self.Language_NameString_Array.append(self.Language_Name)
                                    self.LangIDString_Array.append(self.LangIDString)
                                }
                                if self.LangPriority == "0"
                                {
                                    self.LangPriorityZero_Array.append(self.LangPriority)
                                }
                            }
                            self.GetLangName_Array = self.Language_NameString_Array
                            
                        }
                    }
                    // Make sure to update UI in main thread
                    DispatchQueue.main.async {
                        self.LangTableView.reloadData()
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
        
        
    }
    
    
    //    @objc func TaskTitleTapped()
    //    {
    ////        self.TitleView.isHidden = false
    ////        self.LblMainTitle.isHidden = true
    ////        TFTaskTitleHt.constant = 30
    ////        Tftasktitle.becomeFirstResponder()
    //    }
    
    func isRequiredInformationProvided() -> Bool {
        if (Tftasktitle.text == "") || (DescriptionTextview.text == "") || (DescriptionTextview.text == "Type task description")
        {
            return false
        }
        if (self.GetLangID_Array.count > 0)
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func add_customer_task_details()
    {
        
        self.GetTaskTitle = self.Tftasktitle.text!
        self.GetTaskDescription = self.DescriptionTextview.text
        UserDefaults.standard.set(self.GetTaskTitle,forKey: "EnterTaskTitleKey")
        UserDefaults.standard.set(self.GetTaskDescription,forKey: "EnterTaskDescKey")
        
        print("GetLangID_Array....: \(GetLangID_Array)")
        print("GetTaskTitle....: \(GetTaskTitle)")
        print("GetTaskDescription....: \(GetTaskDescription)")
        
        let urlString = ConstantsClass.baseUrl + ConstantsClass.add_customer_task_details
        print("urlString: \(urlString)")
        
        let headers = [
            "Authorization": "\(self.authentication_key_val)"
        ]
        print("headers: \(headers)")
        
        let parameters = [
            "task_title" : "\(self.GetTaskTitle)",
            "task_description" : "\(self.GetTaskDescription)",
            "task_id" : "\(self.GetTaskID)",
            "language_id" :(GetLangID_Array)
            ] as [String : AnyObject]
        print("parametersaddtaskdetails: \(parameters)")
        Alamofire.request(urlString
            , method: .post, parameters: parameters, encoding:JSONEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:AnyObject]
                {
                    print("response: \(response)")
                    if ((response["response"]as? [String:AnyObject]) != nil)
                    {
                        if let responseDict = response["response"]as? [String:AnyObject]
                        {
                            self.task_IdInt = responseDict["task_id"] as? Int
                            self.task_IdString = String(self.task_IdInt)
                            
                            self.Customer_IdInt = responseDict["customer_id"] as? Int
                            print("Customer_IdInt: \(String(describing: self.Customer_IdInt))")
                            self.Customer_IdString = String(self.Customer_IdInt)
                            
                            self.GetTaskDate = responseDict["task_date"] as! String
                            self.ConvertedGetTaskDate =  StaticUtility.convertDateFormater9( self.GetTaskDate)
                            self.GetTaskTime = responseDict["task_time"] as! String
                            self.GetTaskDay = responseDict["task_day"] as! String
                            
                            
                            
                            print("task_IdString: \(self.task_IdString)")
                            print("Customer_IdString: \(self.Customer_IdString)")
                            print("GetTaskDate: \(self.GetTaskDate)")
                            print("GetTaskTime: \(self.GetTaskTime)")
                            print("GetTaskDay: \(self.GetTaskDay)")
                            
                            UserDefaults.standard.set(self.ConvertedGetTaskDate, forKey: "TaskDateKey")//set
                            UserDefaults.standard.set(self.GetTaskTime, forKey: "TaskTimeKey")//set
                            UserDefaults.standard.set(self.GetTaskDay, forKey: "TaskDayKey")//set
                            
                            
                            if let status = response["status"] as? Bool {
                                if status{
                                    DispatchQueue.main.async{
                                        
                                        let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
                                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "AddTaskLocation") as! AddTaskLocationViewController
                                        
                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                        
                                    }
                                }
                                else
                                {
                                    self.view.makeToast("Something went wrong", duration: 3.0, position: .bottom)
                                    print("Something went wrong")
                                }
                            }
                            else
                            {
                                print("Error occured") // serialized json response
                                
                            }
                        }
                        
                    }
                }
        }
        
    }
    
    
    func review_task_details()
    {
        
        let urlString = ConstantsClass.baseUrl + ConstantsClass.review_task_details
        print("urlString: \(urlString)")
        
        let headers = [
            "Authorization": "\(self.authentication_key_val)"
        ]
        print("headers: \(headers)")
        
        let parameters = [
            "task_id" : "\(self.GetTaskID)",
            ] as [String : AnyObject]
        print("parametersaddtaskdetails: \(parameters)")
        Alamofire.request(urlString
            , method: .post, parameters: parameters, encoding:JSONEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if (response["status"] as? Bool) != nil {
                        if let respArrayGetTasker = response["response"] as? [[String:Any]] {
                            print("respArrayTaskerDetails: \(respArrayGetTasker)")
                            self.AllDataDict = respArrayGetTasker
                            print("AllDataDict:",self.AllDataDict)
                            //
                            for AllData in self.AllDataDict
                            {
                                
                                self.Task_TaskerTitle = AllData["title"]as? String ?? ""
                                self.Task_TaskerDesc = AllData["description"]as? String ?? ""
                            }
                            print("Task_TaskerTitle:",self.Task_TaskerTitle)
                            print("Task_TaskerDesc:",self.Task_TaskerDesc)
                            
                            for AllData in self.AllDataDict
                            {
                                if let language_details = AllData["language_details"] as? [[String:Any]]
                                {
                                    for Languages in language_details
                                    {
                                        self.Task_TaskerLang = Languages["language_name"]as! String
                                        self.Task_TaskerLangAray.append(self.Task_TaskerLang)
                                    }
                                    print("self.Task_TaskerLangAray:",self.Task_TaskerLangAray)
                                 
                                    print("GetLangName_Array:",self.GetLangName_Array)
                                 
                                }
                            }
                            
                            self.Tftasktitle.text =  self.Task_TaskerTitle
                            self.DescriptionTextview.text = self.Task_TaskerDesc
                            
                            if(self.isRequiredInformationProvided())
                            {
                                self.BtnContinue.setTitleColor(COLOR.WhiteColor, for: .normal)
                                self.BtnContinue.backgroundColor = COLOR.PinkColor
                            }
                            else
                            {
                                self.BtnContinue.setTitleColor(COLOR.GrayColor, for: .normal)
                                self.BtnContinue.backgroundColor = COLOR.LightGrayColor
                            }
                        }
                    }
                        
                    else
                    {
                        
                    }
                    // Make sure to update UI in main thread
                    DispatchQueue.main.async {
                        print("review_task_details_list_success")
                        
                        
                        
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
        
    }
    
  
    
    @IBAction func BackbtnClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func CancelbtnClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ContinueClick(_ sender: UIButton) {
        var isValid = true
        if Tftasktitle.text?.count == 0
        {
            self.view.makeToast("Please enter task title")
            isValid = false
        }
        else if (DescriptionTextview.text?.count == 0) || (DescriptionTextview.text == "Type task description")
        {
            self.view.makeToast("Please type task description")
            isValid = false
        }
        else if (self.GetLangID_Array.count == 0)
        {
            self.view.makeToast("Please select atleast one language")
            isValid = false
        }
        else if (Tftasktitle.text?.rangeOfCharacter(from: charactersSet.inverted) != nil ) || (DescriptionTextview.text?.rangeOfCharacter(from: charactersSet.inverted) != nil)
        {
            self.view.makeToast("Emoji & Special characters not allowed")
            isValid = false
        }
        
        if (isValid)
        {
            UserDefaults.standard.set(self.IndexpathArray, forKey: "SelectedLangIndexArray")
            UserDefaults.standard.set(self.GetLangID_Array, forKey: "SelectedLangIDArray")
            UserDefaults.standard.set(self.NewGetLangName_Array, forKey: "SelectedLangNameArray")
            self.add_customer_task_details()
        }
        
    }
    
    @objc private func textFieldDidChange(_ textField: UITextField) {
        Tftasktitle.becomeFirstResponder()
        
        if(isRequiredInformationProvided() ==  true)
        {
            self.BtnContinue.setTitleColor(COLOR.WhiteColor, for: .normal)
            BtnContinue.backgroundColor = COLOR.PinkColor
        }
        else
        {
            self.BtnContinue.setTitleColor(COLOR.GrayColor, for: .normal)
            BtnContinue.backgroundColor = COLOR.LightGrayColor
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == Tftasktitle){
            let currentText = textField.text! + string
            return currentText.count <= maxLenTaskTitle
        }
        if(textField == DescriptionTextview){
            let currentText = textField.text! + string
            return currentText.count <= maxLenTaskDesc
        }
        return true;
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 20))
        doneToolbar.barStyle = .default
        let Cancel: UIBarButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(self.doneButtonAction))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        let items = [Cancel,flexSpace, done]
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.Tftasktitle.inputAccessoryView = doneToolbar
        self.DescriptionTextview.inputAccessoryView = doneToolbar
        
        
    }
    
    @objc func doneButtonAction()
    {
        self.Tftasktitle.resignFirstResponder()
        self.DescriptionTextview.resignFirstResponder()
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.Tftasktitle.resignFirstResponder()
        self.DescriptionTextview.resignFirstResponder()
        
        
        return true
    }
    
    //    @objc func keyboardWillShow(notification: NSNotification) {
    //        if let keyboardSize = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue{
    //            let height = keyboardSize.cgRectValue.height
    //            self.Scrollview.contentSize = CGSize(width: self.view.frame.height, height: self.Scrollview.contentSize.height + height)
    //        }
    //    }
    //
    //    @objc func keyboardWillHide(notification: NSNotification) {
    //        if let keyboardSize = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue{
    //            let height = keyboardSize.cgRectValue.height
    //            self.Scrollview.contentSize = CGSize(width: self.view.frame.height, height: self.Scrollview.contentSize.height - height)
    //        }
    //    }
    
}



