//
//  LanguageTableViewCell.swift
//  LMRA
//
//  Created by Mac on 25/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class LanguageTableViewCell: UITableViewCell {

    @IBOutlet weak var LblLanguage: UILabel!
    @IBOutlet weak var LangCheckBox: UIButton!
    @IBOutlet weak var LanguageView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
