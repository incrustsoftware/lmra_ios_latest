//
//  EditAddressDetailsViewController.swift
//  LMRA
//
//  Created by Mac on 11/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import iOSDropDown
import Alamofire

class EditAddressDetailsViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var LblNickName: UILabel!
    @IBOutlet weak var TFNickName: UITextField!
    @IBOutlet weak var LblArea: UILabel!
    @IBOutlet weak var LblAddressType: UILabel!
    @IBOutlet weak var DropdownArea: DropDown!
    @IBOutlet weak var DropDownAddressType: DropDown!
    @IBOutlet weak var LblStreet: UILabel!
    @IBOutlet weak var TFStreet: UITextField!
    @IBOutlet weak var LblBuildingNm: UILabel!
    @IBOutlet weak var TFBuildingNm: UITextField!
    @IBOutlet weak var LblFloor: UILabel!
    @IBOutlet weak var TFFloor: UITextField!
    @IBOutlet weak var LblAprtNo: UILabel!
    @IBOutlet weak var TFAprtNo: UITextField!
    @IBOutlet weak var LblAdditnDir: UILabel!
    @IBOutlet weak var TFAdditnDir: UITextField!
    @IBOutlet weak var LblNickNmHt: NSLayoutConstraint!
    @IBOutlet weak var LblAreaHT: NSLayoutConstraint!
    @IBOutlet weak var LblAddressTypeHt: NSLayoutConstraint!
    @IBOutlet weak var LblStreetHt: NSLayoutConstraint!
    @IBOutlet weak var LblBuildingnNmht: NSLayoutConstraint!
    @IBOutlet weak var LblFloorNmht: NSLayoutConstraint!
    @IBOutlet weak var LblAprtNoHt: NSLayoutConstraint!
    @IBOutlet weak var LblAddtnDirHt: NSLayoutConstraint!
    @IBOutlet weak var BtnUpdate: UIButton!
    @IBOutlet weak var BtnBack: UIButton!
    
    var maxLenAddNickName:Int = 50
    var maxLenStreet:Int = 50
    var maxLenBuildNM:Int = 50
    var maxLenFloor:Int = 50
    var maxLenAprtNo:Int = 50
    var maxLenAddDir:Int = 50
    let defaults = UserDefaults.standard
    //       var BtnContinueColor = UIColor(red:243, green:243, blue:241, alpha:1.0)
    var Cell_Number = ""
    var Get_CPR_No = ""
    var AddrNickName = ""
    var Area = ""
    var AddressType = ""
    var Street = ""
    var BuildingName = ""
    var Floor = ""
    var AprtNumber = ""
    var AddDir = ""
    var Current_cell_number = ""
    var authentication_key_val = ""
    var Loginauthentication_key_val = ""
    var FullAddress = ""
    var GetAccountSettingAddressID = ""
    var StringGetAreaID = ""
    var StringGetAddressTypeId = ""
    var AllDataDict = [[String:Any]]()
    var AddressTypeArray = [String]()
    var AddressAreaArray = [String]()
    var AddressTypeIDArray = [String]()
    var AddressAreaIDArray = [String]()
    
    var charactersSet = CharacterSet(charactersIn: CHARACTERS.ACCEPTABLE_CHARACTERS)
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DropdownArea.delegate = self
        DropDownAddressType.delegate = self
        self.TFNickName.delegate = self
        self.TFFloor.delegate = self
        self.TFStreet.delegate = self
        self.TFBuildingNm.delegate = self
        self.TFAdditnDir.delegate = self
        self.TFAprtNo.delegate = self
        self.DropdownArea.tintColor = .clear
        self.DropDownAddressType.tintColor = .clear
        BtnUpdate.layer.cornerRadius = 2;
        addDoneButtonOnKeyboard()
        print("viewdidload")
        
        TFStreet.addTarget(self, action: #selector(EditAddressDetailsViewController.txtFieldChanged(_:)),
                           for: .editingChanged)
        TFBuildingNm.addTarget(self, action: #selector(EditAddressDetailsViewController.txtFieldChanged),
                               for: .editingChanged)
        TFFloor.addTarget(self, action: #selector(EditAddressDetailsViewController.txtFieldChanged),
                          for: .editingChanged)
        TFAprtNo.addTarget(self, action: #selector(EditAddressDetailsViewController.txtFieldChanged),
                           for: .editingChanged)
        DropdownArea.addTarget(self, action: #selector(EditAddressDetailsViewController.txtFieldChanged),
                               for: .editingChanged)
        
        DropDownAddressType.addTarget(self, action: #selector(EditAddressDetailsViewController.txtFieldChanged),
                                      for: .editingChanged)
        
        
        
        MainView.addTapGesture{
            self.dismissKeyboard()
        }
        
        //call to get area and area type
        self.get_adress_type()
        
        
        self.Loginauthentication_key_val = UserDefaults.standard.string(forKey: "authentication_key")!//get
        print("Loginauthentication_key_val:",self.Loginauthentication_key_val)
        
        //        // for when user click on particular address
        //        self.GetAccountSettingAddressID = UserDefaults.standard.string(forKey: "SelectedAccountAddAddressID")!//get
        //        print("GetAccountSettingAddressID:",self.GetAccountSettingAddressID)
        //
        //          self.get_address_by_id()
        let isCalledFromReviewAndConfirm = UserDefaults.standard.bool(forKey: "IsFromReviewNdConfirm")
        let isCalledFromAddLocation = UserDefaults.standard.bool(forKey: "EditFromAddLocation")// this is how you retrieve the bool value
        print("isCalledFromAddLocation....:",isCalledFromAddLocation)
        print("isCalledFromReviewAndConfirm....:",isCalledFromReviewAndConfirm)
        if (isCalledFromAddLocation == true)//value from add task location
        {
            self.GetAccountSettingAddressID = UserDefaults.standard.string(forKey: "SelectedGetAddressIdKey")!//get
            self.get_address_by_id()
            
            print("from edit add location")
        }
        else if (isCalledFromReviewAndConfirm == true)//value from review and confirm
        {
            
            self.GetAccountSettingAddressID = UserDefaults.standard.string(forKey: "AddressIDKey") ?? ""//get
            print("GetAccountSettingAddressID: \( self.GetAccountSettingAddressID)")
            self.get_address_by_id()
            
            print("from review and confirm")
        }
        else
        {
            //from account setting page
            // for when user click on particular address
            self.GetAccountSettingAddressID = UserDefaults.standard.string(forKey: "SelectedAccountAddAddressID")!//get
            print("GetAccountSettingAddressID:",self.GetAccountSettingAddressID)
            self.get_address_by_id()
            
        }
        
        
        
        LblNickNmHt.constant = 30
        LblStreetHt.constant = 30
        LblBuildingnNmht.constant = 30
        LblFloorNmht.constant = 30
        LblAprtNoHt.constant = 30
        LblAddtnDirHt.constant = 30
  
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.BtnUpdate.setTitleColor(COLOR.WhiteColor, for: .normal)
        BtnUpdate.backgroundColor = COLOR.PinkColor
        
        self.DropdownArea.text = "Choose Area"
        self.DropDownAddressType.text = "Choose Address type"
        DropdownArea.didSelect{(selectedText , index ,id) in
            self.DropdownArea.text =  selectedText
            print("self.DropdownArea.text: \( self.DropdownArea.text ?? "")")
            
            let GetAreaID = index
            self.StringGetAreaID = String(GetAreaID)
            print("self.DropdownArea.id:",self.StringGetAreaID)
            
        }
        
        DropDownAddressType.didSelect{(selectedText , index ,id) in
            self.DropDownAddressType.text = selectedText
            print("self.DropDownAddressType.text: \(self.DropDownAddressType.text ?? "")")
            
            let GetAddressTypeId = index
            self.StringGetAddressTypeId = String(GetAddressTypeId)
            print("self.GetAddressType.id:",self.StringGetAddressTypeId)
        }
    }
    
    func dismissKeyboard() {
          //Causes the view (or one of its embedded text fields) to resign the first responder status.
          view.endEditing(true)
      }
    
    @IBAction func BackBtnClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
        
    }
    
    @IBAction func UpdateBtnClick(_ sender: Any) {
        //self.update_current_address_customer()
        self.SetAddrcheckValidation()
    }
    
    func SetAddrcheckValidation()
    {
        if (self.DropdownArea.text == "Choose Area")
        {
            
            self.view.makeToast("Please select valid area")
        }
        else if (self.DropDownAddressType.text == "Choose Address type")
        {
            
            self.view.makeToast("Please select valid address type")
        }
        else if (TFStreet.text == "")
        {
            
            self.view.makeToast("Please enter street")
        }
        else if (TFBuildingNm.text == "")
        {
            
            self.view.makeToast("Please enter building name")
        }
        else if (TFFloor.text == "")
        {
            
            self.view.makeToast("Please enter floor")
        }
        else if (TFAprtNo.text == "")
        {
            
            self.view.makeToast("Please enter apartment number")
        }
        else if TFNickName.text?.rangeOfCharacter(from: charactersSet.inverted) != nil {
            self.view.makeToast("Special character not allowed &,.space allowed between characters allowed")
            
        }
        else if TFAdditnDir.text?.rangeOfCharacter(from: charactersSet.inverted) != nil {
            self.view.makeToast("Special character not allowed &,.space allowed between characters allowed")
            
        }
        else
        {
            self.AddrNickName =  TFNickName.text!
            self.Area = self.DropdownArea.text!
            self.AddressType =  self.DropDownAddressType.text!
            self.Street = TFStreet.text!
            self.BuildingName = TFBuildingNm.text!
            self.Floor = TFFloor.text!
            self.AprtNumber = TFAprtNo.text!
            self.AddDir = TFAdditnDir.text!
            self.Get_CPR_No = UserDefaults.standard.string(forKey: "CPRNoKey") ?? ""//get
            self.Current_cell_number = UserDefaults.standard.string(forKey: "CurrentCellNoKey") ?? ""//get
            self.authentication_key_val = UserDefaults.standard.string(forKey: "authentication_key") ?? ""//get
            
            self.FullAddress = ""
            self.FullAddress =  self.AddrNickName + "," + "" +  self.Area + "," + "" +
                self.Street + "," + "" +  self.BuildingName + "," + "" + self.Floor + "," + "" + self.AprtNumber + "," + "" + self.AddDir
            
            print("authentication_key_val: \(self.authentication_key_val)")
            print("Current_cell_number: \(self.Current_cell_number)")
            print("Get_CPR_No: \(self.Get_CPR_No)")
            print("AddDir: \(self.AddDir)")
            print("AprtNumber: \(self.AprtNumber)")
            print("Floor: \(self.Floor)")
            print("BuildingName: \(self.BuildingName)")
            print("Street: \(self.Street)")
            print("AddressType: \(self.AddressType)")
            print("Area: \(self.Area)")
            print("AddrNickName: \(self.AddrNickName)")
            print("FullAddress: \(self.FullAddress)")
            
            
            self.update_current_address_customer()
            
            
        }
    }
    
    
    func get_adress_type()
    {
        self.Loginauthentication_key_val = UserDefaults.standard.string(forKey: "authentication_key")!//get
        
        let urlString = ConstantsClass.baseUrl + ConstantsClass.get_adress_type
        print("urlString: \(urlString)")
        
        let headers = [
            "Authorization": "\(self.Loginauthentication_key_val)"
        ]
        Alamofire.request(urlString, method: .post, parameters: nil, encoding: URLEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any] {
                    print("responseLocation: \(response)")
                    
                    if (response["status"] as? Bool) != nil {
                        if let respArrayGetAddress = response["response"] as? [String:Any] {
                            print("respArrayGetAddress: \(respArrayGetAddress)")
                            
                            print("respArrayGetAddress: \(respArrayGetAddress)")
                            
                            if let AddressType = respArrayGetAddress["address_master_data"]as? [[String:Any]]
                            {
                                for TypeData in AddressType
                                {
                                    let AddressTypeName = TypeData["adrress_master_name"]as? String ?? ""
                                    self.AddressTypeArray.append(AddressTypeName)
                                }
                            }
                            print("AddressTypeArray: \(self.AddressTypeArray)")
                            
                            
                            
                            if let AddressArea = respArrayGetAddress["area_master_data"]as? [[String:Any]]
                            {
                                for TypeData in AddressArea
                                {
                                    let AddressAreaName = TypeData["area_master_name"]as? String ?? ""
                                    self.AddressAreaArray.append(AddressAreaName)
                                }
                            }
                            print("AddressAreaArray: \(self.AddressAreaArray)")
                            
                        }
                    }
                    // Make sure to update UI in main thread
                    DispatchQueue.main.async {
                        print("address_data_success")
                        self.AddressAreaArray.insert("Choose Area", at:0)
                        self.AddressTypeArray.insert("Choose Address type", at:0)
                        self.DropdownArea.optionArray = self.AddressAreaArray
                        self.DropDownAddressType.optionArray = self.AddressTypeArray
                        
                        
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
    }
    
    func get_address_by_id()
    {
        let url = ConstantsClass.baseUrl + ConstantsClass.get_address_by_ids
        print("url: \(url)")
        let headers = [
            "Authorization": "\(self.Loginauthentication_key_val)"
        ]
        
        let parameters = [
            "address_id" : "\(self.GetAccountSettingAddressID)"
            ] as [String : Any]
        
        print("parametersGetAddress: \(parameters)")
        Helper.sharedInstance.showLoader()
        Alamofire.request(url
            , method: .post, parameters: parameters, encoding:JSONEncoding.default,headers: headers).responseJSON
            {  response in
                
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if let responseDict = response["response"]as? [String:Any]
                    {
                        self.TFNickName.text = responseDict["address_nickname"]as? String ?? "" // set response value to textfield
                        self.TFStreet.text = responseDict["street"]as? String ?? ""
                        self.TFBuildingNm.text = responseDict["building_name"]as? String ?? ""
                        self.TFFloor.text = responseDict["floor"]as? String ?? ""
                        self.TFAprtNo.text = responseDict["appartment_no"]as? String ?? ""
                        self.TFAdditnDir.text = responseDict["additional_directions"]as? String ?? ""
                        self.DropdownArea.text = responseDict["area"]as? String ?? ""
                        self.DropDownAddressType.text =  responseDict["appartment"]as? String ?? ""
                         self.StringGetAreaID = String(responseDict["appartment_id"]as? String ?? "")
                         self.StringGetAddressTypeId = String(responseDict["area_id"]as? String ?? "")
                        if let status = response["status"] as? Bool {
                            if status{
                                
                                DispatchQueue.main.async {
                                    print("address_details_success")
                                    Helper.sharedInstance.hideLoader()
                                }
                            }
                            else
                            {
                                self.view.makeToast("Something went wrong", duration: 3.0, position: .bottom)
                                print("Something went wrong")
                            }
                        }
                        else
                        {
                            print("Error occured") // serialized json response
                            
                        }
                    }
                }
        }
    }
    
    func update_current_address_customer()
    {
        
        print("self.DropdownArea.id:",self.StringGetAreaID)
        print("self.GetAddressType.id:",self.StringGetAddressTypeId)
        
        let NickName = self.TFNickName.text
        let Street =  self.TFStreet.text
        let BuildingName = self.TFBuildingNm.text
        let Floor = self.TFFloor.text
        let AppartmentNo = self.TFAprtNo.text
        let AdditionalDir = self.TFAdditnDir.text
        let Area = self.DropdownArea.text
        let AddressType = self.DropDownAddressType.text
        //        let SelectedAddressID = UserDefaults.standard.string(forKey: "SelectedAccountAddAddressID")
        
        let url = ConstantsClass.baseUrl + ConstantsClass.update_current_address_customer
        print("url: \(url)")
        let headers = [
            "Authorization": "\(self.Loginauthentication_key_val)"
        ]
        
        let parameters = [
            "address_nickname" : "\(NickName ?? "")",
            "area" : "\(StringGetAreaID)", //send id
            "appartment" : "\(StringGetAddressTypeId)",// send id
            "street" : "\(Street ?? "")",
            "building_name" : "\(BuildingName ?? "")",
            "floor" : "\(Floor ?? "")",
            "appartment_no" : "\(AppartmentNo ?? "")",
            "additional_directions" : "\(AdditionalDir ?? "")",
            "id" : "\(GetAccountSettingAddressID)"
            ] as [String : Any]
        
        print("parametersAddressUpdate: \(parameters)")
        
        Alamofire.request(url
            , method: .post, parameters: parameters, encoding:JSONEncoding.default,headers: headers).responseJSON
            
            {
                response in
                
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if let status = response["status"] as? Bool {
                        
                        if status{
                            print("update_current_address_success")
                            
                            self.navigationController?.popViewController(animated: true)
                            
                            
                        }
                    }
                    else
                    {
                        self.view.makeToast("Something went wrong", duration: 3.0, position: .bottom)
                        print("Something went wrong")
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
    }
    
    
    @objc func txtFieldChanged(_ textField: UITextField) {
         if (TFStreet.text!.count == 0) || (TFBuildingNm.text!.count == 0) || (TFFloor.text!.count == 0) || (TFAprtNo.text!.count == 0) || (self.DropdownArea.text == "Choose Area") || (self.DropDownAddressType.text == "Choose Address type")
        {
            self.BtnUpdate.setTitleColor(COLOR.GrayColor, for: .normal)
            BtnUpdate.backgroundColor = COLOR.LightGrayColor
        }
        else
        {
            self.BtnUpdate.setTitleColor(COLOR.WhiteColor, for: .normal)
            BtnUpdate.backgroundColor = COLOR.PinkColor
        }
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 20))
        doneToolbar.barStyle = .default
        let Cancel: UIBarButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(self.doneButtonAction))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [Cancel,flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.TFNickName.inputAccessoryView = doneToolbar
        self.TFStreet.inputAccessoryView = doneToolbar
        self.TFBuildingNm.inputAccessoryView = doneToolbar
        self.TFFloor.inputAccessoryView = doneToolbar
        self.TFAprtNo.inputAccessoryView = doneToolbar
        self.TFAdditnDir.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.TFNickName.resignFirstResponder()
        self.TFStreet.resignFirstResponder()
        self.TFBuildingNm.resignFirstResponder()
        self.TFFloor.resignFirstResponder()
        self.TFAprtNo.resignFirstResponder()
        self.TFAdditnDir.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.TFNickName.resignFirstResponder()
        self.TFStreet.resignFirstResponder()
        self.TFBuildingNm.resignFirstResponder()
        self.TFFloor.resignFirstResponder()
        self.TFAprtNo.resignFirstResponder()
        self.TFAdditnDir.resignFirstResponder()
        if textField == DropdownArea
        {
            
            return false
        }
        else if textField == DropDownAddressType
        {
            
            return false
        }
        
        return true
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0
            {
                self.view.frame.origin.y -= 150
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            //            self.view.frame.origin.y = 0
            self.view.frame.origin.y += 150
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    //horizontal scrollview lock
       func scrollViewDidScroll(_ scrollView: UIScrollView) {
           if scrollView.contentOffset.x != 0 {
               scrollView.contentOffset.x = 0
           }
       }
    
//    //do not allow the textfield to edit
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        return false
//    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            
           if(textField == TFNickName){
               let currentText = textField.text! + string
               return currentText.count <= maxLenAddNickName
           }
           if(textField == TFStreet){
               let currentText = textField.text! + string
               return currentText.count <= maxLenStreet
           }
           if(textField == TFBuildingNm){
               let currentText = textField.text! + string
               return currentText.count <= maxLenBuildNM
           }
           if(textField == TFFloor){
               let currentText = textField.text! + string
               return currentText.count <= maxLenFloor
           }
           if(textField == TFAprtNo){
               let currentText = textField.text! + string
               return currentText.count <= maxLenAprtNo
           }
            if(textField == TFAdditnDir){
               let currentText = textField.text! + string
               return currentText.count <= maxLenAddDir
           }
        if textField == DropdownArea
        {
            return false
        }
        else if textField == DropDownAddressType
        {
            return false
        }
             return true;
        }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == DropdownArea || textField == DropDownAddressType
        {
            textField.endEditing(true)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
