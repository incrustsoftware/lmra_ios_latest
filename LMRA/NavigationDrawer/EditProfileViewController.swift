//
//  EditProfileViewController.swift
//  LMRA
//
//  Created by Mac on 08/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire

@available(iOS 12.0, *)
class EditProfileViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var BtBack: UIButton!
    @IBOutlet weak var outerImageView: UIImageView!
    @IBOutlet weak var ImgCamera: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblIfamilyName: UILabel!
    @IBOutlet weak var lblNationality: UILabel!
    @IBOutlet weak var lblCellNumber: UILabel!
    @IBOutlet weak var txtFieldEmail: UITextField!
    
    @IBOutlet weak var BtnContinue: UIButton!
    
    @IBOutlet weak var btnDelete: UIButton!
    
    @IBOutlet weak var imgUser: UIImageView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var BtnImageSelect: UIButton!
    
    //Variables
    var imageString = ""
    var Name = ""
    var FamilyName = ""
    var Nationality = ""
    var EmailID = ""
    var Get_FirstCell_Number = ""
    var Get_UpdatedCellNo = ""
    var CurrentCellNumber = ""
    var Get_CPR_Number = ""
    var Loginauthentication_key_val = ""
    let defaults = UserDefaults.standard
    var BtnCofirmColor = UIColor(red:243, green:243, blue:241, alpha:1.0)
    let picker = UIImagePickerController()
    var imagePicker = UIImagePickerController()
    var maxLenName:Int = 20
    var maxLenFamilyName:Int = 20
    var MaxLenNationality:Int = 20
    var TrimEmailID:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        imagePicker.delegate = self as! UIImagePickerControllerDelegate & UINavigationControllerDelegate
        outerImageView.layer.cornerRadius = outerImageView.frame.height / 2
        ImgCamera.layer.cornerRadius = ImgCamera.frame.height / 2
        
        self.txtFieldEmail.delegate = self
        
        self.Loginauthentication_key_val = UserDefaults.standard.string(forKey: "authentication_key")!//get
        self.Get_CPR_Number = UserDefaults.standard.string(forKey: "CPRNoKey")!//get
        self.get_customer_basic_profile_details()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        scrollView.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        addDoneButtonOnKeyboard()
        
        txtFieldEmail.addTarget(self, action: #selector(EditProfileViewController.txtFieldChanged),
                                  for: .editingChanged)
        
    }
    
    //method to hide keyboard
    @objc func dismissKeyboard() {
        scrollView.contentOffset.y = 0
        // checkForValidation()
        view.endEditing(true)
    }
    
    
    
    @IBAction func BtnContinueClick(_ sender: Any) {
        self.validation()
        
    }
    
    @IBAction func BtnBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func validation()
    {
         self.TrimEmailID = txtFieldEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines) as! String
        if (isValidEmail(testStr: TrimEmailID) == false)
        {
//            BtnContinue.backgroundColor = COLOR.GrayColor
            self.view.makeToast("Please enter valid email address")
        }
        else
        {
            
            self.EmailID = txtFieldEmail.text!
            print("EmailID: \(self.EmailID)")
            self.TrimEmailID = self.EmailID.trimmingCharacters(in: .whitespacesAndNewlines)
            self.confirm_personal_details()
            
            if (txtFieldEmail.text != "")
            {
                BtnContinue.isEnabled = true
//                self.BtnContinue.setTitleColor(COLOR.WhiteColor, for: .normal)
//                BtnContinue.backgroundColor = COLOR.PinkColor
            }
        }
    }
    
    func confirm_personal_details()
    {
        let GetName = self.lblName.text
        let GetFamilyName = self.lblIfamilyName.text
        let GetNationality = self.lblNationality.text
        let GetMobileNo = self.lblCellNumber.text
        let GetEmailId = self.txtFieldEmail.text
        
        
        let url = ConstantsClass.baseUrl + ConstantsClass.confirm_personal_details_customer
        print("url: \(url)")
        let parameters = [
            "name" : "\(GetName ?? "")",
            "family_name" : "\(GetFamilyName ?? "")",
            "nationality" : "\(GetNationality ?? "")",
            "email_id" : "\(self.TrimEmailID)",
            "cell_no" : "\(GetMobileNo ?? "")",
            "cpr_no" : "\(self.Get_CPR_Number)",
            "bitmap_image" : "\(self.imageString)"
            ] as [String : Any]
        
        print("parametersPersonaldetails: \(parameters)")
        Helper.sharedInstance.showLoader()
        Alamofire.request(url
            , method: .post, parameters: parameters, encoding:JSONEncoding.default).responseJSON
            {
                response in
                
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if let status = response["status"] as? Bool {
                        
                        if status{
                            print("confirm_personal_details_customer_success")
                            
                            
                            self.navigationController?.backToViewController(viewController: DashBoardTabViewController.self)//pop to specifica view controller
//                            self.navigationController?.popViewController(animated: true)
                            Helper.sharedInstance.hideLoader()
                            
                        }
                    }
                    else
                    {
                        self.view.makeToast("Something went wrong", duration: 3.0, position: .bottom)
                        print("Something went wrong")
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
    }
    
    func get_customer_basic_profile_details()
    {
        
        let urlString = ConstantsClass.baseUrl + ConstantsClass.get_customer_basic_profile_details
        print("urlString: \(urlString)")
        
        let headers = [
            "Authorization": "\(self.Loginauthentication_key_val)"
        ]
        Alamofire.request(urlString, method: .get, parameters: nil, encoding: URLEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if (response["status"] as? Bool) != nil {
                        if let respArray = response["response"] as? [[String:Any]] {
                            print("respArray: \(respArray)")
                            for Profiledata in respArray
                            {
                                let ProfileName = Profiledata["customer_name"]as? String ?? ""
                                let ProfileFamilyName = Profiledata["customer_family_name"]as? String ?? ""
                                let ProfileNationality = Profiledata["customer_nationality"]as? String ?? ""
                                let ProfileCelNumber = Profiledata["customer_cell_phone_number"]as? String ?? ""
                                let ProfileImage = Profiledata["customer_profile"]as? String ?? ""
                                let ProfileEmailID = Profiledata["customer_email_id"]as? String ?? ""
                                
                                
                                self.lblName.text = ProfileName
                                self.lblIfamilyName.text = ProfileFamilyName
                                self.lblNationality.text = ProfileNationality
                                self.lblCellNumber.text = ProfileCelNumber
                                self.txtFieldEmail.text = ProfileEmailID
                                self.outerImageView.downloaded(from: ProfileImage)
                                self.outerImageView.contentMode = .scaleAspectFill 
                                self.imgUser.isHidden = true
                                
                            }
                            
                            
                        }
                    }
                    // Make sure to update UI in main thread
                    DispatchQueue.main.async {
                        
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
    }
    
    func delete_customer_account()
    {
        let urlString = ConstantsClass.baseUrl + ConstantsClass.delete_customer_account
        print("urlString: \(urlString)")
        
        let headers = [
            "Authorization": "\(self.Loginauthentication_key_val)"
        ]
        
        Alamofire.request(urlString, method: .get, parameters: nil, encoding: URLEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if let status = response["status"] as? Bool {
                        print("Get status true here")
                        
                        if status
                        {
                
                                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "GetStartedViewController") as! GetStartedViewController
                                
                                self.navigationController?.pushViewController(nextViewController, animated: true)
                            
                            
                        }
                    }
                    // Make sure to update UI in main thread
                    DispatchQueue.main.async {
                        
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
        
        
    }
    
    @IBAction func DeleteBtnClick(_ sender: Any) {
        
        let alert = UIAlertController(title: "", message: "Are you sure you want to delete account?", preferredStyle: UIAlertController.Style.alert)
        
       
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
//        alert.addAction(UIAlertAction(title: "Ok",
//                                      style: UIAlertAction.Style.destructive,
//                                      handler: {(_: UIAlertAction!) in
//                                        //delete account action
//                                        self.delete_customer_account()
//        }))
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
           //delete account action
            self.delete_customer_account()
        }))
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    
    @IBAction func BtnSelectImageClick(_ sender: Any) {
        
        print("camera button click")
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as! UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func SelectImageBtnTapped(_ sender: Any) {
        print("camera button click")
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as! UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    //What to do when the picker returns with a photo
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        
        if let chosenImage = info[UIImagePickerController.InfoKey.originalImage]as? UIImage //2
               {
                
                imgUser.image = self.resizeImage(image: chosenImage, targetSize: CGSize(width: 350, height: 200))
                self.imgUser.image = self.resizeImage(image: chosenImage, targetSize: CGSize(width: chosenImage.size.width/10, height: chosenImage.size.height/10))
                outerImageView.contentMode = .scaleAspectFill //3
                outerImageView.image = self.imgUser.image //4
                 self.imgUser.isHidden = true
                  
                }
                  
            picker.dismiss(animated: true, completion: nil)
            
            let imageData:Data = (self.imgUser.image?.jpegData(compressionQuality: 0.7)!)!
            let imageStr = imageData.base64EncodedString()
            self.imageString = imageStr
        
        
        
        
        
//        self.imgUser.isHidden = true
//        let chosenImage = info[UIImagePickerController.InfoKey.originalImage]
//            as? UIImage //2
//        outerImageView.contentMode = .scaleAspectFit //3
//        outerImageView.image = chosenImage //4
//        dismiss(animated: true, completion: nil) //5
//
//        let scaledImage = UIImage.scaleImageWithDivisor(img: chosenImage!, divisor: 3)
//        //        print("scaledImage:",scaledImage)
//
//        let imageData:NSData = scaledImage.pngData()! as NSData
//        self.imageString = imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        
    }
    
    //What to do if the image picker cancels.
    private func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
         let size = image.size
         let widthRatio  = targetSize.width  / size.width
         let heightRatio = targetSize.height / size.height

         // Figure out what our orientation is, and use that to form the rectangle
         var newSize: CGSize
         if(widthRatio > heightRatio) {
             newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
         } else {
             newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
         }

         // This is the rect that we've calculated out and this is what is actually used below
         let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)

         // Actually do the resizing to the rect using the ImageContext stuff
         UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
         image.draw(in: rect)
         let newImage = UIGraphicsGetImageFromCurrentImageContext()!
         UIGraphicsEndImageContext()

         return newImage
     }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 20))
        doneToolbar.barStyle = .default
        let Cancel: UIBarButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(self.doneButtonAction))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [Cancel,flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.txtFieldEmail.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        
        self.txtFieldEmail.resignFirstResponder()
    }
    
    
    
    @objc func keyboardWillShow(notification: NSNotification)
    {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                //                        self.view.frame.origin.y -= keyboardSize.height
                self.view.frame.origin.y -= 150
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification)
    {
        if self.view.frame.origin.y != 0 {
            //                    self.view.frame.origin.y = 0
            self.view.frame.origin.y += 150
        }
    }
    
    //horizontal scrollview lock
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x != 0 {
            scrollView.contentOffset.x = 0
        }
    }
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func validate(value: String) -> Bool
    {
        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    @objc func txtFieldChanged(_ textField: UITextField) {
           if (txtFieldEmail.text!.count == 0)
          {
              self.BtnContinue.setTitleColor(COLOR.GrayColor, for: .normal)
              BtnContinue.backgroundColor = COLOR.LightGrayColor
          }
          else
          {
              self.BtnContinue.setTitleColor(COLOR.WhiteColor, for: .normal)
              BtnContinue.backgroundColor = COLOR.PinkColor
          }
      }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
