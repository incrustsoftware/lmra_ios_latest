//
//  AccountSettingViewController.swift
//  LMRA
//
//  Created by Mac on 08/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire

class AccountSettingViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
 
    
    @IBOutlet weak var Btnback: UIButton!
    
    @IBOutlet weak var BtnAddAddress: UIButton!
    @IBOutlet weak var AccountSettingTableView: UITableView!
 
    
    var AddressDict = [NSDictionary]()
    var authentication_key_val = ""
    var FullAddress = ""
    var Address_IdString = ""
    var Address_Id:Int!
    var GetAddressNickName = ""
    var GetFullAddress = ""
    var BtnSelctedTag:Int!
  
    let defaults = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()

//        UserDefaults.standard.set(false,forKey: "SelectedGetAddressIdKey") // set false for add location
        self.authentication_key_val = UserDefaults.standard.string(forKey: "authentication_key")!//get
        print("authentication_key_val \(authentication_key_val)")
        AccountSettingTableView.separatorStyle = UITableViewCell.SeparatorStyle.none

      
    }
    override func viewWillAppear(_ animated: Bool) {
        self.manage_addresses_customer()
    }
    @IBAction func BackBtnClick(_ sender: Any) {
        
         self.navigationController?.popViewController(animated: true)
    }
   
    @IBAction func BtnAddAddressClick(_ sender: Any) {
       UserDefaults.standard.set(true,forKey: "AddAdressFromAccountSetting")
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SetAddressSettingVC") as! SetAddressAddressSettingViewController
        
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     return self.AddressDict.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = AccountSettingTableView.dequeueReusableCell(withIdentifier: "AccountCell", for: indexPath as IndexPath) as! AccountSettingTableViewCell
    
    self.FullAddress = ""
    let AllData =  self.AddressDict[indexPath.row]
    print("AllDataCellForRow:",AllData)
    
    let Area = AllData["area"] as? String ?? ""
    let Appartment = AllData["appartment"] as? String ?? ""
    let Street = AllData["street"] as? String ?? ""
    let BuildingName = AllData["building_name"] as? String ?? ""
    let Floor = AllData["floor"] as? String ?? ""
    let AppartmentNo = AllData["appartment_no"] as? String ?? ""
    let AdditionalDirections = AllData["additional_directions"] as? String ?? ""
    
    
    self.FullAddress += Area
    self.FullAddress += ", "
    self.FullAddress += Appartment
    self.FullAddress += ", "
    self.FullAddress += Street
    self.FullAddress += ", "
    self.FullAddress += BuildingName
    self.FullAddress += ", "
    self.FullAddress += Floor
    self.FullAddress += ", "
    self.FullAddress += AppartmentNo
    self.FullAddress += ", "
    self.FullAddress += AdditionalDirections
    
    cell.LblFullAddress.text = self.FullAddress
    cell.LblAddressType.text = AllData["address_nickname"] as? String
    
    cell.BtnForwardArrow.tag = indexPath.row
          cell.BtnForwardArrow.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
    
         if self.BtnSelctedTag == indexPath.item
           {
//            
//            self.Address_Id = AllData["id"] as? Int
//            self.Address_IdString = String(self.Address_Id)
//            UserDefaults.standard.set(self.Address_IdString,forKey: "SelectedAccountAddAddressID")
//            print("Address_IdString :\(self.Address_IdString)")
            
            //let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
           // let nextViewController = storyBoard.instantiateViewController(withIdentifier: "EditAddressDetailsVC") as! EditAddressDetailsViewController
            
            //self.navigationController?.pushViewController(nextViewController, animated: true)
                
           }
    
    
    return cell
  }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let cell = AccountSettingTableView.cellForRow(at: indexPath) as! AccountSettingTableViewCell
        
            let AllData =  self.AddressDict[indexPath.row]
            self.Address_Id = AllData["id"] as? Int
            self.Address_IdString = String(self.Address_Id)

      
            UserDefaults.standard.set(self.Address_IdString,forKey: "SelectedAccountAddAddressID")

            print("Address_IdString :\(self.Address_IdString)")
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "EditAddressDetailsVC") as! EditAddressDetailsViewController
        
        self.navigationController?.pushViewController(nextViewController, animated: true)
        

        }
    
    
    @objc func buttonPressed(sender:UIButton){
       
        self.BtnSelctedTag = sender.tag
        print("Selected item in row \(sender.tag)")
    self.AccountSettingTableView.reloadData()
        sender.isSelected = true
        
        
    }
    
    func manage_addresses_customer()
    {

        let urlString = ConstantsClass.baseUrl + ConstantsClass.manage_addresses_customer
        print("urlString: \(urlString)")
        
        let headers = [
            "Authorization": "\(self.authentication_key_val)"
        ]
            Alamofire.request(urlString, method: .get, parameters: nil, encoding: URLEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any] {
                    print("responseLocation: \(response)")
                    
                    if (response["status"] as? Bool) != nil {
                        if let respArray = response["response"] as? [[String:Any]] {
                            print("respArray: \(respArray)")
                            
                            
                            self.AddressDict = response["response"] as! [NSDictionary]
                            print("AddressDict:",self.AddressDict)
                            
                        }
                    }
                    // Make sure to update UI in main thread
                    DispatchQueue.main.async {
                        print("address managed successfully")
                        self.AccountSettingTableView.reloadData()
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
    }
    
    
    
}
