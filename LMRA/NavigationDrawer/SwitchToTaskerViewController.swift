//
//  SwitchToTaskerViewController.swift
//  LMRA
//
//  Created by Mac on 07/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire

class SwitchToTaskerViewController: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var BtnSwitchTaskerView: UIView!
    @IBOutlet weak var BtnSwitchTasker: UIButton!
    @IBOutlet weak var BnSwitchtaskerArrow: UIButton!
    @IBOutlet weak var Manview: UIView!
    var Loginauthentication_key_val = ""
    var StringTaskerProfileSet = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        Manview.layer.cornerRadius = 2
        BtnSwitchTaskerView.layer.cornerRadius = 2
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func BachBtnClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func CancelBtnClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @available(iOS 12.0, *)
    @IBAction func SwitchToTaskerClick(_ sender: Any) {
        self.customer_switch_to_tasker()
        
    }
    
    
    @available(iOS 12.0, *)
    func customer_switch_to_tasker()
    {
        self.Loginauthentication_key_val = UserDefaults.standard.string(forKey: "authentication_key")!//get
        print("Loginauthentication_key_val:",self.Loginauthentication_key_val)
        
        let urlString = ConstantsClass.baseUrl + ConstantsClass.customer_switch_to_tasker
        print("urlString: \(urlString)")
        
        let headers = [
            "Authorization": "\(self.Loginauthentication_key_val)"
        ]
        print("headers: \(headers)")
        Alamofire.request(urlString, method: .get, parameters: nil, encoding: URLEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if (response["status"] as? Bool) != nil {
                        if let respArray = response["response"] as? [String:Any] {
                            print("respArray: \(respArray)")
                            
                            let TaskerAuthKey = respArray["tasker_authentication_key"]as? String ?? ""
                            
                            guard let userType = Optional.some(respArray["user_type"]) else {
                                return
                            }
                            
                            UserDataManager.shared.change(authKey: "\(TaskerAuthKey)", userType: Int("\(userType ?? "2")") ?? UserDataManager.UserType.TASKER.rawValue)
                            
                            let TaskerID = respArray["tasker_id"]as? String ?? ""
                            let TaskerProfileSet = respArray["tasker_profile_set"]as? Int ?? 0
                            self.StringTaskerProfileSet = String(TaskerProfileSet)
                            print("TaskerProfileSet: \(TaskerProfileSet)")
                            if self.StringTaskerProfileSet == "0"
                            {
                                let storyBoard : UIStoryboard = UIStoryboard(name: "Tasker", bundle:nil)
                                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "Tasker_SelectCategoryViewController") as! Tasker_SelectCategoryViewController
                                self.navigationController?.pushViewController(nextViewController, animated: true)
                            }
                            else if self.StringTaskerProfileSet == "1"
                            {
                                
                                let appDelegate = UIApplication.shared.delegate as? AppDelegate
                                let rootViewController = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "TaskerDasboardVC")
                                
                                let navigationController = UINavigationController(rootViewController: rootViewController)
                                let rightViewController = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "SideMenuViewController")
                                let sideMenuController = LGSideMenuController(rootViewController: navigationController,
                                                                              leftViewController: nil,
                                                                              rightViewController: rightViewController)
                                sideMenuController.leftViewWidth = 0
                                sideMenuController.leftViewPresentationStyle = .slideBelow;
                                
                                sideMenuController.rightViewWidth = (appDelegate?.window?.bounds.size.width ?? 320) - 20
                                sideMenuController.leftViewPresentationStyle = .slideBelow
                                navigationController.isNavigationBarHidden = true
                                navigationController.interactivePopGestureRecognizer?.delegate = self
                                //self.interactivePopGestureRecognizer.enabled = NO;
                                appDelegate?.window?.rootViewController = sideMenuController
                                appDelegate?.window?.makeKeyAndVisible()
                            }
                            
                            
                        }
                    }
                    // Make sure to update UI in main thread
                    DispatchQueue.main.async {
                        print("customer_switch_to_tasker_success")
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
        
        
    }
    
    
    //    func customer_switch_to_tasker()
    //    {
    //        let urlString = ConstantsClass.baseUrl + ConstantsClass.customer_switch_to_tasker
    //        print("urlString: \(urlString)")
    //
    //        let headers = [
    //            "Authorization": "\(self.Loginauthentication_key_val)"
    //        ]
    //        Alamofire.request(urlString, method: .get, parameters: nil, encoding: URLEncoding.default,headers: headers).responseJSON
    //            {
    //                response in
    //                if let response = response.result.value as? [String:Any] {
    //                    print("response: \(response)")
    //
    //                    if (response["status"] as? Bool) != nil {
    //                        if let respArrayOpenTask = response["response"] as? [[String:Any]] {
    //                            print("respArraySwitchToTasker: \(respArrayOpenTask)")
    //
    //
    //                        }
    //                    }
    //                    // Make sure to update UI in main thread
    //                    DispatchQueue.main.async {
    //                       print("customer_switch_to_tasker_success")
    //                    }
    //                }
    //                else
    //                {
    //                    print("Error occured") // serialized json response
    //                }
    //        }
    //
    //
    //    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
