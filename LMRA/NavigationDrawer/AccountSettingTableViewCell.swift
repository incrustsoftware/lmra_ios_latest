//
//  AccountSettingTableViewCell.swift
//  LMRA
//
//  Created by Mac on 08/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class AccountSettingTableViewCell: UITableViewCell {

    @IBOutlet weak var LblAddressType: UILabel!
    @IBOutlet weak var AddressView: UIView!
    
   
      @IBOutlet weak var BtnForwardArrow: UIButton!
    @IBOutlet weak var LblFullAddress: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        AddressView.layer.shadowColor = UIColor.gray.cgColor
        AddressView.layer.shadowOpacity = 0.5
        AddressView.layer.shadowOffset = CGSize.zero
        AddressView.layer.shadowRadius = 6
        AddressView.layer.cornerRadius = 5
        clipsToBounds = false
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
