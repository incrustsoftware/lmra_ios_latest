//
//  ResponseHeader.swift
//  LMRA
//
//  Created by Mac on 17/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//


import Foundation

//MARK: ******* Headers for Request *********
class RequestHeader {
    class func getHeadersWithAuth()-> [String:String] {
        let headers = [
            "Accept"       : "application/json",
            "cache-control": "no-cache",
            "content-type": "application/x-www-form-urlencoded",
            "Authorization": "\(UserDataManager.shared.getAccesToken())"
        ]
        return headers
    }
    
    class func getHeadersWithMulipartData()-> [String:String] {
        let headers = [
            "Accept"        : "application/json",
            "Content-type": "multipart/form-data",
            "Authorization": "\(UserDataManager.shared.getAccesToken())"
        ]
        return headers as [String : String]
    }
}




