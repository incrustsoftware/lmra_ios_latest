//
//  APIEngine.swift
//  Food Delivery
//
//  Created by QualtiasIT on 25/07/19.
//  Copyright © 2019 Aniket. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import ReachabilitySwift
import SwiftyJSON
import AlamofireSwiftyJSON
import ReachabilitySwift

class APIEngine: NSObject {
    
    let FCMURL = "https://fcm.googleapis.com/fcm/send"

    let LoginURL = "login"
    let restaurentListURL = "showRestaurants"
    let bannerImageUrl = "showAppSliderImages"
    let dealsUrl = "showDeals"
    let showRestaurantsMenu = "showRestaurantsMenu"
    let AppVersion = "user/appVersion"
    let GetBannerUrl = "user/bannerUrl"
    let showMenuExtraItems = "showMenuExtraItems"
    let addRestaurentRatings = "addRestaurantRating"
    let showRestaurantRatings = "showRestaurantRatings"
    let showDeliveryAddresses = "showDeliveryAddresses"
    let forgotPassword = "forgotPassword"
    let addFavouriteRestaurant = "addFavouriteRestaurant"
    let placeOrder = "placeOrder"
    let addDeliveryAddress = "addDeliveryAddress"
    let showRestaurantsSpecialities = "showRestaurantsSpecialities"
    let showRestaurantsAgainstSpeciality = "showRestaurantsAgainstSpeciality"
    let editUserProfile = "editUserProfile"
    let changePassword = "changePassword"
    let addPaymentMethod = "addPaymentMethod"
    let showOrders = "showOrders"
    let showOrderDetail = "showOrderDetail"
    let showFavouriteRestaurants = "showFavouriteRestaurants"
    let showRestaurantDeals = "showRestaurantDeals"
    
    let PLATFORM = "Food Delivery"
    let kDownloadAppURL = ""
    let signUpUrl = "registerUser"
    let view:UIView = UIView()
    
    // let fcmURL = "updateFcm"
   // let messageURL = "reportList/messageListing"

    //LIVE
    //    let BaseURL = "https://approvals-api.mecure.com/"
    //    let API_VERSION = "1"

    //STAGING
   // let BaseURL = "http://qualitasitproducts.com/akash/fooddelivery/mobileapp_api/api/"
    let BaseURL = "https://www.qitstaging.com/food_delivery/api/api/"
    let API_VERSION = "1"
  
    static let sharedInstance = APIEngine()

    func getNormalHeaders()->[String : String]
    {
        let deviceID : String = UIDevice.current.identifierForVendor!.uuidString

        return ["os": "ios", "duid" : deviceID,"Content-Type":"application/x-www-form-urlencoded"]

    }

//    func getAuthHeaders()->[String : String]
//    {
//        let deviceID : String = UIDevice.current.identifierForVendor!.uuidString
//
//        return ["os": "ios", "duid" : deviceID, "token": UserDefaults.standard.string(forKey: UD_TOKEN)!,"Content-Type":"application/x-www-form-urlencoded"]
//
//    }

    func failureCheck(response : DataResponse<JSON>) -> Bool {
        print(response)
        if (response.response?.statusCode == 200) {

            let jsonResponse = try!  JSON(data: response.data!)

            if (jsonResponse["active"].intValue == 0)
            {
                if (jsonResponse["msg"].stringValue == "Invalid Token")
                {
                   // Helper.sharedInstance.logout()
                    return false;
                }
            }

            return true
        }
        else
        {
            do {
                let jsonResponse = try! JSON(data: response.data!)

                let message = jsonResponse["msg"].stringValue

                if (message.count != 0) {

                    Helper.sharedInstance.showAlert(title: "Message", message: message)
                }
                else
                {

                    let jsonResponse = try!  JSON(data: response.data!)

                    let errorMessage = jsonResponse["msg"].stringValue

                    if (errorMessage != nil && errorMessage.count != 0) {

                        Helper.sharedInstance.showAlert(title: "Message", message: jsonResponse["msg"].stringValue)

                    } else {
                        Helper.sharedInstance.showAlert(title: "Message", message: "Something went wrong!")
                    }
                }
                if (response.response?.statusCode == 401 || jsonResponse["status"].intValue == 401) {
                  //  Helper.sharedInstance.logout()
                }
            } catch let error {
                print(error)
            }
            //            if (jsonResponse["status"].intValue == 401) {
            //                Helper.sharedInstance.logout()
            //            }
            return false
        }
    }

    // MARK: BASE CALLS

    func postAPI(url: String,  param :[String:String], completionHandler:@escaping ( DataResponse<JSON>?, Error?) -> ()) {
       
        if (!Helper.sharedInstance.networkIsAvailableMessage()) {
            return
        }
        var headers: HTTPHeaders = HTTPHeaders()

        headers = self.getNormalHeaders()

        print("URL POST:\(url)")
        print("PARAM :\(param)")
        print("HEADER :\(headers)")
        Helper.sharedInstance.showLoader()
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: headers).validate().responseSwiftyJSON { (response) in
          
            Helper.sharedInstance.hideLoader()
            print("URL POST:\(url)")
            print("PARAM :\(param)")
            print("HEADER :\(headers)")
            print("Response:\(String(describing: response.result.value))")
//            if (self.failureCheck(response: response)) {
//                completionHandler(response, nil)
//            }
        }
    }
    func postRestLAPI(url: String,  param : [String : String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        Helper.sharedInstance.showLoader()
        if (!Helper.sharedInstance.networkIsAvailableMessage()) {
            return
        }
        var headers: HTTPHeaders = HTTPHeaders()
        
        headers = self.getNormalHeaders()
        
//        if (Helper.sharedInstance.isLoggedIN()) {
//
//            headers = self.getAuthHeaders()
//        }
        
        print("URL POST:\(url)")
        print("PARAM :\(param)")
        print("HEADER :\(headers)")
        
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default,headers:headers)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                print("Valdiate")
                print(request)
              //  print(data)
                Helper.sharedInstance.hideLoader()
                return .success
            }
            .responseSwiftyJSON { response in
                debugPrint(response)
                
                //                 if (self.failureCheck(response: response))
                //                 {
                print("In failure")
                print(response)
                completionHandler(response, nil)
                print(response)
                // }
                
        }
        
        //Helper.sharedInstance.showLoader()
        //        Alamofire.request(url,method: .post, parameters: param, encoding:JSONEncoding.default, headers: headers).validate().responseSwiftyJSON { (response) in
        //            Helper.sharedInstance.hideLoader()
        ////            print("URL POST:\(url)")
        ////            print("PARAM :\(param)")
        ////            print("HEADER :\(headers)")
        ////            print("Response:\(String(describing: response.result.value))")
        //
        //
        //            if (self.failureCheck(response: response)) {
        //                print("In failure")
        //                completionHandler(response, nil)
        //            }
        //   }
    }

    func postLAPI(url: String,  param : [String : String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        Helper.sharedInstance.showLoader()
        if (!Helper.sharedInstance.networkIsAvailableMessage()) {
            return
        }
        guard let auth_key = UserDefaults.standard.string(forKey: UserDataManager_KEY.authentication_key) else {
            return
        }
       let headers = [
            "Authorization": "\(auth_key)"
        ]
        
//        if (Helper.sharedInstance.isLoggedIN()) {
//
//            headers = self.getAuthHeaders()
//        }

        print("URL POST:\(url)")
        print("PARAM :\(param)")
        print("HEADER :\(headers)")

        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default,headers:headers)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                //Helper.sharedInstance.hideLoader()
                // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                print("Valdiate")
                print(request)
                print(data)
               Helper.sharedInstance.hideLoader()
                return .success
            }
            .responseSwiftyJSON { response in
                debugPrint(response)
                
//                 if (self.failureCheck(response: response))
//                 {
                    print("In failure")
                    print(response)
                    completionHandler(response, nil)
                    print(response)
                 // }
                
        }
    }
    
    
    func postArraywithstringLAPI(url: String,  param : [String : Any], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
                    Helper.sharedInstance.showLoader()
                    if (!Helper.sharedInstance.networkIsAvailableMessage()) {
                        return
                    }
                    guard let auth_key = UserDefaults.standard.string(forKey: "authentication_key") else {
                        return
                    }
                   let headers = [
                        "Authorization": "\(auth_key)"
                    ]
                    
            //        if (Helper.sharedInstance.isLoggedIN()) {
            //
            //            headers = self.getAuthHeaders()
            //        }

                    print("URL POST:\(url)")
                    print("PARAM :\(param)")
                    print("HEADER :\(headers)")

                    Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default,headers:headers)
                        .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                            print("Progress: \(progress.fractionCompleted)")
                        }
                        .validate { request, response, data in
                            // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                            print("Valdiate")
                            print(request)
                            print(data)
                           Helper.sharedInstance.hideLoader()
                            return .success
                        }
                        .responseSwiftyJSON { response in
                            debugPrint(response)
                            
            //                 if (self.failureCheck(response: response))
            //                 {
                                print("In failure")
                                print(response)
                                completionHandler(response, nil)
                                print(response)
                             // }
                            
                    }
            //Helper.sharedInstance.showLoader()
    //        Alamofire.request(url,method: .post, parameters: param, encoding:JSONEncoding.default, headers: headers).validate().responseSwiftyJSON { (response) in
    //            Helper.sharedInstance.hideLoader()
    ////            print("URL POST:\(url)")
    ////            print("PARAM :\(param)")
    ////            print("HEADER :\(headers)")
    ////            print("Response:\(String(describing: response.result.value))")
    //
    //
    //            if (self.failureCheck(response: response)) {
    //                print("In failure")
    //                completionHandler(response, nil)
    //            }
         //   }
        }

        func postALAPI(url: String,  param : [String : [String]], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
                Helper.sharedInstance.showLoader()
                if (!Helper.sharedInstance.networkIsAvailableMessage()) {
                    return
                }
                guard let auth_key = UserDefaults.standard.string(forKey: "authentication_key") else {
                    return
                }
               let headers = [
                    "Authorization": "\(auth_key)"
                ]
                
        //        if (Helper.sharedInstance.isLoggedIN()) {
        //
        //            headers = self.getAuthHeaders()
        //        }

                print("URL POST:\(url)")
                print("PARAM :\(param)")
                print("HEADER :\(headers)")

                Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default,headers:headers)
                    .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                        print("Progress: \(progress.fractionCompleted)")
                    }
                    .validate { request, response, data in
                        // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                        print("Valdiate")
                        print(request)
                        print(data)
                       Helper.sharedInstance.hideLoader()
                        return .success
                    }
                    .responseSwiftyJSON { response in
                        debugPrint(response)
                        
        //                 if (self.failureCheck(response: response))
        //                 {
                            print("In failure")
                            print(response)
                            completionHandler(response, nil)
                            print(response)
                         // }
                        
                }
        //Helper.sharedInstance.showLoader()
//        Alamofire.request(url,method: .post, parameters: param, encoding:JSONEncoding.default, headers: headers).validate().responseSwiftyJSON { (response) in
//            Helper.sharedInstance.hideLoader()
////            print("URL POST:\(url)")
////            print("PARAM :\(param)")
////            print("HEADER :\(headers)")
////            print("Response:\(String(describing: response.result.value))")
//
//
//            if (self.failureCheck(response: response)) {
//                print("In failure")
//                completionHandler(response, nil)
//            }
     //   }
    }

    func postWLAPI(url: String,  param : [String : String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {

        if (!Helper.sharedInstance.networkIsAvailableMessage()) {
            return
        }
        var headers: HTTPHeaders = HTTPHeaders()

        headers = self.getNormalHeaders()
 
//        if (Helper.sharedInstance.isLoggedIN()) {
//
//            headers = self.getAuthHeaders()
//        }
        print("URL POST:\(url)")
        print("PARAM :\(param)")
        print("HEADER :\(headers)")


        Alamofire.request(url,method: .post, parameters: param, encoding: JSONEncoding.default, headers: headers).validate().responseSwiftyJSON { (response) in
            print("URL POST:\(url)")
            print("PARAM :\(param)")
            print("HEADER :\(headers)")
            print("Response:\(String(describing: response.result.value))")

            if (self.failureCheck(response: response)) {
                completionHandler(response, nil)
            }
        }
    }

    func putLAPI(url: String,  param : [String : String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {

        if (!Helper.sharedInstance.networkIsAvailableMessage()) {
            return
        }
        var headers: HTTPHeaders = HTTPHeaders()
        headers = self.getNormalHeaders()

//        if (Helper.sharedInstance.isLoggedIN()) {
//
//            headers = self.getAuthHeaders()
//        }
        print("URL PUT:\(url)")
        print("PARAM :\(param)")
        print("HEADER :\(headers)")
        Helper.sharedInstance.showLoader()
        Alamofire.request(url,method: .put, parameters: param, encoding: JSONEncoding.default, headers: headers).validate().responseSwiftyJSON { (response) in
            Helper.sharedInstance.hideLoader()
            print("URL PUT:\(url)")
            print("PARAM :\(param)")
            print("HEADER :\(headers)")
            print("Response:\(String(describing: response.result.value))")
            if (self.failureCheck(response: response)) {
                completionHandler(response, nil)
            }
        }
    }

    func deleteLAPI(url: String,  param : [String : String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {

        if (!Helper.sharedInstance.networkIsAvailableMessage()) {
            return
        }
        var headers: HTTPHeaders = HTTPHeaders()
        headers = self.getNormalHeaders()
//        if (Helper.sharedInstance.isLoggedIN()) {
//            headers = self.getAuthHeaders()
//        }
        print("URL DELETE:\(url)")
        print("PARAM :\(param)")
        print("HEADER :\(headers)")

       // Helper.sharedInstance.showLoader()
        Alamofire.request(url,method: .delete, parameters: param, encoding: JSONEncoding.default, headers: headers).validate().responseSwiftyJSON { (response) in

            Helper.sharedInstance.hideLoader()

            print("URL DELETE:\(url)")
            print("PARAM :\(param)")
            print("HEADER :\(headers)")
            print("Response:\(String(describing: response.result.value))")

            if (self.failureCheck(response: response))
            {
                completionHandler(response, nil)
            }

        }

    }

    func getLAPI(url: String,  param : [String : String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {

        if (!Helper.sharedInstance.networkIsAvailableMessage())
        {
            return
        }

     //   var headers: HTTPHeaders = HTTPHeaders()

       // headers = self.getNormalHeaders()

//        if (Helper.sharedInstance.isLoggedIN()) {
//
//            headers = self.getAuthHeaders()
//        }
        guard let auth_key = UserDefaults.standard.string(forKey: "authentication_key") else {
             return
         }
        let headers = [
             "Authorization": "\(auth_key)"
         ]

        Helper.sharedInstance.showLoader()

        print("URL GET:\(url)")
        print("PARAM :\(param)")
        print("HEADER :\(headers)")

        Alamofire.request(url, method: .get, parameters: param, encoding: JSONEncoding.default,headers:headers ).validate().responseSwiftyJSON { (response) in

            Helper.sharedInstance.hideLoader()

            print("URL GET:\(url)")
            print("PARAM :\(param)")
            print("HEADER :\(headers)")
            print("RESPONSE RESULT :\(response)")

//            if (self.failureCheck(response: response))
//            {
//                completionHandler(response, nil)
//            }
        }

    }
    func getWithoutParamLAPI(url: String, completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {

            if (!Helper.sharedInstance.networkIsAvailableMessage())
            {
                return
            }

         //   var headers: HTTPHeaders = HTTPHeaders()

           // headers = self.getNormalHeaders()

    //        if (Helper.sharedInstance.isLoggedIN()) {
    //
    //            headers = self.getAuthHeaders()
    //        }
            guard let auth_key = UserDefaults.standard.string(forKey: "authentication_key") else {
                 return
             }
            let headers = [
                 "Authorization": "\(auth_key)"
             ]

            Helper.sharedInstance.showLoader()

            print("URL GET:\(url)")
            //print("PARAM :\(param)")
            print("HEADER :\(headers)")

            Alamofire.request(url, method: .get,encoding: JSONEncoding.default,headers:headers ).validate().responseSwiftyJSON { (response) in

            Helper.sharedInstance.hideLoader()

                print("URL GET:\(url)")
               // print("PARAM :\(param)")
                print("HEADER :\(headers)")
                print("RESPONSE RESULT :\(response)")

    //            if (self.failureCheck(response: response))
    //            {
    //                completionHandler(response, nil)
    //            }
            }

        }

    func getWLAPI(url: String,  param : [String : String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {

        if (!Helper.sharedInstance.networkIsAvailableMessage())
        {
            return
        }

        var headers: HTTPHeaders = HTTPHeaders()

        headers = self.getNormalHeaders()

//        if (Helper.sharedInstance.isLoggedIN()) {
//
//            headers = self.getAuthHeaders()
//        }

        Helper.sharedInstance.showLoader()

        print("URL GET:\(url)")
        print("PARAM :\(param)")
        print("HEADER :\(headers)")

        Alamofire.request(url, method: .get, parameters: param, encoding: JSONEncoding.default, headers: headers).validate().responseSwiftyJSON { (response) in

            Helper.sharedInstance.hideLoader()

            print("URL GET:\(url)")
            print("PARAM :\(param)")
            print("HEADER :\(headers)")
            print("RESPONSE RESULT :\(response)")

            if (self.failureCheck(response: response))
            {
                completionHandler(response, nil)
            }
        }

    }


    func getQUICKAPI(url: String,  param : [String : String], headers:[String:String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {

        if (!Helper.sharedInstance.networkIsAvailableMessage())
        {
            return
        }

        Helper.sharedInstance.showLoader()

        print("URL GET:\(url)")
        print("PARAM :\(param)")
        print("HEADER :\(headers)")

        Alamofire.request(url, method: .get, parameters: param, encoding: JSONEncoding.default, headers: headers).validate().responseSwiftyJSON { (response) in

            Helper.sharedInstance.hideLoader()

            print("URL GET:\(url)")
            print("PARAM :\(param)")
            print("HEADER :\(headers)")
            print("Response:\(String(describing: response.result.value))")
            print("RESPONSE RESULT :\(response)")

            if (self.failureCheck(response: response))
            {
                completionHandler(response, nil)
            }
        }

    }

    func postQUICKAPI(url: String,  param : [String : String], headers:[String:String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {

        if (!Helper.sharedInstance.networkIsAvailableMessage())
        {
            return
        }

        Helper.sharedInstance.showLoader()

        print("URL POST:\(url)")
        print("PARAM :\(param)")
        print("HEADER :\(headers)")

        let ulr =  NSURL(string:url as String)
        var request = URLRequest(url: ulr! as URL)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let data = try! JSONSerialization.data(withJSONObject: param, options: JSONSerialization.WritingOptions.prettyPrinted)

        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        if let json = json {
            print(json)
        }
        request.httpBody = json!.data(using: String.Encoding.utf8.rawValue);

        request.allHTTPHeaderFields = headers

        Alamofire.request(request  as! URLRequestConvertible)
            .responseSwiftyJSON { response in
                // do whatever you want here
                print(response.request)
                print(response.response)
                print(response.data)
                print(response.result)

                Helper.sharedInstance.hideLoader()

                print("URL POST:\(url)")
                print("PARAM :\(param)")
                print("HEADER :\(headers)")
                print("Response:\(String(describing: response.result.value))")
                print("RESPONSE RESULT :\(response)")
                print("RESPONSE RESULT ERROR :\(response.result.error)")

                if (self.failureCheck(response: response))
                {
                    completionHandler(response, nil)
                }
                else
                {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Object does not exist"])

                    completionHandler(response, error)
                }

        }

    }

    func postQUICKArrayAPI(url: String,  param : [String : Array<[String:String]>], headers:[String:String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {

        if (!Helper.sharedInstance.networkIsAvailableMessage())
        {
            return
        }

        Helper.sharedInstance.showLoader()

        print("URL POST:\(url)")
        print("PARAM :\(param)")
        print("HEADER :\(headers)")

        let ulr =  NSURL(string:url as String)
        var request = URLRequest(url: ulr! as URL)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let data = try! JSONSerialization.data(withJSONObject: param, options: JSONSerialization.WritingOptions.prettyPrinted)

        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        if let json = json {
            print(json)
        }
        request.httpBody = json!.data(using: String.Encoding.utf8.rawValue);

        request.allHTTPHeaderFields = headers

        Alamofire.request(request  as URLRequestConvertible)
            .responseSwiftyJSON { response in
                // do whatever you want here
                print(response.request)
                print(response.response)
                print(response.data)
                print(response.result)

                Helper.sharedInstance.hideLoader()

                print("URL POST:\(url)")
                print("PARAM :\(param)")
                print("HEADER :\(headers)")
                print("Response:\(String(describing: response.result.value))")
                print("RESPONSE RESULT :\(response)")

                if (self.failureCheck(response: response))
                {
                    completionHandler(response, nil)
                }

        }

    }

    // MARK: POST API

    func loginAPI(param : [String : String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {

        let strURL = "\(BaseURL)\(LoginURL)"

        self.postLAPI(url: strURL, param: param, completionHandler: { response, error in

            completionHandler(response, error)


        })
    }
    
    func signUPAPI(param : [String : String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {

        let strURL = "\(BaseURL)\(signUpUrl)"

        self.postLAPI(url: strURL, param: param, completionHandler: { response, error in

            completionHandler(response, error)


        })
    }
    func getcategoryListAPI(user_type:String, completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(ConstantsClass.baseUrl)\(ConstantsClass.get_category)\(user_type)"
        
        self.getLAPI(url: strURL, param: [:], completionHandler: { response, error in
            
            completionHandler(response, error)
            
            
        })
    }
    func getprofiledetailsAPI(param:[String:String],completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
    
    let strURL = "\(ConstantsClass.baseUrl)\(ConstantsClass.get_Tasker_profileDetails)"
    
        self.getLAPI(url: strURL, param:param,completionHandler: { response, error in
        
        completionHandler(response, error)
        
        
    })
    }
    func getSubcategoryListAPI(param:[String:String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(ConstantsClass.baseUrl)\(ConstantsClass.get_services_subcategories)"
        
        self.postLAPI(url: strURL, param:param, completionHandler: { response, error in
            
            completionHandler(response, error)
            
            
        })
    }
    func getTaskerSubcategoryListAPI(completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(ConstantsClass.baseUrl)\(ConstantsClass.get_tasker_subcategories)"
        
        self.getWithoutParamLAPI(url: strURL,completionHandler: { response, error in
            
            completionHandler(response, error)
            
            
        })
    }
    func getTaskerActiveTaskListAPI(completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(ConstantsClass.baseUrl)\(ConstantsClass.get_active_task)"
        
        self.getWithoutParamLAPI(url: strURL,completionHandler: { response, error in
            
            completionHandler(response, error)
            
            
        })
    }
    func getAddSubcategoryListAPI(param:[String:[String]], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
           
           let strURL = "\(ConstantsClass.baseUrl)\(ConstantsClass.add_service_tasker_subcateogories)"
           
           self.postALAPI(url: strURL, param:param, completionHandler: { response, error in
               
               completionHandler(response, error)
               
               
           })
       }
    func getSamePriceForallHourlyListAPI(param:[String:Any], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(ConstantsClass.baseUrl)\(ConstantsClass.same_price_forall)"
        
        self.postArraywithstringLAPI(url: strURL, param:param, completionHandler: { response, error in
            
            completionHandler(response, error)
            
            
        })
    }
    func getSamePriceForallFiexdListAPI(param:[String:Any], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(ConstantsClass.baseUrl)\(ConstantsClass.add_tasker_samepriceforall)"
        
        self.postArraywithstringLAPI(url: strURL, param:param, completionHandler: { response, error in
            
            completionHandler(response, error)
            
            
        })
    }
    func addLanguagesAPI(param:[String:Any], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(ConstantsClass.baseUrl)\(ConstantsClass.update_Tasker_languages)"
        
        self.postArraywithstringLAPI(url: strURL, param:param, completionHandler: { response, error in
            
            completionHandler(response, error)
            
            
        })
    }
    func addReasonAPI(param:[String:Any], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(ConstantsClass.baseUrl)\(ConstantsClass.get_offline_mode)"
        
        self.postArraywithstringLAPI(url: strURL, param:param, completionHandler: { response, error in
            
            completionHandler(response, error)
            
            
        })
    }
    func declineReasonAPI(param:[String:Any], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(ConstantsClass.baseUrl)\(ConstantsClass.decline_task_invitation)"
        
        self.postArraywithstringLAPI(url: strURL, param:param, completionHandler: { response, error in
            
            completionHandler(response, error)
            
            
        })
    }
    func getTaskDetailsAPI(param:[String:Any], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(ConstantsClass.baseUrl)\(ConstantsClass.get_task_detail)"
        
        self.postArraywithstringLAPI(url: strURL, param:param, completionHandler: { response, error in
            
            completionHandler(response, error)
            
            
        })
    }
    func getTaskInviteAPI(param:[String:Any], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(ConstantsClass.baseUrl)\(ConstantsClass.accept_task_invitation)"
        
        self.postArraywithstringLAPI(url: strURL, param:param, completionHandler: { response, error in
            
            completionHandler(response, error)
            
            
        })
    }
    func getTaskCancelAPI(param:[String:Any], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(ConstantsClass.baseUrl)\(ConstantsClass.cancel_task_invitation)"
        
        self.postArraywithstringLAPI(url: strURL, param:param, completionHandler: { response, error in
            
            completionHandler(response, error)
            
            
        })
    }
    func getupdateProfileAPI(param:[String:Any], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(ConstantsClass.baseUrl)\(ConstantsClass.update_tasker_basic_profile_details)"
        
        self.postArraywithstringLAPI(url: strURL, param:param, completionHandler: { response, error in
            
            completionHandler(response, error)
            
            
        })
    }
    func getAddTaskerRateAPI(param:[String:String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(ConstantsClass.baseUrl)\(ConstantsClass.add_tasker_rate)"
        
        self.postLAPI(url: strURL, param:param, completionHandler: { response, error in
            
            completionHandler(response, error)
            
            
        })
    }
    func getUpdateTaskerRateAPI(param:[String:String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
           
           let strURL = "\(ConstantsClass.baseUrl)\(ConstantsClass.update_rate)"
           
           self.postLAPI(url: strURL, param:param, completionHandler: { response, error in
               
               completionHandler(response, error)
               
               
           })
       }
    func getUpdateTaskerWorkingScheduleAPI(param:[String:String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(ConstantsClass.baseUrl)\(ConstantsClass.add_Working_schedule)"
        
        self.postLAPI(url: strURL, param:param, completionHandler: { response, error in
            
            completionHandler(response, error)
            
            
        })
    }
    func UpdateTaskerWorkingScheduleAPI(param:[String:String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(ConstantsClass.baseUrl)\(ConstantsClass.update_tasker_working_schedule)"
        
        self.postLAPI(url: strURL, param:param, completionHandler: { response, error in
            
            completionHandler(response, error)
            
            
        })
    }
    func getBannerImageAPI(param : [String : String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(BaseURL)\(bannerImageUrl)"
        
        self.postLAPI(url: strURL, param: param, completionHandler: { response, error in
            
            completionHandler(response, error)
            
            
        })
    }
    func getDealsAPI(param : [String : String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(BaseURL)\(dealsUrl)"
        
        self.postLAPI(url: strURL, param: param, completionHandler: { response, error in
            
            completionHandler(response, error)
        })
    }
    func getRestaurentMenuAPI(param : [String : String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(BaseURL)\(showRestaurantsMenu)"
        
        self.postLAPI(url: strURL, param: param, completionHandler: { response, error in
            
            completionHandler(response, error)
        })
    }
    func getRestaurentExtraMenuAPI(param : [String : String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(BaseURL)\(showMenuExtraItems)"
        
        self.postLAPI(url: strURL, param: param, completionHandler: { response, error in
            
            completionHandler(response, error)
        })
    }
    func addRestaurentRatingsAPI(param : [String : String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(BaseURL)\(addRestaurentRatings)"
        
        self.postLAPI(url: strURL, param: param, completionHandler: { response, error in
            
            completionHandler(response, error)
        })
    }
    func showRestaurentRatingsAPI(param : [String : String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(BaseURL)\(showRestaurantRatings)"
        
        self.postLAPI(url: strURL, param: param, completionHandler: { response, error in
            
            completionHandler(response, error)
        })
    }
    func showDeliveryAddressAPI(param : [String : String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(BaseURL)\(showDeliveryAddresses)"
        
        self.postLAPI(url: strURL, param: param, completionHandler: { response, error in
            
            completionHandler(response, error)
        })
    }
    
    func forgotPasswordAPI(param : [String : String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(BaseURL)\(forgotPassword)"
        
        self.postLAPI(url: strURL, param: param, completionHandler: { response, error in
            
            completionHandler(response, error)
        })
    }
    func addFavorateResturentAPI(param : [String : String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(BaseURL)\(addFavouriteRestaurant)"
        
        self.postLAPI(url: strURL, param: param, completionHandler: { response, error in
            
            completionHandler(response, error)
        })
    }
    func placeOrderAPI(param : [String : String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(BaseURL)\(placeOrder)"
        
        self.postLAPI(url: strURL, param: param, completionHandler: { response, error in
            
            completionHandler(response, error)
        })
    }
    func addDeliveryAddressAPI(param : [String : String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(BaseURL)\(addDeliveryAddress)"
        
        self.postLAPI(url: strURL, param: param, completionHandler: { response, error in
            
            completionHandler(response, error)
        })
    }
    func showRestaurentSpecialityAPI(param : [String : String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(BaseURL)\(showRestaurantsSpecialities)"
        
        self.postLAPI(url: strURL, param: param, completionHandler: { response, error in
            
            completionHandler(response, error)
        })
    }
    func showRestaurentAgainstSpecialityAPI(param : [String : String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(BaseURL)\(showRestaurantsAgainstSpeciality)"
        
        self.postRestLAPI(url: strURL, param: param, completionHandler: { response, error in
            
            completionHandler(response, error)
        })
    }
    func editProfileAPI(param : [String : String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(BaseURL)\(editUserProfile)"
        
        self.postLAPI(url: strURL, param: param, completionHandler: { response, error in
            
            completionHandler(response, error)
        })
    }
    func changPasswordAPI(param : [String : String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(BaseURL)\(changePassword)"
        
        self.postLAPI(url: strURL, param: param, completionHandler: { response, error in
            
            completionHandler(response, error)
        })
    }
    func addPaymentDetailsAPI(param : [String : String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(BaseURL)\(addPaymentMethod)"
        
        self.postLAPI(url: strURL, param: param, completionHandler: { response, error in
            
            completionHandler(response, error)
        })
    }
    func ShowOrdersAPI(param : [String : String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(BaseURL)\(showOrders)"
        
        self.postLAPI(url: strURL, param: param, completionHandler: { response, error in
            
            completionHandler(response, error)
        })
    }
    func ShowOrderDetilsAPI(param : [String : String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(BaseURL)\(showOrderDetail)"
        
        self.postLAPI(url: strURL, param: param, completionHandler: { response, error in
            
            completionHandler(response, error)
        })
    }
    func ShowFavorateRestaurentAPI(param : [String : String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(BaseURL)\(showFavouriteRestaurants)"
        
        self.postLAPI(url: strURL, param: param, completionHandler: { response, error in
            
            completionHandler(response, error)
        })
    }
    func ShowRestaurentDealsAPI(param : [String : String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(BaseURL)\(showRestaurantDeals)"
        
        self.postLAPI(url: strURL, param: param, completionHandler: { response, error in
            
            completionHandler(response, error)
        })
    }
    
    //
//
    // MARK: GET API

//    func getRestaurentListAPI(param : [String : String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
//
//        let strURL = "\(BaseURL)\(restaurentListURL)"
//
//        self.getLAPI(url: strURL, param: param, completionHandler: { response, error in
//
//            completionHandler(response, error)
//
//
//        })
//    }
    func getLoginAPI(param : [String : String], completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(BaseURL)\(LoginURL)"
        
        self.getLAPI(url: strURL, param: param, completionHandler: { response, error in
            
            completionHandler(response, error)
            
            
        })
    }
    func getTimeSlotAPI(completionHandler:@escaping (DataResponse<JSON>?, Error?) -> ()) {
        
        let strURL = "\(ConstantsClass.baseUrl)\(ConstantsClass.get_timeslots)"
        
        self.getWithoutParamLAPI(url: strURL, completionHandler: { response, error in
            
            completionHandler(response, error)
            
            
        })
    }
}
