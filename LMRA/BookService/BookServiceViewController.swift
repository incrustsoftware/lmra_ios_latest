//
//  BookServiceViewController.swift
//  LMRA
//
//  Created by Mac on 31/07/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire

@available(iOS 12.0, *)
class BookServiceViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource, UISearchControllerDelegate,UISearchBarDelegate{
    
//UICollectionViewDelegateFlowLayout
    @IBOutlet weak var SearchBar: UISearchBar!
    @IBOutlet weak var bookServiceCollectionViewCell: UICollectionView!
    
    var Get_User_Type:Int!
    var category = ""
    var CategoryString_Array = [String] ()
    var SelectedCatID:Int!
    var StringCatID = ""
    var MainCategoryName = ""
    var AllDataDict = [NSDictionary]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
         SearchBar.delegate = self
        
//        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
//        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
//        bookServiceCollectionViewCell.collectionViewLayout = layout
//
       
//        if #available(iOS 13.0, *) {
//            SearchBar.searchTextField.textColor = .white
//        } else {
//
//            // Fallback on earlier versions
//        }
        
        
       SearchBar.compatibleSearchTextField.textColor = UIColor.white

        self.Get_User_Type = UserDefaults.standard.integer(forKey: UserDataManager_KEY.userType)
        
        UISearchBar.appearance().setImage(UIImage(named: "search"), for: UISearchBar.Icon.search, state: UIControl.State.normal)
               registerNibs()
        self.getCategory()
        
       addDoneButtonOnKeyboard()
    }
    
    func registerNibs(){
             bookServiceCollectionViewCell.register(UINib(nibName: "SubCategoryCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "SubCategoryCell")
     
         }
    @IBAction func btnSignupClicked(_ sender: Any)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        if #available(iOS 12.0, *) {
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "CreateYourAccountVC") as! CreateYourAccountVC
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        } else {
            // Fallback on earlier versions
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.CategoryString_Array.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        

         let cell =  bookServiceCollectionViewCell.dequeueReusableCell(withReuseIdentifier: "SubCategoryCell", for: indexPath as IndexPath) as! SubCategoryCollectionViewCell
        
        cell.CategoryName.text = self.CategoryString_Array[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
         let AllData =  self.AllDataDict[indexPath.row]
        self.SelectedCatID = AllData["id"] as? Int ?? 0
        self.StringCatID = String(self.SelectedCatID)
        print("StringCatID: \(StringCatID)")
        
        self.MainCategoryName = AllData["category"] as? String ?? ""
        UserDefaults.standard.set(self.StringCatID, forKey: "SelectedMainCategoryID")// SetCategoryID
        UserDefaults.standard.set(self.MainCategoryName, forKey: "SelectedMainCategoryName")// set MainCategoryName
        
        let stroyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let objVc:SignSubCategoryViewController =
            storyboard?.instantiateViewController(withIdentifier: "SignSubCategoryVC") as! SignSubCategoryViewController
        
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(objVc, animated: true)

    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        //     return CGSize (width: 150, height: 150)
//             if collectionView == bookServiceCollectionViewCell
//             {
//                //device screen size
//                let width = UIScreen.main.bounds.size.width
//                //calculation of cell size
//                return CGSize(width: ((width / 2) - 15)   , height: 155)
//             }
//             else if collectionView == bookServiceCollectionViewCell
//             {
//                return CGSize (width: (self.view.frame.size.width/3), height:250)
//            }
//            else
//             {
//                return collectionView.frame.size
//            }
//         }
//         
//         func collectionView(_ collectionView: UICollectionView,
//                             layout collectionViewLayout: UICollectionViewLayout,
//                             insetForSectionAt section: Int) -> UIEdgeInsets {
//    
//                return UIEdgeInsets(top: 10, left:5, bottom: 5, right:5)
//         
//         }
//         
//         // 4
//         func collectionView(_ collectionView: UICollectionView,
//                             layout collectionViewLayout: UICollectionViewLayout,
//                             minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//  
//                return 4
//
//         }
    
    func getCategory()
    {
        let urlString = ConstantsClass.baseUrl + ConstantsClass.get_category + ("?user_type=\(self.Get_User_Type!)")
        print("urlString: \(urlString)")
        
        Alamofire.request(urlString, method: .get, parameters: nil, encoding: URLEncoding.default).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if (response["status"] as? Bool) != nil {
                        if let respArray = response["response"] as? [[String:Any]] {
                            print("respArray: \(respArray)")
                            self.AllDataDict = response["response"] as! [NSDictionary]
                            print("AllDataDict:",self.AllDataDict)
                            
                            for Data in respArray
                            {
                                self.category = Data["category"]as! String
                                self.CategoryString_Array.append(self.category)
                            }
                            print("CategoryString_Array: \(self.CategoryString_Array)")
                            
                        }
                    }
                    // Make sure to update UI in main thread
                    DispatchQueue.main.async {
                        self.bookServiceCollectionViewCell.reloadData()
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
    }
    
    @IBAction func btnBackClicked(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
          if searchText == "" {
            self.CategoryString_Array.removeAll()
            self.getCategory()
            bookServiceCollectionViewCell.reloadData()
          }
          else {
            CategoryString_Array = searchText.isEmpty ? CategoryString_Array : CategoryString_Array.filter({(dataString: String) -> Bool in
                // If dataItem matches the searchText, return true to include it
                return dataString.range(of: searchText, options: .caseInsensitive) != nil
                
            })
            
            bookServiceCollectionViewCell.reloadData()
            
          }
      }
    

//    func updateSearchResultsForSearchController(searchController: UISearchController) {
//        if let searchText = searchController.searchBar.text {
//            CategoryString_Array = searchText.isEmpty ? CategoryString_Array : CategoryString_Array.filter({(dataString: String) -> Bool in
//                return dataString.rangeOfString(searchText, options: .CaseInsensitiveSearch) != nil
//            })
//
//            bookServiceCollectionViewCell.reloadData()
//        }
//    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 20))
        doneToolbar.barStyle = .default
        let Cancel: UIBarButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(self.doneButtonAction))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [Cancel,flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.SearchBar.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.SearchBar.resignFirstResponder()
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        self.SearchBar.resignFirstResponder()
        return Int(string) != nil
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
//                self.view.frame.origin.y -= 150
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
                                self.view.frame.origin.y = 0
//            self.view.frame.origin.y += 150
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
}
//extension UISearchBar {
//
//    // Due to searchTextField property who available iOS 13 only, extend this property for iOS 13 previous version compatibility
//    var compatibleSearchTextField: UITextField {
//        guard #available(iOS 13.0, *) else { return legacySearchField }
//        return self.searchTextField
//    }
//
//    private var legacySearchField: UITextField {
//        if let textField = self.subviews.first?.subviews.last as? UITextField {
//            // Xcode 11 previous environment
//            return textField
//        } else if let textField = self.value(forKey: "searchField") as? UITextField {
//            // Xcode 11 run in iOS 13 previous devices
//            return textField
//        } else {
//            // exception condition or error handler in here
//            return UITextField()
//        }
//    }
//}
