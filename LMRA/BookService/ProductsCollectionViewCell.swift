//
//  ProductsCollectionViewCell.swift
//  LMRA
//
//  Created by Mac on 12/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class ProductsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var MainView: UIView!
    
    @IBOutlet weak var ImgProduct: UIImageView!
    
    @IBOutlet weak var LblProduct: UILabel!
    
    
    
}
