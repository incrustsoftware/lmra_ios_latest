//
//  InviteSuccessfullViewController.swift
//  LMRA
//
//  Created by Mac on 20/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class InviteSuccessfullViewController: UIViewController {

     var BtnHometColor = UIColor(red:243, green:243, blue:241, alpha:1.0)//whilte
    
    @IBOutlet weak var BtnInvite: UIButton!
    @IBOutlet weak var BtnHome: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad(
        )
       self.BtnHome.setTitleColor(BtnHometColor, for: .normal)
        self.BtnHome.backgroundColor = .clear
        self.BtnHome.layer.cornerRadius = 5
        self.BtnHome.layer.borderWidth = 2
        self.BtnHome.layer.borderColor = colorWithHexString(hexString: "#F3F3F1").cgColor

    }
    
    @IBAction func BtnInviteAnotherTaskerClick(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SearchResultVC") as! SearchResultViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func BtnHomeClick(_ sender: Any) {
      
        let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "DashBoardTabVC") as! DashBoardTabViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    func colorWithHexString(hexString: String, alpha:CGFloat = 1.0) -> UIColor {
         
         // Convert hex string to an integer
         let hexint = Int(self.intFromHexString(hexStr: hexString))
         let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
         let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
         let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
         
         // Create color object, specifying alpha as well
         let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
         return color
     }
     
     func intFromHexString(hexStr: String) -> UInt32 {
         var hexInt: UInt32 = 0
         // Create scanner
         let scanner: Scanner = Scanner(string: hexStr)
         // Tell scanner to skip the # character
         scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
         // Scan hex value
         scanner.scanHexInt32(&hexInt)
         return hexInt
     }

}
