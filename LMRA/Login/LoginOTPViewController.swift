//
//  LoginOTPViewController.swift
//  LMRA
//
//  Created by Mac on 17/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire

@available(iOS 12.0, *)
@available(iOS 12.0, *)
@available(iOS 12.0, *)
@available(iOS 12.0, *)
class LoginOTPViewController: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var LblTimer: UILabel!
    @IBOutlet weak var BtnVerify: UIButton!
    @IBOutlet weak var BtnResend: UIButton!
    @IBOutlet weak var TFFirst: UITextField!
    @IBOutlet weak var TFSecond: UITextField!
    @IBOutlet weak var TFThird: UITextField!
    @IBOutlet weak var TFFourth: UITextField!
    
    var BtnVerifyColor = UIColor(red:243, green:243, blue:241, alpha:1.0)
    var Get_Login_Cell_Number = ""
    var Get_CPR_Number = ""
    var Get_SMS_OTPString = ""
    var Get_ChnageCellNoSMS_OTP:Int = 0
    var Get_SMS_ResendOTPString = ""
    var RESEND_SMS_OTP:Int = 0
    var UpdatedCellNo = ""
    var authentication_keyLoginSMS = ""
    var Firstval = ""
    var Secondval = ""
    var Thirdval = ""
    var FourthVal = ""
    var seconds = 120 //This variable will hold a starting value of seconds. It could be any amount above 0.
    var timer = Timer()
    var isTimerRunning = false //This will be used to make sure only one timer is created at a time.
    var firstval:Int = 0
    var secondval:Int = 0
    var thirdval:Int = 0
    var Fourthdval:Int = 0
    var Get_LoginSMS_OTP:Int!
    var StringGet_LoginSMS_OTP = ""
    var is_guest_user = "0"
    
    
    var BtnResendOnColor = UIColor(red:255, green:255, blue:94, alpha:1.0)
    override func viewDidLoad() {
        super.viewDidLoad()
        BtnVerify.layer.cornerRadius = 2;
        runTimer()//otp timer function
        
        TFFirst.addTarget(self, action: #selector(LoginOTPViewController.txtFieldChanged(_:)),
                          for: .editingChanged)
        TFSecond.addTarget(self, action: #selector(LoginOTPViewController.txtFieldChanged),
                           for: .editingChanged)
        TFThird.addTarget(self, action: #selector(LoginOTPViewController.txtFieldChanged),
                          for: .editingChanged)
        TFFourth.addTarget(self, action: #selector(LoginOTPViewController.txtFieldChanged),
                           for: .editingChanged)
        
        
        
        
        self.Get_LoginSMS_OTP =  UserDefaults.standard.integer(forKey: "LoginOTPKey")
        self.StringGet_LoginSMS_OTP = String(self.Get_LoginSMS_OTP)
        
        print("StringGet_LoginSMS_OTP: \(self.StringGet_LoginSMS_OTP)")
        print("Get_LoginSMS_OTP: \(self.Get_LoginSMS_OTP ?? 0)")
        
        
        let digits = StringGet_LoginSMS_OTP.compactMap{ $0.wholeNumberValue }
        self.firstval = digits[0]
        self.secondval = digits[1]
        self.thirdval = digits[2]
        self.Fourthdval = digits[3]
        
        TFFirst.text = String(self.firstval)
        TFSecond.text = String(self.secondval)
        TFThird.text = String(self.thirdval)
        TFFourth.text = String(self.Fourthdval)
        TFFirst.textContentType = .oneTimeCode
        TFSecond.textContentType = .oneTimeCode
        TFThird.textContentType = .oneTimeCode
        TFFourth.textContentType = .oneTimeCode
        
        addDoneButtonOnKeyboard()
        
        self.TFFirst.delegate = self
        self.TFSecond.delegate = self
        self.TFThird.delegate = self
        self.TFFourth.delegate = self
        
        self.TFFirst.addTarget(self, action: #selector (self.changeCharacter), for: .editingChanged)
        self.TFSecond.addTarget(self, action: #selector (self.changeCharacter), for: .editingChanged)
        self.TFThird.addTarget(self, action: #selector (self.changeCharacter), for: .editingChanged)
        self.TFFourth.addTarget(self, action: #selector (self.changeCharacter), for: .editingChanged)
        self.BtnVerify.setTitleColor(BtnVerifyColor, for: .normal)
        BtnVerify.backgroundColor = COLOR.PinkColor
        // Do any additional setup after loading the view.
    }
    
    @objc func changeCharacter(textField: UITextField)
    {
        if textField.text!.utf8.count == 1
        {
            switch textField {
            case TFFirst:
                TFSecond.becomeFirstResponder()
            case TFSecond:
                TFThird.becomeFirstResponder()
            case TFThird:
                TFFourth.becomeFirstResponder()
            case TFFourth:
                print("OTP send to server")// here send otp to server
            default:
                break
            }
        }
        else if textField.text!.isEmpty
        {
            switch textField {
            case TFFourth:
                TFThird.becomeFirstResponder()
            case TFThird:
                TFSecond.becomeFirstResponder()
            case TFSecond:
                TFFirst.becomeFirstResponder()
            default:
                break
            }
        }
    }
    
    func runTimer()
    {
        self.BtnResend.isEnabled = false
        BtnResend.setTitleColor(.gray, for: .normal)
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: #selector(OTPViewController.updateTimer), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer()
    {
        seconds -= 1  //This will decrement(count down)the seconds.
        LblTimer.text = timeString(time: TimeInterval(seconds))
        if seconds ==  0
        {
            BtnResend.isEnabled = true
            self.BtnResend.setTitleColor(COLOR.PinkColor, for: .normal)
            timer.invalidate()
        }
    }
    
    func timeString(time:TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i", minutes, seconds)
    }
    
    
    @IBAction func ResendBtnClick(_ sender: Any) {
        timer.invalidate()
        seconds = 120
        LblTimer.text = timeString(time: TimeInterval(seconds))
        runTimer()
        
        self.TFFirst.text = ""
        self.TFSecond.text = ""
        self.TFThird.text = ""
        self.TFFourth.text = ""
        let CheckOTPFromChnageCellNo: Bool = false
        UserDefaults.standard.set(CheckOTPFromChnageCellNo,forKey: "isCellNoChange")
        
        self.ResendOTPSMS()
    }
    
    @IBAction func ChangeNumClick(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ChangePhnNoVC") as! ChnagePhnNoViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func btncloseClicked(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnVerifyOtpClicked(_ sender: Any)
    {
        
        self.Firstval = self.TFFirst.text!
        self.Secondval = self.TFSecond.text!
        self.Thirdval = self.TFThird.text!
        self.FourthVal = self.TFFourth.text!
        
        print("firstval: \(Firstval)")
        print("Secondval: \(Secondval)")
        print("Thirdval: \(Thirdval)")
        print("FourthVal: \(FourthVal)")
        
        self.StringGet_LoginSMS_OTP = (Firstval) + (Secondval) + (Thirdval) + (FourthVal)
        print("Get_SMS_OTPString: \(self.StringGet_LoginSMS_OTP)")
        
        
        self.VerifyOTPSMS()
        UserDefaults.standard.set("0" , forKey: "FirstLogin") //set logged in
        print("User Logged in")
        
    }
    
    func VerifyOTPSMS()
    {
        let url = ConstantsClass.baseUrl + ConstantsClass.verify_otp_customer_account
        print("url: \(url)")
        self.Get_Login_Cell_Number = UserDefaults.standard.string(forKey: "LogincellNoKey")!//get
        self.Get_CPR_Number = UserDefaults.standard.string(forKey: "CPRNoKey") ?? ""//get
        print("Get_Login_Cell_Number: \(self.Get_Login_Cell_Number)")
        
        
        let parameters = [
            "cell_no" : "\(self.Get_Login_Cell_Number)",
            "cpr_no" : "\(self.Get_CPR_Number)",
            "otp" : "\(self.StringGet_LoginSMS_OTP)",
            "is_guest_user" : "\(self.is_guest_user)"
            ] as [String : Any]
        
        print("parametersVerifySMS: \(parameters)")
        Helper.sharedInstance.showLoader()
        Alamofire.request(url
            , method: .post, parameters: parameters, encoding:JSONEncoding.default).responseJSON
            {
                response in
                if let data = response.data {
                    if let responseData = response.result.value as? [String:Any] {
                        print("response: \(response)")
                        if let status = responseData["status"] as? Bool {
                            if status{
                                UserDataManager.shared.setUserDetails(userDetails: data)
                                if let responseDict = responseData["response"]as? [String:Any]
                                {
                                    
                                    self.authentication_keyLoginSMS = responseDict["authentication_key"]as? String ?? ""
                                    print("authentication_keyLoginSMS:",self.authentication_keyLoginSMS)
                                    UserDefaults.standard.set(self.authentication_keyLoginSMS, forKey:"authentication_key") //set LoginAuthKey
                                    
                                    DispatchQueue.main.async {
                                        Helper.sharedInstance.hideLoader()
                                        
                                        /////////////////////////////////////////////////
                                        let rootViewController = Storyboard().dashFlowStoryboard.instantiateViewController(withIdentifier: "CustomerDashBoardViewController")
                                        let navigationController = UINavigationController(rootViewController: rootViewController)
                                        let rightViewController = Storyboard().dashFlowStoryboard.instantiateViewController(withIdentifier: "CutomerMenuVC")
                                        let sideMenuController = LGSideMenuController(rootViewController: navigationController,
                                                                                      leftViewController: nil,
                                                                                      rightViewController: rightViewController)
                                        sideMenuController.leftViewWidth = 0
                                        sideMenuController.leftViewPresentationStyle = .slideBelow;
                                        
                                        sideMenuController.rightViewWidth = (UIApplication.shared.keyWindow?.bounds.size.width ?? 320) - 20
                                        sideMenuController.leftViewPresentationStyle = .slideBelow
                                        navigationController.isNavigationBarHidden = true
                                        navigationController.interactivePopGestureRecognizer?.delegate = self
                                        self.view.window?.rootViewController = sideMenuController
                                        self.view.window?.makeKeyAndVisible()
                                        
                                        print("confirm_otp_success")
                                        self.view.makeToast("Phone No Verified", duration: 6.0, position: .bottom)
                                        //                                        let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
                                        //                                        storyBoard.instantiateViewController(withIdentifier: "DashBoardTabVC") as! DashBoardTabViewController
                                        let nextViewController =  Storyboard().dashFlowStoryboard.instantiateViewController(withIdentifier: "CustomerDashBoardViewController")
                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                        
                                        
                                    }
                                }
                                else
                                {
                                    self.view.makeToast("Something went wrong", duration: 3.0, position: .bottom)
                                    print("Something went wrong")
                                }
                            }
                            else
                            {
                                Helper.sharedInstance.hideLoader()
                                self.view.makeToast("Confirm OTP failed", duration: 3.0, position: .bottom)
                                
                            }
                        }
                    }
                }
                
        }
    }
    
    func ResendOTPSMS()
    {
        let url = ConstantsClass.baseUrl + ConstantsClass.resend_otp_to_create_customer_account
        print("url: \(url)")
        let parameters = [
            "cell_no" : "\(self.Get_Login_Cell_Number)",
            "cpr_no" : "\(self.Get_CPR_Number)"
            ] as [String : Any]
        
        print("parametersRESENDOTP: \(parameters)")
        
        Alamofire.request(url
            , method: .post, parameters: parameters, encoding:JSONEncoding.default).responseJSON
            {
                response in
                
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if let status = response["status"] as? Bool {
                        print("Get status true here")
                        
                        if status
                        {
                            self.RESEND_SMS_OTP = response["response"]as! Int
                            print("RESEND_SMS_OTP: \(self.RESEND_SMS_OTP)")
                            
                            self.StringGet_LoginSMS_OTP = String(self.RESEND_SMS_OTP)
                            print("StringGet_LoginSMS_OTP: \(self.StringGet_LoginSMS_OTP)")
                            
                            let digits = self.StringGet_LoginSMS_OTP.compactMap{ $0.wholeNumberValue }
                            self.firstval = digits[0]
                            self.secondval = digits[1]
                            self.thirdval = digits[2]
                            self.Fourthdval = digits[3]
                            self.TFFirst.text = String(self.firstval)
                            self.TFSecond.text = String(self.secondval)
                            self.TFThird.text = String(self.thirdval)
                            self.TFFourth.text = String(self.Fourthdval)
                            Helper.sharedInstance.hideLoader()
                        }
                        else
                        {
                            Helper.sharedInstance.hideLoader()
                            self.view.makeToast("Confirm OTP failed", duration: 3.0, position: .bottom)
                            
                        }
                    }
                    else
                    {
                        self.view.makeToast("Something went wrong", duration: 3.0, position: .bottom)
                        print("Something went wrong")
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
    }
    
    @objc func txtFieldChanged(_ textField: UITextField) {
        if (TFFirst.text!.count == 0) || (TFSecond.text!.count == 0) || (TFThird.text!.count == 0) || (TFFourth.text!.count == 0)
        {
            self.BtnVerify.setTitleColor(COLOR.GrayColor, for: .normal)
            BtnVerify.backgroundColor = COLOR.LightGrayColor
        }
        else
        {
            self.BtnVerify.setTitleColor(COLOR.WhiteColor, for: .normal)
            BtnVerify.backgroundColor = COLOR.PinkColor
        }
    }
    
    
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 20))
        doneToolbar.barStyle = .default
        let Cancel: UIBarButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(self.doneButtonAction))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        let items = [Cancel,flexSpace, done]
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.TFFirst.inputAccessoryView = doneToolbar
        self.TFSecond.inputAccessoryView = doneToolbar
        self.TFThird.inputAccessoryView = doneToolbar
        self.TFFourth.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.TFFirst.resignFirstResponder()
        self.TFSecond.resignFirstResponder()
        self.TFThird.resignFirstResponder()
        self.TFFourth.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.TFFirst.resignFirstResponder()
        self.TFSecond.resignFirstResponder()
        self.TFThird.resignFirstResponder()
        self.TFFourth.resignFirstResponder()
        
        return true
    }
    
    
    
    
    func colorWithHexString(hexString: String, alpha:CGFloat = 1.0) -> UIColor {
        
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    
    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        
        return hexInt
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


@available(iOS 12.0, *)
extension LoginOTPViewController: UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.text?.utf8.count == 1 && !string.isEmpty
        {
            return false
        }
        else
        {
            return true
        }
    }
}
