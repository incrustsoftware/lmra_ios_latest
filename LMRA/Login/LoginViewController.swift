//
//  LoginViewController.swift
//  LMRA
//
//  Created by Mac on 31/07/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire

@available(iOS 12.0, *)
class LoginViewController: UIViewController,UITextFieldDelegate,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var BtnContinue: UIButton!
    @IBOutlet weak var TFCellNo: UITextField!
    @IBOutlet weak var TFCellNoHt: NSLayoutConstraint!
    @IBOutlet weak var LblCellNo: UILabel!
    @IBOutlet weak var RecoverAccountView: UIView!
    @IBOutlet weak var LblCallNumber: UILabel!
    @IBOutlet weak var BtnCancle: UIButton!
    @IBOutlet weak var BackgroundView: UIView!
    var LoginCell_Number = ""
    var LoginName = ""
    var Get_CPR_Number = ""
    var loginAuthKey = ""
    var LoginOTP = ""
    var LoginCPRNo = ""
    var maxLenCellNo:Int = 11// for now 11
    var User_type:Int! // value 2 for tasker and value 1 for customer
    var User_TypeString = ""
    var User_Device_Type = "2" // val 2 for IOS device
    var isCalledFromSubcategory:Bool = false
    let defaults = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()
        self.TFCellNo.delegate = self
        self.BackgroundView.isHidden = true
        self.RecoverAccountView.isHidden = true
        self.BtnCancle.isHidden = true
        
        self.navigationController?.navigationBar.isHidden = true
        UserDefaults.standard.set(self.User_Device_Type, forKey: "UserDeviceTypeIDKey")
        
        TFCellNo.addTarget(self, action: #selector(LoginViewController.textFieldDidChange(_:)),
                           for: .editingChanged)
        let cellnumberTap = UITapGestureRecognizer(target:self,action:#selector(self.cellnumberTapTapped))
        
        // add it to the label
        LblCellNo.addGestureRecognizer(cellnumberTap)
        LblCellNo.isUserInteractionEnabled = true
        
        BackgroundView.addTapGesture{
            self.BackgroundView.isHidden = true
            self.RecoverAccountView.isHidden = true
            self.BtnCancle.isHidden = true
        }
               
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        addDoneButtonOnKeyboard()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.isCalledFromSubcategory = UserDefaults.standard.bool(forKey: "isFromSubcategory")// this is how you retrieve the bool value
        print("isCalledFromSubcategory......:",isCalledFromSubcategory)
        if (isCalledFromSubcategory)//value from search result
        {
            self.view.makeToast("Please SignUp or Login First", duration: 5.0, position: .bottom)
        }
    }
    
    @objc func cellnumberTapTapped()
    {
        TFCellNoHt.constant = 20
        TFCellNo.becomeFirstResponder()
    }
    

    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if (TFCellNo.text!.count > 0 )
        {
            self.BtnContinue.setTitleColor(COLOR.WhiteColor, for: .normal)
            BtnContinue.backgroundColor = COLOR.PinkColor
        }
        else
        {
            self.BtnContinue.setTitleColor(COLOR.GrayColor, for: .normal)
            BtnContinue.backgroundColor = COLOR.LightGrayColor
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == TFCellNo){
            let currentText = textField.text! + string
            return currentText.count <= maxLenCellNo
        }
        return true;
    }
    
    @IBAction func BtnCancleclick(_ sender: Any) {
        self.BackgroundView.isHidden = true
        self.RecoverAccountView.isHidden = true
        self.BtnCancle.isHidden = true
    }
    
    @IBAction func BackbtnClick(_ sender: Any) {
        var isPopView = false
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: GetStartedViewController.self) {
                isPopView = true
                self.navigationController?.popToViewController(controller, animated: true)
                break
            }
        }
        
        if isPopView == false {
            let vc = Storyboard().mainStoryboard.instantiateViewController(withIdentifier: "GetStartedViewController")
            navigationController?.pushViewController(vc, animated: false)
        }
        
        //self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func BtnContinueClick(_ sender: Any) {
        checkValidation()
        
    }
    
    @IBAction func BtnSignUpClick(_ sender: Any) {
//        self.TFCellNo.text = ""
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "CreateYourAccountVC") as! CreateYourAccountVC
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func BtnRecoverAccountClick(_ sender: Any) {
//        self.TFCellNo.text = ""
        
        
        RecoverAccountView.layer.cornerRadius = 10
        BtnCancle.layer.cornerRadius = 10
        self.BackgroundView.isHidden = false
        self.RecoverAccountView.isHidden = false
        self.BtnCancle.isHidden = false
        
        self.LblCallNumber.isUserInteractionEnabled = true
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(tappedOnLabel(_ :)))
        tapgesture.numberOfTapsRequired = 1
        self.LblCallNumber.addGestureRecognizer(tapgesture)
        
       
    }
    
    
    @objc func tappedOnLabel(_ gesture: UITapGestureRecognizer) {
        print("on lable call tap")
        
        if let url = URL(string: "tel://\(LblCallNumber.text!)"),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
        
    }
    
    
    
    func checkValidation()  {
        if (TFCellNo.text! == "" ) || (TFCellNo.text!.count != 11 ){// for now
//            BtnContinue.backgroundColor = COLOR.LightGrayColor
            self.view.makeToast("Please enter valid cell phone number")
        }
        else if (!TFCellNo.text!.hasPrefix("973"))
        {
            self.view.makeToast("Cell phone number should start with 973")
        }
        else
        {
            self.LoginCell_Number = TFCellNo.text!
            defaults.set(self.LoginCell_Number, forKey: "LogincellNoKey")//set
            defaults.set(self.LoginCell_Number, forKey: "cellNoKey")//set
            print("LoginCell_Number: \(self.LoginCell_Number)")
            
            if (TFCellNo.text!.count == 11 ) // for now
            {
 
                self.customer_sign_in()
                
            }
        }
    }
    
    
    func customer_sign_in()
    {
        let url = ConstantsClass.baseUrl + ConstantsClass.customer_sign_in
        print("url: \(url)")
        
        //for now
        let relay_ID = "2gyhgygiughiuhiujiojoijpokopkpkpokoopkopopugffserljiojoi" //static
        UserDefaults.standard.set(relay_ID, forKey: "RelayIDKey")
        let parameters = [
            "cell_no" : "\(self.LoginCell_Number)",
            "user_device_type" : "\(self.User_Device_Type)",
            "relay_id" : "\(relay_ID)"
            ] as [String : Any]
        
        print("ho: \(parameters)")
        
        Alamofire.request(url
            , method: .post, parameters: parameters, encoding:JSONEncoding.default).responseJSON
            {
                response in
                
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    if let status = response["status"] as? Bool {
                        print("status: \(status)")
                        if status{
                            if let responseDict = response["response"]as? [String:Any]
                            {
                        
                                self.LoginOTP = responseDict["otp"]as? String ?? ""
                                self.loginAuthKey = responseDict["token"]as? String ?? ""
                                print("loginAuthKey:",self.loginAuthKey)
                                self.LoginCPRNo = responseDict["identity_card_number"]as? String ?? ""
                                self.LoginName = responseDict["name"]as? String ?? ""
                                guard let GetUserType = responseDict["user_type"]as? String else {return}
                                
                                UserDefaults.standard.set(GetUserType, forKey: UserDataManager_KEY.userType) // for chnage
                                
                                if GetUserType == "1"
                                {
                                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "LoginOTPVC") as! LoginOTPViewController
                                    UserDataManager.shared.setTaskerOrCustomer(userType: UserDataManager.UserType.CUSTOMER)
                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                }
                                else if  GetUserType == "2"
                                {
                                    let storyBoard : UIStoryboard = UIStoryboard(name: "Tasker", bundle:nil)
                                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "Tasker_VerifyOTPViewController") as! Tasker_VerifyOTPViewController
                                    nextViewController.isfromLogin = true
                                    UserDataManager.shared.setTaskerOrCustomer(userType: UserDataManager.UserType.TASKER)
                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                }
                                
                                print("LoginCPRNo: \(self.LoginCPRNo)")
                                
                                
                                
                                UserDefaults.standard.set(self.LoginCPRNo, forKey:"CPRNoKey")
                                UserDefaults.standard.set(self.LoginOTP, forKey:"LoginOTPKey")
                                UserDefaults.standard.set(self.LoginName, forKey:"LoginTaskerNameKey")
                                DispatchQueue.main.async {
                                    print("sign_in_success")
                                    
                                }
                            }
                            else
                            {
                                self.view.makeToast("Something went wrong", duration: 3.0, position: .bottom)
                                print("Something went wrong")
                            }
                        }
                        else
                        {
                           self.view.makeToast("User not found", duration: 3.0, position: .bottom)
                            
                        }
                    }
                }
        }
    }
    
    func LoginSentOTPSMS()
    {
        let url = ConstantsClass.baseUrl + ConstantsClass.send_otp_to_create_customer_account
        print("url: \(url)")
        self.User_type =  UserDefaults.standard.integer(forKey: UserDataManager_KEY.userType)
        self.User_TypeString = String(self.User_type)
        self.Get_CPR_Number =  UserDefaults.standard.string(forKey: "CPRNoKey")!
        
        let parameters = [
            "cell_no" : "\(self.LoginCell_Number)",
            "cpr_no" : "\(self.Get_CPR_Number)",
            "user_type" : "\(self.User_TypeString)"
            ] as [String : Any]
        
        print("parameters: \(parameters)")
        
        Alamofire.request(url
            , method: .post, parameters: parameters, encoding:JSONEncoding.default).responseJSON
            {
                response in
                
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if let responseDict = response["response"]as? [String:Any]
                    {
                      
                        if let status = response["status"] as? Bool {
                            if status{
                                DispatchQueue.main.async {
                                    
                                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                                    
                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                }
                            }
                            else
                            {
                                self.view.makeToast("Something went wrong", duration: 3.0, position: .bottom)
                                print("Something went wrong")
                            }
                        }
                        else
                        {
                            print("Error occured") // serialized json response
                            
                        }
                    }
                }
        }
    }
    
    
    
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 20))
        doneToolbar.barStyle = .default
        let Cancel: UIBarButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(self.doneButtonAction))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [Cancel,flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.TFCellNo.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.TFCellNo.resignFirstResponder()
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        self.TFCellNo.resignFirstResponder()
        return Int(string) != nil
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                //                        self.view.frame.origin.y -= keyboardSize.height
                self.view.frame.origin.y -= 150
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            //                    self.view.frame.origin.y = 0
            self.view.frame.origin.y += 150
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    
}
