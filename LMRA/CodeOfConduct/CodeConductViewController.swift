//
//  CodeConductViewController.swift
//  LMRA
//
//  Created by Mac on 20/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire

class CodeConductViewController: UIViewController {
    @IBOutlet weak var BtnDecline: UIButton!
    @IBOutlet weak var BtnAccept: UIButton!
    var authentication_key_val = ""
    var GetTaskID = ""
    var GetCustomerID = ""
    var GetTaskerID = ""
    var BtnAcceptColor = UIColor(red:243, green:243, blue:241, alpha:1.0)//whilte
    var DeclineColor = UIColor(red:255/255, green:55/255, blue:94/255, alpha:1.0)//pink
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.BtnAccept.setTitleColor(BtnAcceptColor, for: .normal)
        BtnAccept.backgroundColor = colorWithHexString(hexString: "#ff375e")
        
        self.BtnDecline.setTitleColor(DeclineColor, for: .normal)
        self.BtnDecline.backgroundColor = .clear
        self.BtnDecline.layer.cornerRadius = 5
        self.BtnDecline.layer.borderWidth = 2
        //        self.BtnDecline.layer.borderColor = (UIColor( red: 255, green: 55, blue:94, alpha: 1.0 )).cgColor
        self.BtnDecline.layer.borderColor = colorWithHexString(hexString: "#ff375e").cgColor
        
        self.authentication_key_val = UserDefaults.standard.string(forKey: "authentication_key")!//get
        self.GetTaskID = UserDefaults.standard.string(forKey: "TaskIDKey")!//get
        self.GetCustomerID = UserDefaults.standard.string(forKey: "CustomerIDKey")!//get
        self.GetTaskerID = UserDefaults.standard.string(forKey: "SelectedTaskerID") ?? ""//get
        print("GetTaskerID:",(GetTaskerID))
        // Do any additional setup after loading the view.
    }
    
    func invite_tasker()
    {
        let urlString = ConstantsClass.baseUrl + ConstantsClass.invite_tasker
        print("urlString: \(urlString)")
        
        let headers = [
            "Authorization": "\(self.authentication_key_val)"
        ]
        print("headers: \(headers)")
        
        let parameters = [
            "task_id" : "\(self.GetTaskID)",
            "tasker_id" : "\(self.GetTaskerID)"
            ] as [String : Any]
        print("parametersaddSearchTasker: \(parameters)")
        Alamofire.request(urlString
            , method: .post, parameters: parameters, encoding:JSONEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if (response["status"] as? Bool) != nil {
                        if let respArray = response["response"] as? [[String:Any]] {
                            print("respArray: \(respArray)")
                            
                            
                            
                        }
                    }
                    // Make sure to update UI in main thread
                    DispatchQueue.main.async {
                        print("Invite successfully.")
                        let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "InviteSuccessVC") as! InviteSuccessfullViewController
                        
                        self.navigationController?.pushViewController(nextViewController, animated: true)
                        
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
        
    }
    
    @IBAction func CancelbtnClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func AcceptbtnClick(_ sender: Any) {
        
        self.invite_tasker()
        
    }
    
    @IBAction func DeclinebtnClick(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SearchResultVC") as! SearchResultViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)

//        self.dismiss(animated: true, completion: nil)
    }
    func colorWithHexString(hexString: String, alpha:CGFloat = 1.0) -> UIColor {
        
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    
    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
    
}
