//
//  SecondTabViewController.swift
//  LMRA
//
//  Created by Mac on 17/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire

class SecondTabViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UISearchControllerDelegate,UISearchBarDelegate {
    
    @IBOutlet weak var BtnNotification: UIButton!
    @IBOutlet weak var BookedServicesCollectionView: UICollectionView!
    @IBOutlet weak var SearchBar: UISearchBar!
    
    
    var Get_User_Type:Int!
    var category = ""
    var CategoryString_Array = [String] ()
    var CategoryString_ArrayDummy = [String] ()
    var StringCatID = ""
    var SelectedCatID:Int!
    var MainCategoryName = ""
    var AllDataDict = [NSDictionary]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //  view.backgroundColor = COLOR.PurpleColor
        
        self.navigationController?.isNavigationBarHidden = true
        tabBarController?.selectedIndex = 1
//        let swipLeft = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
//        swipLeft.direction = .left
//        self.view.addGestureRecognizer(swipLeft)
        
        UISearchBar.appearance().setImage(UIImage(named: "search"), for: UISearchBar.Icon.search, state: UIControl.State.normal)
        
        registerNibs()
        self.Get_User_Type = UserDefaults.standard.integer(forKey: UserDataManager_KEY.userType)
        print("Get_User_Type: \(Get_User_Type)")
        SearchBar.delegate = self
        SearchBar.compatibleSearchTextField.textColor = UIColor.white
        //        if #available(iOS 13.0, *) {
        //            SearchBar.searchTextField.textColor = .white
        //        } else {
        //            // Fallback on earlier versions
        //        }
        
        
        
        //        self.getCategory()
    }
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//         return .lightContent
//     }
       
    
    func registerNibs(){
        BookedServicesCollectionView.register(UINib(nibName: "SubCategoryCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "SubCategoryCell")
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.CategoryString_Array.count
        //        return self.AllDataDict.count
    }
    
    override func viewWillAppear(_ animated: Bool) {
        GLOBALVARIABLE.TabBarName = "SecondTab"
        self.getCategory()
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell =  BookedServicesCollectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoryCell", for: indexPath as IndexPath) as! SubCategoryCollectionViewCell
        
        cell.CategoryName.text = self.CategoryString_Array[indexPath.row]
        //        let AllData =  self.AllDataDict[indexPath.row]
        //        cell.CategoryName.text = AllData["category"] as? String
        //        let image = AllData["category_image"] as? String ?? ""
        //        cell.CategoryImage.downloaded(from: image )
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let AllData =  self.AllDataDict[indexPath.row]
        self.SelectedCatID = AllData["id"] as? Int ?? 0
        self.StringCatID = String(self.SelectedCatID)
        print("StringCatID: \(StringCatID)")
        
        self.MainCategoryName = AllData["category"] as? String ?? ""
        UserDefaults.standard.set(self.StringCatID, forKey: "SelectedMainCategoryID")// SetCategoryID
        UserDefaults.standard.set(self.MainCategoryName, forKey: "SelectedMainCategoryName")// set MainCategoryName
        
        let stroyboard:UIStoryboard = UIStoryboard(name: "DashFlow", bundle: nil)
        let objVc:SubCategoryViewController =
            storyboard?.instantiateViewController(withIdentifier: "SubCategoryVC") as! SubCategoryViewController
        
        //self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(objVc, animated: true)
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            self.CategoryString_Array.removeAll()
            self.getCategory()
            
            BookedServicesCollectionView.reloadData()
        }
        else {
            CategoryString_Array = searchText.isEmpty ? CategoryString_Array : CategoryString_Array.filter({(dataString: String) -> Bool in
                // If dataItem matches the searchText, return true to include it
                return dataString.range(of: searchText, options: .caseInsensitive) != nil
                
            })
            
            BookedServicesCollectionView.reloadData()
            
        }
        
    }
    
    
    
    //    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    //        // When there is no text, filteredData is the same as the original data
    //        // When user has entered text into the search box
    //        // Use the filter method to iterate over all items in the data array
    //        // For each item, return true if the item should be included and false if the
    //        // item should NOT be included
    //        CategoryString_Array = searchText.isEmpty ? CategoryString_Array : CategoryString_Array.filter({(dataString: String) -> Bool in
    //            // If dataItem matches the searchText, return true to include it
    //            return dataString.range(of: searchText, options: .caseInsensitive) != nil
    //        })
    //
    //        BookedServicesCollectionView.reloadData()
    //    }
    //
    //
    //    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
    //            self.SearchBar.showsCancelButton = true
    //    }
    //    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
    //            SearchBar.showsCancelButton = false
    //            SearchBar.text = ""
    //            SearchBar.resignFirstResponder()
    //          BookedServicesCollectionView.reloadData()
    //    }
    
    @IBAction func BtnNotificationClick(_ sender: Any) {
        
        let stroyboard:UIStoryboard = UIStoryboard(name: "DashFlow", bundle: nil)
        let objVc:NotificationViewController =
            storyboard?.instantiateViewController(withIdentifier: "NotificationVc") as! NotificationViewController
        self.navigationController?.pushViewController(objVc, animated: true)
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case .right:
                print("Swiped right")
                
            case .down:
                print("Swiped down")
            case .left:
                
                print("Swiped left")
                self.tabBarController?.selectedIndex = 3;
            case .up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    
    func getCategory()
    {
        let urlString = ConstantsClass.baseUrl + ConstantsClass.get_category + ("?user_type=\(self.Get_User_Type ?? 0)")
        print("urlString: \(urlString)")
        
        Alamofire.request(urlString, method: .get, parameters: nil, encoding: URLEncoding.default).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if (response["status"] as? Bool) != nil {
                        if let respArray = response["response"] as? [[String:Any]] {
                            print("respArray: \(respArray)")
                            self.CategoryString_Array.removeAll()
                            for Data in respArray
                            {
                                self.category = Data["category"]as! String
                                self.CategoryString_Array.append(self.category)
                                
                            }
                            
                            print("CategoryString_Array: \(self.CategoryString_Array)")
                            
                            self.AllDataDict = response["response"] as! [NSDictionary]
                            print("AllDataDict:",self.AllDataDict)
                            
                            
                        }
                    }
                    // Make sure to update UI in main thread
                    DispatchQueue.main.async {
                        self.BookedServicesCollectionView.reloadData()
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
}
