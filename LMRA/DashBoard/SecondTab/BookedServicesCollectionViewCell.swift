//
//  BookedServicesCollectionViewCell.swift
//  LMRA
//
//  Created by Mac on 18/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class BookedServicesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var InnerView: UIView!
    @IBOutlet weak var ImgBookedService: UIImageView!
    @IBOutlet weak var LblBookedServices: UILabel!
    
}

