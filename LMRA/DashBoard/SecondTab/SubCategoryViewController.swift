//
//  SubCategoryViewController.swift
//  LMRA
//
//  Created by Mac on 10/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire

class SubCategoryViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UISearchControllerDelegate,UISearchBarDelegate{
   
    @IBOutlet weak var SubCategoryCollectionView: UICollectionView!
    
    @IBOutlet weak var LblMainSubCatName: UILabel!
    @IBOutlet weak var SearchBar: UISearchBar!
    var category = ""
    var SubCategoryString_Array = [String] ()
    var GetCategoryID = ""
    var StringSubCatID = ""
    var GetSubCategoryName = ""
    var SelectedSubCatID:Int!
    var AllDataDict = [NSDictionary]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        UISearchBar.appearance().setImage(UIImage(named: "search"), for: UISearchBar.Icon.search, state: UIControl.State.normal)
        registerNibs()
        SearchBar.delegate = self

        SearchBar.compatibleSearchTextField.textColor = UIColor.white
        self.GetCategoryID = UserDefaults.standard.string(forKey: "SelectedMainCategoryID") ?? ""
        self.GetSubCategoryName = UserDefaults.standard.string(forKey: "SelectedMainCategoryName") ?? ""
        self.LblMainSubCatName.text = self.GetSubCategoryName
        self.get_service_subcategories()
        addDoneButtonOnKeyboard()
       
    }
    
    
    func registerNibs(){
          SubCategoryCollectionView.register(UINib(nibName: "SubCategoryCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "SubCategoryCell")
  
      }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        self.SubCategoryString_Array.count
       }
       
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           
        let cell =  SubCategoryCollectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoryCell", for: indexPath as IndexPath) as! SubCategoryCollectionViewCell
        
        cell.CategoryName.text = self.SubCategoryString_Array[indexPath.item]

        
        return cell
        
       }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
           let AllData =  self.AllDataDict[indexPath.row]
             self.SelectedSubCatID = AllData["subcategory_id"] as? Int ?? 0
             self.StringSubCatID = String(self.SelectedSubCatID)
             print("StringCatID: \(StringSubCatID)")
             UserDefaults.standard.set(self.StringSubCatID, forKey: "SelectedSubCategoryID") //setsubcategoryID
        
        let stroyboard:UIStoryboard = UIStoryboard(name: "DashFlow", bundle: nil)
        let objVc:ServicesTimeSlotViewController =
            storyboard?.instantiateViewController(withIdentifier: "ServiceTimetVC") as! ServicesTimeSlotViewController
        
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(objVc, animated: true)
        
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
                if searchText == "" {
                 self.SubCategoryString_Array.removeAll()
                  self.get_service_subcategories()
                  SubCategoryCollectionView.reloadData()
                }
                else {
                  SubCategoryString_Array = searchText.isEmpty ? SubCategoryString_Array : SubCategoryString_Array.filter({(dataString: String) -> Bool in
                      // If dataItem matches the searchText, return true to include it
                      return dataString.range(of: searchText, options: .caseInsensitive) != nil
                      
                  })
                  
                  SubCategoryCollectionView.reloadData()
                  
                }
            }
   
    func get_service_subcategories()
       {
           let url = ConstantsClass.baseUrl + ConstantsClass.get_service_subcategories
           print("url: \(url)")
           let parameters = [
               "category_id" : "\(self.self.GetCategoryID)"
               ] as [String : Any]
           
           print("parameters: \(parameters)")
           
           Alamofire.request(url
               , method: .post, parameters: parameters, encoding:JSONEncoding.default).responseJSON
               {
                   response in
                   
                   if let response = response.result.value as? [String:Any] {
                       print("response: \(response)")
                    if (response["status"] as? Bool) != nil {
                        if let respArray = response["response"] as? [[String:Any]] {
                            print("respArray: \(respArray)")
                            self.AllDataDict = response["response"] as! [NSDictionary]
                            print("AllDataDict:",self.AllDataDict)
                            for Data in respArray
                            {
                                self.category = Data["subcategory_name"]as! String
                                self.SubCategoryString_Array.append(self.category)
                            }
                            print("SubCategoryString_Array: \(self.SubCategoryString_Array)")

                            
                        }
                    }
                    else
                       {
                           self.view.makeToast("Something went wrong", duration: 3.0, position: .bottom)
                           print("Something went wrong")
                       }
                    // Make sure to update UI in main thread
                                 DispatchQueue.main.async {
                          self.SubCategoryCollectionView.reloadData()
                                 }
                   }
                
                   else
                   {
                       print("Error occured") // serialized json response
                   }
           }
       }
    
    @IBAction func BtnServiceClick(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
  
    @IBAction func BackBtnClicked(_ sender: Any) {
         navigationController?.popViewController(animated: true)
    }
    
    @IBAction func NotificationBtnClick(_ sender: Any) {
        
        let stroyboard:UIStoryboard = UIStoryboard(name: "DashFlow", bundle: nil)
        let objVc:NotificationViewController =
            storyboard?.instantiateViewController(withIdentifier: "NotificationVc") as! NotificationViewController
        
        self.navigationController?.pushViewController(objVc, animated: true)
    }
    
    func addDoneButtonOnKeyboard()
           {
               let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 20))
               doneToolbar.barStyle = .default
               let Cancel: UIBarButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(self.doneButtonAction))
               let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
               let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
               
               let items = [Cancel,flexSpace, done]
               doneToolbar.items = items
               doneToolbar.sizeToFit()
               
               self.SearchBar.inputAccessoryView = doneToolbar
           }
           
           @objc func doneButtonAction()
           {
               self.SearchBar.resignFirstResponder()
           }
           
           func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
               self.SearchBar.resignFirstResponder()
               return Int(string) != nil
           }
           
           @objc func keyboardWillShow(notification: NSNotification) {
               if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                   if self.view.frame.origin.y == 0 {
                       self.view.frame.origin.y -= keyboardSize.height
       //                self.view.frame.origin.y -= 150
                   }
               }
           }
           
           @objc func keyboardWillHide(notification: NSNotification) {
               if self.view.frame.origin.y != 0 {
                                       self.view.frame.origin.y = 0
       //            self.view.frame.origin.y += 150
               }
           }
           
           override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
               view.endEditing(true)
               super.touchesBegan(touches, with: event)
           }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
