//
//  SubCategoryCollectionViewCell.swift
//  LMRA
//
//  Created by Mac on 10/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class SubCategoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var CategoryImage: UIImageView!
    
    @IBOutlet weak var CategoryName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
         MainView.layer.cornerRadius = 2;
    }

}
