//
//  ThirdTabViewController.swift
//  LMRA
//
//  Created by Mac on 17/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit


class ThirdTabViewController: UIViewController {
    
    @IBOutlet weak var BtnNotification: UIButton!
    
    @IBOutlet weak var UpcomingView: UIView!
    @IBOutlet weak var OpenView: UIView!
    @IBOutlet weak var BtnUpcoming: UIButton!
    @IBOutlet weak var BtnOpen: UIButton!
    
    var Tasks:TaskPageViewController!
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "TaskSegue")
        {
            if segue.destination.isKind(of: TaskPageViewController.self)
            {
                Tasks = (segue.destination as? TaskPageViewController)!
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
         //view.backgroundColor = COLOR.PurpleColor
         tabBarController?.selectedIndex = 3
//        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
//        swipeLeft.direction = .left
//        self.view.addGestureRecognizer(swipeLeft)
        
        self.BtnOpen.setTitleColor(COLOR.PinkColor, for: .normal)
        self.OpenView.backgroundColor = COLOR.PinkColor
        self.BtnUpcoming.setTitleColor(COLOR.WhiteColor, for: .normal)
        self.UpcomingView.backgroundColor = COLOR.WhiteColor //remove
        self.UpcomingView.isHidden = true
  
    }
 
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//         return .lightContent
//     }
       
    
    @objc func ChangeButtonColor(_ notification: NSNotification) {
        let GetIndex = notification.userInfo?["Index"] as? String ?? "0"
        print("getindex:",GetIndex)
        if GetIndex == "0"
        {
            print("index0")
            BtnOpen.setTitleColor(COLOR.PinkColor, for: .normal)
            OpenView.backgroundColor = COLOR.PinkColor
            BtnUpcoming.setTitleColor(COLOR.WhiteColor, for: .normal)
            UpcomingView.backgroundColor = COLOR.WhiteColor
            self.UpcomingView.isHidden = true
            self.OpenView.isHidden = false
            
           
        }
        if GetIndex == "1"
        {
            print("index1")
            BtnUpcoming.setTitleColor(COLOR.PinkColor, for: .normal)
            UpcomingView.backgroundColor = COLOR.PinkColor
            BtnOpen.setTitleColor(COLOR.WhiteColor, for: .normal)
            OpenView.backgroundColor = COLOR.WhiteColor
            self.OpenView.isHidden = true
            self.UpcomingView.isHidden = false

        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        self.BtnOpen.setTitleColor(COLOR.PinkColor, for: .normal)
//        self.OpenView.backgroundColor = COLOR.PinkColor
//        self.BtnUpcoming.setTitleColor(COLOR.WhiteColor, for: .normal)
//        self.UpcomingView.backgroundColor = COLOR.WhiteColor
         GLOBALVARIABLE.TabBarName = "ThirdTab"
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.ChangeButtonColor(_:)), name: NSNotification.Name(rawValue: "ButtonChangeColor"), object: nil)
        
    }
    
    @IBAction func OpenClick(_ sender: Any) {
        
        self.BtnOpen.setTitleColor(COLOR.PinkColor, for: .normal)
        self.OpenView.backgroundColor = COLOR.PinkColor
        self.BtnUpcoming.setTitleColor(COLOR.WhiteColor, for: .normal)
        self.UpcomingView.backgroundColor = COLOR.WhiteColor // remove
        self.UpcomingView.isHidden = true
        self.OpenView.isHidden = false
        
        Tasks.setViewControllerFromIndex(index: 0)
        self.BtnOpen.tag = 1
    }
    
    @IBAction func UpcomingClick(_ sender: Any) {
        
        self.BtnUpcoming.setTitleColor(COLOR.PinkColor, for: .normal)
        self.UpcomingView.backgroundColor = COLOR.PinkColor
        self.BtnOpen.setTitleColor(COLOR.WhiteColor, for: .normal)
        self.OpenView.backgroundColor = COLOR.WhiteColor //remove
        self.OpenView.isHidden = true
        self.UpcomingView.isHidden = false
        Tasks.setViewControllerFromIndex(index: 1)
//        self.BtnUpcoming.tag = 2
    }
    
    @IBAction func NotificationClick(_ sender: Any) {
        
        let stroyboard:UIStoryboard = UIStoryboard(name: "DashFlow", bundle: nil)
        let objVc:NotificationViewController =
            storyboard?.instantiateViewController(withIdentifier: "NotificationVc") as! NotificationViewController
        
        self.navigationController?.pushViewController(objVc, animated: true)
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case .right:
                print("Swiped right")
               
            case .down:
                print("Swiped down")
            case .left:
                
                print("Swiped left")
                self.tabBarController?.selectedIndex = 3;
            case .up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
}
