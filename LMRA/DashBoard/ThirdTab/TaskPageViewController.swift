//
//  TaskPageViewController.swift
//  LMRA
//
//  Created by Mac on 05/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit



class TaskPageViewController: UIPageViewController {
    var ThirdPage: ThirdTabViewController!
    
    private(set) lazy var orderedViewControllers: [UIViewController] = {
        return [UIStoryboard(name: "DashFlow", bundle: nil) .
            instantiateViewController(withIdentifier: "OpenTaskVC") as! OpenTaskViewController,UIStoryboard(name: "DashFlow", bundle: nil) .instantiateViewController(withIdentifier: "UpcomingTaskVC")as! UpcomingTaskViewController]
    }()
    
    
     override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.dataSource = self
        setViewControllerFromIndex(index: 0)
        // Do any additional setup after loading the view.
    }
    
  
    func setViewControllerFromIndex(index:Int) {
        self.setViewControllers([orderedViewControllers[index]],direction : UIPageViewController.NavigationDirection.forward,animated: true ,completion: nil)

    }
    
}
// MARK: UIPageViewControllerDataSource

extension TaskPageViewController: UIPageViewControllerDataSource,UIPageViewControllerDelegate{
    
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        
        return orderedViewControllers.count
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let CurrentIndex:Int = orderedViewControllers.firstIndex(of: viewController) ?? 0
//        print("CurrentIndex:",CurrentIndex)
        
        let SelectedIndex0:[String: String] = ["Index": "\(CurrentIndex)"  ]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ButtonChangeColor"), object: nil, userInfo: SelectedIndex0)
        
        if(CurrentIndex <= 0)
        {
            
            return nil
        }
      
        
       
        return orderedViewControllers[CurrentIndex-1]
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let CurrentIndex:Int = orderedViewControllers.firstIndex(of:viewController) ?? 0
//        print("CurrentIndex1:",CurrentIndex)
        let SelectedIndex1:[String: String] = ["Index": "\(CurrentIndex)"  ]
        
         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ButtonChangeColor"), object: nil, userInfo: SelectedIndex1)
        if(CurrentIndex >= orderedViewControllers.count-1)
        {
            return nil
        }
        

        return orderedViewControllers[CurrentIndex+1]
        
    }
    
}
