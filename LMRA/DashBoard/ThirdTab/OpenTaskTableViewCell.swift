//
//  OpenTaskTableViewCell.swift
//  LMRA
//
//  Created by Mac on 05/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class OpenTaskTableViewCell: UITableViewCell {

    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var LblTaskerTechnician: UILabel!
    @IBOutlet weak var BtnForwardArrow: UIButton!
    @IBOutlet weak var LblTaskStatus: UILabel!
    @IBOutlet weak var LblTaskTitle: UILabel!
    var TaskerId:Int!
    override func awakeFromNib() {
        super.awakeFromNib()
        MainView.layer.shadowColor = UIColor.gray.cgColor
        MainView.layer.shadowOpacity = 0.5
        MainView.layer.shadowOffset = CGSize.zero
        MainView.layer.shadowRadius = 6
        MainView.layer.cornerRadius = 5
        clipsToBounds = false
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bindCelldata(AllDataDict:[String:Any]){

        self.LblTaskTitle.text = AllDataDict["title"] as? String ?? ""
        self.LblTaskStatus.text = AllDataDict["invitation_status"] as? String ?? ""
        self.LblTaskerTechnician.text = AllDataDict["subcategory_name"] as? String ?? ""
        self.TaskerId = AllDataDict["id"] as? Int ?? 0
        print(" self.TaskerId...",self.TaskerId)
       }

}
