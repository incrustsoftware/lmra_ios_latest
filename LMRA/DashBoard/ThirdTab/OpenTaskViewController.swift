//
//  OpenTaskViewController.swift
//  LMRA
//
//  Created by Mac on 05/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire

class OpenTaskViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var OpenTaskTableView: UITableView!
    
    var authentication_key_val = ""
    var AllDataDict = [[String:Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        OpenTaskTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
   self.authentication_key_val = UserDefaults.standard.string(forKey: "authentication_key")!//ge
    print("authentication_key_valOpen:",self.authentication_key_val)
       
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
         self.open_tasks_customer()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.AllDataDict.count

      
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = OpenTaskTableView.dequeueReusableCell(withIdentifier: "OpenTaskCell", for: indexPath as IndexPath) as! OpenTaskTableViewCell
        
        cell.bindCelldata(AllDataDict: self.AllDataDict[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         let cell = OpenTaskTableView.dequeueReusableCell(withIdentifier: "OpenTaskCell", for: indexPath as IndexPath) as! OpenTaskTableViewCell
        
        let AllData =  self.AllDataDict[indexPath.row]
        let TaskID = AllData["id"] as?Int
        let StringTaskId = TaskID
        UserDefaults.standard.set(StringTaskId, forKey:"CustTaskerIDKey")
        let invitation_status = AllData["invitation_status"] as? String ?? ""
        if (invitation_status == "Waiting")
        {
            self.RedirectWaiting()

        }
        if (invitation_status == "Accepted")
        {
            self.RedirectAccepted()
        }

     
        
        
    }
    
    
    func RedirectAccepted()
       {
           let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
           let nextViewController = storyBoard.instantiateViewController(withIdentifier: "AcceptedVC") as! AcceptedTaskViewController
           self.navigationController?.pushViewController(nextViewController, animated: true)
       }
       
       func RedirectWaiting()
       {
           let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
           let nextViewController = storyBoard.instantiateViewController(withIdentifier: "WaitingTaskVC") as! WaitingTaskDetailsViewController
           self.navigationController?.pushViewController(nextViewController, animated: true)
       }
    func open_tasks_customer()
       {
           let urlString = ConstantsClass.baseUrl + ConstantsClass.open_tasks_customer
           print("urlString: \(urlString)")
           
           let headers = [
               "Authorization": "\(self.authentication_key_val)"
           ]
           Alamofire.request(urlString, method: .get, parameters: nil, encoding: URLEncoding.default,headers: headers).responseJSON
               {
                   response in
                   if let response = response.result.value as? [String:Any] {
                       print("response: \(response)")
                       
                      if let status = response["status"] as? Bool {
                       print("Get status true here")

                       if status
                       {
                           if let respArrayOpenTask = response["response"] as? [[String:Any]] {
                               print("respArrayOpenTask: \(respArrayOpenTask)")
                             self.AllDataDict = respArrayOpenTask
                             
                           }
                       }
                       else
                       {
                        self.OpenTaskTableView.isHidden = true
                        }
                    }
                       // Make sure to update UI in main thread
                       DispatchQueue.main.async {
                        Helper.sharedInstance.hideLoader()
                           self.OpenTaskTableView.reloadData()
                       }
                   }
                   else
                   {
                    Helper.sharedInstance.hideLoader()
                       print("Error occured") // serialized json response
                   }
           }
           
           
       }
    

}
