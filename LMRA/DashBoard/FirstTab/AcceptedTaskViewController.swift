//
//  AcceptedTaskViewController.swift
//  LMRA
//
//  Created by Mac on 03/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire
import MessageUI

class AcceptedTaskViewController: UIViewController,MFMessageComposeViewControllerDelegate {
    
    
    @IBOutlet weak var UpperView: UIView!
    @IBOutlet weak var TaskDetailsView: UIView!
    @IBOutlet weak var AppointmentView: UIView!
    @IBOutlet weak var ProfileView: UIView!
    @IBOutlet weak var BtnBack: UIButton!
    @IBOutlet weak var LblTaskTitle: UILabel!
    @IBOutlet weak var LblTaskTime: UILabel!
    @IBOutlet weak var ImgTaskerStatus: UIImageView!
    @IBOutlet weak var LblTaskerStatus: UILabel!
    @IBOutlet weak var BtnEdit: UIButton!
    @IBOutlet weak var LblTaskDay: UILabel!
    @IBOutlet weak var LblTaskTimeSlot: UILabel!
    @IBOutlet weak var LblTaskDate: UILabel!
    @IBOutlet weak var LblTaskDayTime: UILabel!
    @IBOutlet weak var LblTaskTitleDetails: UILabel!
    @IBOutlet weak var LblTaskDescription: UILabel!
    @IBOutlet weak var ImgTaskerProfile: UIImageView!
    @IBOutlet weak var LblTaskerProfileName: UILabel!
    @IBOutlet weak var LblTaskerSubProfileName: UILabel!
    @IBOutlet weak var BtnSMS: UIButton!
    @IBOutlet weak var BtnCall: UIButton!
    
    @IBOutlet weak var BtnTAskDetailsForwardArrow: UIButton!
    var CustTask_id = ""
    var authentication_key_val = ""
    var A_invitation_status = ""
    var A_TaskerTitle =  ""
    var A_TaskerDesc = ""
    var A_TaskerDayTime =  ""
    var A_TaskerTimeSlot =  ""
    var A_TaskerDate =  ""
    var A_TaskerDay =  ""
    var convertedate = ""
    var A_TaskerName = ""
    var A_TaskerImageURL = ""
    var A_TaskerMobileNo = ""
    var A_TaskID:Int!
    var A_TaskIDString = ""
    var IsProfileClick:Bool = false
    var AllDataDict = [[String:Any]]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        TaskDetailsView.layer.shadowColor = UIColor.gray.cgColor
        TaskDetailsView.layer.shadowOpacity = 0.5
        TaskDetailsView.layer.shadowOffset = CGSize.zero
        TaskDetailsView.layer.shadowRadius = 6
        TaskDetailsView.layer.cornerRadius = 5
        TaskDetailsView.clipsToBounds = false
        
        ProfileView.layer.shadowColor = UIColor.gray.cgColor
        ProfileView.layer.shadowOpacity = 0.5
        ProfileView.layer.shadowOffset = CGSize.zero
        ProfileView.layer.shadowRadius = 6
        ProfileView.layer.cornerRadius = 5
        ProfileView.clipsToBounds = false
        
        AppointmentView.layer.shadowColor = UIColor.gray.cgColor
        AppointmentView.layer.shadowOpacity = 0.5
        AppointmentView.layer.shadowOffset = CGSize.zero
        AppointmentView.layer.shadowRadius = 6
        AppointmentView.layer.cornerRadius = 5
        AppointmentView.clipsToBounds = false
        
        self.BtnSMS.layer.cornerRadius = 2
        self.BtnSMS.layer.borderWidth = 1
        self.BtnSMS.layer.borderColor = COLOR.pinkCgColor
        self.BtnSMS.setTitleColor(COLOR.PinkColor, for: .normal)
        self.BtnSMS.backgroundColor = COLOR.WhiteColor
        
        self.BtnCall.setTitleColor(COLOR.WhiteColor, for: .normal)
        
        self.BtnCall.backgroundColor = COLOR.PinkColor
        self.BtnCall.layer.borderColor = COLOR.pinkCgColor
        self.BtnCall.layer.cornerRadius = 2
        self.BtnCall.layer.borderWidth = 1
        
        TaskDetailsView.addTapGesture{
            let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TaskerTaskVC") as! TaskerTaskDetailsViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        
        ProfileView.addTapGesture{
            let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TaskerProfileDetailsVC") as! TaskerProfileDetailsViewController
            nextViewController.IsProfileClick = true
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        
        self.ImgTaskerProfile.layer.cornerRadius = self.ImgTaskerProfile.frame.size.width/2
        self.ImgTaskerProfile.clipsToBounds = true
        self.ImgTaskerProfile.contentMode = .scaleAspectFill //3

        self.authentication_key_val = UserDefaults.standard.string(forKey: "authentication_key") ?? ""//ge
        self.CustTask_id = UserDefaults.standard.string(forKey: "CustTaskerIDKey") ?? ""//get
        print("CustTask_id....: \(self.CustTask_id)")
        
        self.waiting_accepted_task_details()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func TaskDetailsForwardClick(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TaskerTaskVC") as! TaskerTaskDetailsViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    @IBAction func BtnProfileArrowClick(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TaskerProfileDetailsVC") as! TaskerProfileDetailsViewController
        nextViewController.IsProfileClick = true
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
    
    @IBAction func BackBtnClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        //        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func BtnSMSClick(_ sender: Any) {
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = "Message Body"
            controller.recipients = [self.A_TaskerMobileNo]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    @IBAction func BtnCallClick(_ sender: Any) {
        
        if let url = URL(string: "tel://\(self.A_TaskerMobileNo)"),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func waiting_accepted_task_details()
    {
        
        
        let urlString = ConstantsClass.baseUrl + ConstantsClass.waiting_accepted_task_details
        print("urlString: \(urlString)")
        
        let headers = [
            "Authorization": "\(self.authentication_key_val)"
        ]
        print("headers: \(headers)")
        
        let parameters = [
            "task_id" : "\(self.CustTask_id)",
            ] as [String : AnyObject]
        print("parametersaddtaskdetails: \(parameters)")
        Alamofire.request(urlString
            , method: .post, parameters: parameters, encoding:JSONEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if (response["status"] as? Bool) != nil {
                        if let respArrayGetTasker = response["response"] as? [[String:Any]] {
                            print("respArrayTasker: \(respArrayGetTasker)")
                            self.AllDataDict = respArrayGetTasker
                            
                            print("AllDataDict:",self.AllDataDict)
                            
                            for AllData in self.AllDataDict
                            {
                                self.A_TaskID = AllData["task_id"] as! Int
                                self.A_TaskIDString = String(self.A_TaskID)
                                self.A_invitation_status = AllData["invitation_status"] as? String ?? ""
                                self.A_TaskerTitle = AllData["title"]as! String
                                self.A_TaskerDesc = AllData["description"]as! String
                                self.A_TaskerDayTime = AllData["day_time"]as! String
                                self.A_TaskerTimeSlot = AllData["timeslot"]as! String
                                self.A_TaskerDate = AllData["date"]as! String
                                self.convertedate = StaticUtility.convertDateFormater4(self.A_TaskerDate)
                                self.A_TaskerDay = AllData["task_day"]as! String
                            }
                            
                            for AllData in self.AllDataDict
                            {
                                if let tasker_data = AllData["tasker_data"] as? [[String:Any]]
                                {
                                    for TaskerInfo in tasker_data
                                    {
                                        self.A_TaskerName = TaskerInfo["name"]as! String
                                        self.A_TaskerImageURL =  TaskerInfo["profile_image"]as! String
                                        self.A_TaskerMobileNo = TaskerInfo["cell_phone_number"] as! String
                                        
                                    }
                                    print("self.A_TaskerName:",self.A_TaskerName)
                                    print("self.A_TaskerImageURL:",self.A_TaskerImageURL)
                                }
                            }
                            print("A_invitation_status:",self.A_invitation_status)
                            if self.A_invitation_status == "Accepted"
                            {
                                self.LblTaskerStatus.text = self.A_TaskerName + " accepted your task"
                            }
                            self.LblTaskTitleDetails.text = self.A_TaskerTitle
                            self.LblTaskDescription.text = self.A_TaskerDesc
                            
                            self.LblTaskDay.text = self.A_TaskerDay
                            self.LblTaskDate.text = self.convertedate
                            self.LblTaskDayTime.text = self.A_TaskerDayTime
                            self.LblTaskerProfileName.text = self.A_TaskerName
                            self.ImgTaskerProfile.downloaded(from: self.A_TaskerImageURL)
                            self.LblTaskTimeSlot.text = self.A_TaskerTimeSlot
                            
                            UserDefaults.standard.set(self.A_TaskIDString, forKey: "TaskerTaskIDKey")
                            
                        }
                    }
                        
                    else
                    {
                        
                    }
                    // Make sure to update UI in main thread
                    DispatchQueue.main.async {
                        print("waiting_task_details_list_success")
                        
                        
                        
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
