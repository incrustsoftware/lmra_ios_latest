//
//  WaitingTaskDetailsViewController.swift
//  LMRA
//
//  Created by Mac on 03/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire

class WaitingTaskDetailsViewController: UIViewController {
    @IBOutlet weak var UpperView: UIView!
    @IBOutlet weak var TaskDetailsView: UIView!
    @IBOutlet weak var AppointmentView: UIView!
    
    @IBOutlet weak var BtnTaskDetailsForward: UIButton!
    @IBOutlet weak var BtnInviteAnotherTasker: UIButton!
    @IBOutlet weak var BtnCancelTask: UIButton!
    
    @IBOutlet weak var BtnBack: UIButton!
    
    @IBOutlet weak var LblTaskTitle: UILabel!
    
    @IBOutlet weak var LblTaskDescription: UILabel!
    @IBOutlet weak var LblTaskTime: UILabel!
    
    @IBOutlet weak var ImgTaskerStatus: UIImageView!
    @IBOutlet weak var LblTaskerStatus: UILabel!
    @IBOutlet weak var BtnEdit: UIButton!
    
    @IBOutlet weak var LblTaskDay: UILabel!
    
    @IBOutlet weak var LblTaskTimeSlot: UILabel!
    @IBOutlet weak var LblTaskDate: UILabel!
    @IBOutlet weak var LblTaskDayTime: UILabel!
    
    
    
    var CustTask_id = ""
    var authentication_key_val = ""
    var W_invitation_status = ""
    var W_TaskerTitle =  ""
    var W_TaskerDesc = ""
    var W_TaskerDayTime =  ""
    var W_TaskerTimeSlot =  ""
    var W_TaskerDate =  ""
    var W_TaskerDay =  ""
    var convertedate = ""
    var W_TaskID:Int!
    var W_TaskIDString = ""
    var W_TaskerName = ""
    var W_TaskerImageURL = ""
    var W_TaskerMobileNo = ""
    var IsCalledFromInviteTasker:Bool = false
    var AllDataDict = [[String:Any]]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        
        AppointmentView.layer.shadowColor = UIColor.gray.cgColor
        AppointmentView.layer.shadowOpacity = 0.5
        AppointmentView.layer.shadowOffset = CGSize.zero
        AppointmentView.layer.shadowRadius = 6
        AppointmentView.layer.cornerRadius = 5
        AppointmentView.clipsToBounds = false
        
        
        TaskDetailsView.layer.shadowColor = UIColor.gray.cgColor
        TaskDetailsView.layer.shadowOpacity = 0.5
        TaskDetailsView.layer.shadowOffset = CGSize.zero
        TaskDetailsView.layer.shadowRadius = 6
        TaskDetailsView.layer.cornerRadius = 5
        TaskDetailsView.clipsToBounds = false
        
        TaskDetailsView.addTapGesture{
            let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TaskerTaskVC") as! TaskerTaskDetailsViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        
        self.BtnCancelTask.layer.cornerRadius = 2
        self.BtnCancelTask.layer.borderWidth = 1
        self.BtnCancelTask.layer.borderColor = COLOR.pinkCgColor
        self.BtnCancelTask.setTitleColor(COLOR.PinkColor, for: .normal)
        self.BtnCancelTask.backgroundColor = COLOR.WhiteColor
        
        self.BtnInviteAnotherTasker.setTitleColor(COLOR.WhiteColor, for: .normal)
        self.BtnInviteAnotherTasker.backgroundColor = COLOR.PinkColor
        self.BtnInviteAnotherTasker.layer.borderColor = COLOR.pinkCgColor
        self.BtnInviteAnotherTasker.layer.cornerRadius = 2
        self.BtnInviteAnotherTasker.layer.borderWidth = 1
        self.authentication_key_val = UserDefaults.standard.string(forKey: "authentication_key")!//ge
        self.CustTask_id = UserDefaults.standard.string(forKey: "CustTaskerIDKey")!//get
        print("CustTask_id....: \(self.CustTask_id)")
        self.waiting_accepted_task_details()
        
        
    }
    
    @IBAction func BtnTaskDetailsForwardClick(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TaskerTaskVC") as! TaskerTaskDetailsViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    @IBAction func BackBtnClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        //        self.dismiss(animated: true, completion: nil)
    }
    
    
    func waiting_accepted_task_details()
    {
        let urlString = ConstantsClass.baseUrl + ConstantsClass.waiting_accepted_task_details
        print("urlString: \(urlString)")
        
        let headers = [
            "Authorization": "\(self.authentication_key_val)"
        ]
        print("headers: \(headers)")
        
        let parameters = [
            "task_id" : "\(self.CustTask_id)",
            ] as [String : Any]
        print("parametersaddtaskdetails: \(parameters)")
        Alamofire.request(urlString
            , method: .post, parameters: parameters, encoding:JSONEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if (response["status"] as? Bool) != nil {
                        if let respArrayGetTasker = response["response"] as? [[String:Any]] {
                            print("respArrayTasker: \(respArrayGetTasker)")
                            self.AllDataDict = respArrayGetTasker
                            
                            print("AllDataDict:",self.AllDataDict)
                            
                            for AllData in self.AllDataDict
                            {
                                self.W_TaskID = AllData["task_id"] as? Int ?? 0
                                self.W_TaskIDString = String(self.W_TaskID)
                                self.W_invitation_status = AllData["invitation_status"] as? String ?? ""
                                self.W_TaskerTitle = AllData["title"]as? String ?? ""
                                self.W_TaskerDesc = AllData["description"]as? String ?? ""
                                self.W_TaskerDayTime = AllData["day_time"]as? String ?? ""
                                self.W_TaskerTimeSlot = AllData["timeslot"]as? String ?? ""
                                self.W_TaskerDate = AllData["date"]as? String ?? ""
                                self.convertedate = StaticUtility.convertDateFormater4(self.W_TaskerDate)
                                self.W_TaskerDay = AllData["task_day"]as? String ?? ""
                                
                                
                                
                            }
                            for AllData in self.AllDataDict
                            {
                                if let tasker_data = AllData["tasker_data"] as? [[String:Any]]
                                {
                                    for TaskerInfo in tasker_data
                                    {
                                        self.W_TaskerName = TaskerInfo["name"]as! String
                                        self.W_TaskerImageURL =  TaskerInfo["profile_image"]as! String
                                        self.W_TaskerMobileNo = TaskerInfo["cell_phone_number"] as! String
                                        
                                    }
                                    print("self.A_TaskerName:",self.W_TaskerName)
                                    print("self.A_TaskerImageURL:",self.W_TaskerImageURL)
                                }
                            }
                            
                            print("W_invitation_status:",self.W_invitation_status)
                            if self.W_invitation_status == "Waiting"
                            {
                                self.LblTaskerStatus.text = "Waiting on a " + self.W_TaskerName + " tasker to accept your job"
                            }
                            self.LblTaskTitle.text = self.W_TaskerTitle
                            self.LblTaskDescription.text = self.W_TaskerDesc
                            
                            self.LblTaskDay.text = self.W_TaskerDay
                            self.LblTaskDate.text = self.convertedate
                            self.LblTaskDayTime.text = self.W_TaskerDayTime
                            self.LblTaskTimeSlot.text = self.W_TaskerTimeSlot
                            //                                self.LblTaskTime.text = self.W_TaskerDayTime
                            print("self.W_TaskerTimeSlot:",self.W_TaskerTimeSlot)
                            UserDefaults.standard.set(self.W_TaskIDString, forKey: "TaskerTaskIDKey")
                            
                        }
                    }
                        
                    else
                    {
                        
                    }
                    // Make sure to update UI in main thread
                    DispatchQueue.main.async {
                        print("waiting_task_details_list_success")
                        
                        
                        
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
        
    }
    
    
    func cancel_task_customer()
    {
        let urlString = ConstantsClass.baseUrl + ConstantsClass.cancel_task_customer
        print("urlString: \(urlString)")
        
        let headers = [
            "Authorization": "\(self.authentication_key_val)"
        ]
        print("headers: \(headers)")
        
        let parameters = [
            "task_id" : "\(self.CustTask_id)",
            ] as [String : Any]
        print("parametersCancleTask: \(parameters)")
        Alamofire.request(urlString
            , method: .post, parameters: parameters, encoding:JSONEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    if let status = response["status"] as? Bool {
                        if status
                        {
                            print("cancel_task_success")
                            self.navigationController?.popViewController(animated: true)
                        }
                        
                    }
                        
                    else
                    {
                        
                    }
                    // Make sure to update UI in main thread
                    DispatchQueue.main.async {
                        print("Canceled Task Successfully")
                        
                        
                        
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
        
    }
    
    
    @IBAction func InviteAnotherTaskerBtnClick(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SearchResultVC") as! SearchResultViewController
        nextViewController.IsCalledFromInviteTasker = true
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    
    @IBAction func CancelBtnClick(_ sender: Any) {
        self.cancel_task_customer()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
