//
//  CustomerTaskUpdateTableViewCell.swift
//  LMRA
//
//  Created by Mac on 02/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class CustomerTaskUpdateTableViewCell: UITableViewCell {
    
    @IBOutlet weak var InnerView1: UIView!
    @IBOutlet weak var Innerview2: UIView!
    @IBOutlet weak var LblTaskupdateTime: UILabel!
    @IBOutlet weak var ImageTaskUpdate: UIImageView!
    @IBOutlet weak var LblTaskUpdateStatus: UILabel!
    @IBOutlet weak var LblTaskUpdateServiceName: UILabel!
    @IBOutlet weak var LblTaskUpdateDesc: UILabel!
    @IBOutlet weak var LblTaskTitle: UILabel!
    @IBOutlet weak var ImageTaskerAvailable: UIImageView!
    @IBOutlet weak var LblTaskerAvailableName: UILabel!
    @IBOutlet weak var BtnTaskUpateArrow: UIButton!
    @IBOutlet weak var LblTaskerAvailableSubName: UILabel!
    @IBOutlet weak var BtnSMS: UIButton!
    @IBOutlet weak var BtnSMSCALLStackView: UIStackView!
    @IBOutlet weak var BtnCall: UIButton!
    @IBOutlet weak var BtnTaskerAvailableArrow: UIButton!
    var AllTaskerStatus = ""
    var GetAllTaskerStatus = ""
    var AllTaskerName = ""
    var AllTaskerFamilyName = ""
    var AllTaskerImageUrl = ""
    var AllTaskerDesc = ""
    var AllTaskerUpdateTime = ""
    var GetTaskerMobile = ""
    var GetMobileNumber = ""
    var GetServiceName = ""
    var GetTaskerName = ""
    var GetTaskerID:Int!
    var GetAllStatusArray = [String]()
    var GetTaskerMobileArray = [String]()
    var AllTaskerID:Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        
        self.BtnSMS.layer.cornerRadius = 2
        self.BtnSMS.layer.borderWidth = 1
        self.BtnSMS.layer.borderColor = COLOR.pinkCgColor
        self.BtnSMS.setTitleColor(COLOR.PinkColor, for: .normal)
        self.BtnSMS.backgroundColor = COLOR.WhiteColor
        
        
        self.BtnCall.backgroundColor = COLOR.PinkColor
        self.BtnCall.layer.borderColor = COLOR.pinkCgColor
        self.BtnCall.setTitleColor(COLOR.WhiteColor, for: .normal)
        self.BtnCall.layer.cornerRadius = 2
        self.BtnCall.layer.borderWidth = 1
        
        
        InnerView1.layer.cornerRadius = 5
        InnerView1.clipsToBounds = false
        
        Innerview2.layer.cornerRadius = 5
        Innerview2.clipsToBounds = false
        
        ImageTaskerAvailable.layer.cornerRadius = ImageTaskerAvailable.frame.height/2
        ImageTaskerAvailable.clipsToBounds = true
        
        
        // Initialization code
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func bindCelldata(AllDataDict:[String:Any]){
        
        self.AllTaskerName = ""
        self.AllTaskerFamilyName = ""
        
        self.AllTaskerUpdateTime = AllDataDict["time"] as? String ?? "0"
        self.LblTaskupdateTime.text = self.AllTaskerUpdateTime + "pm"
        self.LblTaskTitle.text = AllDataDict["title"] as? String ?? ""
        self.LblTaskUpdateDesc.text = AllDataDict["description"] as? String ?? ""
        self.LblTaskUpdateServiceName.text = AllDataDict["subcategory_name"] as? String ?? ""
        self.AllTaskerID = AllDataDict["task_id"] as? Int
        self.GetAllTaskerStatus = AllDataDict["invitation_status"] as? String ?? "0"
        
        
        if let tasker_data = AllDataDict["tasker_data"]as? [[String:Any]]
        {
            
            for TaskerInfo  in tasker_data
            {
                self.GetTaskerMobile = ""
                self.AllTaskerImageUrl = ""
                self.GetTaskerMobile = "\(self.GetTaskerMobile)\(TaskerInfo["cell_phone_number"] as? String ?? "")"
                self.GetTaskerMobileArray.append(self.GetTaskerMobile)
                self.AllTaskerImageUrl = "\(self.AllTaskerImageUrl)\(TaskerInfo["profile_image"] as? String ?? "")"
                self.AllTaskerName =  "\(self.AllTaskerName) \(TaskerInfo["name"] as? String ?? "")"
                self.AllTaskerFamilyName =  "\(self.AllTaskerFamilyName) \(TaskerInfo["family_name"] as? String ?? "")"
                self.GetTaskerID = TaskerInfo["id"] as? Int ?? 0
                print ("AllTaskerImageUrl.....: \(self.AllTaskerImageUrl)")
                print ("GetTaskerID.....:",self.GetTaskerID)
                self.ImageTaskerAvailable.downloaded(from: self.AllTaskerImageUrl)
                self.ImageTaskerAvailable.contentMode = .scaleAspectFill //3
                self.LblTaskerAvailableName.text = AllTaskerName + " " + self.AllTaskerFamilyName
                UserDefaults.standard.set( self.GetTaskerMobileArray, forKey: "TaskermobileKey")
                
                
            }
        }
        
        if self.GetAllTaskerStatus == "Accepted"
        {
            self.LblTaskupdateTime.text = AllDataDict["task_update_time"] as? String ?? ""
            print ("LblTaskupdateTime.....: \(self.LblTaskupdateTime.text)")
            self.ImageTaskUpdate.image = UIImage(named: "TaskComplete")
            self.LblTaskUpdateStatus.text = self.AllTaskerStatus
            self.Innerview2.isHidden = false
            self.AllTaskerStatus = self.AllTaskerName + self.AllTaskerFamilyName + " Accepted your task" //for now
            
            
        }
        if self.GetAllTaskerStatus == "Waiting"
        {
            self.LblTaskupdateTime.text = AllDataDict["task_update_time"] as? String ?? ""
            print ("LblTaskupdateTime.....: \(self.LblTaskupdateTime.text)")
            self.ImageTaskUpdate.image = UIImage(named: "WaitingIcon")
            self.LblTaskUpdateStatus.text = self.AllTaskerStatus
            self.Innerview2.isHidden = true
            
            self.AllTaskerStatus = "Waiting for " + self.AllTaskerName +  self.AllTaskerFamilyName + " to accept"
        }
        
        
    }

    
}
