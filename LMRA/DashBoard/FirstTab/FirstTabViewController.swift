//
//  FirstTabViewController.swift
//  LMRA
//
//  Created by Mac on 17/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//
import Foundation
import UIKit
import Alamofire
import MessageUI

@available(iOS 12.0, *)
class FirstTabViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,MFMessageComposeViewControllerDelegate
{
   
    
    
    @IBOutlet weak var LblDayTimeMessage: UILabel!
    @IBOutlet weak var LblCutomerName: UILabel!
    @IBOutlet weak var View1: UIView!
    @IBOutlet weak var View2: UIView!
    @IBOutlet weak var AcceptedTaskTableView: UITableView!
    var authentication_key_val = ""
    var GetCustomerName = ""
    var SelectedGetTaskerID = ""
    var BtnSelctedTag:Int!
    var BtnSelctedTag1:Int!
     var BtnSelctedTag2:Int!
    var BtnSelctedTagProfileArrow:Int!
    var AllDataDict = [[String:Any]]()
    var GetMobileArray = [String]()
    var GetTaskerMobileNumer = ""
    var IsProfileArrowClick:Bool = false
    let cellSpacingHeight: CGFloat = 50
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNeedsStatusBarAppearanceUpdate()

       // view.backgroundColor = COLOR.PurpleColor
    
        // let hour = NSCalendar.currentCalendar().component(.Hour, fromDate: NSDate()) Swift 2 legacy
        let hour = Calendar.current.component(.hour, from: Date())

        switch hour {
        case 6..<12 : self.LblDayTimeMessage.text = (NSLocalizedString("Good Morning", comment: "Morning"))
        case 12 : self.LblDayTimeMessage.text = (NSLocalizedString("Good Noon", comment: "Noon"))
        case 13..<17 :self.LblDayTimeMessage.text = (NSLocalizedString("Good Afternoon", comment: "Afternoon"))
        case 17..<22 : self.LblDayTimeMessage.text = (NSLocalizedString("Good Evening", comment: "Evening"))
        default: self.LblDayTimeMessage.text = (NSLocalizedString("Good Night", comment: "Night"))
        }
        
        tabBarController?.selectedIndex = 0
//        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
//               swipeLeft.direction = .left
//               self.view.addGestureRecognizer(swipeLeft)
        
//        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
//        swipeRight.direction = .right
//        self.view.addGestureRecognizer(swipeRight)
//
        // Hide the Navigation Bar
        //        self.navigationController?.setNavigationBarHidden(true, animated: true)        // Do any additional setup after loading the view.
        
        self.authentication_key_val = UserDefaults.standard.string(forKey: "authentication_key") ?? ""//get
        self.GetCustomerName = UserDefaults.standard.string(forKey: "LoginTaskerNameKey") ?? ""
       
        self.LblCutomerName.text = self.GetCustomerName
        AcceptedTaskTableView.delegate = self
        AcceptedTaskTableView.dataSource = self
//        self.View1.isHidden = false
        self.get_customer_task_list()
      
        
    }
    
//  override var preferredStatusBarStyle: UIStatusBarStyle {
//      return .lightContent
//  }
    
    
    override func viewWillAppear(_ animated: Bool) {
   
       
        GLOBALVARIABLE.TabBarName = "FirstTab"
        self.tabBarController?.tabBar.isHidden = false
       self.get_customer_task_list()

    }
    
  
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.AllDataDict.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = AcceptedTaskTableView.dequeueReusableCell(withIdentifier: "CustomerCell", for: indexPath as IndexPath) as! CustomerTaskUpdateTableViewCell
        
       
        
        
        cell.bindCelldata(AllDataDict: self.AllDataDict[indexPath.row])
        
        let AllData =  self.AllDataDict[indexPath.row]
        let invitation_status = AllData["invitation_status"] as? String ?? ""
        
        cell.BtnTaskUpateArrow.tag = indexPath.row
        cell.BtnCall.tag = indexPath.row
        cell.BtnSMS.tag = indexPath.row
        cell.BtnTaskerAvailableArrow.tag = indexPath.row
        
        cell.BtnTaskUpateArrow.addTarget(self, action: #selector(BtnPressed), for: .touchUpInside)
        cell.BtnCall.addTarget(self, action: #selector(BtnCallPressed), for: .touchUpInside)
        cell.BtnSMS.addTarget(self, action: #selector(BtnSMSPressed), for: .touchUpInside)
        cell.BtnTaskerAvailableArrow.addTarget(self, action: #selector(BtnProfileArrow), for: .touchUpInside)
        
        self.GetMobileArray = cell.GetTaskerMobileArray

        if self.BtnSelctedTag ==  indexPath.row{
            self.BtnSelctedTag = nil
             let TaskID = AllData["task_id"] as?Int
             let StringTaskId = TaskID

            UserDefaults.standard.set(StringTaskId, forKey:"CustTaskerIDKey")
          
            if(invitation_status == "Accepted")
            {
                
                self.RedirectAccepted()
            }
            
            if(invitation_status == "Waiting")
            {
                self.RedirectWaiting()
            }
        }
        
        if self.BtnSelctedTag1 ==  indexPath.row{
             self.BtnSelctedTag1 = nil
            self.GetTaskerMobileNumer = self.GetMobileArray[indexPath.row]
            print("GetTaskerMobileNumer:\(GetTaskerMobileNumer)")
            if let url = URL(string: "tel://\(self.GetTaskerMobileNumer)"),
                  UIApplication.shared.canOpenURL(url) {
                  UIApplication.shared.open(url, options: [:], completionHandler: nil)
                   }
            
        }
        
        if self.BtnSelctedTag2 ==  indexPath.row{
            
             self.BtnSelctedTag2 = nil
                self.GetTaskerMobileNumer = self.GetMobileArray[indexPath.row]
                print("GetTaskerMobileNumer:\(GetTaskerMobileNumer)")
               
            if (MFMessageComposeViewController.canSendText()) {
                
                let controller = MFMessageComposeViewController()
                controller.body = "Message Body"
                controller.recipients = [self.GetTaskerMobileNumer]
                controller.messageComposeDelegate = self
                self.present(controller, animated: true, completion: nil)
            }
            }
        
        
        if self.BtnSelctedTagProfileArrow ==  indexPath.row{
            self.BtnSelctedTagProfileArrow = nil

            self.SelectedGetTaskerID = String(cell.GetTaskerID)
        
            UserDefaults.standard.set(self.SelectedGetTaskerID, forKey:"SelectedTaskerID")
            
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TaskerProfileDetailsVC") as! TaskerProfileDetailsViewController
            nextViewController.IsProfileArrowClick = true
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }
        
        
//        let TaskTitleTap = UITapGestureRecognizer(target:self,action:#selector(self.TaskTitleTap))
//        
//        // add it to the label
//        cell.LblTaskTitle.addGestureRecognizer(TaskTitleTap)
//        cell.LblTaskTitle.isUserInteractionEnabled = true
//        
//        let TaskerNameTap = UITapGestureRecognizer(target:self,action:#selector(self.TaskerNameTap))
//        
//        // add it to the label
//        cell.LblTaskerAvailableName.addGestureRecognizer(TaskTitleTap)
//        cell.LblTaskerAvailableName.isUserInteractionEnabled = true
        
        return cell
        
    }
  

    // Set the spacing between sections
       func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
           return cellSpacingHeight
       }
    
    func RedirectAccepted()
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "AcceptedVC") as! AcceptedTaskViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    func RedirectWaiting()
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "WaitingTaskVC") as! WaitingTaskDetailsViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @objc func BtnPressed(sender:UIButton){
        self.BtnSelctedTag = sender.tag
        print("BtnSelctedTag.... :",self.BtnSelctedTag ?? "")
        sender.isSelected  = true
        self.AcceptedTaskTableView.reloadData()
        
    }
    
 @objc func BtnCallPressed(sender:UIButton){
        self.BtnSelctedTag1 = sender.tag
        print("BtnSelctedTag1.... :",self.BtnSelctedTag1 ?? "")
        sender.isSelected  = true
        self.AcceptedTaskTableView.reloadData()
        
    }
    @objc func BtnSMSPressed(sender:UIButton){
        self.BtnSelctedTag2 = sender.tag
        print("BtnSelctedTag2.... :",self.BtnSelctedTag2 ?? "")
   
        self.AcceptedTaskTableView.reloadData()
        
    }
    
    @objc func BtnProfileArrow(sender:UIButton){
           self.IsProfileArrowClick = true
           self.BtnSelctedTagProfileArrow = sender.tag
           print("BtnSelctedTagProfileArrow.... :",self.BtnSelctedTagProfileArrow ?? "")
           sender.isSelected  = true
           self.AcceptedTaskTableView.reloadData()
           
       }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
            self.dismiss(animated: true, completion: nil)
       }
    
    func get_customer_task_list()
    {
        let urlString = ConstantsClass.baseUrl + ConstantsClass.get_customer_task_list
        print("urlString: \(urlString)")
        
        let headers = [
            "Authorization": "\(self.authentication_key_val)"
        ]
        print("headers: \(headers)")
        
        Alamofire.request(urlString, method: .get, parameters: nil, encoding: URLEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if (response["status"] as? Bool) != nil {
                        if let respArrayGetTasker = response["response"] as? [[String:Any]] {
                            print("respArrayGetTasker: \(respArrayGetTasker)")
                            self.AllDataDict = respArrayGetTasker
                            
                            print("AllDataDictCustomerTaskList:",self.AllDataDict)
                            
//                            if self.AllDataDict.count == 0
//                            {
//                                self.AcceptedTaskTableView.isHidden = true
//                                self.View1.isHidden = false
//                            }
//
                        }
                    }
                 if (response["status"] as? Bool) == false
                  {
                    print("task_list_fail")
                    self.View1.isHidden = false
                    self.AcceptedTaskTableView.isHidden = true
                        
                    }
                    // Make sure to update UI in main thread
                    DispatchQueue.main.async {
                        print("Get Task Updatesuccessfully.")
                        self.AcceptedTaskTableView.reloadData()
                        
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
        
        
    }
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
              
              if let swipeGesture = gesture as? UISwipeGestureRecognizer {
                  
                  switch swipeGesture.direction {
                  case .right:
                      print("Swiped right")
                      self.navigationController?.popViewController(animated: true)
                  case .down:
                      print("Swiped down")
                
                  case .left:
              
                      print("Swiped left")
              
                     self.tabBarController?.selectedIndex = 3;
                      
                  case .up:
                      print("Swiped up")
                  default:
                      break
                  }
              }
          }
    
    @IBAction func BookServiceClick(_ sender: Any) {
          self.tabBarController?.selectedIndex = 1;

        
    }
    
    @IBAction func NotificationBtnClick(_ sender: Any) {
        
        let stroyboard:UIStoryboard = UIStoryboard(name: "DashFlow", bundle: nil)
        let objVc:NotificationViewController =
            storyboard?.instantiateViewController(withIdentifier: "NotificationVc") as! NotificationViewController
        self.navigationController?.pushViewController(objVc, animated: true)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
  
 
    
}
