//
//  TaskerTaskDetailsViewController.swift
//  LMRA
//
//  Created by Mac on 04/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire

class TaskerTaskDetailsViewController: UIViewController,UIScrollViewDelegate {
    
    
    @IBOutlet weak var Scrollview: UIScrollView!
    @IBOutlet weak var TaskerDetailsview: UIView!
    @IBOutlet weak var LblTaskDetailsInner: UILabel!
    @IBOutlet weak var LblTaskDetailsOuter: UILabel!
    @IBOutlet weak var BtnCancel: UIButton!
    @IBOutlet weak var BtnBack: UIButton!
    @IBOutlet weak var BtnCancelTask: UIButton!
    @IBOutlet weak var TaskInfoView: UIView!
    @IBOutlet weak var LblTaskDesc: UILabel!
    @IBOutlet weak var LblTaskTitle: UILabel!
    @IBOutlet weak var AppointmentView: UIView!
    @IBOutlet weak var LblTaskDay: UILabel!
    @IBOutlet weak var LblTaskDayTime: UILabel!
    @IBOutlet weak var LblTaskTimeSlot: UILabel!
    @IBOutlet weak var LblTaskDate: UILabel!
    @IBOutlet weak var AddressView: UIView!
    @IBOutlet weak var LblFullAddress: UILabel!
    @IBOutlet weak var LanguageView: UIView!
    @IBOutlet weak var LblTaskerLang: UILabel!
    
    var TaskerTask_Id = ""
    var Task_invitation_status = ""
    var Task_TaskerTitle =  ""
    var Task_TaskerDesc = ""
    var Task_TaskerDayTime =  ""
    var Task_TaskerTimeSlot =  ""
    var Task_TaskerDate =  ""
    var Task_TaskerDay =  ""
    var Task_TaskerLang =  ""
    var GetTask_TaskerLang = ""
    var Task_TaskerLangAray = [String]()
    var convertedate = ""
    var TaskID:Int!
    var TaskIDString = ""
    var FullAddress = ""
    var AllDataDict = [[String:Any]]()
    var authentication_key_val = ""
     var lastContentOffset: CGFloat = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
       self.Scrollview.delegate = self
     self.TaskerDetailsview.isHidden = true
        self.BtnCancelTask.layer.cornerRadius = 2
        self.BtnCancelTask.layer.borderWidth = 1
        self.BtnCancelTask.layer.borderColor = COLOR.pinkCgColor
        self.BtnCancelTask.setTitleColor(COLOR.PinkColor, for: .normal)
        self.BtnCancelTask.backgroundColor = COLOR.WhiteColor
        
        
        
        AddressView.layer.shadowColor = UIColor.gray.cgColor
        AddressView.layer.shadowOpacity = 0.5
        AddressView.layer.shadowOffset = CGSize.zero
        AddressView.layer.shadowRadius = 6
        AddressView.layer.cornerRadius = 5
        AddressView.clipsToBounds = false
        
        TaskInfoView.layer.shadowColor = UIColor.gray.cgColor
        TaskInfoView.layer.shadowOpacity = 0.5
        TaskInfoView.layer.shadowOffset = CGSize.zero
        TaskInfoView.layer.shadowRadius = 6
        TaskInfoView.layer.cornerRadius = 5
        TaskInfoView.clipsToBounds = false
    
        AppointmentView.layer.shadowColor = UIColor.gray.cgColor
        AppointmentView.layer.shadowOpacity = 0.5
        AppointmentView.layer.shadowOffset = CGSize.zero
        AppointmentView.layer.shadowRadius = 6
        AppointmentView.layer.cornerRadius = 5
        AppointmentView.clipsToBounds = false
        
        LanguageView.layer.shadowColor = UIColor.gray.cgColor
        LanguageView.layer.shadowOpacity = 0.5
        LanguageView.layer.shadowOffset = CGSize.zero
        LanguageView.layer.shadowRadius = 6
        LanguageView.layer.cornerRadius = 5
        LanguageView.clipsToBounds = false
        self.authentication_key_val = UserDefaults.standard.string(forKey: "authentication_key")!//get
        self.TaskerTask_Id = UserDefaults.standard.string(forKey: "TaskerTaskIDKey")!//get
        print("TaskerTask_Id: \(self.TaskerTask_Id)")
        
        self.review_task_details()
        // Do any additional setup after loading the view.
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
          self.lastContentOffset = Scrollview.contentOffset.y
      }
      
      // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
      func scrollViewDidScroll(_ scrollView: UIScrollView) {
          if self.lastContentOffset < Scrollview.contentOffset.y {
              // did move up
              self.TaskerDetailsview.isHidden = false
              self.LblTaskDetailsOuter.isHidden = true
          } else if self.lastContentOffset > Scrollview.contentOffset.y {
              // did move down
          } else {
              // didn't move
          }
      }
      
    
    @IBAction func BackBtnClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func CancelClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func review_task_details()
    {
        let urlString = ConstantsClass.baseUrl + ConstantsClass.review_task_details
        print("urlString: \(urlString)")
        
        let headers = [
            "Authorization": "\(self.authentication_key_val)"
        ]
        print("headers: \(headers)")
        
        let parameters = [
            "task_id" : "\(self.TaskerTask_Id)",
            ] as [String : AnyObject]
        print("parametersaddtaskdetails: \(parameters)")
        Alamofire.request(urlString
            , method: .post, parameters: parameters, encoding:JSONEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if (response["status"] as? Bool) != nil {
                        if let respArrayGetTasker = response["response"] as? [[String:Any]] {
                            print("respArrayTaskerDetails: \(respArrayGetTasker)")
                            self.AllDataDict = respArrayGetTasker
                            print("AllDataDict:",self.AllDataDict)
                            //
                            for AllData in self.AllDataDict
                            {
                                
                                //                                self.TaskID = AllData["id"] as? Int
                                //                                self.TaskIDString = String(self.TaskID)
                                self.Task_invitation_status = AllData["invitation_status"] as? String ?? ""
                                self.Task_TaskerTitle = AllData["title"]as! String
                                self.Task_TaskerDesc = AllData["description"]as! String
                                self.Task_TaskerDayTime = AllData["day_time"]as! String
                                self.Task_TaskerTimeSlot = AllData["timeslot"]as! String
                                self.Task_TaskerDate = AllData["date"]as! String
                                self.convertedate = StaticUtility.convertDateFormater4(self.Task_TaskerDate)
                                self.Task_TaskerDay = StaticUtility.convertDateFormater5(self.convertedate)
                                
                            }
                            
                            for Address in self.AllDataDict
                            {
                                
                                if let language_details = Address["address_array"] as? [String:Any] 
                                {
                                    let Area = language_details["area"] as? String ?? ""
                                    let Appartment = language_details["appartment"] as? String ?? ""
                                    let Street = language_details["street"] as? String ?? ""
                                    let BuildingName = language_details["building_name"] as? String ?? ""
                                    let Floor = language_details["floor"] as? String ?? ""
                                    let AppartmentNo = language_details["appartment_no"] as? String ?? ""
                                    let AdditionalDirections = language_details["additional_directions"] as? String ?? ""
                                    
                                    print("Area:",Area)
                                    if (Area != "")
                                    {
                                        self.FullAddress += Area
                                        self.FullAddress += ", "
                                    }
                                    
                                    if (Appartment != "")
                                    {
                                        self.FullAddress += Appartment
                                        self.FullAddress += ", "
                                    }
                                    if (Street != "")
                                    {
                                        self.FullAddress += Street
                                        self.FullAddress += ", "
                                    }
                                    if (BuildingName != "")
                                    {
                                        self.FullAddress += BuildingName
                                        self.FullAddress += ", "
                                    }
                                    if (Floor != "")
                                    {
                                        self.FullAddress += Floor
                                        self.FullAddress += ", "
                                    }
                                    if (AppartmentNo != "")
                                    {
                                        self.FullAddress += AppartmentNo
                                        self.FullAddress += ", "
                                    }
                                    if (AdditionalDirections != "")
                                    {
                                        self.FullAddress += AdditionalDirections
                                    }
                                    else if self.FullAddress != ""
                                    {
                                        self.FullAddress.removeLast()
                                    }
                                    
                                }
                                print("FullAddress:",self.FullAddress)
                                
                            }
                            
                            
                            
                            for AllData in self.AllDataDict
                            {
                                if let language_details = AllData["language_details"] as? [[String:Any]]
                                {
                                    for Languages in language_details
                                    {
                                        self.Task_TaskerLang = Languages["language_name"]as! String
                                        self.Task_TaskerLangAray.append(self.Task_TaskerLang)
                                    }
                                    print("self.Task_TaskerLang:",self.Task_TaskerLang)
                                    
                                }
                            }
                            
                            self.GetTask_TaskerLang = ""
                            self.GetTask_TaskerLang = self.Task_TaskerLangAray.joined(separator: ", ")
                            
                            self.LblTaskTitle.text = self.Task_TaskerTitle
                            self.LblTaskDesc.text = self.Task_TaskerDesc
                            self.LblTaskDay.text = self.Task_TaskerDay
                            self.LblTaskDate.text = self.convertedate
                            self.LblTaskDayTime.text = self.Task_TaskerDayTime
                            self.LblTaskTimeSlot.text = self.Task_TaskerTimeSlot
                            self.LblTaskerLang.text = self.GetTask_TaskerLang
                            self.LblFullAddress.text = self.FullAddress
                            
                        }
                    }
                        
                    else
                    {
                        
                    }
                    // Make sure to update UI in main thread
                    DispatchQueue.main.async {
                        print("review_task_details_list_success")
                        
                        
                        
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
