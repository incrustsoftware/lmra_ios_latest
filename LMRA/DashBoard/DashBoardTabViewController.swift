//
//  DashBoardTabViewController.swift
//  LMRA
//
//  Created by Mac on 17/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class DashBoardTabViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
  delegate = self
        print("tap")
    

        // Do any additional setup after loading the view.
    }

}
extension DashBoardTabViewController: UITabBarControllerDelegate  {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {

        guard let fromView = selectedViewController?.view, let toView = viewController.view else {
          return false // Make sure you want this as false
        }

        if fromView != toView {
          UIView.transition(from: fromView, to: toView, duration: 0.3, options: [.transitionCrossDissolve], completion: nil)
        }

        return true
    }
}
