//
//  ReviewAndSearchViewController.swift
//  LMRA
//
//  Created by Mac on 19/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire

class ReviewAndSearchViewController: UIViewController,UIScrollViewDelegate {
    
    @IBOutlet weak var Scrollview: UIScrollView!
    @IBOutlet weak var LblTaskTitle: UILabel!
    @IBOutlet weak var LblTaskDescription: UILabel!
    @IBOutlet weak var LblTaskDay: UILabel!
    @IBOutlet weak var LblTaskDate: UILabel!
    @IBOutlet weak var LblTaskTime: UILabel!
    @IBOutlet weak var LblTaskDayTime: UILabel!
    @IBOutlet weak var LblAddNickName: UILabel!
    @IBOutlet weak var LblFullAddress: UILabel!
    @IBOutlet weak var LblTaskLanguages: UILabel!
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var ReviewView: UIView!
    @IBOutlet weak var BtnCancel: UIButton!
    @IBOutlet weak var LblInnerReviewTitle: UILabel!
    @IBOutlet weak var LblMainReviewTitle: UILabel!
    @IBOutlet weak var BtnBack: UIButton!
    @IBOutlet weak var BtnSearch: UIButton!
    @IBOutlet weak var LanguageView: UIView!
    @IBOutlet weak var AddressView: UIView!
    @IBOutlet weak var Appointmentview: UIView!
    @IBOutlet weak var TaskDetailsView: UIView!
    let defaults = UserDefaults.standard
    
    var authentication_key_val = ""
    var GetTaskID = ""
    var GetCustomerID = ""
    var GetTaskTitle = ""
    var GetTaskDescriptipn = ""
    var GetSelectedTaskDay = ""
    var GetSelectedTasTimeslot = ""
    var GetSelectedTasTimeDayTime = ""
    var GetSelectedTaskDate = ""
    //     var GetSelectedTaskLang = ""
    var GetSelectedNickName = ""
    var GetSelectedfullAddress = ""
    var GetLang = ""
    var SelectedGetAddressId = ""
     var SelectedTaskTime = ""
     var SelectedTaskDay = ""
     var SelectedTaskDate = ""
    var convertedTaskDate = ""
    var GetSelectedLangIDArray = [String] ()
     var GetSelectedLangIDArray2 = [String] ()
   
    var lastContentOffset: CGFloat = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.Scrollview.delegate = self
        self.ReviewView.isHidden = true
        
        AddressView.layer.shadowColor = UIColor.gray.cgColor
        AddressView.layer.shadowOpacity = 0.5
        AddressView.layer.shadowOffset = CGSize.zero
        AddressView.layer.shadowRadius = 6
        AddressView.layer.cornerRadius = 5
        AddressView.clipsToBounds = false
        
        
        Appointmentview.layer.shadowColor = UIColor.gray.cgColor
        Appointmentview.layer.shadowOpacity = 0.5
        Appointmentview.layer.shadowOffset = CGSize.zero
        Appointmentview.layer.shadowRadius = 6
        Appointmentview.layer.cornerRadius = 5
        Appointmentview.clipsToBounds = false
        
        LanguageView.layer.shadowColor = UIColor.gray.cgColor
        LanguageView.layer.shadowOpacity = 0.5
        LanguageView.layer.shadowOffset = CGSize.zero
        LanguageView.layer.shadowRadius = 6
        LanguageView.layer.cornerRadius = 5
        LanguageView.clipsToBounds = false
        
        TaskDetailsView.layer.shadowColor = UIColor.gray.cgColor
        TaskDetailsView.layer.shadowOpacity = 0.5
        TaskDetailsView.layer.shadowOffset = CGSize.zero
        TaskDetailsView.layer.shadowRadius = 6
        TaskDetailsView.layer.cornerRadius = 5
        TaskDetailsView.clipsToBounds = false
        

        
        self.authentication_key_val = UserDefaults.standard.string(forKey: "authentication_key")!//get
        self.GetTaskID = UserDefaults.standard.string(forKey: "TaskIDKey") ?? ""//get
       print("GetTaskIDReviewAndSearch: \(self.GetTaskID)")
        self.GetCustomerID = UserDefaults.standard.string(forKey: "CustomerIDKey") ?? ""//get
        self.GetCustomerID = UserDefaults.standard.string(forKey: "CustomerIDKey") ?? ""//get
        
        self.GetTaskTitle = UserDefaults.standard.string(forKey: "EnterTaskTitleKey") ?? ""//get
        self.GetTaskDescriptipn = UserDefaults.standard.string(forKey: "EnterTaskDescKey") ?? ""//get
        
        self.GetSelectedTaskDay = UserDefaults.standard.string(forKey: "SelectedserviceDayKey") ?? ""//get
        self.GetSelectedTasTimeslot = UserDefaults.standard.string(forKey: "SelectedTimeSlotKey") ?? ""//get
        self.GetSelectedTasTimeDayTime = UserDefaults.standard.string(forKey: "SelectedTimeDayKey") ?? ""//get
        self.GetSelectedTaskDate = UserDefaults.standard.string(forKey: "SelectedServiceDateKy") ?? ""//get
        
        self.GetSelectedNickName = UserDefaults.standard.string(forKey: "SelectedAddressNickNameKey") ?? ""//get
        self.GetSelectedfullAddress = UserDefaults.standard.string(forKey: "SelectedFullAddressKey") ?? ""//get
        
        
        self.GetSelectedLangIDArray = (UserDefaults.standard.object(forKey:"SelectedGetLangIDArrayKey" )as? [String])!
        print("GetSelectedLangIDArray :\(String(describing: self.GetSelectedLangIDArray))") //get
         self.SelectedGetAddressId = UserDefaults.standard.string(forKey: "SelectedGetAddressIdKey") ?? ""//get
         self.SelectedTaskTime = UserDefaults.standard.string(forKey: "TaskTimeKey")!//get
         self.SelectedTaskDay = UserDefaults.standard.string(forKey: "TaskDayKey")!//get
        self.GetSelectedTaskDate = UserDefaults.standard.string(forKey: "TaskDateKey")!//get
        self.convertedTaskDate = StaticUtility.convertDateFormater8(self.GetSelectedTaskDate)
        
        self.GetSelectedLangIDArray2 = self.GetSelectedLangIDArray
        
        let GetSelectedTaskLang = UserDefaults.standard.object(forKey:"SelectedTaskLangKey" )as? [String]
        print("GetSelectedTaskLang :\(String(describing: GetSelectedTaskLang))")
        
        self.GetLang = ""
        self.GetLang = GetSelectedTaskLang!.joined(separator: ", ")
        
        self.LblTaskLanguages.text = self.GetLang
        self.LblTaskTitle.text = self.GetTaskTitle
        self.LblTaskDescription.text = self.GetTaskDescriptipn
        self.LblTaskDay.text = self.GetSelectedTaskDay
        self.LblTaskTime.text = self.GetSelectedTasTimeslot
        self.LblTaskDayTime.text = self.GetSelectedTasTimeDayTime
        self.LblTaskDate.text = self.convertedTaskDate
        self.LblAddNickName.text = self.GetSelectedNickName
        self.LblFullAddress.text = self.GetSelectedfullAddress
        
        // Do any additional setup after loading the view.
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = Scrollview.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.lastContentOffset < Scrollview.contentOffset.y {
            // did move up
            self.ReviewView.isHidden = false
            self.LblMainReviewTitle.isHidden = true
        } else if self.lastContentOffset > Scrollview.contentOffset.y {
            // did move down
        } else {
            // didn't move
        }
    }
    
    @IBAction func BackBtnClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func CancelBtnClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func SearchBtnClick(_ sender: Any) {
        
        self.ReviewView.isHidden = false
        self.LblMainReviewTitle.isHidden = true
      
     
        //for now
        let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SearchResultVC") as! SearchResultViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
 
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
}
