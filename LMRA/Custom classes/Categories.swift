//
//  Categories.swift
//  OilPrice
//
//  Created by  on 09/03/18.
//  Copyright © 2018 Ashwini. All rights reserved.
//

import UIKit

extension NSObject {
    func jsonString() -> String {
        let data: NSData? = try! JSONSerialization.data(withJSONObject: self, options: []) as NSData?
        var jsonStr: String = ""
        if data != nil {
            jsonStr = String(data: data! as Data, encoding: String.Encoding.ascii)!
        }
        return jsonStr
    }
}

extension NSMutableArray {
    open override func object(at index: Int) -> Any {
        if self.count > index {
            return super.object(at: index)
        } else {
            return TimeZone(abbreviation: "UTC")!
        }
    }
}

extension DateFormatter {
    
    convenience init (format: String) {
        self.init()
        dateFormat = format
        locale = Locale.current
    }
}

func convertTimestampeToDate(timestamp: Double) -> Date {
    return Date(timeIntervalSince1970: timestamp/1000)
}

extension Sequence where Iterator.Element: Hashable {
    func unique() -> [Iterator.Element] {
        var alreadyAdded = Set<Iterator.Element>()
        return self.filter { alreadyAdded.insert($0).inserted }
    }
}

extension String {
    func convertToDictionary() -> NSDictionary {
        let jsonData = self.data(using: .utf8)!
        let dictionary = try? JSONSerialization.jsonObject(with: jsonData, options: .mutableLeaves)
        return dictionary as! NSDictionary
    }
    
    var isAlphanumeric: Bool {
        return !isEmpty && range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil
    }
    
    func isValidDouble(maxDecimalPlaces: Int) -> Bool {
        // Use NumberFormatter to check if we can turn the string into a number
        // and to get the locale specific decimal separator.
        let formatter = NumberFormatter()
        formatter.allowsFloats = true // Default is true, be explicit anyways
        let decimalSeparator = formatter.decimalSeparator ?? "."  // Gets the locale specific decimal separator. If for some reason there is none we assume "." is used as separator.
        
        // Check if we can create a valid number. (The formatter creates a NSNumber, but
        // every NSNumber is a valid double, so we're good!)
        if formatter.number(from: self) != nil {
            // Split our string at the decimal separator
            let split = self.components(separatedBy: decimalSeparator)
            
            // Depending on whether there was a decimalSeparator we may have one
            // or two parts now. If it is two then the second part is the one after
            // the separator, aka the digits we care about.
            // If there was no separator then the user hasn't entered a decimal
            // number yet and we treat the string as empty, succeeding the check
            let digits = split.count == 2 ? split.last ?? "" : ""
            
            // Finally check if we're <= the allowed digits
            return digits.count <= maxDecimalPlaces
        }
        
        return false // couldn't turn string into a valid number
    }
    
    func fileName() -> String {
        return NSURL(fileURLWithPath: self).deletingPathExtension?.lastPathComponent ?? ""
    }
    
    func fileExtension() -> String {
        return NSURL(fileURLWithPath: self).pathExtension ?? ""
    }
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    
    func stringByAppendingPathComponent(pathComponent: String) -> String {
        return (self as NSString).appendingPathComponent(pathComponent)
    }
    
    func trimmedString() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }
        return String(data: data, encoding: .utf8)
    }
    
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
    
    func base64String() -> String {
        let data = self.data(using: String.Encoding.utf8, allowLossyConversion: false)!
        return data.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
    }
    
    func base64Decoded() -> Data? {
        if let data = Data(base64Encoded: self, options: .ignoreUnknownCharacters) {
            return data
        }
        return nil
    }
    
    func base64Decoded123() -> String? {
        if let data = Data(base64Encoded: self, options: .ignoreUnknownCharacters) {
            return String(data: data, encoding: .utf8)
        }
        return nil
    }
    
    static func randomString(length: Int) -> String {
        let letters: NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
    
    func jsonObject() -> AnyObject {
        let data = self.data(using: String.Encoding.utf8)
        var anyObj: AnyObject? = try? JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions(rawValue: 0)) as AnyObject
        if anyObj == nil {
            anyObj = NSDictionary()
        }
        return anyObj!
    }
    
    func convertStringToDate(actualFormat: String, expectedFormat: String) -> Date? {
        let simpleDateFormat = DateFormatter()
        simpleDateFormat.dateFormat = actualFormat //format our date String
        simpleDateFormat.timeZone = NSTimeZone.local
        if let date = simpleDateFormat.date(from: self) {
            let dateFormat = DateFormatter()
            dateFormat.dateFormat = expectedFormat //format return
            dateFormat.timeZone = NSTimeZone.local
            let datestring = dateFormat.string(from: date)
            if datestring.count > 0 {
                return dateFormat.date(from: datestring)
            } else {
                return Date()
            }
        }
        return Date()
    }
    
    func intValue() -> Int {
        return NSString(string: self).integerValue
    }
    
    func floatValue() -> Float {
        return NSString(string: self).floatValue
    }
    
    func doubleValue() -> Double {
        return NSString(string: self).doubleValue
    }
    
    func isAlphaneumeric() -> Bool {
        let alphabetSet: CharacterSet = CharacterSet(charactersIn: " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz/")
        let numberSet: CharacterSet = CharacterSet(charactersIn: "0123456789")
        
        let string: NSString = NSString(string: self)
        var range: NSRange = string .rangeOfCharacter(from: alphabetSet)
        
        if range.length > 0 {
            range = string.rangeOfCharacter(from: numberSet)
            if range.length > 0 {
                return true
            }
        }
        return false
    }
    
    func isAlphaNeumeric() -> Bool {
        let characterset = NSCharacterSet(charactersIn: " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz/1234567890")
        if self.rangeOfCharacter(from: characterset.inverted) != nil {return false}
        return true
    }
    
    func hasOnlyAlphabets() -> Bool {
        let characterset = NSCharacterSet(charactersIn: " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")
        if self.rangeOfCharacter(from: characterset.inverted) != nil {return false}
        return true
    }
    
    func hasOnlyDigits() -> Bool {
        let characterset = NSCharacterSet(charactersIn: "1234567890")
        if self.rangeOfCharacter(from: characterset.inverted) != nil {return false}
        return true
    }
    
//    func isValidEmail() -> Bool {
//        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
//        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
//        return emailTest.evaluate(with: self)
//    }
}

extension Date {
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }

    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }

    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }

    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }

    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }

    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }

    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }

    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
    
    func convertDateToString(expectedFormat: String) -> String? {
        let formatter = DateFormatter()
        formatter.dateFormat = expectedFormat
        return formatter.string(from: self)
    }
    
    func convertDateToString(actualFormat: String, expectedFormat: String) -> String? {
        let simpleDateFormat = DateFormatter()
        simpleDateFormat.dateFormat = actualFormat //format our date String
        simpleDateFormat.timeZone = NSTimeZone.local
        let str = simpleDateFormat.string(from: self)
        if str.count > 0 {
            let dateFormat = DateFormatter()
            dateFormat.dateFormat = expectedFormat //format return
            dateFormat.timeZone = NSTimeZone.local
            if let date = simpleDateFormat.date(from: str) {
                let dateStr = dateFormat.string(from: date)
                return dateStr
            } else {
               return ""
            }
        } else {
            return ""
        }
    }
    
    
    //Last Month Start
    func getLastMonthStart() -> Date? {
        let components: NSDateComponents = Calendar.current.dateComponents([.year, .month], from: self) as NSDateComponents
        components.month -= 1
        return Calendar.current.date(from: components as DateComponents)!
    }
    
    //Last Month End
    func getLastMonthEnd() -> Date? {
        let components: NSDateComponents = Calendar.current.dateComponents([.year, .month], from: self) as NSDateComponents
        components.day = 1
        components.day -= 1
        return Calendar.current.date(from: components as DateComponents)!
    }
    
}

extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(_ quality: JPEGQuality) -> Data? {
        return self.jpegData(compressionQuality: quality.rawValue)
    }
    
    func resizedWith(size: CGFloat) -> UIImage {
        let scale = size / self.size.width
        let newHeight = self.size.height * scale
        
        UIGraphicsBeginImageContext(CGSize(width: size, height: newHeight))
        self.draw(in: CGRect(x: 0, y: 0, width: size, height: newHeight))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    func compressImage() -> Data? {
        let imgData = self.jpegData(compressionQuality: 1.0)
        let imageSize = Double(imgData!.count) / 1024.0
        if imageSize > 200 {
            let actualHeight: CGFloat = self.size.height
            let actualWidth: CGFloat = self.size.width
            let imgRatio: CGFloat = actualWidth/actualHeight
            let maxWidth: CGFloat = 1024.0
            let resizedHeight: CGFloat = maxWidth/imgRatio
            let compressionQuality: CGFloat = 0.5
            
            let rect: CGRect = CGRect(x: 0, y: 0, width: maxWidth, height: resizedHeight)
            UIGraphicsBeginImageContext(rect.size)
            self.draw(in: rect)
            let img: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
            //let imageData: Data = UIImageJPEGRepresentation(img, compressionQuality)!
            let imageData = img.jpegData(compressionQuality: compressionQuality)
            UIGraphicsEndImageContext()
            return imageData
        } else {
            let imageData: Data = self.pngData()!
            return imageData
        }
    }
}

extension UIView {
    
    /*@IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            self.layoutIfNeeded()
            self.layer.cornerRadius = newValue
            self.layer.masksToBounds = (newValue > 0)
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return self.layer.borderWidth
        }
        set {
            self.layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor {
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        }
        set {
            self.layer.borderColor = newValue.cgColor
        }
    }*/
    
    static func loadInstanceFromNib() -> UIView {
        return Bundle.main.loadNibNamed(String(describing: self), owner: nil, options: nil)![0] as? UIView ?? UIView()
    }
    
    func getSpanshot() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, 0.0)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    func addBlurEffect() {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        self.insertSubview(blurEffectView, at: 0)
        blurEffectView.addFullResizeConstraints(parent: self)
        self.backgroundColor = UIColor.clear
    }
    
    func constraintWithIdentifier(_ identifier: String) -> NSLayoutConstraint? {
        return self.constraints.filter { $0.identifier == identifier }.first
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
        self.layer.masksToBounds = true
    }

    func addFullResizeConstraints(parent: UIView) {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        parent.addConstraint(NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: parent, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1.0, constant: 0.0))
        parent.addConstraint(NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: parent, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1.0, constant: 0.0))
        parent.addConstraint(NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: parent, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1.0, constant: 0.0))
        parent.addConstraint(NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: parent, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1.0, constant: 0.0))
    }
    
    func applyLeftPresent(delay: TimeInterval, duration: TimeInterval) {
        self.alpha = 0
        self.transform = CGAffineTransform(translationX: -15.0, y: 0)
        
        UIView.animateKeyframes(withDuration: (duration/4.0) * 3.0, delay: delay, options: UIView.KeyframeAnimationOptions(rawValue: 0), animations: {
            self.alpha = 1.0
        }) { _ in
            
        }
        
        UIView.animateKeyframes(withDuration: duration, delay: delay, options: UIView.KeyframeAnimationOptions(rawValue: 0), animations: {
            self.transform = CGAffineTransform.identity
        }) { _ in
            
        }
    }
    
    func applyPopPresent(delay: TimeInterval, duration: TimeInterval) {
        self.alpha = 0
        self.layer.transform = CATransform3DMakeScale(1.2, 1.2, 1.2)
        
        UIView.animateKeyframes(withDuration: duration, delay: delay, options: UIView.KeyframeAnimationOptions(rawValue: 0), animations: {
            self.alpha = 1.0
            self.layer.transform = CATransform3DIdentity
        }) { _ in
            
        }
    }
    
    func applyTopPresent(delay: TimeInterval, duration: TimeInterval) {
        self.alpha = 0
        self.transform = CGAffineTransform(translationX: 0, y: -15.0)
        
        UIView.animateKeyframes(withDuration: (duration/4.0) * 3.0, delay: delay, options: UIView.KeyframeAnimationOptions(rawValue: 0), animations: {
            self.alpha = 1.0
        }) { _ in
            
        }
        
        UIView.animateKeyframes(withDuration: duration, delay: delay, options: UIView.KeyframeAnimationOptions(rawValue: 0), animations: {
            self.transform = CGAffineTransform.identity
        }) { _ in
            
        }
    }
    
    /*func addSizingConstraint(with attribute: NSLayoutAttribute, constraint: CGFloat) {
     addConstraint(NSLayoutConstraint(item: self, attribute: attribute, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: constraint))
     }
     
     func addEqualConstraint(to anotherView: UIView, attribute: NSLayoutAttribute, multiplier: CGFloat) {
     anotherView.superview?.addConstraint(NSLayoutConstraint(item: self, attribute: attribute, relatedBy: .equal, toItem: anotherView, attribute: attribute, multiplier: multiplier, constant: 0))
     }
     
     func addCenterConstraint(to anotherView: UIView, attribute: NSLayoutAttribute, multiplier: CGFloat, constraint: CGFloat) {
     anotherView.superview?.addConstraint(NSLayoutConstraint(item: self, attribute: attribute, relatedBy: .equal, toItem: anotherView, attribute: attribute, multiplier: multiplier, constant: constraint))
     }
     
     func addAlignmentConstraint(to anotherView: UIView, attribute: NSLayoutAttribute, multiplier: CGFloat, constraint: CGFloat) {
     anotherView.addConstraint(NSLayoutConstraint(item: self, attribute: attribute, relatedBy: .equal, toItem: anotherView, attribute: attribute, multiplier: multiplier, constant: constraint))
     }
     
     func addSpacingConstraint(to anotherView: UIView, firstAttribute: NSLayoutAttribute, secondAttribute: NSLayoutAttribute, multiplier: CGFloat, constraint: CGFloat) {
     anotherView.superview?.addConstraint(NSLayoutConstraint(item: self, attribute: firstAttribute, relatedBy: .equal, toItem: anotherView, attribute: secondAttribute, multiplier: multiplier, constant: constraint))
     }
     
     func addRatioConstraint(withFirstAttribute firstAttribute: NSLayoutAttribute, secondAttribute: NSLayoutAttribute, multiplier: CGFloat) {
     addConstraint(NSLayoutConstraint(item: self, attribute: firstAttribute, relatedBy: .equal, toItem: self, attribute: secondAttribute, multiplier: multiplier, constant: 0))
     }*/
    
    func addSelfConstraint(firstAttribute: NSLayoutConstraint.Attribute, secondAttribute: NSLayoutConstraint.Attribute, multiplier: CGFloat, constraint: CGFloat) {
        addConstraint(NSLayoutConstraint(item: self, attribute: firstAttribute, relatedBy: .equal, toItem: nil, attribute: secondAttribute, multiplier: multiplier, constant: constraint))
    }
    
    func addChildConstraint(anotherView: UIView, firstAttribute: NSLayoutConstraint.Attribute, relation: NSLayoutConstraint.Relation, secondAttribute: NSLayoutConstraint.Attribute, multiplier: CGFloat, constraint: CGFloat) {
        anotherView.superview?.addConstraint(NSLayoutConstraint(item: self, attribute: firstAttribute, relatedBy: relation, toItem: anotherView, attribute: secondAttribute, multiplier: multiplier, constant: constraint))
    }
    
    func addParentConstraint(anotherView: UIView, firstAttribute: NSLayoutConstraint.Attribute, relation: NSLayoutConstraint.Relation, secondAttribute: NSLayoutConstraint.Attribute, multiplier: CGFloat, constraint: CGFloat) {
        self.addConstraint(NSLayoutConstraint(item: anotherView, attribute: firstAttribute, relatedBy: relation, toItem: self, attribute: secondAttribute, multiplier: multiplier, constant: constraint))
    }
    
    static func mk_loadInstanceFromNib() -> UIView {
        return Bundle.main.loadNibNamed(String(describing: self), owner: nil, options: nil)![0] as? UIView ?? UIView()
    }
}

extension UIColor {
    convenience init(hexCode: UInt32) {
        let red = CGFloat((hexCode & 0xFF0000) >> 16)/256.0
        let green = CGFloat((hexCode & 0xFF00) >> 8)/256.0
        let blue = CGFloat(hexCode & 0xFF)/256.0
        self.init(red: red, green: green, blue: blue, alpha: 1.0)
    }
    
    convenience init(hexStringColor: String) {
        var cString: String = hexStringColor.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if cString.hasPrefix("#") {
            cString.remove(at: cString.startIndex)
        }
        
        if (cString.count) != 6 {
            self.init(red: 120.0/255.0, green: 120.0/255.0, blue: 120.0/255.0, alpha: 1.0)
        } else {
            var rgbValue: UInt32 = 0
            Scanner(string: cString).scanHexInt32(&rgbValue)
            
            self.init(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                      green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                      blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                      alpha: CGFloat(1.0))
        }
    }
    
    convenience init(gray: CGFloat) {
        self.init(red: gray/255.0, green: gray/255.0, blue: gray/255.0, alpha: 1.0)
    }
    
    convenience init(rgb: CGFloat, alpha: CGFloat) {
        self.init(red: rgb/255.0, green: rgb/255.0, blue: rgb/255.0, alpha: alpha)
    }
    
    var components: (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) {
        let color = CIColor(color: self)
        return (color.red, color.green, color.blue, color.alpha)
    }
}

extension UIViewController {
    var isModal: Bool {
        if let index = navigationController?.viewControllers.firstIndex(of: self), index > 0 {
            return false
        } else if presentingViewController != nil {
            return true
        } else if navigationController?.presentingViewController?.presentedViewController == navigationController {
            return true
        } else if tabBarController?.presentingViewController is UITabBarController {
            return true
        } else {
            return false
        }
    }
}

extension UserDefaults {
    func object<T: Codable>(_ type: T.Type, with key: String, usingDecoder decoder: JSONDecoder = JSONDecoder()) -> T? {
        guard let data = self.value(forKey: key) as? Data else { return nil }
        return try? decoder.decode(type.self, from: data)
    }
    
    func set<T: Codable>(object: T, forKey key: String, usingEncoder encoder: JSONEncoder = JSONEncoder()) {
        let data = try? encoder.encode(object)
        self.set(data, forKey: key)
    }
}

extension UIWindow {
    func setWindowRootViewController(rootController: UIViewController) {
        let previousRootViewController: UIViewController? = self.rootViewController
        
        self.rootViewController = rootController
        
        for subview in self.subviews {
            if String(describing: type(of: subview)).lowercased() == "uitransitionview" {
                subview.removeFromSuperview()
            }
        }
        
        if previousRootViewController != nil {
            previousRootViewController?.dismiss(animated: false, completion: {
                if let view = previousRootViewController?.view {
                    view.removeFromSuperview()
                }
            })
        }
    }
    
    
    class func getLastControllerWithCompletion(controller: UINavigationController?) -> UIViewController? {
        if let last = controller?.viewControllers.last as? UINavigationController {
            return self.getLastControllerWithCompletion(controller: last)
        }
        return controller?.viewControllers.last
    }
    
    class func getTopPresentedControllerWithCompletion(controller: UIViewController?) -> UIViewController? {
        if let presented = controller?.presentedViewController {
            return self.getTopPresentedControllerWithCompletion(controller: presented)
        }
        return controller
    }
}


extension CALayer {
    
    func addBorder(edge: UIRectEdge, color: CGColor, thickness: CGFloat, width: CGFloat) {
        
        let border = CALayer()
        
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect(x: 0, y: 0, width: width, height: thickness)
        case UIRectEdge.bottom:
            border.frame = CGRect(x: 0, y: self.bounds.height - thickness, width: width, height: thickness)
        case UIRectEdge.left:
            border.frame = CGRect(x: 0, y: 0, width: thickness, height: self.bounds.height)
        case UIRectEdge.right:
            border.frame = CGRect(x: width - thickness, y: 0, width: thickness, height: self.bounds.height)
        default:
            break
        }
        border.backgroundColor = color
        self.addSublayer(border)
    }
}

extension FileManager {
    func clearTmpDirectory() {
        do {
            let tmpDirectory = try contentsOfDirectory(atPath: NSTemporaryDirectory())
            try tmpDirectory.forEach {[unowned self] file in
                let path = String.init(format: "%@%@", NSTemporaryDirectory(), file)
                try self.removeItem(atPath: path)
            }
        } catch {
            UtilityLog.sharedInstant.printLog(value:error)
        }
    }
    
    func deleteTempFile(filePath: URL) {
        do {
            let file: String = filePath.path
            let path = String.init(format: "%@", file)
            try self.removeItem(atPath: path)
        } catch {
            UtilityLog.sharedInstant.printLog(value:"Could not clear temp folder: \(error)")
        }
    }
}

extension NSMutableAttributedString {
    func normalFont_White(_ text: String) -> NSMutableAttributedString {
        let attriString = [NSAttributedString.Key.font: UIFont(name: "HyundaiSansHeadOffice-Regular", size: UIScreen.main.bounds.size.height * (14.0 / 568.0))!, NSAttributedString.Key.foregroundColor: UIColor.white]
        let normal = NSMutableAttributedString(string: "\(text)", attributes: attriString)
        self.append(normal)
        return self
    }
    
    func small(_ text: String) -> NSAttributedString {
        let attrs = [NSAttributedString.Key.font: UIFont(name: "HyundaiSansHeadOffice-Regular", size: UIScreen.main.bounds.size.height * (11.0 / 568.0))!, NSAttributedString.Key.foregroundColor: UIColor.white]
        let smallStr = NSAttributedString(string: "\(text)", attributes: attrs)
        self.append(smallStr)
        return self
    }
}

extension NSDictionary {
    func convertToString() -> String {
        let jsonData = try? JSONSerialization.data(withJSONObject: self, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        return jsonString!
    }
}
