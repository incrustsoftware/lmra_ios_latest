//
//  CustomConstraint.swift
//  ORLink
//
//  Created by  on 20/06/17.
//  Copyright © 2017  Technologies. All rights reserved.
//

import UIKit

class CustomConstraint: NSLayoutConstraint {
    
    @IBInspectable var dynamic: Bool = false
    
    override func setValue(_ value: Any?, forKey key: String) {
        super.setValue(value, forKey: key)
        
        if self.dynamic == true {
            self.constant = getCalculated(self.constant)
        }
    }
}
