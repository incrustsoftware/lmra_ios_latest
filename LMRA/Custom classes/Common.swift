//
//  Utility.swift
//  Copyright © 2016  Technologies. All rights reserved.
//

import UIKit
import Foundation
import CoreData
import Photos
import MobileCoreServices

#if RELEASE
let IS_DEVELOPMENT = false
#else
let IS_DEVELOPMENT = true
#endif

#if targetEnvironment(simulator)
let DEVICE_IS_SIMULATOR = true
#else
let DEVICE_IS_SIMULATOR = false
#endif

typealias VoidCompletion = () -> Void
typealias BooleanCompletion = (_ check: Bool) -> Void

var device_TokenData: Data = Data()

var screenHeight = UIScreen.main.bounds.height

let screenWidth = UIScreen.main.bounds.width

func deviceOrientaion() {
    if UIDevice.current.orientation.isLandscape {
        screenHeight = UIScreen.main.bounds.width
    }
}

func isDevice() -> String {
    deviceOrientaion()
    
    if screenHeight == 896 {
        return DEVICES.iPhoneXR
    } else if screenHeight == 812 {
        return DEVICES.iPhoneX
    } else if screenHeight == 736 {
        return DEVICES.iPhonePlus
    } else if screenHeight == 667 {
        return DEVICES.iPhone6
    } else if screenHeight == 568 {
        return DEVICES.iPhoneSE
    } else if screenHeight == 1366 {
        return DEVICES.iPadPro_12_9
    } else if screenHeight == 834 {
        return DEVICES.iPadPro_10_5
    } else {
        return DEVICES.iPad
    }
}

func cprintx(_ text: Any) {
    if IS_DEVELOPMENT {
        UtilityLog.sharedInstant.printLog(value:text)
    }
}

func safeAreaHeight() -> CGFloat {
    return (isDevice() == DEVICES.iPhoneX) ?667:751
}

//func getCalculated(_ width: CGFloat) -> CGFloat {
     //   return (width > 0) ?UIScreen.main.bounds.size.height * (width/568.0):0
//}

func getCalculated(_ value: CGFloat) -> CGFloat {
    if isDevice() == DEVICES.iPhoneX || isDevice() == DEVICES.iPhoneXR {
        return safeAreaHeight() * (value / 568.0)
    } else {
        return screenHeight * (value / 568.0)
    }
    
}
func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
    let size = image.size
    let widthRatio  = targetSize.width  / size.width
    let heightRatio = targetSize.height / size.height

    // Figure out what our orientation is, and use that to form the rectangle
    var newSize: CGSize
    if(widthRatio > heightRatio) {
        newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
    } else {
        newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
    }

    // This is the rect that we've calculated out and this is what is actually used below
    let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)

    // Actually do the resizing to the rect using the ImageContext stuff
    UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
    image.draw(in: rect)
    let newImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()

    return newImage
}

