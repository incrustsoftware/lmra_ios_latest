//
//  CustomView.swift
//  MMPC
//
//  Created by Vinod on 08/09/19.
//  Copyright © 2019 Vinod. All rights reserved.
//

import Foundation
import UIKit

open class CustomView: UIView {
    
    //@IBInspectable var setBorder: Bool = false
    //@IBInspectable var borderWidth: CGFloat = 0
    //@IBInspectable var borderColor: UIColor?
    //@IBInspectable var cornerRadius: CGFloat = 0
    
    let relativeFontConstant:CGFloat = 0.046
    @IBInspectable var isOvalShape: Bool = false
    
    @IBInspectable var borderColor:UIColor? {
        set {
            layer.borderColor = newValue!.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            else {
                return nil
            }
        }
    }
    @IBInspectable var borderWidth:CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    /* BORDER RADIUS */
    @IBInspectable var cornerRadius:CGFloat {
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        get {
            return layer.cornerRadius
        }
    }
    
    /* SHADOW */
    @IBInspectable var isShodowOn: Bool = false
    @IBInspectable var shadowColor:UIColor? {
        set {
            layer.shadowColor = newValue!.cgColor
        }
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            else {
                return nil
            }
        }
    }
    @IBInspectable var shadowOpacity:Float {
        set {
            layer.shadowOpacity = newValue
        }
        get {
            return layer.shadowOpacity
        }
    }
    @IBInspectable var shadowOffset:CGSize {
        set {
            layer.shadowOffset = newValue
        }
        get {
            return layer.shadowOffset
        }
    }
    @IBInspectable var shadowRadius:CGFloat {
        set {
            layer.shadowRadius = newValue
        }
        get {
            return layer.shadowRadius
        }
    }
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        
//        if setBorder == true {
//            self.layer.cornerRadius = cornerRadius
//            self.layer.borderWidth = borderWidth
//            self.layer.borderColor = UIColor.black.cgColor
//        }
        
        if isOvalShape {
            let buttonHeight = getCalculated(self.frame.height)
            self.layer.cornerRadius = buttonHeight / 2
        }
        else {
            self.layer.cornerRadius = cornerRadius
        }
        
        if isShodowOn {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { // Change `2.0` to the desired number of seconds.
               self.layer.masksToBounds = false
                self.layer.shadowColor = self.shadowColor?.cgColor
                self.layer.shadowOpacity = 0.3
                self.layer.shadowOffset = self.shadowOffset
                self.layer.shadowRadius = 3

               self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
               self.layer.shouldRasterize = true
               self.layer.rasterizationScale = UIScreen.main.scale
            }
            
        }
        
        
        
    }
    
}
