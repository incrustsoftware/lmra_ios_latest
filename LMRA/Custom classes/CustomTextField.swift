//
//  CustomTextField.swift
//  Viva
//
//  Created by  on 07/07/16.
//  Copyright © 2016 . All rights reserved.
//

import UIKit

open class CustomTextField: UITextField {
    
    private var maxLengths = [UITextField: Int]()
    
    var indexPath: IndexPath = IndexPath()
    
    @IBInspectable var autoFont: Bool = false
    @IBInspectable var setBorder: Bool = false
    @IBInspectable var fontSize: CGFloat = 0
    
    @IBInspectable var borderWidth: CGFloat = 0
    @IBInspectable var cornerRadius: CGFloat = 0
    
    @IBInspectable var padding: CGFloat = 0
    
    @IBInspectable var isAttributed: Bool = false
    @IBInspectable var attributedFonts: String = ""
    @IBInspectable var attributedSizes: String = ""
    
    @IBInspectable var optionalRadius: CGFloat = 0
    @IBInspectable var optionalCorner: CGFloat = 0
    
    @IBInspectable var placeColor: UIColor = UIColor.clear
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        
        if setBorder {
            self.layer.borderWidth = borderWidth
            self.layer.cornerRadius = cornerRadius
        }
        else {
            self.layer.cornerRadius = cornerRadius
        }
        
        
        if autoFont == true {
            if isAttributed == true {
                self.attributedText = CustomBase.getModifiedAttributedString(self.attributedText!, withFonts: self.attributedFonts, withSizes: self.attributedSizes, defaultFontSize: self.fontSize)
            } else {
                if UIDevice().userInterfaceIdiom == .phone {
                    switch UIScreen.main.nativeBounds.height {
                    case 1136:
                        //UtilityLog.sharedInstant.printLog(value:"iPhone 5 or 5S or 5C")
                        self.font = UIFont(name: self.font!.fontName, size: fontSize - 5)
                        
                    case 1334:
                        //UtilityLog.sharedInstant.printLog(value:"iPhone 6/6S/7/8")
                        self.font = UIFont(name: self.font!.fontName, size: fontSize - 2)
                        
                    case 1920, 2208, 2436:
                        //UtilityLog.sharedInstant.printLog(value:"iPhone 6+/6S+/7+/8+/X/XS")
                        self.font = UIFont(name: self.font!.fontName, size: fontSize)
                        
                    case 1792, 2688:
                        //UtilityLog.sharedInstant.printLog(value:"iPhone XR, XS Max, 11")
                        self.font = UIFont(name: self.font!.fontName, size: fontSize + 3)
                        
                    default:
                        UtilityLog.sharedInstant.printLog(value:"Unknown")
                    }
                }
                
            }
        }
        
        
        if self.optionalCorner > 0 {
            let corners: UIRectCorner = (optionalCorner == 1) ?UIRectCorner.topLeft:(optionalCorner == 2) ?UIRectCorner.bottomLeft:(optionalCorner == 3) ?UIRectCorner.bottomRight:(optionalCorner == 4) ?UIRectCorner.topRight:(optionalCorner == 12) ?[UIRectCorner.topLeft, UIRectCorner.bottomLeft]:(optionalCorner == 23) ?[UIRectCorner.bottomLeft, UIRectCorner.bottomRight]:(optionalCorner == 34) ?[UIRectCorner.bottomRight, UIRectCorner.topRight]:(optionalCorner == 41) ?[UIRectCorner.topLeft, UIRectCorner.topRight]:(optionalCorner == 123) ?[UIRectCorner.topLeft, UIRectCorner.bottomLeft, UIRectCorner.bottomRight]:(optionalCorner == 234) ?[UIRectCorner.bottomLeft, UIRectCorner.bottomRight, UIRectCorner.topRight]:(optionalCorner == 341) ?[UIRectCorner.bottomRight, UIRectCorner.topRight, UIRectCorner.topLeft]:(optionalCorner == 412) ?[UIRectCorner.topRight, UIRectCorner.topLeft, UIRectCorner.bottomLeft]:UIRectCorner.allCorners
            
            let radius =  UIScreen.main.bounds.size.height * (self.optionalRadius / baseHeight)
            var rect = self.bounds
            rect.size.width = UIScreen.main.bounds.size.width * (rect.size.width / 320.0)
            rect.size.height =  UIScreen.main.bounds.size.height * (rect.size.height / baseHeight)
            let maskPath: UIBezierPath = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let maskLayer: CAShapeLayer = CAShapeLayer()
            maskLayer.frame = rect
            maskLayer.path = maskPath.cgPath
            self.layer.mask = maskLayer
        }
        
       // self.setValue(placeColor, forKeyPath: "_placeholderLabel.textColor")
        self.reloadPlaceholder()
        
        let oldText = self.text
        self.text = "1"
        self.text = oldText
    }
    
    func reloadPlaceholder() {
        if self.placeholder?.last == "*"{
            let attribute = NSMutableAttributedString.init(string: self.placeholder!)
            attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: NSMakeRange((self.placeholder?.count)! - 1, 1))
            self.attributedPlaceholder = attribute
        }
    }
    
    func addDoneButton(target: UIViewController, selector: Selector) {
        let toolbar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 44))
        toolbar.isTranslucent = false
        toolbar.tintColor = UIColor.white
        toolbar.barTintColor = UIColor(red: 27.0/255.0, green: 36.0/255.0, blue: 66.0/255.0, alpha: 1.0)
        
        let doneButton = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: target, action: selector)
        let flexibleButton = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        toolbar.items = [flexibleButton, doneButton]
        self.inputAccessoryView = toolbar
    }
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        if self.textAlignment == .center {
            return super.textRect(forBounds: bounds)
        } else {
            let finalPadding: CGFloat = ((isDevice() == DEVICES.iPhoneX || isDevice() == DEVICES.iPhoneXR) ?safeAreaHeight(): UIScreen.main.bounds.size.height) * (self.padding / baseHeight)
            //return bounds.insetBy(dx: CGFloat(finalPadding), dy: CGFloat(0))
            let padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: finalPadding)
            //return UIEdgeInsetsInsetRect(bounds, padding)
            let newCGRect = bounds.inset(by: padding)
            return newCGRect
        }
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        if self.textAlignment == .center {
            return super.textRect(forBounds: bounds)
        } else {
            let finalPadding: CGFloat = ((isDevice() == DEVICES.iPhoneX || isDevice() == DEVICES.iPhoneXR) ?safeAreaHeight(): UIScreen.main.bounds.size.height) * (self.padding / baseHeight)
            //return bounds.insetBy(dx: CGFloat(finalPadding), dy: CGFloat(0))
            let padding = UIEdgeInsets(top: 0, left: finalPadding, bottom: 0, right: 0)
            // return UIEdgeInsetsInsetRect(bounds, padding)
            let newCGRect = bounds.inset(by: padding)
            return newCGRect
        }
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        if self.textAlignment == .center {
            return super.textRect(forBounds: bounds)
        } else {
            let finalPadding: CGFloat = ((isDevice() == DEVICES.iPhoneX) ?safeAreaHeight(): UIScreen.main.bounds.size.height) * (self.padding / baseHeight)
            //return bounds.insetBy(dx: CGFloat(finalPadding), dy: CGFloat(0))
            let padding = UIEdgeInsets(top: 0, left: finalPadding, bottom: 0, right: 0)
            // return UIEdgeInsetsInsetRect(bounds, padding)
            let newCGRect = bounds.inset(by: padding)
            return newCGRect
        }
    }
    
    override open func addGestureRecognizer(_ gestureRecognizer: UIGestureRecognizer) {
        //Prevent long press to show the magnifying glass
        if gestureRecognizer is UILongPressGestureRecognizer {
            gestureRecognizer.isEnabled = false
        }
        
        super.addGestureRecognizer(gestureRecognizer)
    }
    
    func setBorderToTextField(width: CGFloat, borderColor: CGColor) {
        let border = CALayer()
        border.borderWidth = width
        border.borderColor = borderColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - border.borderWidth, width: self.frame.size.width, height: self.frame.size.height)
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    
    /*
     // Only override drawRect: if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func drawRect(rect: CGRect] {
     // Drawing code
     }
     */
    
    @IBInspectable var maxLength: Int {
        get {
            guard let length = maxLengths[self] else {
                return Int.max
            }
            
            return length
        }
        set {
            maxLengths[self] = newValue
            addTarget(self, action: #selector(limitLength), for: .editingChanged)
        }
    }
    
    @objc func limitLength(textField: UITextField) {
        guard let prospectiveText = textField.text, prospectiveText.count > maxLength else {
            return
        }
        
        let selection = selectedTextRange
        let maxCharIndex = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
        
        #if swift(>=4.0)
        text = String(prospectiveText[..<maxCharIndex])
        #else
        text = prospectiveText.substring(to: maxCharIndex)
        #endif
        
        selectedTextRange = selection
    }
    
    open override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(UIResponderStandardEditActions.paste(_:)) || action == #selector(UIResponderStandardEditActions.cut(_:)) || action == #selector(UIResponderStandardEditActions.copy(_:)) || action == #selector(UIResponderStandardEditActions.select(_:)) || action == #selector(UIResponderStandardEditActions.selectAll(_:)) {
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }
}

extension UITextField {
    open func canPerformAction(action: Selector, withSender sender: AnyObject?) -> Bool {
        if action == #selector(UIResponderStandardEditActions.paste(_:)) || action == #selector(UIResponderStandardEditActions.cut(_:)) || action == #selector(UIResponderStandardEditActions.copy(_:)) || action == #selector(UIResponderStandardEditActions.select(_:)) || action == #selector(UIResponderStandardEditActions.selectAll(_:)) {
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }
}



