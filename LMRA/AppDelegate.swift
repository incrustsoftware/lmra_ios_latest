//
//  AppDelegate.swift
//  LMRA
//
//  Created by Mac on 29/07/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import IQKeyboardManager
import SVProgressHUD

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate, UIGestureRecognizerDelegate {
    var window: UIWindow?
    var drawerContainer: MMDrawerController?
    var navigationController: UINavigationController?
    let hasLoggedIn = UserDefaults.standard.bool(forKey: "hasloggedin")
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        //called navigation
        // buildNavigationDrawer()
        
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().toolbarTintColor = UIColor.init(named: "colorTaskerGreen")
        // Override point for customization after application launch.
        //
        
        //disabled user interaction for progress bar
        SVProgressHUD.setDefaultMaskType(.black)
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        var vc = UIViewController()
        
        /////////////////////login check//////////////////////////
        //Tasker_SelectCategoryViewController
        //TaskerDasboardVC
        
        //FourthTabViewController
        
        if UserDataManager.shared.isUserLoggedIn() {
            if let userType:UserDataManager.UserType = Optional.some(UserDataManager.UserType(rawValue: UserDataManager.shared.getUserType())!)  {
                if userType == .CUSTOMER {
                    let rootViewController = Storyboard().dashFlowStoryboard.instantiateViewController(withIdentifier: "CustomerDashBoardViewController")
                    
                    let navigationController = UINavigationController(rootViewController: rootViewController)
                    let rightViewController = Storyboard().dashFlowStoryboard.instantiateViewController(withIdentifier: "CutomerMenuVC")
                    let sideMenuController = LGSideMenuController(rootViewController: navigationController,
                                                                  leftViewController: nil,
                                                                  rightViewController: rightViewController)
                    sideMenuController.leftViewWidth = 0
                    sideMenuController.leftViewPresentationStyle = .slideBelow;
                    
                    sideMenuController.rightViewWidth = (self.window?.bounds.size.width ?? 320) - 20
                    sideMenuController.leftViewPresentationStyle = .slideBelow
                    navigationController.isNavigationBarHidden = true
                    navigationController.interactivePopGestureRecognizer?.delegate = self
                    //self.interactivePopGestureRecognizer.enabled = NO;
                    self.window?.rootViewController = sideMenuController
                    self.window?.makeKeyAndVisible()
                    
                }
                else if userType == .TASKER {
                    let rootViewController = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "TaskerDasboardVC")
                    
                    let navigationController = UINavigationController(rootViewController: rootViewController)
                    let rightViewController = Storyboard().taskerStoryboard.instantiateViewController(withIdentifier: "SideMenuViewController")
                    let sideMenuController = LGSideMenuController(rootViewController: navigationController,
                                                                  leftViewController: nil,
                                                                  rightViewController: rightViewController)
                    sideMenuController.leftViewWidth = 0
                    sideMenuController.leftViewPresentationStyle = .slideBelow;
                    
                    sideMenuController.rightViewWidth = (self.window?.bounds.size.width ?? 320) - 20
                    sideMenuController.leftViewPresentationStyle = .slideBelow
                    navigationController.isNavigationBarHidden = true
                    navigationController.interactivePopGestureRecognizer?.delegate = self
                    //navigationController.navigationBar.isTranslucent = true
                    //self.interactivePopGestureRecognizer.enabled = NO;
                    self.window?.rootViewController = sideMenuController
                    self.window?.makeKeyAndVisible()
                }
                else {
                    let launchStoryboard = UIStoryboard(name: "Onborading", bundle: nil)
                    vc = launchStoryboard.instantiateViewController(withIdentifier: "FirstViewController")
                    let navigationVC = UINavigationController(rootViewController: vc)
                    navigationVC.navigationBar.isHidden = true
                    self.window? .rootViewController = navigationVC
                    self.window?.makeKeyAndVisible()
                }
            }
        }
        else {
            let launchStoryboard = UIStoryboard(name: "Onborading", bundle: nil)
            vc = launchStoryboard.instantiateViewController(withIdentifier: "FirstViewController")
            let navigationVC = UINavigationController(rootViewController: vc)
            navigationVC.navigationBar.isHidden = true
            self.window? .rootViewController = navigationVC
            self.window?.makeKeyAndVisible()
        }
        
        
        
        registerForPushNotifications()
        
        func getNotificationSettings() {
            UNUserNotificationCenter.current().getNotificationSettings { settings in
                print("Notification settings: \(settings)")
            }
        }
        return true
    }
    
    //        if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
    //
    //
    //            //Helper.sharedInstance.updateFCMCall()
    //
    //            let storyboard = UIStoryboard(name: "Main", bundle: nil)
    //
    //                let leftViewController = storyboard.instantiateViewController(withIdentifier: "MenuTableViewController")
    //
    //                let rightViewController = UITableViewController()
    //
    //                let navigationController = storyboard.instantiateViewController(withIdentifier: "NavDashboard")
    //
    //                let sideMenuController = LGSideMenuController(rootViewController: navigationController,
    //                                                              leftViewController: leftViewController,
    //                                                              rightViewController: rightViewController)
    //
    //                sideMenuController.leftViewWidth = 280.0;
    //                sideMenuController.leftViewPresentationStyle = .scaleFromLittle;
    //
    //                sideMenuController.rightViewWidth = 0.0;
    //                sideMenuController.leftViewPresentationStyle = .slideBelow;
    //
    //            window.rootViewController = sideMenuController
    //            window.makeKeyAndVisible()
    //
    //        }
    
    //For notifications
    func registerForPushNotifications() {
        UNUserNotificationCenter.current()
            .requestAuthorization(options: [.alert, .sound, .badge]) {
                [weak self] granted, error in
                
                print("Permission granted: \(granted)")
                guard granted else { return }
                self?.getNotificationSettings()
        }
    }
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    func application(
        _ application: UIApplication,
        didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data
    ) {
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined()
        print("Device Token: \(token)")
        
        UserDefaults.standard.setValue(token, forKey: "DeviceToken")
    }
    
    func application(
        _ application: UIApplication,
        didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    
    func application(_ application: UIApplication,
                     didReceiveRemoteNotification userInfo: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
    {
        print(userInfo)
    }
    
    
    //    func userNotificationCenter(center: UNUserNotificationCenter, willPresentNotification notification: UNNotification, withCompletionHandler completionHandler: (UNNotificationPresentationOptions) -> Void)
    //    {
    //        //Handle the notification
    //        completionHandler(
    //            [UNNotificationPresentationOptions.alert,
    //             UNNotificationPresentationOptions.sound,
    //             UNNotificationPresentationOptions.badge])
    //    }
    //    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        completionHandler([.alert, .badge, .sound])
    }
    
    //    func buildNavigationDrawer()
    //    {
    //        let mainStoryBoard:UIStoryboard = UIStoryboard(name:"DashFlow", bundle:nil)
    //
    //        let mainPage:DashBoardTabViewController = mainStoryBoard.instantiateViewController(withIdentifier: "DashBoardTabVC") as! DashBoardTabViewController
    //
    //        let rightSideMenu:SlideMenuViewController = mainStoryBoard.instantiateViewController(withIdentifier: "SlideMenuVC") as! SlideMenuViewController
    //
    //          //wrape into navigation controllers
    //        let rightSideMenuNav = UINavigationController(rootViewController:rightSideMenu)
    //
    //
    //        //create MMDrawer
    //        drawerContainer = MMDrawerController(center: mainPage, leftDrawerViewController:rightSideMenuNav, rightDrawerViewController: rightSideMenuNav)
    //
    //        drawerContainer!.openDrawerGestureModeMask = MMOpenDrawerGestureMode.panningCenterView
    //        drawerContainer!.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.panningCenterView
    //
    //        window?.rootViewController = drawerContainer
    //    }
}

