//
//  TaskLocationViewController.swift
//  Alamofire
//
//  Created by Mac on 19/08/20.
//

import UIKit

class TaskLocationViewController: UIViewController {

    @IBOutlet weak var LblAddAddress: UILabel!
    @IBOutlet weak var BtnAddAddress: UIButton!
    @IBOutlet weak var AddressView: UIView!
    @IBOutlet weak var LblFullAddress: UILabel!
    
    @IBOutlet weak var BtnContinue: UIButton!
    @IBOutlet weak var LblMainTaskLocation: UILabel!
    @IBOutlet weak var LblInnerTaskLocation: UILabel!
    @IBOutlet weak var BtnEdit: UIButton!
    @IBOutlet weak var ToolView: UIView!
    @IBOutlet weak var LblAddressType: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.ToolView.isHidden = true
        AddressView.layer.cornerRadius = 5;
        AddressView.layer.masksToBounds = true;
        self.AddressView.layer.borderWidth = 1
        self.AddressView.layer.borderColor = UIColor(red:142/255, green:154/255, blue:16/255, alpha: 1).cgColor
        // Do any additional setup after loading the view.
    }
    
    @IBAction func BtnEditAddressClick(_ sender: Any) {
    }
    @IBAction func BtnBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func BtnCancelClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func BtnContinueClick(_ sender: Any) {
    }
    @IBAction func BtnAddAddress(_ sender: Any) {
        self.ToolView.isHidden = false
        self.LblMainTaskLocation.isHidden = true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
