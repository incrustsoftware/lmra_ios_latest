//
//  ConfirmYourDetailsVC.swift
//  LMRA
//
//  Created by Mac on 03/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class ConfirmYourDetailsVC: UIViewController ,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate{
    
    @IBOutlet weak var outerImageView: UIImageView!
    @IBOutlet weak var ImgCamera: UIImageView!
    
    @IBOutlet weak var ImgCameraView: UIView!
    
    @IBOutlet weak var BtnSmallCamera: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var txtFieldName: UITextField!
    @IBOutlet weak var lblIfamilyName: UILabel!
    @IBOutlet weak var txtFieldFamilyname: UITextField!
    @IBOutlet weak var lblNationality: UILabel!
    @IBOutlet weak var txtFieldNationality: UITextField!
    @IBOutlet weak var lblCellNumber: UILabel!
    @IBOutlet weak var txtfieldCellNumber: UITextField!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var txtFieldEmail: UITextField!
    @IBOutlet weak var LblCurrentCellNumber: UILabel!
    @IBOutlet weak var BtnConfirm: UIButton!
    @IBOutlet weak var txtFieldNameHeight: NSLayoutConstraint!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var txtfieldFamilyNameHeight: NSLayoutConstraint!
    @IBOutlet weak var txtFieldNtionalityHeight: NSLayoutConstraint!
  
    @IBOutlet weak var txtFieldEmailHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var BtnImageSelect: UIButton!
    
    //Variables
    var imageString = ""
    var Name = ""
    var FamilyName = ""
    var Nationality = ""
    var EmailID = ""
    var Get_FirstCell_Number = ""
    var Get_UpdatedCellNo = ""
    var CurrentCellNumber = ""
    var Get_CPR_Number = ""
    var TrimEmailID = ""
    let defaults = UserDefaults.standard
 
    var maxLenName:Int = 50
    var maxLenFamilyName:Int = 50
    var MaxLenNationality:Int = 50
    var MaxEmailID:Int = 50
    
    var charactersSet = CharacterSet(charactersIn: CHARACTERS.ACCEPTABLE_CHARACTERS)
    let picker = UIImagePickerController()
     var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        picker.delegate = self
        BtnConfirm.layer.cornerRadius = 2;
        // Do any additional setup after loading the view.
        imagePicker.delegate = self as! UIImagePickerControllerDelegate & UINavigationControllerDelegate
        outerImageView.layer.cornerRadius = outerImageView.frame.height / 2
        ImgCameraView.layer.cornerRadius = ImgCameraView.frame.height / 2
         BtnSmallCamera.layer.cornerRadius = BtnSmallCamera.frame.height / 2
        
        self.txtFieldName.delegate = self
        self.txtFieldFamilyname.delegate = self
        self.txtFieldNationality.delegate = self
        self.txtFieldEmail.delegate = self
        
        txtFieldName.addTarget(self, action: #selector(ConfirmYourDetailsVC.txtFieldChanged(_:)),
                          for: .editingChanged)
        txtFieldFamilyname.addTarget(self, action: #selector(ConfirmYourDetailsVC.txtFieldChanged),
                           for: .editingChanged)
        txtFieldNationality.addTarget(self, action: #selector(ConfirmYourDetailsVC.txtFieldChanged),
                          for: .editingChanged)
        txtFieldEmail.addTarget(self, action: #selector(ConfirmYourDetailsVC.txtFieldChanged),
                           for: .editingChanged)
        
        
        let nameTap = UITapGestureRecognizer(target:self,action:#selector(self.NameTapped))
        
        // add it to the label
        lblName.addGestureRecognizer(nameTap)
        lblName.isUserInteractionEnabled = true
        let familyNameTap = UITapGestureRecognizer(target:self,action:#selector(self.familyNameTapped))
        
        // add it to the label
        lblIfamilyName.addGestureRecognizer(familyNameTap)
        lblIfamilyName.isUserInteractionEnabled = true
        let nationalityTap = UITapGestureRecognizer(target:self,action:#selector(self.NationalityTapped))
        
        // add it to the label
        lblNationality.addGestureRecognizer(nationalityTap)
        lblNationality.isUserInteractionEnabled = true
        
        let emailTap = UITapGestureRecognizer(target:self,action:#selector(self.emailTapped))
        
        // add it to the label
        lblEmail.addGestureRecognizer(emailTap)
        lblEmail.isUserInteractionEnabled = true
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        scrollView.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        addDoneButtonOnKeyboard()
        
        self.Get_CPR_Number = UserDefaults.standard.string(forKey: "CPRNoKey")!//get
        let isCalledFromChangedNumber = UserDefaults.standard.bool(forKey: "isCellNoChange")// this
        
        if(isCalledFromChangedNumber == true)
        {
            self.Get_UpdatedCellNo = UserDefaults.standard.string(forKey: "ChnageCellNo")!//get
            print("UpdatedCellNo: \(self.Get_UpdatedCellNo)")
            self.CurrentCellNumber = self.Get_UpdatedCellNo
        }
        else
        {
            self.Get_FirstCell_Number = UserDefaults.standard.string(forKey: "cellNoKey")!//get
            print("UpdatedCellNo: \(self.Get_FirstCell_Number)")
            self.CurrentCellNumber = self.Get_FirstCell_Number
        }
        
        print("CurrentCellNumber: \(self.CurrentCellNumber)")
        UserDefaults.standard.set( self.CurrentCellNumber, forKey: "CurrentCellNoKey")
        
        self.LblCurrentCellNumber.text = CurrentCellNumber
    }
    
    //method to hide keyboard
    @objc func dismissKeyboard() {
        scrollView.contentOffset.y = 0
        // checkForValidation()
        view.endEditing(true)
    }
    
    @objc func NameTapped()
    {
        txtFieldNameHeight.constant = 30
        txtFieldName.becomeFirstResponder()
    }
    
    @objc func familyNameTapped()
    {
        txtfieldFamilyNameHeight.constant = 30
        txtFieldFamilyname.becomeFirstResponder()
    }
    
    @objc func NationalityTapped()
    {
        txtFieldNtionalityHeight.constant = 30
        txtFieldNationality.becomeFirstResponder()
    }
    
    @objc func emailTapped()
    {
        txtFieldEmailHeight.constant = 30
        txtFieldEmail.becomeFirstResponder()
    }
    
    @IBAction func btnContinueClicked(_ sender: Any) {
        self.validation()
    }
    
    func validation()
    {
        self.TrimEmailID = txtFieldEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines) as! String
        if (txtFieldName.text == "")
        {
            self.BtnConfirm.setTitleColor(COLOR.GrayColor, for: .normal)
            BtnConfirm.backgroundColor = COLOR.LightGrayColor
            self.view.makeToast("Please Enter Name")
        }
        else if txtFieldName.text?.rangeOfCharacter(from: charactersSet.inverted) != nil {
            self.view.makeToast("Special character not allowed &,.space allowed between characters allowed")
            
        }
        else if (txtFieldFamilyname.text == "")
        {
            self.BtnConfirm.setTitleColor(COLOR.GrayColor, for: .normal)
            BtnConfirm.backgroundColor = COLOR.LightGrayColor
            self.view.makeToast("Please Enter Family Name")
        }
        else if txtFieldFamilyname.text?.rangeOfCharacter(from: charactersSet.inverted) != nil {
            self.view.makeToast("Special character not allowed &,.space allowed between characters allowed")
            
        }
        else if (txtFieldNationality.text == "")
        {
            self.BtnConfirm.setTitleColor(COLOR.GrayColor, for: .normal)
            BtnConfirm.backgroundColor = COLOR.LightGrayColor
            self.view.makeToast("Please Enter Nationality")
        }
        else if txtFieldNationality.text?.rangeOfCharacter(from: charactersSet.inverted) != nil {
            self.view.makeToast("Special character not allowed &,.space allowed between characters allowed")
            
        }
        else if (isValidEmail(testStr: TrimEmailID ?? "") == false)
        {
            self.BtnConfirm.setTitleColor(COLOR.GrayColor, for: .normal)
            BtnConfirm.backgroundColor = COLOR.LightGrayColor
            self.view.makeToast("Please enter valid email address")
        }    
        else
        {
            self.Name =  txtFieldName.text ?? ""
            self.FamilyName = txtFieldFamilyname.text ?? ""
            self.Nationality =  txtFieldNationality.text ?? ""
            self.EmailID = txtFieldEmail.text ?? ""
            self.TrimEmailID = self.EmailID.trimmingCharacters(in: .whitespacesAndNewlines)
            
            defaults.set(self.Name, forKey: "LoginTaskerNameKey")//set
            defaults.set(self.FamilyName, forKey: "FamilyKey")//set
            defaults.set(self.Nationality, forKey: "NtionalityKey")//set
            defaults.set(self.TrimEmailID, forKey: "EmailKey")//set
            
            print("Name: \(self.Name)")
            print("FamilyName: \(self.FamilyName)")
            print("Nationality: \(self.Nationality)")
            print("EmailID: \(self.TrimEmailID)")
            
            
             self.confirm_personal_details()

//            if (txtFieldName.text != "" ) && (txtFieldFamilyname.text != "" ) && (txtFieldNationality.text != "" ) && (txtFieldEmail.text != "")
//            {
////                  self.confirm_personal_details()
//
//            }
        }
    }
    
    func confirm_personal_details()
    {
        let url = ConstantsClass.baseUrl + ConstantsClass.confirm_personal_details_customer
        print("url: \(url)")
        let parameters = [
            "name" : "\(self.Name)",
            "family_name" : "\(self.FamilyName)",
            "nationality" : "\(self.Nationality)",
            "email_id" : "\(self.TrimEmailID)",
            "cell_no" : "\(self.CurrentCellNumber)",
            "cpr_no" : "\(self.Get_CPR_Number)",
            "bitmap_image" : "\(self.imageString)"
            ] as [String : Any]
        
        print("parametersPersonaldetails: \(parameters)")
        Helper.sharedInstance.showLoader()
        Alamofire.request(url
            , method: .post, parameters: parameters, encoding:JSONEncoding.default).responseJSON
            {
                response in
                  
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if let status = response["status"] as? Bool {
                        
                        if status{
                            print("send_otp_success")
                            DispatchQueue.main.async {
                                Helper.sharedInstance.hideLoader()
                                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "EmailOTPVC") as! EmailOTPViewController
                                
                                self.navigationController?.pushViewController(nextViewController, animated: true)
                            }
                        }
                    }
                    else
                    {
                        self.view.makeToast("Something went wrong", duration: 3.0, position: .bottom)
                        print("Something went wrong")
                    }
                }
                else
                {
                        
                    print("Error occured") // serialized json response
                }
        }
    }
    
    @IBAction func btnBackClicked(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
   
    @IBAction func BtnSelectImageClick(_ sender: Any) {
        
        print("camera button click")
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as! UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func SelectImageBtnTapped(_ sender: Any) {
        print("camera button click")
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as! UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    //What to do when the picker returns with a photo
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        

           if let chosenImage = info[UIImagePickerController.InfoKey.originalImage]as? UIImage //2
           {
            
            imgUser.image = self.resizeImage(image: chosenImage, targetSize: CGSize(width: 350, height: 200))
            self.imgUser.image = self.resizeImage(image: chosenImage, targetSize: CGSize(width: chosenImage.size.width/10, height: chosenImage.size.height/10))
            outerImageView.contentMode = .scaleAspectFill //3
            outerImageView.image = self.imgUser.image //4
             self.imgUser.isHidden = true
              
            }
              
        picker.dismiss(animated: true, completion: nil)
        
        let imageData:Data = (self.imgUser.image?.jpegData(compressionQuality: 0.7)!)!
        let imageStr = imageData.base64EncodedString()
        self.imageString = imageStr
        
        
        
//        self.imgUser.isHidden = true
//        let chosenImage = info[UIImagePickerController.InfoKey.originalImage]
//            as? UIImage //2
//        outerImageView.contentMode = .scaleAspectFill //3
//        outerImageView.image = chosenImage //4
//        dismiss(animated: true, completion: nil) //5
//
//
//
//
//
//        let scaledImage = UIImage.scaleImageWithDivisor(img: chosenImage!, divisor: 3)
////        print("scaledImage:",scaledImage)
//
//        let imageData:NSData = scaledImage.pngData()! as NSData
//        self.imageString = imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
//
//        let FileType = ".png"
//        let ImageEncoded = self.imageString
        
    }
    
    //What to do if the image picker cancels.
    private func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
 func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
      let size = image.size
      let widthRatio  = targetSize.width  / size.width
      let heightRatio = targetSize.height / size.height

      // Figure out what our orientation is, and use that to form the rectangle
      var newSize: CGSize
      if(widthRatio > heightRatio) {
          newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
      } else {
          newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
      }

      // This is the rect that we've calculated out and this is what is actually used below
      let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)

      // Actually do the resizing to the rect using the ImageContext stuff
      UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
      image.draw(in: rect)
      let newImage = UIGraphicsGetImageFromCurrentImageContext()!
      UIGraphicsEndImageContext()

      return newImage
  }
    
    @objc func txtFieldChanged(_ textField: UITextField) {
         if (txtFieldName.text!.count == 0) || (txtFieldFamilyname.text!.count == 0) || (txtFieldNationality.text!.count == 0) || (txtFieldEmail.text!.count == 0)
         {
             self.BtnConfirm.setTitleColor(COLOR.GrayColor, for: .normal)
             BtnConfirm.backgroundColor = COLOR.LightGrayColor
         }
         else
         {
            self.BtnConfirm.setTitleColor(COLOR.WhiteColor, for: .normal)
             BtnConfirm.backgroundColor = COLOR.PinkColor
         }
     }
    
    
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 20))
        doneToolbar.barStyle = .default
        let Cancel: UIBarButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(self.doneButtonAction))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [Cancel,flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.txtFieldName.inputAccessoryView = doneToolbar
        self.txtFieldFamilyname.inputAccessoryView = doneToolbar
        self.txtFieldNationality.inputAccessoryView = doneToolbar
        self.txtFieldEmail.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.txtFieldName.resignFirstResponder()
        self.txtFieldFamilyname.resignFirstResponder()
        self.txtFieldNationality.resignFirstResponder()
        self.txtFieldEmail.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == txtFieldName){
            let currentText = textField.text! + string
            return currentText.count <= maxLenName
        }
        
        if(textField == txtFieldFamilyname){
            let currentText = textField.text! + string
            return currentText.count <= maxLenFamilyName
        }
        
        if(textField == txtFieldNationality){
            let currentText = textField.text! + string
            return currentText.count <= MaxLenNationality
        }
        if(textField == txtFieldEmail){
            let currentText = textField.text! + string
            return currentText.count <= MaxEmailID
        }
        
        return true;
    }
    
    @objc func keyboardWillShow(notification: NSNotification)
    {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                //                        self.view.frame.origin.y -= keyboardSize.height
                self.view.frame.origin.y -= 150
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification)
    {
        if self.view.frame.origin.y != 0 {
            //                    self.view.frame.origin.y = 0
            self.view.frame.origin.y += 150
        }
    }
    
    func isValidEmail(testStr:String) -> Bool
    {
                let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
//        let emailRegEx =   "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
//            "\\@" +
//            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
//            "(" +
//            "\\." +
//            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
//        ")+"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func validate(value: String) -> Bool
    {
        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
}

extension UIImage {
    class func scaleImageWithDivisor(img: UIImage, divisor: CGFloat) -> UIImage {
        let size = CGSize(width: img.size.width/divisor, height: img.size.height/divisor)
        UIGraphicsBeginImageContext(size)
        img.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return scaledImage!
    }
}
