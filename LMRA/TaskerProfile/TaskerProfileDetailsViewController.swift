//
//  TaskerProfileDetailsViewController.swift
//  LMRA
//
//  Created by Mac on 20/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire

class TaskerProfileDetailsViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    @IBOutlet weak var JobHistoryCollectionView: UICollectionView!
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var ProfileView: UIView!
    @IBOutlet weak var BtnCancel: UIButton!
    @IBOutlet weak var ImgTasker: UIImageView!
    @IBOutlet weak var LblTaskerName: UILabel!
    @IBOutlet weak var LblSubName: UILabel!
    @IBOutlet weak var LblTaskCompleted: UILabel!
    @IBOutlet weak var LblTaskerLanguage: UILabel!
    @IBOutlet weak var LblTaskerRate: UILabel!
    @IBOutlet weak var LblTaskerPrice: UILabel!
    @IBOutlet weak var ImgServic1: UIImageView!
    @IBOutlet weak var ImgServic2: UIImageView!
    @IBOutlet weak var ImgServic3: UIImageView!
    @IBOutlet weak var BtnInvite: UIButton!
    @IBOutlet weak var LblTaskerRate3: UILabel!
    @IBOutlet weak var LblTaskerRate2: UILabel!
    @IBOutlet weak var LblTaskerRate1: UILabel!
    @IBOutlet weak var LblSubcategory3: UILabel!
    @IBOutlet weak var LblSubcategory2: UILabel!
    @IBOutlet weak var LblSubcategory1: UILabel!
    @IBOutlet weak var LblNationality: UILabel!
    var authentication_key_val = ""
    var GetTaskerID = ""
    var AllLanguages = ""
    var AllDataDict = [[String:Any]]()
    var SubCategoryName_Array = [String] ()
    var TaskerRate_Array = [String] ()
    var TaskTitle_Array = [String] ()
    var TaskeDescription_Array = [String] ()
     var TaskDate_Array = [String] ()
    
    var IsProfileArrowClick:Bool = false // from dashboard click
    var IsProfileClick:Bool = false //from accpeted details page
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ProfileView.layer.shadowColor = UIColor.gray.cgColor
        ProfileView.layer.shadowOpacity = 0.5
        ProfileView.layer.shadowOffset = CGSize.zero
        ProfileView.layer.shadowRadius = 6
        ProfileView.layer.cornerRadius = 5
        ProfileView.clipsToBounds = false
        
    
        
       
        self.authentication_key_val = UserDefaults.standard.string(forKey: "authentication_key") ?? ""//get
        ImgTasker.layer.cornerRadius = ImgTasker.frame.size.width/2
        ImgTasker.clipsToBounds = true
       
        self.GetTaskerID = UserDefaults.standard.string(forKey: "SelectedTaskerID") ?? ""//get
        print("GetTaskerID:",(GetTaskerID))
          self.get_tasker_details()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        print("isFromTaskerProfile:",(IsProfileArrowClick))
        if IsProfileArrowClick || IsProfileClick
        {
            self.BtnInvite.isHidden = true
        }
 
    }
    
    @IBAction func CancelbtnClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func InviteBtnClick(_ sender: Any){
        let storyBoard : UIStoryboard = UIStoryboard(name: "DashFlow", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "CodeConductVC") as! CodeConductViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.TaskTitle_Array.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = JobHistoryCollectionView.dequeueReusableCell(withReuseIdentifier: "JobHistoryCell", for: indexPath as IndexPath) as! JobHistoryCollectionViewCell
        
        cell.CellView.layer.shadowColor = UIColor.gray.cgColor
        cell.CellView.layer.shadowOpacity = 0.5
        cell.CellView.layer.shadowOffset = CGSize.zero
        cell.CellView.layer.shadowRadius = 6
        cell.CellView.layer.cornerRadius = 5
        cell.CellView.clipsToBounds = false
        
        cell.DescriptionView.roundCorners([.bottomRight, .bottomLeft], radius: 5)
        cell.LblJobHistoryTitle.text = self.TaskTitle_Array[indexPath.item]
        cell.LblJobHistoryDesc.text = self.TaskeDescription_Array[indexPath.item]
        let ConvertedTaskDate = self.TaskDate_Array[indexPath.item]
        cell.LblHistoryDate.text = StaticUtility.convertDateFormater7(ConvertedTaskDate)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func get_tasker_details()
    {
        
        let urlString = ConstantsClass.baseUrl + ConstantsClass.get_tasker_details
        print("urlString: \(urlString)")
        
        let headers = [
            "Authorization": "\(self.authentication_key_val)"
        ]
        print("headers: \(headers)")
        
        let parameters = [
            "tasker_id" : "\(self.GetTaskerID)"
            ] as [String : Any]
        print("parametersaddTaskerDetails: \(parameters)")
        Helper.sharedInstance.showLoader()
        Alamofire.request(urlString
            , method: .post, parameters: parameters, encoding:JSONEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if let status = response["status"] as? Bool {
                        print("Get status true here")
                        
                        if status
                        {
                            if let respArray = response["response"] as? [[String:Any]] {
                                print("respArrayTaskerDetails: \(respArray)")
                                
                                self.AllDataDict = respArray
                                print("AllDataDictTaskerDetails:",self.AllDataDict)
                                
                                for TaskerData in self.AllDataDict
                                {
                                    let TaskerName = TaskerData["tasker_name"]as? String ?? ""
                                    let TaskerProfileImage = TaskerData["tasker_user_profile"]as? String ?? ""
                                    let TaskerFamilyName = TaskerData["tasker_family_name"]as? String ?? ""
                                    let TaskerTaskCount = TaskerData["tasker_completed_tasks_count"]as? Int ?? 0
                                    let TaskerNationality = TaskerData["tasker_nationality"]as? String ?? ""
                                    
                                    let TaskerTaskCountString = String(TaskerTaskCount)
                                    self.LblTaskerName.text = TaskerName + " " + TaskerFamilyName
                                    self.ImgTasker.downloaded(from:TaskerProfileImage )
                                    self.ImgTasker.contentMode = .scaleAspectFill //3
                                    self.LblTaskCompleted.text = TaskerTaskCountString  + " Completed Tasks"
                                    self.LblNationality.text = TaskerNationality
                                    
                                    if let tasker_Rate = TaskerData["tasker_rate"]as? [[String:Any]]
                                    {
                                        for rateData in tasker_Rate
                                        {
                                            let AmountType = rateData["rate"] as? String ?? ""
                                            print ("AmountType.....: \(AmountType)")
                                            self.TaskerRate_Array.append(AmountType)
                                            
                                        }
                                        if self.TaskerRate_Array.count > 0
                                        {
                                            if self.TaskerRate_Array.count == 1
                                            {
                                                
                                                self.LblTaskerRate1.text = "BHD " + self.TaskerRate_Array[0]
                                                self.LblTaskerRate2.isHidden = true
                                                self.LblTaskerRate3.isHidden = true
                                                
                                            }
                                            if self.TaskerRate_Array.count == 2
                                            {
                                                self.LblTaskerRate1.text = "BHD " + self.TaskerRate_Array[0]
                                                self.LblTaskerRate2.text = "BHD " + self.TaskerRate_Array[1]
                                                self.LblTaskerRate3.isHidden = true
                                            }
                                            if self.TaskerRate_Array.count == 3
                                            {
                                                self.LblTaskerRate1.text = "BHD " + self.TaskerRate_Array[0]
                                                self.LblTaskerRate2.text = "BHD " + self.TaskerRate_Array[1]
                                                self.LblTaskerRate3.text = "BHD " + self.TaskerRate_Array[2]
                                            }
                                            
                                        }
                                        else
                                        {
                                            self.LblTaskerRate1.text = "BHD "
                                            self.LblTaskerRate2.isHidden = true
                                            self.LblTaskerRate3.isHidden = true
                                        }
                                        
                                    }
                                    if let tasker_languages = TaskerData["tasker_languages"]as? [[String:Any]]
                                    {
                                        
                                        for langData in tasker_languages
                                        {
                                            
                                            let language_name = langData["language_name"] as? String ?? ""
                                            print ("language_name.....: \(language_name)")
                                            self.AllLanguages =  "\(self.AllLanguages) \(langData["language_name"] as? String ?? "") ,"
                                        }
                                        
                                        var All_Subcatgory = self.AllLanguages
                                        var langSpeaker = "Speakes"
//                                         All_Subcatgory.removeLast()
                                        if All_Subcatgory.count > 0
                                        {
                                             All_Subcatgory.removeLast()
                                        }
                                       

                                        //concate attributed value as per font size
                                        let myMutableString1 = NSMutableAttributedString(
                                            string: langSpeaker,
                                            attributes: [NSAttributedString.Key.font:UIFont(name: "STCForward-Regular", size: 14.0)!])

                                        let myMutableString2 = NSMutableAttributedString(
                                        string: All_Subcatgory,
                                        attributes: [NSAttributedString.Key.font:UIFont(name: "STCForward-Bold", size: 14.0)!])
                                        //Add more attributes here

                                        let label = NSMutableAttributedString()
                                        label.append(myMutableString1)
                                        label.append(myMutableString2)
                                        
                                        //Apply to the label
                                        self.LblTaskerLanguage.attributedText = label
                                        
                                        
                                    }
                                    if let tasker_subcategories = TaskerData["tasker_subcategories"]as? [[String:Any]]
                                    {
                                        
                                        for SubcategoryData in tasker_subcategories
                                        {
                                            
                                            let subcategory_name = SubcategoryData["subcategory_name"] as? String ?? ""
                                            let subcategory_image = SubcategoryData["subcategory_image"] as? String ?? ""
                                            self.SubCategoryName_Array.append(subcategory_name)
                                            print ("subcategory_name.....: \(subcategory_name)")
                                            
                                        }
                                        if self.SubCategoryName_Array.count > 0
                                        {
                                            if self.SubCategoryName_Array.count == 1
                                            {
                                                // self.ImgServic1.downloaded(from: subcategory_image)
                                                self.LblSubcategory1.text = self.SubCategoryName_Array[0]
                                                
                                                self.ImgServic2.isHidden = true
                                                self.LblSubcategory2.isHidden = true
                                                
                                                self.ImgServic3.isHidden = true
                                                self.LblSubcategory3.isHidden = true
                                                
                                            }
                                            if self.SubCategoryName_Array.count == 2
                                            {
                                                // self.ImgServic1.downloaded(from: subcategory_image)
                                                self.LblSubcategory1.text = self.SubCategoryName_Array[0]
                                                self.ImgServic1.isHidden = false
                                                
                                                self.LblSubcategory2.text = self.SubCategoryName_Array[1]
                                                self.ImgServic2.isHidden = false
                                                
                                                self.ImgServic3.isHidden = true
                                                self.LblSubcategory3.isHidden = true
                                            }
                                            if self.SubCategoryName_Array.count == 3
                                            {
                                                // self.ImgServic1.downloaded(from: subcategory_image)
                                                self.LblSubcategory1.text = self.SubCategoryName_Array[0]
                                                self.ImgServic1.isHidden = false
                                                
                                                self.LblSubcategory2.text = self.SubCategoryName_Array[1]
                                                self.ImgServic2.isHidden = false
                                                
                                                self.LblSubcategory3.text = self.SubCategoryName_Array[2]
                                                self.ImgServic2.isHidden = false
                                            }
                                            
                                        }
                                        else
                                        {
                                            
                                        }
                                        
                                    }
                                    if let tasker_history = TaskerData["tasker_history"]as? [[String:Any]]
                                    {
                                        if tasker_history.count > 0
                                        {
                                        for taskerHistoryData in tasker_history
                                        {
                                            
                                            let task_title = taskerHistoryData["task_title"] as? String ?? ""
                                            let task_description = taskerHistoryData["task_description"] as? String ?? ""
                                            let task_date = taskerHistoryData["task_date"] as? String ?? ""
                                            self.TaskTitle_Array.append(task_title)
                                            self.TaskeDescription_Array.append(task_description)
                                            self.TaskDate_Array.append(task_date)
                                            print ("task_date.....: \(task_date)")
                                            
                                        }
                                        }
                                        else
                                        {
                                            Helper.sharedInstance.hideLoader()
                                            self.JobHistoryCollectionView.isHidden = true
                                        }
                                    }
                                }
                                
                                
                            }
                        }
                        else
                        {
                            print("status false")
                        }
                        
                    }
                    
                    
                    // Make sure to update UI in main thread
                    DispatchQueue.main.async {
                        Helper.sharedInstance.hideLoader()
                        self.JobHistoryCollectionView.reloadData()
                        print("Tasker Search successfully.")
                    }
                }
                else
                {
                    Helper.sharedInstance.hideLoader()
                    print("Error occured") // serialized json response
                }
        }
    }
    
    
    
}

