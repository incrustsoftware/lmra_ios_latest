//
//  JobHistoryCollectionViewCell.swift
//  LMRA
//
//  Created by Mac on 20/08/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class JobHistoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var CellView: UIView!
    @IBOutlet weak var DescriptionView: UIView!
    
    @IBOutlet weak var LblJobHistoryTitle: UILabel!
    @IBOutlet weak var LblHistoryDate: UILabel!
    
    @IBOutlet weak var LblJobHistoryDesc: UILabel!
    @IBOutlet weak var LblJobHistoryDescriptionTitle: UILabel!
    
   

    
}
   
