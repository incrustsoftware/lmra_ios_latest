//
//  CutomerMenuVC.swift
//  LMRA
//
//  Created by Mac on 23/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire

@available(iOS 12.0, *)
@available(iOS 12.0, *)
class CutomerMenuVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var ProfileView: UIView!
    @IBOutlet weak var MenuTableview: UITableView!
    @IBOutlet weak var SwitchTaskerView: UIView!
    
    @IBOutlet weak var LblEditProfile: UILabel!
    @IBOutlet weak var LblPrifileName: UILabel!
    @IBOutlet weak var ProfileImage: UIImageView!
    
    @IBOutlet weak var BtnSwitchToTasker: UIButton!
    @IBOutlet weak var BtnCancel: UIButton!
    
    var LoginSMSauthentication_key_val = ""
    var GetCustomerUsertype:Int!
    var StringGetCustomerUsertype = ""
    var AllDataDict = [[String:Any]]()
    var window: UIWindow?
    let MenuIconArray = [UIImage(named: "PurpleUser"),UIImage(named: "PurpleUser"), UIImage(named: "PurplePage"),UIImage(named: "PurpleFAQ"),UIImage(named: "PurpleHelp"),UIImage(named: "PurpleAboutUs"),UIImage(named: "PurpleLogOut")]
    
    var MenuTitleArray = ["Account Settings","Payment History","How it works","FAQ","Help","About us","Log out"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ProfileImage.layer.masksToBounds = false
        ProfileImage.layer.cornerRadius = ProfileImage.frame.height/2
        ProfileImage.clipsToBounds = true
        super.viewDidLoad()
        
        self.LoginSMSauthentication_key_val = UserDefaults.standard.string(forKey: "authentication_key")!//get
        LblEditProfile.textColor = COLOR.PinkColor// UIColor(red:255/255, green:55/255, blue:94/255, alpha:1.0)//pink
        let EditProfileTapped = UITapGestureRecognizer(target:self,action:#selector(self.EditProfileTapped))
        
        // add it to the label
        LblEditProfile.addGestureRecognizer(EditProfileTapped)
        LblEditProfile.isUserInteractionEnabled = true
        
        SwitchTaskerView.addTapGesture{
            
            let navigationController = self.sideMenuController?.rootViewController as! UINavigationController
            let vc:SwitchToTaskerViewController = self.storyboard?.instantiateViewController(withIdentifier: "SwitchToTaskerVC")as!SwitchToTaskerViewController
            navigationController.pushViewController(vc, animated: true)
            self.sideMenuController?.hideRightView(animated: false) {}
  
        }
        
        //for now
        // set userType from login
        self.GetCustomerUsertype =  UserDefaults.standard.integer(forKey: UserDataManager_KEY.userType)
        print("GetCustomerUsertype: \(GetCustomerUsertype)")
        self.StringGetCustomerUsertype = String(self.GetCustomerUsertype)
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.get_customer_basic_profile_details()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.MenuTitleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = MenuTableview.dequeueReusableCell(withIdentifier: "MenuDrawerCell", for: indexPath as IndexPath) as! MenuDrawerTableViewCell
        
        cell.LblMenuDrawer.text = self.MenuTitleArray[indexPath.row]
        let Images = MenuIconArray[indexPath .row]
        cell.ImgMenuDrawer.image = Images
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0
        {
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc:AccountSettingViewController = self.storyboard?.instantiateViewController(withIdentifier: "AccountSettingVC")as!AccountSettingViewController
            navigationController.pushViewController(vc, animated: true)
            sideMenuController?.hideRightView(animated: false) {}
        }
        if indexPath.row == 1
        {
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc:PaymentHistoryViewController = self.storyboard?.instantiateViewController(withIdentifier: "PaymentHistoryVC")as!PaymentHistoryViewController
            navigationController.pushViewController(vc, animated: true)
            sideMenuController?.hideRightView(animated: false) {}
        }
        else if indexPath.row == 6
        {
            // call logout API
            self.sign_out()
            
            
        }
    }
    
    @IBAction func BtnCancleClick(_ sender: Any) {
        sideMenuController?.hideRightView(animated: false) {}
   
    }
    
    @IBAction func BtnSwitchToTaskerClick(_ sender: Any) {
    
        let navigationController = sideMenuController?.rootViewController as! UINavigationController
        let vc:SwitchToTaskerViewController = self.storyboard?.instantiateViewController(withIdentifier: "SwitchToTaskerVC")as!SwitchToTaskerViewController
        navigationController.pushViewController(vc, animated: true)
        sideMenuController?.hideRightView(animated: false) {}
    }
    
    
    
    @objc func EditProfileTapped()
    {

        let navigationController = sideMenuController?.rootViewController as! UINavigationController
        let vc:EditProfileViewController = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileVC")as!EditProfileViewController
        navigationController.pushViewController(vc, animated: true)
        sideMenuController?.hideRightView(animated: false) {}
        
    }
    
    
    
    func sign_out()
    {
        
        
        let urlString = ConstantsClass.baseUrl + ConstantsClass.sign_out
        print("urlString: \(urlString)")
        
        let headers = [
            "Authorization": "\(self.LoginSMSauthentication_key_val)"
        ]
        print("headers: \(headers)")
        
        let parameters = [
            "user_type" : "\(self.StringGetCustomerUsertype)",
            ] as [String : Any]
        print("parameterssignOut: \(parameters)")
        Alamofire.request(urlString
            , method: .post, parameters: parameters, encoding:JSONEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any] {
                    print("responseSignOut: \(response)")
                    if let status = response["status"] as? Bool {
                        if status
                        {
                            UserDataManager.shared.clearUserDetails()
                            print("sign_out_success")
//
//                            let navigationController = self.sideMenuController?.rootViewController as! UINavigationController
//                            var isPopView = false
//                            for controller in self.navigationController!.viewControllers as Array {
//                                if controller.isKind(of: LoginViewController.self) {
//                                    isPopView = true
//                                    self.navigationController!.popToViewController(controller, animated: true)
//                                    break
//                                }
//                            }
//
//                            if isPopView == false {
//
//                                let vc = Storyboard().mainStoryboard.instantiateViewController(withIdentifier: "LoginVC")
//                                self.navigationController?.pushViewController(vc, animated: false)
//                            }
                            
                            self.sideMenuController?.hideRightView(animated: true) {}
                            self.view.makeToast("sign_out_success", duration: 6.0, position: .bottom)
                            
                            let navigationController = self.sideMenuController?.rootViewController as! UINavigationController
                            var isPopView = false
                            for controller in navigationController.viewControllers as Array {
                                if controller.isKind(of: LoginViewController.self) {
                                    isPopView = true
                                    navigationController.popToViewController(controller, animated: true)
                                    break
                                }
                            }
                            
                            if isPopView == false {
                                let vc = Storyboard().mainStoryboard.instantiateViewController(withIdentifier: "LoginVC")
                                navigationController.pushViewController(vc, animated: false)
                            }
                            
                            
                        }
                        
                    }
                    // Make sure to update UI in main thread
                    DispatchQueue.main.async {
                        print("waiting_task_details_list_success")
                        
                        
                        
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
        
    }
    
    
    
    func get_customer_basic_profile_details()
    {
        
        self.LoginSMSauthentication_key_val = UserDefaults.standard.string(forKey: "authentication_key")!//get
        let urlString = ConstantsClass.baseUrl + ConstantsClass.get_customer_basic_profile_details
        print("urlString: \(urlString)")
        
        let headers = [
            "Authorization": "\(self.LoginSMSauthentication_key_val)"
        ]
        print("headers: \(headers)")
        Helper.sharedInstance.showLoader()
        Alamofire.request(urlString, method: .get, parameters: nil, encoding: URLEncoding.default,headers: headers).responseJSON
            {
                response in
                if let response = response.result.value as? [String:Any] {
                    print("response: \(response)")
                    
                    if (response["status"] as? Bool) != nil {
                        if let respArray = response["response"] as? [[String:Any]] {
                            print("respArray: \(respArray)")
                            for Profiledata in respArray
                            {
                                let ProfileName = Profiledata["customer_name"]as? String ?? ""
                                let ProfileImage = Profiledata["customer_profile"]as? String ?? ""
                                let CustomerCellNo = Profiledata["customer_cell_phone_number"]as? String ?? ""
                                UserDefaults.standard.set(CustomerCellNo, forKey: "CustomerLoginCellNoKey")
                                print("CustomerCellNo: \(CustomerCellNo)")
                                self.LblPrifileName.text = ProfileName
                                self.ProfileImage.downloaded(from: ProfileImage)
                                self.ProfileImage.contentMode = .scaleAspectFill //3
                                Helper.sharedInstance.hideLoader()
                            }
                            
                            
                        }
                    }
                    // Make sure to update UI in main thread
                    DispatchQueue.main.async {
                        
                    }
                }
                else
                {
                    print("Error occured") // serialized json response
                }
        }
        
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
