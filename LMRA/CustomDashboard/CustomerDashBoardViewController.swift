//
//  CustomerDashBoardViewController.swift
//  LMRA
//
//  Created by Mac on 23/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit


@available(iOS 12.0, *)
class CustomerDashBoardViewController: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tabSelectionCV: UICollectionView!
    @IBOutlet weak var tapBaseView: UIView!
    
    weak var currentViewController: UIViewController?
    var arrDashboardData = [[String:String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.arrDashboardData.append(["title":"",
                                      "image": "Home",
                                      "selectedImage":"selectedHome",
                                      "isSelected":"1"])
        self.arrDashboardData.append(["title":"",
                                      "image":"calender",
                                      "selectedImage":"selectedCalendar",
                                      "isSelected":"0"])
        self.arrDashboardData.append(["title":"",
                                             "image":"Diamond",
                                             "selectedImage":"selectedDiamond",
                                             "isSelected":"0"])
        self.arrDashboardData.append(["title":"",
                                      "image": "hamburger",
                                      "selectedImage":"hamburger",
                                      "isSelected":"0"])
        
        self.currentViewController = Storyboard().dashFlowStoryboard.instantiateViewController(withIdentifier: "FirstTabVC") as! FirstTabViewController
        self.currentViewController!.view.translatesAutoresizingMaskIntoConstraints = false
        self.addChild(self.currentViewController!)
        self.addSubview(subView: self.currentViewController!.view, toView: self.containerView)
        self.changeTab(selectedIndex: 0)
        self.tabSelectionCV.reloadData()
        // Do any additional setup after loading the view.
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        self.setStatusBarColor(color: UIColor.init(named: "colorTaskerIndigo")!, styleForStatus: .lightContent)
//        setNeedsStatusBarAppearanceUpdate()
//    }
    
    override func viewDidAppear(_ animated: Bool) {
        tapBaseView.setCardView(cornerRadius: 0, shadowColor: UIColor.lightGray, shadowOffsetHeight: 0, shadowOffsetWidth: 2, shadowOpacity: 2)
    }
    
    func addSubview(subView:UIView, toView parentView:UIView) {
        parentView.addSubview(subView)
        
        var viewBindingsDict = [String: AnyObject]()
        viewBindingsDict["subView"] = subView
        parentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[subView]|",
                                                                 options: [], metrics: nil, views: viewBindingsDict))
        parentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[subView]|",
                                                                 options: [], metrics: nil, views: viewBindingsDict))
    }
    
    func cycleFromViewController(oldViewController: UIViewController, toViewController newViewController: UIViewController) {
        oldViewController.willMove(toParent: nil)
        self.addChild(newViewController)
        self.addSubview(subView: newViewController.view, toView:self.containerView!)
        // TODO: Set the starting state of your constraints here
        newViewController.view.layoutIfNeeded()
        
        // TODO: Set the ending state of your constraints here
        
        UIView.animate(withDuration: 0.5, animations: {
            // only need to call layoutIfNeeded here
            newViewController.view.layoutIfNeeded()
        },
                       completion: { finished in
                        oldViewController.view.removeFromSuperview()
                        oldViewController.removeFromParent()
                        newViewController.didMove(toParent: self)
        })
    }
    
    func changeTab(selectedIndex:Int){
        for(index,element) in arrDashboardData.enumerated() {
            var data = element
            if selectedIndex == index {
                data["isSelected"] = "1"
            }
            else {
                data["isSelected"] = "0"
            }
            self.arrDashboardData[index] = data
        }
        tabSelectionCV.reloadData()
    }
}


//MARK:- CollectionView Delegate Methods

@available(iOS 12.0, *)
extension CustomerDashBoardViewController:UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       print("self.arrDashboardData.count",self.arrDashboardData.count)
        return self.arrDashboardData.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:CustomerDashCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomerDashCell", for: indexPath)as!CustomerDashCollectionViewCell
        let data = arrDashboardData[indexPath.row]
        if data["isSelected"] == "1" {
            cell.imgDash.image = UIImage.init(named: self.arrDashboardData[indexPath.row]["selectedImage"] ?? "")
        }
        else {
            cell.imgDash.image = UIImage.init(named: self.arrDashboardData[indexPath.row]["image"] ?? "")
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            self.currentViewController = Storyboard().dashFlowStoryboard.instantiateViewController(withIdentifier: "FirstTabVC")
            self.currentViewController!.view.translatesAutoresizingMaskIntoConstraints = false
            self.addChild(self.currentViewController!)
            self.addSubview(subView: self.currentViewController!.view, toView: self.containerView)
            self.changeTab(selectedIndex: indexPath.row)
        }
        else if indexPath.row == 1 {
            self.currentViewController = Storyboard().dashFlowStoryboard.instantiateViewController(withIdentifier: "SecondTabVC")
            self.currentViewController!.view.translatesAutoresizingMaskIntoConstraints = false
            self.addChild(self.currentViewController!)
            self.addSubview(subView: self.currentViewController!.view, toView: self.containerView)
            self.changeTab(selectedIndex: indexPath.row)
        }
        else if indexPath.row == 2 {
          self.currentViewController = Storyboard().dashFlowStoryboard.instantiateViewController(withIdentifier: "ThirdTabVC")
                     self.currentViewController!.view.translatesAutoresizingMaskIntoConstraints = false
                     self.addChild(self.currentViewController!)
                     self.addSubview(subView: self.currentViewController!.view, toView: self.containerView)
                     self.changeTab(selectedIndex: indexPath.row)
        }
        else if indexPath.row == 3 {
            sideMenuController?.showRightView(animated: true, completionHandler: {
            })
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width/4, height: collectionView.frame.size.height)
    }
}
