//
//  ViewController.swift
//  LMRA
//
//  Created by Mac on 30/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

//    if (myListData.isIs_dual_role()) {
//        if (user_type.equalsIgnoreCase("2")) {
//            if (myListData.getTask_invitation_status().equalsIgnoreCase("Waiting")) {
//
//                holder.rel_line.setVisibility(View.VISIBLE);
//                holder.rel_line.setBackgroundColor(context.getResources().getColor(R.color.text_color_green));
//                holder.rel_profile.setVisibility(View.VISIBLE);
//                holder.txtDetail.setText("New Task Invite");
//                holder.txtTask.setText("Task");
//                holder.txtservicename.setVisibility(View.GONE);
//                holder.txtDetail.setTextColor(context.getResources().getColor(R.color.inviteTask));
//                holder.txtDaytime1.setText(myListData.getTask_day_time() + ", " + myListData.getNotification_time());
//                holder.txtName.setText(myListData.getCustomer_name());
//                holder.rd_week_date1.setBackground(context.getResources().getDrawable(R.drawable.tasker_invite));
//                Glide.with(context)
//                        .load(myListData.getCustomer_profile())
//                        .circleCrop()
//                        .into(holder.customer_image);
//
//                holder.rel_invite.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Intent details = new Intent(context, Tasker_Task_Details_Activity.class);
//                        details.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        details.putExtra("task_id", myListData.getTask_id());
//                        context.startActivity(details);
//                    }
//                });
//
//
//            } else if (myListData.getTask_invitation_status().equalsIgnoreCase("Cancelled")) {
//
//                holder.rel_line.setVisibility(View.VISIBLE);
//                holder.rel_line.setBackgroundColor(context.getResources().getColor(R.color.text_color_green));
//                holder.rel_profile.setVisibility(View.GONE);
//                holder.txtservicename.setText(myListData.getTask_subcategory());
//                holder.txtDetail.setText("Your Task Is Cancelled");
//                holder.txtTask.setText("Hiring");
//                holder.txtDetail.setTextColor(context.getResources().getColor(R.color.date));
//                holder.txtDaytime1.setText(myListData.getTask_day_time() + ", " + myListData.getNotification_time());
//                holder.rd_week_date1.setBackground(context.getResources().getDrawable(R.drawable.tasker_white_upd));
//
//
//            } else if (myListData.getTask_invitation_status().equalsIgnoreCase("Declined")) {
//                holder.rel_line.setVisibility(View.VISIBLE);
//                holder.rel_line.setBackgroundColor(context.getResources().getColor(R.color.inviteTask));
//                holder.rel_profile.setVisibility(View.GONE);
//                holder.txtservicename.setText(myListData.getTask_subcategory());
//                holder.txtDetail.setText("Your Task Is Declined");
//                holder.txtTask.setText("Hiring");
//                holder.txtDetail.setTextColor(context.getResources().getColor(R.color.date));
//                holder.txtDaytime1.setText(myListData.getTask_day_time() + ", " + myListData.getNotification_time());
//                holder.rd_week_date1.setBackground(context.getResources().getDrawable(R.drawable.tasker_white_upd));
//
//                holder.rel_invite.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        notificationInterface.callbackMethodNotification("1");
//                    }
//                });
//
//            } else if (myListData.getTask_invitation_status().equalsIgnoreCase("Accepted")) {
//                holder.rel_line.setVisibility(View.VISIBLE);
//                holder.rel_line.setBackgroundColor(context.getResources().getColor(R.color.inviteTask));
//                holder.rel_profile.setVisibility(View.GONE);
//                holder.txtservicename.setText(myListData.getTask_subcategory());
//                holder.txtDetail.setText("Your Task Is Accepted");
//                holder.txtTask.setText("Hiring");
//                holder.txtDetail.setTextColor(context.getResources().getColor(R.color.date));
//                holder.txtDaytime1.setText(myListData.getTask_day_time() + ", " + myListData.getNotification_time());
//                holder.rd_week_date1.setBackground(context.getResources().getDrawable(R.drawable.tasker_white_upd));
//
//                holder.rel_invite.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        notificationInterface.callbackMethodNotification("1");
//                    }
//                });
//            }
//        } else if (user_type.equalsIgnoreCase("1")) {
//            if (myListData.getTask_invitation_status().equalsIgnoreCase("Waiting")) {
//
//                holder.rel_line.setVisibility(View.VISIBLE);
//                holder.rel_line.setBackgroundColor(context.getResources().getColor(R.color.text_color_green));
//                holder.rel_profile.setVisibility(View.VISIBLE);
//                holder.txtDetail.setText("New Task Invite");
//                holder.txtTask.setText("Task");
//                holder.txtservicename.setVisibility(View.GONE);
//                holder.txtDetail.setTextColor(context.getResources().getColor(R.color.inviteTask));
//                holder.txtDaytime1.setText(myListData.getTask_day_time() + ", " + myListData.getNotification_time());
//                holder.txtName.setText(myListData.getCustomer_name());
//                holder.rd_week_date1.setBackground(context.getResources().getDrawable(R.drawable.tasker_invite));
//                Glide.with(context)
//                        .load(myListData.getCustomer_profile())
//                        .circleCrop()
//                        .into(holder.customer_image);
//
//                holder.rel_invite.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        notificationInterface.callbackMethodNotification("2");
//                    }
//                });
//
//
//            } else if (myListData.getTask_invitation_status().equalsIgnoreCase("Cancelled")) {
//
//                holder.rel_line.setVisibility(View.VISIBLE);
//                holder.rel_line.setBackgroundColor(context.getResources().getColor(R.color.text_color_green));
//                holder.rel_profile.setVisibility(View.GONE);
//                holder.txtservicename.setText(myListData.getTask_subcategory());
//                holder.txtDetail.setText("Your Task Is Cancelled");
//                holder.txtTask.setText("Hiring");
//                holder.txtDetail.setTextColor(context.getResources().getColor(R.color.date));
//                holder.txtDaytime1.setText(myListData.getTask_day_time() + ", " + myListData.getNotification_time());
//                holder.rd_week_date1.setBackground(context.getResources().getDrawable(R.drawable.tasker_white_upd));
//
//                holder.rel_invite.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        notificationInterface.callbackMethodNotification("2");
//                    }
//                });
//
//
//            } else if (myListData.getTask_invitation_status().equalsIgnoreCase("Declined")) {
//                holder.rel_line.setVisibility(View.VISIBLE);
//                holder.rel_line.setBackgroundColor(context.getResources().getColor(R.color.inviteTask));
//                holder.rel_profile.setVisibility(View.GONE);
//                holder.txtservicename.setText(myListData.getTask_subcategory());
//                holder.txtDetail.setText("Your Task Is Declined");
//                holder.txtTask.setText("Hiring");
//                holder.txtDetail.setTextColor(context.getResources().getColor(R.color.date));
//                holder.txtDaytime1.setText(myListData.getTask_day_time() + ", " + myListData.getNotification_time());
//                holder.rd_week_date1.setBackground(context.getResources().getDrawable(R.drawable.tasker_white_upd));
//
//
//            } else if (myListData.getTask_invitation_status().equalsIgnoreCase("Accepted")) {
//                holder.rel_line.setVisibility(View.VISIBLE);
//                holder.rel_line.setBackgroundColor(context.getResources().getColor(R.color.inviteTask));
//                holder.rel_profile.setVisibility(View.GONE);
//                holder.txtservicename.setText(myListData.getTask_subcategory());
//                holder.txtDetail.setText("Your Task Is Accepted");
//                holder.txtTask.setText("Hiring");
//                holder.txtDetail.setTextColor(context.getResources().getColor(R.color.date));
//                holder.txtDaytime1.setText(myListData.getTask_day_time() + ", " + myListData.getNotification_time());
//                holder.rd_week_date1.setBackground(context.getResources().getDrawable(R.drawable.tasker_white_upd));
//
//
//            }
//        }
//    } else {
//        if (user_type.equalsIgnoreCase("2")) {
//            if (myListData.getTask_invitation_status().equalsIgnoreCase("Waiting")) {
//
//                //holder.rel_line.setVisibility(View.VISIBLE);
//                holder.rel_line.setBackgroundColor(context.getResources().getColor(R.color.text_color_green));
//                holder.rel_profile.setVisibility(View.VISIBLE);
//                holder.txtDetail.setText("New Task Invite");
//                holder.txtTask.setText("Task");
//                holder.txtservicename.setVisibility(View.GONE);
//                holder.txtDetail.setTextColor(context.getResources().getColor(R.color.inviteTask));
//                holder.txtDaytime1.setText(myListData.getTask_day_time() + ", " + myListData.getNotification_time());
//                holder.txtName.setText(myListData.getCustomer_name());
//                holder.rd_week_date1.setBackground(context.getResources().getDrawable(R.drawable.tasker_invite));
//                Glide.with(context)
//                        .load(myListData.getCustomer_profile())
//                        .circleCrop()
//                        .into(holder.customer_image);
//
//                holder.rel_invite.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Intent details = new Intent(context, Tasker_Task_Details_Activity.class);
//                        details.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        details.putExtra("task_id", myListData.getTask_id());
//                        context.startActivity(details);
//                    }
//                });
//
//
//            } else if (myListData.getTask_invitation_status().equalsIgnoreCase("Cancelled")) {
//
//                //holder.rel_line.setVisibility(View.VISIBLE);
//                holder.rel_line.setBackgroundColor(context.getResources().getColor(R.color.text_color_green));
//                holder.rel_profile.setVisibility(View.GONE);
//                holder.txtservicename.setText(myListData.getTask_subcategory());
//                holder.txtDetail.setText("Your Task Is Cancelled");
//                holder.txtTask.setText("Hiring");
//                holder.txtDetail.setTextColor(context.getResources().getColor(R.color.date));
//                holder.txtDaytime1.setText(myListData.getTask_day_time() + ", " + myListData.getNotification_time());
//                holder.rd_week_date1.setBackground(context.getResources().getDrawable(R.drawable.tasker_white_upd));
//
//            }
//
//        } else if (user_type.equalsIgnoreCase("1")) {
//
//            if (myListData.getTask_invitation_status().equalsIgnoreCase("Declined")) {
//               // holder.rel_line.setVisibility(View.VISIBLE);
//                holder.rel_line.setBackgroundColor(context.getResources().getColor(R.color.inviteTask));
//                holder.rel_profile.setVisibility(View.GONE);
//                holder.txtservicename.setText(myListData.getTask_subcategory());
//                holder.txtDetail.setText("Your Task Is Declined");
//                holder.txtTask.setText("Hiring");
//                holder.txtDetail.setTextColor(context.getResources().getColor(R.color.date));
//                holder.txtDaytime1.setText(myListData.getTask_day_time() + ", " + myListData.getNotification_time());
//                holder.rd_week_date1.setBackground(context.getResources().getDrawable(R.drawable.tasker_white_upd));
//
//
//
//            } else if (myListData.getTask_invitation_status().equalsIgnoreCase("Accepted")) {
//                //holder.rel_line.setVisibility(View.VISIBLE);
//                holder.rel_line.setBackgroundColor(context.getResources().getColor(R.color.inviteTask));
//                holder.rel_profile.setVisibility(View.GONE);
//                holder.txtservicename.setText(myListData.getTask_subcategory());
//                holder.txtDetail.setText("Your Task Is Accepted");
//                holder.txtTask.setText("Hiring");
//                holder.txtDetail.setTextColor(context.getResources().getColor(R.color.date));
//                holder.txtDaytime1.setText(myListData.getTask_day_time() + ", " + myListData.getNotification_time());
//                holder.rd_week_date1.setBackground(context.getResources().getDrawable(R.drawable.tasker_white_upd));
//
//            } else if (myListData.getTask_invitation_status().equalsIgnoreCase("Cancelled")) {
//
//                //holder.rel_line.setVisibility(View.VISIBLE);
//                holder.rel_line.setBackgroundColor(context.getResources().getColor(R.color.text_color_green));
//                holder.rel_profile.setVisibility(View.GONE);
//                holder.txtservicename.setText(myListData.getTask_subcategory());
//                holder.txtDetail.setText("Your Task Is Cancelled");
//                holder.txtTask.setText("Hiring");
//                holder.txtDetail.setTextColor(context.getResources().getColor(R.color.date));
//                holder.txtDaytime1.setText(myListData.getTask_day_time() + ", " + myListData.getNotification_time());
//                holder.rd_week_date1.setBackground(context.getResources().getDrawable(R.drawable.tasker_white_upd));
//
//
//            }
//        }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
